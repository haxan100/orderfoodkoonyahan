	var colors = [];
	var ict_unit = [];
	var efficiency = [];

	var dynamicColors = function() {
	var r = Math.floor(Math.random() * 255);
	var g = Math.floor(Math.random() * 255);
	var b = Math.floor(Math.random() * 255);
	return "rgb(" + r + "," + g + "," + b + ")";
	};
	
	for(let i=0;i<500;i++){
		this.colors.push('#'+Math.floor(Math.random()*16777215).toString(16));
	}

	
	function chartFunc(nameID, type, dataLabels, JudulLabel, JudulData, Judul) {
		console.log(dataLabels);
		var ctx = document.getElementById(nameID).getContext("2d");
		var labelsan = [];
		var data = [];

		dataLabels.forEach((e) => {
			labelsan.push(e[JudulLabel]);
			data.push(e[JudulData]);
		});

		var myChart = new Chart(ctx, {
			type: type,
			data: {
				labels: labelsan,
				datasets: [
					{
						label: JudulLabel,
						data: data,
						backgroundColor: this.colors,
						borderWidth: 1,
					},
				],
			},
			options: {
				scales: {
					yAxes: [
						{
							ticks: {
								beginAtZero: true,
							},
						},
					],
				},
			},
		});
	}
