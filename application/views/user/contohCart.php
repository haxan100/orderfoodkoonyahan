<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/js/fontawesome.min.js" integrity="sha512-5qbIAL4qJ/FSsWfIq5Pd0qbqoZpk5NcUVeAAREV2Li4EKzyJDEGlADHhHOSSCw0tHP7z3Q4hNHJXa81P92borQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<style>
    .summary {
        background-color: #ddd;
        border-top-right-radius: 1rem;
        border-bottom-right-radius: 1rem;
        padding: 4vh;
        color: rgb(65, 65, 65);
    }


    .summary .col-2 {
        padding: 0;
    }

    .summary .col-10 {
        padding: 0;
    }

    .row {
        margin: 0;
    }

    .title b {
        font-size: 1.5rem;
    }

    .main {
        margin: 0;
        padding: 2vh 0;
        width: 100%;
    }

    .col-2,
    .col {
        padding: 0 1vh;
    }

    a {
        padding: 0 1vh;
    }

    .close {
        margin-left: auto;
        font-size: 0.7rem;
    }

    .back-to-shop {
        margin-top: 4.5rem;
    }

    h5 {
        margin-top: 4vh;
    }

    hr {
        margin-top: 1.25rem;
    }

    form {
        padding: 2vh 0;
    }

    select {
        border: 1px solid rgba(0, 0, 0, 0.137);
        padding: 1.5vh 1vh;
        margin-bottom: 4vh;
        outline: none;
        width: 100%;
        background-color: rgb(247, 247, 247);
    }

    input {
        border: 1px solid rgba(0, 0, 0, 0.137);
        padding: 1vh;
        margin-bottom: 4vh;
        outline: none;
        width: 100%;
        background-color: rgb(247, 247, 247);
    }

    input:focus::-webkit-input-placeholder {
        color: transparent;
    }
</style>

<?php
$bu = base_url();
?>


<div class="container">
    	<div class="pizzaro-order-steps">
            
		</div>
    <div class="row">
        <div class="col-md-8 cart ">
            <div class="container">
                <?php foreach ($cart_content as $key => $k) {
                ?>
                    <div class="row  border-bottom">
                        <div class="row main align-items-center">
                            <!-- <div class="col-2"><img class="img-fluid" src="<?= $bu; ?>assets/images/foods/<?= $k->foto ?>"></div> -->
                            <div class="col">
                                <div class="row"><?= $k->nama_menu ?></div>
                                <div class="row text-muted"><?= convert_to_rupiah($k->harga) ?></div>
                            </div>
                            <div class="col">
                                <!-- <a href="#">-</a><a href="#" class="border">1</a><a href="#">+</a> -->
                                <input type="number" value="<?= $k->qty ?>" data-harga=<?= $k->harga ?> title="Qty" class="input-text qty text" data-rowid="c20ad4d76fe97759aa27a0c99bff6710" data-id=<?= $k->id_menu ?> style="margin-top: 30%;">

                            </div>
                            <div class="col">
                                <!-- <a href="#">-</a><a href="#" class="border">1</a><a href="#">+</a> -->
                                <textarea class="catatan" data-harga=<?= $k->harga ?>  data-catatan='' data-id=<?= $k->id_menu ?> name="catatan" id="catatan" cols="10" rows="1"><?= $k->catatan ?>  </textarea>
                            </div>
                            <div class="col">
                                <span class="close cart_remove_item" data-id=<?= $k->id_menu ?>><i class="fa fa-trash fa-lg"></i></span>
                                <span id="TOTAL<?= $k->id_menu ?>"><?= convert_to_rupiah($k->total) ?></span>
                            </div>
                        </div>
                        </div>
                <?php
                } ?>
            </div>


        </div>
        <div class="col-md-4 summary">
            <div>
                <h5><b>Summary</b></h5>
            </div>
            <hr>
            <div class="row">
                <div class="col">ITEMS </div>
                <div class="col text-right"><?= $totalcart ?></div>
            </div>
            <div class="row">
                <div class="col">TOTAL Harga</div>
                <div class="col text-right cart_total_format"><?= convert_to_rupiah($totalHarga) ?></div>
            </div>
            <br>

            <input type="button" class="button" id="update_cart" value="CekOut">
        </div>
    </div>

</div>

<script type="text/javascript" src="<?= $bu; ?>assets/kasir/frontend/js/tether.min.js"></script>
<script type="text/javascript" src="<?= $bu; ?>assets/kasir/frontend/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?= $bu; ?>assets/kasir/frontend/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?= $bu; ?>assets/kasir/frontend/js/social.share.min.js"></script>
<script type="text/javascript" src="<?= $bu; ?>assets/kasir/frontend/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="<?= $bu; ?>assets/kasir/frontend/js/scripts.min.js"></script>
<script type="text/javascript" src="<?= $bu; ?>assets/kasir/frontend/js/rupiah.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script type="text/javascript">
    $('.cart_remove_item').each(function(index, el) {
        $(this).on('click', function(event) {
            cart_remove_item = $(this);
            event.preventDefault();
            var rowid = $(this).data('rowid');
            var id = $(this).data('id');
            Swal.fire({
                title: 'Anda Yakin ?',
                text: 'Item ini akan di Hapus !',
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya',
                cancelButtonText: 'Batal',
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                            url: '<?= $bu; ?>/Cart/hapusSatuKeranjang',
                            type: "POST",
                            dataType: 'json',
                            data: {
                                id
                            },
                        }).done(function(res) {
                            Swal.fire(
                                'Success',
                                res.msg,
                                'success'
                            )
                            if (res.status) {
                                setTimeout(() => {
                                    location.reload();
                                }, 1000);
                            } else {
                                alert("Gagal!")
                            }
                        })
                        .fail(function(error) {
                            console.log(error.responseText);
                        });
                }
            })
            return false
            swal({
                title: 'Anda Yakin ?',
                text: 'Item ini akan di Hapus !',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya',
                cancelButtonText: 'Batal',
                closeOnConfirm: true
            }, function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                            url: '<?= $bu; ?>/Cart/hapusSatuKeranjang',
                            type: "POST",
                            dataType: 'json',
                            data: {
                                id
                            },
                        }).done(function(res) {
                            console.log(res)
                            console.log(res.status)
                            // return false
                            if (res.status) {
                                setTimeout(() => {
                                    location.reload();
                                }, 1000);
                            } else {
                                alert("Gagal!")
                            }
                        })
                        .fail(function(error) {
                            console.log(error.responseText);
                        });
                }
            });
        });
    });

    $('#update_cart').on('click', function(event) {
        var total = $('.cart_total_format').html();
        var t = total.replace("Rp.", "");
        var t = parseInt(t.replace(".", ""));
        if (t == 0 || t == '') {
            Swal.fire(
                'Maaf',
                'Coba Lagi Nanti',
                'error'
            )
        } else {
            var url = bu + 'Cart/checkout';
            window.location = url;
            return false
        }
    });

    $('.catatan').each(function(index, el) {
        $(this).on('change keyup', function(event) {
            event.preventDefault();
            rowid = $(this).data('rowid');
            var catatan = event.target.value
            var harga = $(this).data('harga');
            var id = $(this).data('id');
            $.ajax({
                    url: '<?= $bu; ?>/Cart/updateCartonlyCatatan',
                    type: "POST",
                    dataType: 'json',
                    data: {
                        id,
                        catatan
                    },
                })
                .done(function(res) {
                    var totalHarga = res.harga;
                    // $('#TOTAL' + id).html(convertToRupiah(SubTotalHarga));
                    $('.cart_total_format').html(convertToRupiah(totalHarga));
                })
                .fail(function(error) {
                    console.log(error.responseText);
                });
        });
    });

    $('.qty').each(function(index, el) {
        $(this).on('change keyup', function(event) {
            event.preventDefault();
            rowid = $(this).data('rowid');
            var harga = $(this).data('harga');
            var id = $(this).data('id');
            qty = $(this).val();
            var SubTotalHarga = qty * harga;
            if (qty > 0) {
                $.ajax({
                        url: '<?= $bu; ?>/Cart/updateCart',
                        type: "POST",
                        dataType: 'json',
                        data: {
                            id,
                            qty
                        },
                    })
                    .done(function(res) {
                        var totalHarga = res.harga;
                        $('#TOTAL' + id).html(convertToRupiah(SubTotalHarga));
                        $('.cart_total_format').html(convertToRupiah(totalHarga));
                    })
                    .fail(function(error) {
                        console.log(error.responseText);
                    });
            } else {
                Swal.fire(
                    'Info',
                    "Jumlah harus lebih dari 0",
                    'error'
                )

                $(this).val(qty).focus();
            }
        });
    });

</script>
<script type="text/javascript">
    var url = '<?= $bu ?>/Kasir/login';
    $('#keluar').click(function(e) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Logout'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: "post",
                    url: "<?= $bu ?>/kasir/logout",
                    dataType: "json",
                    success: function(r) {
                        if (r.error) {
                            Swal.fire(
                                'gagal!',
                                r.pesan,
                                'errorr'
                            )
                        } else {
                            Swal.fire(
                                'Berhasil!',
                                r.pesan,
                                'success'
                            )
                            setTimeout(() => {
                                window.location = url;
                            }, 2000);

                        }

                    }
                });


            }
        })
    });
    $('.addToCart').each(function(index, el) {
        var data = {};
        $(this).on('click', function(event) {
            event.preventDefault();
            data['id'] = $(this).data('id');
            data['qty'] = $(this).attr('data-qty');
            $.ajax({
                    url: 'http://dansdigitalmedia.com/resto/cart/additem',
                    type: 'POST',
                    data: {
                        data: data
                    },
                })
                .done(function(res) {
                    swal({
                        title: "Sukses",
                        text: "Order Menu Berhasil",
                        timer: 2000,
                        showConfirmButton: false,
                        type: "success"
                    });
                    $('.cart_count').html(res.cart_count);
                    $('.cart_total_format').html(res.cart_total_format);
                    $('.cart_dropdown_container').html(res.cart_dropdown_container);
                    $('.cart_count_footer').html(res.cart_count_footer);
                })
                .fail(function(error) {
                    console.log(error.responseText);
                });
        });
    });
</script>
</body>

</html>