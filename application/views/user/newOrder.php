<?php
$bu = base_url();
// var_dump($id_meja);die;
// $nama = $_SESSION['nama_kasir'];
// $id_user = $_SESSION['id_kasir'];
?>
<div class="wrapper">
  <div class="content-order m-0 p-0">
    <div class="row m-0">
      <div class="col-md-8 p-0 bg-white border-right">


        <div class="d-flex flex-column p-3 border-bottom product-info-form">
          <div class="form-row mb-2">
            <div class="form-group col-md-4">
              <label for="product_title">Product Title</label>
              <input type="text" name="product_title" id="produk" placeholder="Product Title" autocomplete="off" class="form-control form-control-lg">
            </div>
            <div class="form-group col-md-4"><label for="category">Category</label>
              <select name="category" id="kategori" class="form-control custom-select custom-select-lg">
                <option value="default">Filter by Category..</option>
                <?php
                foreach ($dataKategori as $k => $v) {
                ?>
                  <option value='<?= $v->id_kategori ?>'>
                    <?= $v->nama_kategori ?>
                  </option>
                <?php
                }
                ?>
              </select>
            </div>
            <div class="form-group col-md-3">
              <button type="submit" class="btn btn-primary btn-block btn-lg find-product-btn">
                <!----> Cari
              </button>
            </div>
          </div>
        </div>
        <div class="d-flex flex-wrap mb-5 p-3 product-list bg-light">
          <div class="col-md-12">
            <div class="row" id="prodTampil">
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4 p-0 full-height">
        <div class="cart_form">
          <div class="p-0 border-bottom">

            <div class="d-flex flex-wrap p-3">
              <span class="mr-auto text-title text-black-50">Customer <i class="fas fa-user"></i> </span>
              <div class="form-group">
                
                <input type="text" class="form-control custumer" name="custumer" id="custumer" aria-describedby="helpId" placeholder="Masukan Nama ">                
                <p class="text-danger" id="warningNama">Mohon Masukan Nama!</p>

              </div>
            </div>
          </div>
          <div class="p-0 cart-list border-left" id="keranjang">

          </div>


          <div class="d-flex flex-column p-3 ml-auto fixed-bottom col-md-4 border-top cart-summary">
            <div style="">
              <div class="d-flex justify-content-between mb-2 cart-summary-label mt-0"><span>Total</span> <span class="cart_total_formats">0</span></div>
            </div>
            <div>
              <div>
                <div class="mt-2">
                  <!---->
                  <div class="d-flex mt-2">
                    <div class="flex-grow-1">
                      <button type="submit" id="orderNow" class="btn btn-primary btn-lg btn-block">
                        Order Now
                      </button>
                      <button type="submit" id="cancle" class="btn btn-danger btn-lg btn-block">
                        Cancle Order
                      </button>
                    </div>
                  </div>
                </div>
                <!---->
              </div>
            </div>
            <!---->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

</div>
<!---->
</body>

<script>
  $(document).ready(function() {
    var bu = '<?= $bu ?>';
    $('#cancle').click(function(e) {
      e.preventDefault();
      Swal.fire({
        title: 'Apakah Anda Yakin ',
        text: 'Untuk membatalkan Orderan Ini?',
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya'
      }).then((result) => {
        if (result.isConfirmed) {
          var data = {
            id_cabang: '<?= $id_cabang ?>',
            id_user: '<?= $id_meja ?>'
          }

          $.ajax({
            type: "post",
            url: bu + "/Kasir/CancleOrder",
            data: data,
            dataType: "json",
            success: function(r) {
              if (r.status) {
                alertify.success(r.pesan);
                $('#keranjang').html('');
                cart()
              } else {
                alertify.error(r.pesan);
              }
            }
          });
        }
      })
    });

    $('#warningNama').hide();
    $('#warningJenis').hide();

    $('#totalHargaSemua').val('<?= $totalHarga ?>');
    $('#orderNow').click(function(e) {
      e.preventDefault();
      var custumer = $('#custumer').val();
      var jeniss = $('#jenis').val();
      var jenis_order = $('#jenis_orderan').val();
      var pilihTempat = $('#pilihTempat').val();
      var meja = $('#meja_id').val();
      var id_cabang = '<?= $id_cabang ?>'
      var totalharga = $('#totalHargaSemua').val();
      Swal.fire({
        title: 'Apakah Anda Yakin ',
        text: 'Akan Pesanan Ini?',
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya'
      }).then((result) => {
        if (result.isConfirmed) {
          //success
          if (custumer == '' || custumer == null) {

            Swal.fire(
              'Mohon',
              'Isi Nama Custumer!',
              'error'
            )
            $('#warningNama').show();
            return false
          }
          if (jeniss == 'default') {
            Swal.fire(
              'Mohon',
              'Pilih Jenis Order!',
              'error'
              )
              $('#warningJenis').show();
              setTimeout(() => {
                $('#warningJenis').hide();
                
              }, 5500);

            return false
          } else if (jeniss == '0') {
            if (jenis_order == 'default') {
              Swal.fire(
                'Mohon',
                'Pilih Jenis Orderan!',
                'error'
              )
              return false
            }
          } else if (jeniss == '1') {
            console.log(pilihTempat);
            if (pilihTempat == 'default') {
              Swal.fire(
                'Mohon',
                'Pilih Tempat!',
                'error'
              )
              return false
            } else if (pilihTempat == 'dine_in') {
              if (meja == 'default') {
                console.log("addd")
                Swal.fire(
                  'Mohon',
                  'Pilih Nomor Meja!',
                  'error'
                )
                return false
              }

            }
          }
          var data = {
            nama: custumer,
            id_cabang:  '<?= $id_cabang ?>',
            meja_id: '<?= $id_meja ?>',
            pilihtempat: pilihTempat,
            totalharga: totalharga,
            orderan: jenis_order,
            jenis: jeniss,
          }
          $.ajax({
              url: "<?= $bu; ?>User/konfirmasiUser",
              type: 'POST',
              dataType: 'json',
              data: data,
            }).done(function(res) {
              if (res.status) {
                var w = "<?= $bu; ?>Kasir/Selesai/?id=" + res.id;
                var link = "<?= $bu; ?>User/print/" + res.id;
                Swal.fire({
                  title: 'Terimakasih ',
                  text: 'Pesanan Anda Akan Kami Proses',
                  icon: 'success',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Okey'
                }).then((result) => {
                  if (result.isConfirmed) {
                    //success
                    window.location = link;
                  }
                })
              } else {
                Swal.fire(
                  'Maaf',
                  res.msg,
                  'error'
                )
              }
            })
            .fail(function(error) {
              console.log(error);
            });
        }
      })

    });



    $('#pilih_orderan').hide()
    $('#pilihtempatkan').hide()
    $('#offline2').hide()
    $('#pilih_tempat').hide()
    $('#meja').hide()

    $('#jenis').on('change', function() {
      var jenis = $('#jenis').val();
      if (jenis == 0) {
        $('#pilih_orderan').show()
        $('#pilihtempatkan').hide()
        $('#meja_id').hide()
        $('#pilih_tempat').hide()
      } else if (jenis == 1) {
        $('#pilih_orderan').hide()
        $('#pilih_tempat').show()
        $('#pilihtempatkan').show()
      } else {
        $('#pilih_orderan').hide()
        $('#pilihtempatkan').hide()
        $('#offline2').hide()
      }
    });

    $('#pilihTempat').on('change', function() {
      var kec = $('#pilihTempat').val();
      if (kec == 'dine_in') {
        $('#meja').show()
      } else {
        $('#meja').hide()
      }

    });


    var kategori = $('#kategori').val();
    var produk = $('#produk').val();
    

    $('.find-product-btn').click(function(e) {
      e.preventDefault();
      kategori = $('#kategori').val();
      produk = $('#produk').val();
       getProduk()

    });

    var transaksi = 'traveloka'
    switch (transaksi) {
      case 'gofood':
        $('#gofood').addClass('active');
        break;
      case 'gofood':
        $('#gofood').addClass('active');
        break;
      case 'traveloka':
        $('#traveloka').addClass('active');
        break;
      case 'shopee':
        $('#shopee').addClass('active');
        break;
      case 'traveloka':
        $('#traveloka').addClass('active');
        break;

      default:
        $('#traveloka').addClass('active');
        break;
    }

    var data
    getProduk()

    function getProduk() {
      $.ajax({
        type: "post",
        url: "<?= $bu ?>/Kasir/getProduk",
        data: {
          id_kategori: 0,
          page: 1,
          id_cabang: <?=$id_cabang?>,
          id_kategori: kategori,
          produk: produk

        },
        dataType: "json",
        success: function(r) {
          $('#prodTampil').html('');
          $.each(r.data.data, function(i, v) {
            $('#prodTampil').append(generateProduk(v));
          });
        }
      });
    }

    function generateProduk(produk) {
      var nama_produk = produk.nama_menu
      var panjang = nama_produk.length
      if (panjang > 20) {
        // var nama_produk = nama_produk.replace(/.(?=.{4})/g, '*')
        var nama_produk = nama_produk
      }
      return `
      <div class="col-md-3"
        style="
          padding-bottom: 16px;"
        >
          <div class="card-sl">
              <div class="card-image">
                  <img class="gambar" src="<?= $bu; ?>assets/images/foods/${produk.foto}"
                  style="
                      width: 100%;
                      height: 15vw;
                      object-fit: cover;
                  "
                />
              </div>

              <div class="card-heading">
                  ${produk.nama_kategori}
              </div>
              <div class="card-text">
                ${nama_produk}
              </div>
              <div class="card-text">
                  ${convertToRupiah(produk.harga)}
              </div>


              <a data-id="${produk.id_menu}"  data-harga="${convertToRupiah(produk.harga)}" data-qty="1"  href="#" class="card-button add_to_cart_button"> Order </a>
          </div>
      </div>

`;
    }
    cart()
    getTotal()
    $('#keluar').click(function(e) {
      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Logout'
      }).then((result) => {
        if (result.isConfirmed) {
          $.ajax({
            type: "post",
            url: "<?= $bu ?>/kasir/logout",
            dataType: "json",
            success: function(r) {
              if (r.error) {
                Swal.fire(
                  'gagal!',
                  r.pesan,
                  'errorr'
                )
              } else {
                Swal.fire(
                  'Berhasil!',
                  r.pesan,
                  'success'
                )
                setTimeout(() => {
                  window.location = '<?= $bu ?>';
                }, 2000);

              }

            }
          });


        }
      })
    });

    function getTotal() {
      $.ajax({
        type: "post",
        url: "<?= base_url() ?>User/getTotalHarga",
        data: {
          id_meja: <?=$id_meja?>,
        },
        dataType: "json",
        success: function(r) {
          console.log(r)
          $('.cart_total_formats').html(convertToRupiah(r.data));
        }
      });
    }

    function cart() {
      // $('.cart_total_formats').html(<?= $totalHarga ?>);
      $.ajax({
        type: "post",
        url: "<?= base_url() ?>Cart/getCartByIDUserMeja",
        data: {
          id_cabang: <?=$id_cabang?>,
          id_meja: <?=$id_meja?>,
        },
        dataType: "json",
        success: function(r) {
          var content = ''
          var total = 0;
          r.data.forEach(data => {
            console.log(data);
            content += `
      <div class="d-flex flex-column pl-3 pt-3 pb-3 border-bottom">
        <div class="d-flex mb-2">
          <button type="button" aria-label="Close" 
          data-id=${data.id_menu}
          class="cart_remove_item close cart-item-remove bg-light mr-2 ml-auto">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="d-flex justify-content-between mb-2">
          <span class="text-bold text-break cart-item-title">
            ${data.nama_menu}
          </span>
          <input type="number"                
          name="qty" id="qty"
          value="${data.qty}"
          data-id=${data.id_produk}
          data-harga=${data.harga}
          autocomplete="off" min="0" class="form-control qty form-control-custom cart-product-=quantity mr-2 ml-3">
        </div>

        <div class="d-flex flex-row justify-content-between mr-2 cart-item-summary">
          <div>
            <div class="d-flex flex-column">
              <div class="text-success">
                <i class="fas fa-tags cart-discount-tag"></i>Harga</div>
              <div>Total</div>
            </div>
          </div>
          <div>
            <div class="d-flex flex-column">
              <div class="text-right">Price:${convertToRupiah(data.harga)}</div>

              <div class="text-right">
              <span id="TOTAL${data.id_produk}">${convertToRupiah(data.qty*data.harga)}</span>
              </div>
            </div>
          </div>
        </div>
        <button type="button" class="btn btn-link customize-btn text-bold mr-auto p-0 text-primary text-decoration-none">
          Catatan
        </button>
        <div class="form-group">


          <input type="text" data-catatan=''  class="form-control catatan" name=""
          data-id=${data.id_menu}
          value="${data.catatan}"
          name="catatan" id="catatan"
          aria-describedby="helpId" placeholder="Masukan catatan">
        </div>
      </div>
      `
            total += 1
          });
          $('#keranjang').append(content);
          $('.jumlah').html(total);

        }
      });

    }


    $('body').on('keypress', '.catatan', function(index, el) {
      console.log(index, el)

      console.log("object")
      $(this).on('change keyup', function(event) {
        event.preventDefault();
        rowid = $(this).data('rowid');
        var catatan = event.target.value
        var harga = $(this).data('harga');
        var id = $(this).data('id');
        var id_meja = '<?= $id_meja ?>'
        $.ajax({
            url: '<?= $bu; ?>/Cart/updateCartonlyCatatanUser',
            type: "POST",
            dataType: 'json',
            data: {
              id,
              id_meja,
              catatan
            },
          })
          .done(function(res) {
            var totalHarga = res.harga;
            // $('#TOTAL' + id).html(convertToRupiah(SubTotalHarga));
            $('.cart_total_format').html(convertToRupiah(totalHarga));
          })
          .fail(function(error) {
            console.log(error.responseText);
          });
      });
    })



    $('.catatan').each(function(index, el) {
      console.log(index, el)
      return false
      $(this).on('change keyup', function(event) {
        event.preventDefault();
        rowid = $(this).data('rowid');
        var catatan = event.target.value
        var harga = $(this).data('harga');
        var id = $(this).data('id');
        $.ajax({
            url: '<?= $bu; ?>/Cart/updateCartonlyCatatanUser',
            type: "POST",
            dataType: 'json',
            data: {
              id,
              catatan
            },
          })
          .done(function(res) {
            var totalHarga = res.harga;
            // $('#TOTAL' + id).html(convertToRupiah(SubTotalHarga));
            $('.cart_total_format').html(convertToRupiah(totalHarga));
          })
          .fail(function(error) {
            console.log(error.responseText);
          });
      });
    });

    $('body').on('click', '.add_to_cart_button', function() {

      var id_produk = $(this).data('id');
      var id_cabang = '<?= $id_cabang ?>'
      var id_meja = '<?= $id_meja ?>'
      var harga = $(this).data('harga');
      var qty = 1;
      $('.btn-tawar').html('Tunggu...');
      $('.btn-tawar').prop('disabled', true);
      $.ajax({
        type: "POST",
        dataType: 'json',
        url: "<?= $bu; ?>User/setBid",
        data: {
          id_cabang,
          id_produk,
          id_meja,
          harga,
          qty
        },
      }).done(function(e) {
        console.log(" Ini Adalah Hasilmya" + e.harga)
        if (e.status) {
          alertify.success(e.msg);
          $('.cart_total_formats').html('');

          $('.cart_counts').html(e.total + " Item");
          $('.cart_total_formats').html("Rp " + e.harga);
        } else {
          alertify.error(e.msg);
        }
      }).fail(function(e) {
        $('.cart_total_formats').html('');

        alertify.error(e.msg);
      }).always(function(e) {

        $('#totalHargaSemua').val(e.harga);
        console.log("Coba Always: " + e.harga)
        $('#keranjang').html('');
        // $('.cart_total_formats').html(<?= $totalHarga ?>);
        // $('.cart_total_formats').html(e.harga);
        $('.cart_total_formats').html('');

        $('.jumlah').html();
        cart()
        getTotal()

        setTimeout(() => {}, 2000);

        setTimeout(() => {
          $('.btn-tawar').html('Order');
          $('.btn-tawar').prop('disabled', false);
        }, 100);
      });




    })


    $('body').on('keypress', '.qty', function(index, el) {
      $(this).on('change keyup', function(event) {
        event.preventDefault();
        rowid = $(this).data('rowid');
        var harga = $(this).data('harga');
        var id = $(this).data('id');
        var id_meja = '<?=$id_meja ?>';
        qty = $(this).val();
        var SubTotalHarga = qty * harga;
        if (qty > 0) {
          $.ajax({
              url: '<?= $bu; ?>/Cart/updateCartonlyCatatanUserqty',
              type: "POST",
              dataType: 'json',
              data: {
                id,
                id_meja,

                qty
              },
            })
            .done(function(res) {
              var totalHarga = res.harga;
              $('#TOTAL' + id).html(convertToRupiah(SubTotalHarga));
              $('.cart_total_formats').html(convertToRupiah(totalHarga));
            })
            .fail(function(error) {
              console.log(error.responseText);
            });
        } else {
          Swal.fire(
            'Info',
            "Jumlah harus lebih dari 0",
            'error'
          )

          $(this).val(qty).focus();
        }
      });
    });
    $('body').on('click', '.cart_remove_item', function(index, el) {
     
        cart_remove_item = $(this);
        event.preventDefault();
        var rowid = $(this).data('rowid');
        var id = $(this).data('id');
        var id_meja = '<?= $id_meja?>';
        console.log($(this).data());
        Swal.fire({
          title: 'Anda Yakin ?',
          text: 'Item ini akan di Hapus !',
          icon: 'question',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ya',
          cancelButtonText: 'Batal',
        }).then((result) => {
          if (result.isConfirmed) {
            $.ajax({
                url: '<?= $bu; ?>/Cart/hapusSatuKeranjangUser',
                type: "POST",
                dataType: 'json',
                data: {
                  id,id_meja
                },
              }).done(function(res) {
                // return console.log(res)
                Swal.fire(
                  res.msg,
                  res.status
                )
                if (res.status) {
                  setTimeout(() => {
                    location.reload();
                  }, 1000);
                } else {
                  alert("Gagal!")
                }
              })
              .fail(function(error) {
                console.log(error.responseText);
              });
          }
        })
        return false
        swal({
          title: 'Anda Yakin ?',
          text: 'Item ini akan di Hapus !',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ya',
          cancelButtonText: 'Batal',
          closeOnConfirm: true
        }, function(isConfirm) {
          if (isConfirm) {
            $.ajax({
                url: '<?= $bu; ?>/Cart/hapusSatuKeranjang',
                type: "POST",
                dataType: 'json',
                data: {
                  id
                },
              }).done(function(res) {
                console.log(res)
                console.log(res.status)
                // return false
                if (res.status) {
                  setTimeout(() => {
                    location.reload();
                  }, 1000);
                } else {
                  alert("Gagal!")
                }
              })
              .fail(function(error) {
                console.log(error.responseText);
              });
          }
        });
      });

    $('#custumer').keydown(function (e) { 
      $('#warningNama').hide();      
    });

  });
</script>

</html>