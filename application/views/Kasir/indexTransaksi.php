<?php
$bu = base_url();
// var_dump($konten[2]); 
// echo  $totalHarga; die;
?>


<div id="content" class="site-content" tabindex="-1">
	<div class="col-full">
		<div id="primary" class="content-area">
			<main id="main" class="site-main">
				<?php
				if (!$kasir_login_slider) {
				?>
					<div class="home-v1-slider">
						<div id="owl-main" class="owl-carousel owl-inner-nav owl-ui-sm">
							<?php
							foreach ($slider as $key) {
								// var_dump($key);die;
							?>
								<div class="item slider-1">
									<img src="<?= $bu; ?>assets/images/slider/<?= $key->foto; ?> " class="img-responsive" alt="">
								</div>

							<?php
							}
							?>

						</div>
					</div>
				<?php
				}
				?>

				<div class="section-products">
					<h2 class="section-title">Transaksi</h2>
					<div id="produkWrapper" class="row">
						<div class="container">
							<div class="row hidden-md-up">
								<div>

									<div class="x_content">
										<div class="row">
											<div class="col-sm-12">

												<div class="card-box table-responsive">
													<span class="px-0 pb-0 col-lg-2 col-md-3 col-sm-6 col-xs-12 my-1">
														<button class="btn btn-info waves-effect waves-light" id="resetDate"><span class="fa fa-sync"></span> Reset</button>
													</span>
													</p>
													<table id="datatable_data" class="table table-striped table-bordered">
														<thead>
															<tr>
																<th>No.</th>
																<th>Nama User</th>
																<th>Kode Transaksi<br>Tanggal Transaksi</th>
																<th>Tipe Pesan</th>
																<th>Nomor Meja<br> (Jika Dine In)</th>
																<th> Jual</th>
																<th> Catatan</th>
																<th>Status</th>
																<th style="width: 100px !important; ">Aksi</th>
															</tr>
														</thead>
													</table>
												</div>
											</div>
										</div>
									</div>

								</div>

							</div>

							<br>


						</div>
					</div>

			</main>
		</div>
	</div>
</div>
<nav aria-label="Page navigation example" class="example">
	<input type="hidden" id="_page" value=1></input>
	<ul class="pagination" id="pagination-wrapper">

	</ul>
</nav>
	<!-- Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Detail Transaksi</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<table id="datatable_detail" class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>No.</th>
								<th>Nama Menu</th>
								<th> QTY</th>
								<th>Harga</th>
								<th>Total Harga</th>
							</tr>
						</thead>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<script src="<?= $bu; ?>assets/admin/vendors/datatables.net/js/jquery.dataTables.js"></script>
	<script src="<?= $bu; ?>assets/admin/vendors/datatables.net-responsive/js/dataTables.responsive.js"></script>
	<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
	<script type="text/javascript">
		document.addEventListener("DOMContentLoaded", function(event) {
			// $('#tableDestroy').on('click', function() {
			// });

			// $('#exampleModal').modal('show');
			var bu = '<?= base_url(); ?>';
			var url_form_ubah = bu + 'Admin/ubah_admin_proses';
			var url_form_tambah = bu + 'Admin/tambah_admin_proses';
			$('#datatable_detail').dataTable().fnClearTable();
			$('#datatable_detail').dataTable().fnDestroy();
			$('body').on('click', '.btn_edit', function() {
				url_form = url_form_ubah;

				var id_transaksi = $(this).data('id_transaksi');
				console.log(id_transaksi);
				$('#exampleModal').modal('show');
				// datatables.destroy();

				var datatables = $('#datatable_detail').DataTable({
					dom: "Bfrltip",
					// "retrieve": true,
					'pageLength': 10,
					"responsive": true,
					"processing": true,
					"bProcessing": true,
					"autoWidth": false,
					"serverSide": true,
					"columnDefs": [{
							"targets": 0,
							"className": "dt-body-center dt-head-center",
							"width": "20px",
							"orderable": false
						},
						{
							"targets": 1,
							"className": "dt-head-center"
						},
						{
							"targets": 2,
							"className": "dt-head-center"
						}, {
							"targets": 3,
							"className": "dt-head-center"
						},
					],
					"order": [
						[1, "desc"]
					],
					'ajax': {
						url: bu + 'Data/getAllTransaksiDetail',
						type: 'POST',
						"data": function(d) {

							d.id = id_transaksi;

							return d;
						}
					},
					language: {
						searchPlaceholder: "Cari",

					},
					"lengthMenu": [
						[10, 25, 50, 1000],
						[10, 25, 50, 1000]
					]

				});
				datatables.destroy();
			});



			$('body').on('click', '.btn_print', function() {

				var id_transaksi = $(this).data('id_transaksi');
				window.location.href="<?= base_url() ?>Kasir/print/"+id_transaksi
				
			});

			function notifikasi(sel, msg, err) {
				var alert_type = 'alert-success ';
				if (err) alert_type = 'alert-danger ';
				var html = '<div class="alert ' + alert_type + ' alert-dismissible show p-4">' + msg + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
				$(sel).html(html);
				$('html, body').animate({
					// scrollTop: $(sel).offset().top - 75
				}, 500);
			}

			$("#form").submit(function(e) {
				console.log('form submitted');
				// return false;

				$.ajax({
					url: url_form,
					method: 'post',
					dataType: 'json',
					data: new FormData(this),
					processData: false,
					contentType: false,
					cache: false,
					async: false,
				}).done(function(e) {
					console.log(e);
					if (e.status) {
						notifikasi('#alertNotif', e.message, false);
						// Swal.fire(e.message)
						Swal.fire(
							':)',
							e.message,
							'success'
						)

						$('#modal-detail').modal('hide');
						// setTimeout(function() {
						// 	location.reload();
						// }, 4000);
						datatable.ajax.reload();
						// window.location.reload();
						// resetForm();
					} else {
						Swal.fire({
							icon: 'error',
							title: 'Oopssss...',
							text: e.message,

						})
					}
				}).fail(function(e) {
					console.log(e);
					notifikasi('#alertNotif', 'Terjadi kesalahan!', true);
				});
				return false;
			});

			function notifikasiModal(modal, sel, msg, err) {
				var alert_type = 'alert-success ';
				if (err) alert_type = 'alert-danger ';
				var html = '<div class="alert ' + alert_type + ' alert-dismissible show p-4">' + msg + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
				$(sel).html(html);
				$(modal).animate({
				}, 500);
			}		

			var datatable = $('#datatable_data').DataTable({
				dom: "Bfrltip",
				'pageLength': 10,
				"responsive": true,
				"processing": true,
				"bProcessing": true,
				"autoWidth": false,
				"serverSide": true,
				"columnDefs": [{
						"targets": [0,7,8],
						"className": "dt-body-center dt-head-center",
						"width": "20px",
						"orderable": false
					},
					{
						"targets": 1,
						"className": "dt-head-center"
					},
					{
						"targets": 2,
						"className": "dt-head-center"
					}, {
						"targets": 3,
						"className": "dt-head-center"
					}, {
						"targets": 4,
						"className": "dt-head-center"
					},
				],
				"order": [
					[1, "desc"]
				],
				'ajax': {
					url: bu + 'Data/getAllTransaksi_offline',
					type: 'POST',
					"data": function(d) {
						d.date = $('#datepicker').val();
						d.id_cabang = '<?= $id_cabang ?>';

						return d;
					}
				},

				buttons: [

					'excelHtml5',
					'pdfHtml5',
					{
						text: "Excel",
						extend: "excelHtml5",
						className: "btn btn-round btn-info",
						title: 'Data Guru Kelas',

						exportOptions: {
							columns: [1, 2, 3, 4]
						}
					}, {
						text: "PDF",
						extend: "pdfHtml5",
						className: "<br>btn btn-round btn-danger",
						title: 'Data Guru Kelas',

						exportOptions: {
							columns: [1, 2, 3, 4]
						}
					}
				],
				language: {
					searchPlaceholder: "Cari",

				},
				"lengthMenu": [
					[10, 25, 50, 1000],
					[10, 25, 50, 1000]
				]

			});
			$('#btnExport').on('click', function() {
				var date = $('#datepicker').val();
				// console.log(tipe_produk);return(false);dt_filter_status
				var url = bu + 'Export/master_list_transaksi_export_offline/?id_cabang=<?= $id_cabang ?>';
				// url += '&tipe_bid='+tipe_bid;
				url += '&date=' + date;
				window.location = url;
				console.log(url);
				return (false);

			});


		});
	</script>