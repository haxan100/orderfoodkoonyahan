<!DOCTYPE html>
<html lang="en-US" itemscope="itemscope" itemtype="http://schema.org/WebPage">
<?php
defined('BASEPATH') or exit('No direct script access allowed');
// $title = array_key_exists('title', $header) ? $header['title'] : $ci->namaAplikasi();
// $page = array_key_exists('page', $header) ? $header['page'] : 'master_admin';
// $js = array_key_exists('js', $header) ? $header['js'] : array();
$bu = base_url();


?>
<style>
	.pagination {
		display: flex;
		justify-content: center;
	}


	.produkAwal {
		width: 25% !important;
	}

	.img-prod {
		display: block !important;
		max-width: 180px !important;
		height: auto !important;
		max-height: 97px !important;
		border-radius: 14px;
	}
	.header-v1 .header-wrap {
    display: block!important;
    align-items: center;
    margin-bottom: 25px;
}
</style>

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?= $konten[0]->isi ?></title>
	<link rel="shortcut icon" href="<?= $bu; ?>assets/kasir/img\logo-icon.png">
	<link rel="stylesheet" type="text/css" href="<?= $bu; ?>assets/kasir/frontend\css\bootstrap.min.css" media="all">
	<link rel="stylesheet" type="text/css" href="<?= $bu; ?>assets/kasir/frontend\css\font-awesome.min.css" media="all">
	<link rel="stylesheet" type="text/css" href="<?= $bu; ?>assets/kasir/frontend\css\animate.min.css" media="all">
	<link rel="stylesheet" type="text/css" href="<?= $bu; ?>assets/kasir/frontend\css\font-pizzaro.css" media="all">
	<link rel="stylesheet" type="text/css" href="<?= $bu; ?>assets/kasir/frontend\css\style.css" media="all">
	<link rel="stylesheet" type="text/css" href="<?= $bu; ?>assets/kasir/frontend\css\colors\red.css" media="all">
	<link rel="stylesheet" type="text/css" href="<?= $bu; ?>assets/kasir/frontend\css\jquery.mCustomScrollbar.min.css" media="all">
	<link href="<?= $bu; ?>assets/kasir/https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CYanone+Kaffeesatz:200,300,400,700" rel="stylesheet">
	<link href="<?= $bu; ?>assets/kasir/backend\js\sweetalert2.css" rel="stylesheet" type="text/css">
	<script src="<?= $bu; ?>assets/kasir/backend\js\sweetalert2.min.js"></script>
	<script type="text/javascript" src="<?= $bu; ?>assets/kasir/frontend\js\jquery.min.js"></script>
</head>
<script>
	var bu = '<?= $bu ?>';
</script>


<body class="page-template-template-homepage-v1 home-v1">
	<div id="page" class="hfeed site">
		<header id="masthead" class="site-header header-v1" style="background-image: none; ">
			<div class="col-full">
				<a class="skip-link screen-reader-text" href="<?= $bu; ?>assets/kasir/#site-navigation">Skip to navigation</a>
				<a class="skip-link screen-reader-text" href="<?= $bu; ?>assets/kasir/#content">Skip to content</a>
				<div class="header-wrap">
					<div class="site-branding">
						<a href="<?= $bu; ?>" class="custom-logo-link" rel="home">
							<img src="<?= $bu; ?>assets/kasir/img\logo-front.png">
						</a>
					</div>
					<div class="header-info-wrapper">
						<!-- <div class="header-phone-numbers">
							<span class="intro-text">Telp. dan Order ke</span>
							<span id="city-phone-number-label" class="phone-number"><?= $konten[2]->isi ?></span>
						</div> -->
		

					</div>
				</div>
				<div class="pizzaro-secondary-navigation">
				</div>
			</div>
		</header>