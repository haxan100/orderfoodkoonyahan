<?php
$bu = base_url();
?>

<div id="content" class="site-content" tabindex="-1">
  <div class="col-full">
    <div class="card-header bg-white">
      <div class="row justify-content-between">
        <div class="col">
          <p class="text-muted"> Nama User : <span class="font-weight-bold text-dark"><?=$Data->nama_user?></span></p>
          <p class="text-muted"> Kode Transaksi : <span class="font-weight-bold text-dark"><?=$Data->kode_transaksi?></span> </p>
        </div>
      </div>
    </div>

    <table class="table" id="datatable_awal">
      <thead class="thead-dark">
        <tr>
          <th scope="col">#</th>
          <th scope="col">Nama Menu</th>
          <th scope="col">Catatan</th>
          <th scope="col">QTY</th>
          <th scope="col">Harga</th>
          <th scope="col">Harga Total</th>
        </tr>
      </thead>
    </table>

  </div>
</div>
</div>

</body>


<script>
  $(document).ready(function() {

    var bu = '<?= base_url(); ?>'
    var datatable = $('#datatable_awal').DataTable({
      dom: "Bfrltip",
      'pageLength': 10,
      "responsive": true,
      "processing": true,
      "bProcessing": true,
      "autoWidth": false,
      "serverSide": true,
      "columnDefs": [{
          "targets": 0,
          "className": "dt-body-center dt-head-center",
          "width": "20px",
          "orderable": false
        },
        {
          "targets": 1,
          "className": "dt-head-center"
        },
        {
          "targets": 2,
          "className": "dt-head-center"
        }, {
          "targets": 3,
          "className": "dt-head-center"
        },
        {
          "targets": [4, 5],
          "className": "dt-head-center",
          "orderable": false
        },

      ],
      "order": [
        [1, "desc"]
      ],
      'ajax': {
        url: bu + 'Data/getAllTransaksiDetail',
        type: 'POST',
        "data": function(d) {
          d.id = '<?= $id ?>';
          d.status = $('#status').val();

          return d;
        }
      },

      language: {
        searchPlaceholder: "Cari",

      },
      // columnDefs: [{
      // 	targets: -1,
      // 	visible: false
      // }],
      "lengthMenu": [
        [10, 25, 50, 1000],
        [10, 25, 50, 1000]
      ]
    });
    $('#exampleModal').modal('hide');


  });
</script>