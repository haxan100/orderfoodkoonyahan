<html>
<title>Print</title>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $title_pdf; ?></title>
    <style>
        #table {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #table td,
        #table th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        #table tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        #table tr:hover {
            background-color: #ddd;
        }

        #table th {
            padding-top: 10px;
            padding-bottom: 10px;
            text-align: left;
            background-color: #4CAF50;
            color: white;
        }
    </style>
</head>
<?php


?>

<body>
    <div style="text-align:center">
        <h3> Laporan PDF </h3>
    </div>
    <center>
        <table id="table">
            <td width='70%' align='left' style='padding-right:80px; vertical-align:top'>
                <span style='font-size:12pt'><b>Nama Toko</b></span></br>
                <?= $konten[0]->isi ?>
                </br>
                Telp :<?= $konten[2]->isi ?>
            </td>
            <td style='vertical-align:top' width='30%' align='left'>
                <b><span style='font-size:12pt'>Transaksi</span></b></br>
                Kode Trans. : <?= $Data->kode_transaksi ?> </br>
                Tanggal :<?= $Data->created_at ?></br>
            </td>
        </table>
        <table>
            <br>
            <!-- <td width='70%' align='left' style='padding-right:80px; vertical-align:top'>
                Nama User : <?= $Data->nama_user ?></br>
            </td> -->
        </table>
        <table  id="table">

            <tr align='center'>
                <td width='5%'>Nomor</td>
                <td width='30%'>Nama Barang</td>
                <td width='20%'>Catatan</td>
                <td width='23%'>Harga</td>
                <td width='10%'>Qty</td>
                <td width='39%'>Total Harga</td>
            </tr>
            <?php
            $no = 1;
            foreach ($getData as $key => $value) {
            ?>
                <tr>
                    <td><?= $no++ ?></td>
                    <td><?= $value->nama_menu ?></td>
                    <td><?= $value->catatan ?></td>
                    <td><?= rupiah($value->harga) ?></td>
                    <td><?= $value->qty ?></td>
                    <td style='text-align:right'><?= rupiah($value->total) ?></td>
                </tr>

            <?php
            } ?>


            <tr>
                <td colspan='4'>
                    <div style='text-align:right'>Total : </div>
                </td>
                <td style='text-align:right' colspan='2'><?= rupiah($Data->harga_total) ?></td>
            </tr>
            <tr>
                <td colspan='6'>
                    <div style='text-align:right'>
                        <span style='text-align:left'>Terbilang :</span>
                        <span style='text-align:right'><?= terbilang($Data->harga_total) ?> </span>
                    </div>
                </td>
            </tr>

        </table>

    </center>
</body>

</html>