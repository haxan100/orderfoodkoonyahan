<?php
$bu = base_url()

?>
<link rel="shortcut icon" href="<?= $bu; ?>assets/kasir/img\logo-icon.png">
<!-- <link rel="stylesheet" type="text/css" href="<?= $bu; ?>assets/kasir/frontend\css\bootstrap.min.css" media="all"> -->
<link rel="stylesheet" type="text/css" href="<?= $bu; ?>assets/kasir/frontend\css\font-awesome.min.css" media="all">
<link rel="stylesheet" type="text/css" href="<?= $bu; ?>assets/kasir/frontend\css\animate.min.css" media="all">
<link rel="stylesheet" type="text/css" href="<?= $bu; ?>assets/kasir/frontend\css\font-pizzaro.css" media="all">
<link rel="stylesheet" type="text/css" href="<?= $bu; ?>assets/kasir/frontend\css\style.css" media="all">
<link rel="stylesheet" type="text/css" href="<?= $bu; ?>assets/kasir/frontend\css\colors\red.css" media="all">
<link rel="stylesheet" type="text/css" href="<?= $bu; ?>assets/kasir/frontend\css\jquery.mCustomScrollbar.min.css" media="all">
<link href="<?= $bu; ?>assets/kasir/https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CYanone+Kaffeesatz:200,300,400,700" rel="stylesheet">
<link href="<?= $bu; ?>assets/kasir/backend\js\sweetalert2.css" rel="stylesheet" type="text/css">

<div id="content" class="site-content" tabindex="-1">
<div class="container">
<div id="content" class="site-content" tabindex="-1">
			<div class="col-full">
				<div class="pizzaro-breadcrumb">
					<nav class="woocommerce-breadcrumb">
						
					</nav>
				</div>
				<div id="primary" class="content-area">
					<main id="main" class="site-main">
						<div class="pizzaro-order-steps">
							<ul>
								<li class="cart">
									<span class="step">1</span>Order Cart
								</li>
								<li class="checkout">
									<span class="step">2</span>Konfirmasi
								</li>
								<li class="complete">
									<span class="step">3</span>Order Selesai
								</li>
							</ul>
						</div>
						<div id="post-9" class="post-9 page type-page status-publish hentry">
							<header class="entry-header">
								<h1 class="entry-title">Order Selesai</h1>
							</header>
							<div class="entry-content">
								<div class="woocommerce">
									<p class="woocommerce-thankyou-order-received">Terima Kasih, Order Anda Kami Terima.</p>
									<div class="container">
										
										<div class="row">
											<div class="col-md-8">
												<li class="order">No. Order #:<strong><?= $Data->kode_transaksi ?></strong></li>
												
											</div>
											<div class="col-md-4"><li class="date">Tanggal :<strong><?= $Data->created_at ?></strong></li></div>
										</div>
										
									</div>
									<div class="clear"></div>
									<div class="clear"></div>
									<h2>Order Detail</h2>
									<table class="shop_table order_details">
										<thead>
											<tr>
												<th class="product-name">Menu</th>
												<th class="product-name">Catatan</th>
												<th class="product-total">Total</th>
											</tr>
										</thead>
										<tbody>
											<?php
											foreach ($getData as $k) {
											?>
												<tr class="order_item">
													<td class="product-name">
														<a href="#"><?= $k->nama_menu ?></a> <strong class="product-quantity"> X <?= $k->qty ?></strong>
													</td>
													<td class="product-name">
														<strong class="product-quantity">  <?= $k->catatan ?></strong>
													</td>
													<td class="product-total"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol"> </span> <?= convert_to_rupiah($k->total) ?></span>
													</td>
												</tr>

											<?php
											}
											?>
										</tbody>
										<tfoot>
											<tr>
												<th scope="row"></th>
												<th scope="row">Total :</th>
												<td><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol"></span> <?= convert_to_rupiah($Data->harga_total) ?></span></td>
											</tr>
										</tfoot>
									</table>
									<header>
										<h2>Detail Pembeli</h2>
									</header>
									<table class="shop_table customer_details">
										<tbody>
											<tr>
												<th>Nama :</th>
												<td> <?= $Data->nama_user ?></td>
											</tr>
											<tr>
												<th>Tipe Pesan :</th>
												<td> <?= strtoupper(str_replace('_', ' ', $Data->tipe_pesan))   ?></td>
											</tr>
											<tr id="no_meja">
												<th>No. Meja :</th>
												<td> <?= $Data->nomor_meja ?></td>
											</tr>
										</tbody>
									</table>
									<?php
									if($print_transaksi){
									?>
										<a class="btn btn-primary btn-sm PrintKasir" href="<?= base_url() ?>Kasir/print/<?= $id ?>" role="button"> Print Kasir</a>
										<a class="btn btn-primary btn-sm PrinThermal" href="<?= base_url() ?>Kasir/printPos/<?= $id ?>" role="button"> Print Thermal</a>
										<!-- <a class="btn btn-primary btn-sm thermalPos " href="#" role="button"> Print Thermal Pos Langsung</a> -->

									<?php }?>
								</div>
							</div>
						</div>
					</main>
				</div>
			</div>
		</div>
</div>
</div>
</div>

</body>


<script>
  $(document).ready(function() {

    var bu = '<?= base_url(); ?>'
    var datatable = $('#datatable_awal').DataTable({
      dom: "Bfrltip",
      'pageLength': 10,
      "responsive": true,
      "processing": true,
      "bProcessing": true,
      "autoWidth": false,
      "serverSide": true,
      "columnDefs": [{
          "targets": 0,
          "className": "dt-body-center dt-head-center",
          "width": "20px",
          "orderable": false
        },
        {
          "targets": 1,
          "className": "dt-head-center"
        },
        {
          "targets": 2,
          "className": "dt-head-center"
        }, {
          "targets": 3,
          "className": "dt-head-center"
        },
      ],
      "order": [
        [1, "desc"]
      ],
      'ajax': {
        url: bu + 'Data/getAllTransaksiDetail',
        type: 'POST',
        "data": function(d) {
          d.id = '<?= $id?>';
          d.status = $('#status').val();

          return d;
        }
      },

      language: {
        searchPlaceholder: "Cari",

      },
      // columnDefs: [{
      // 	targets: -1,
      // 	visible: false
      // }],
      "lengthMenu": [
        [10, 25, 50, 1000],
        [10, 25, 50, 1000]
      ]
    });
    $('#exampleModal').modal('hide');


  });
</script>