<?php
$bu = base_url()
?>

<div id="content" class="site-content" tabindex="-1">
  <div class="contener" style="margin: 37px;">
    <div class="col-full">
      <div class="form-group">
        <label for="status">Status</label>
        <select class="form-control" name="status" id="status">
          <option value="default">Pilih Status</option>
          <option value="created">Order Baru</option>
          <option value="finish">Sudah Selesai</option>
        </select>
      </div>
      <table class="table" id="datatable_awal">
        <thead class="thead-dark">
          <tr>
            <th scope="col">#</th>
            <th scope="col">Nama User</th>
            <th scope="col">Kode Transaksi</th>
            <th scope="col">Tipe Pesanan</th>
            <th scope="col">Harga Total</th>
            <th scope="col">Created </th>
            <th scope="col">Status</th>
            <th scope="col">Aksi</th>
          </tr>
        </thead>
      </table>
  
    </div>

  </div>
</div>
</div>

</body>


<script>
  $(document).ready(function() {

    var bu = '<?= base_url(); ?>'
    var datatable = $('#datatable_awal').DataTable({
      dom: "Bfrltip",
      'pageLength': 10,
      "responsive": true,
      "processing": true,
      "bProcessing": true,
      "autoWidth": false,
      "serverSide": true,
      "columnDefs": [{
          "targets": [0,7],
          "className": "dt-body-center dt-head-center",
          "width": "20px",
          "orderable": false
        },
        {
          "targets":[ 1,2,3,4,5,6],
          "className": "dt-head-center"
        },
      ],
      "order": [
        [1, "desc"]
      ],
      'ajax': {
        url: bu + 'Data/getAllTransaksiKasir',
        type: 'POST',
        "data": function(d) {
          d.status = $('#status').val();;
          return d;
        }
      },

      language: {
        searchPlaceholder: "Cari",

      },
      // columnDefs: [{
      // 	targets: -1,
      // 	visible: false
      // }],
      "lengthMenu": [
        [10, 25, 50, 1000],
        [10, 25, 50, 1000]
      ]
    });
    $('#exampleModal').modal('hide');

    $('#status').on('change', function() {
      datatable.ajax.reload()
    });

    $('body').on('click', '.btn_print', function() {
      var id_transaksi = $(this).data('id_transaksi');
      link = "<?= base_url() ?>Kasir/print/" + id_transaksi
      window.open(`${link}`);
    });
    $('body').on('click', '.btn_edit', function() {
      var id_transaksi = $(this).data('id_transaksi');
      link = "<?= base_url() ?>Kasir/orderandetail/" + id_transaksi
      window.open(`${link}`);
    });
    $('body').on('click', '.btn_finishing', function() {
      var id_transaksi = $(this).data('id_transaksi');
      Swal.fire({
        title: 'APakah Anda Yakin ',
        text: 'Akan Menyelesaikan Orderan Ini?',
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya'
      }).then((result) => {
        if (result.isConfirmed) {
          selesaikanOrderan(id_transaksi)
          datatable.ajax.reload()

        }
      })
    });
    $('body').on('click', '.btn_edits', function() {
      url_form = url_form_ubah;

      var id_transaksi = $(this).data('id_transaksi');
      console.log(id_transaksi);
      $('#exampleModal').modal('show');
      // datatables.destroy();

      var datatables = $('#datatable_detail').DataTable({
        dom: "Bfrltip",
        // "retrieve": true,
        'pageLength': 10,
        "responsive": true,
        "processing": true,
        "bProcessing": true,
        "autoWidth": false,
        "serverSide": true,
        "columnDefs": [{
            "targets": 0,
            "className": "dt-body-center dt-head-center",
            "width": "20px",
            "orderable": false
          },
          {
            "targets": 1,
            "className": "dt-head-center"
          },
          {
            "targets": 2,
            "className": "dt-head-center"
          }, {
            "targets": 3,
            "className": "dt-head-center"
          },
        ],
        "order": [
          [1, "desc"]
        ],
        'ajax': {
          url: bu + 'Data/getAllTransaksiDetail',
          type: 'POST',
          "data": function(d) {
            d.id = id_transaksi;
            return d;
          }
        },

        language: {
          searchPlaceholder: "Cari",

        },
        // columnDefs: [{
        // 	targets: -1,
        // 	visible: false
        // }],
        "lengthMenu": [
          [10, 25, 50, 1000],
          [10, 25, 50, 1000]
        ]

      });
      datatables.destroy();


    });
    function selesaikanOrderan(id) {
      $.ajax({
            type: "post",
            url: bu + 'Kasir/SelsaiOrder',
            data: {
              id
            },
            dataType: "json",
            success: function (r) {
              if(r.status){
                Swal.fire(
                  'Oke',
                  'Data Telah TerUpdate',
                  'success'
                )
              }else{
                Swal.fire(
                  'Maaf',
                  'Gagal',
                  'error'
                )
              }
            }
          });
    }

  });
</script>