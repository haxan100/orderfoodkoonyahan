<html lang="en">
<?php
$bu = base_url();
?>

<head>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/AlertifyJS/1.13.1/css/alertify.css" integrity="sha512-MpdEaY2YQ3EokN6lCD6bnWMl5Gwk7RjBbpKLovlrH6X+DRokrPRAF3zQJl1hZUiLXfo2e9MrOt+udOnHCAmi5w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="csrf-token" content="tfA9T1k6AcJ1NGKI2ZHeLYSghTmZdXxrC7SUMq2l">
  <script>
    'use strict';
    window.settings = {
      app_link: "<?= base_url() ?>assets/thing",
      csrfToken: "tfA9T1k6AcJ1NGKI2ZHeLYSghTmZdXxrC7SUMq2l",

      logged_in: "1",
      logged_in_user: "HKnbW4PuuGKZkCcqJDZ8jqHDh",
      access_token: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3RfdG9rZW4iLCJzdWIiOnsidXNlcl9pZCI6NSwidXNlcl9zbGFjayI6IkhLbmJXNFB1dUdLWmtDY3FKRFo4anFIRGgifSwiaWF0IjoxNjYxNzc3Mzk5LCJleHAiOjE2NjE4NjM3OTl9.l2sTU_XoDAEbwxbtwRpoxHFcEatrR3d6RhHtOowFF7U",
      logged_user_store_slack: "1zRPg6txi9xD5PA02zJQNFIJv",

      language: "en",
      menu_language: "",

      restaurant_mode: "1",
      currency_code: "USD"
    }
  </script>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no">

  <link rel="icon" type="image/png" sizes="32x32" href="/images/favicon_32_32.png">
  <link rel="apple-touch-icon" href="<?= base_url() ?>assets/thing/images/logo_apple_touch_icon.png">

  <link rel="stylesheet" href="<?= base_url() ?>assets/thing/css/font.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/thing/plugins/bootstrap/bootstrap.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/thing/plugins/datatables/datatables.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/thing/plugins/datatables/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/thing/plugins/fontawesome/all.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/thing/css/web.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/thing/css/nav.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/thing/css/tables.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/thing/css/form.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/thing/css/button.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/thing/css/labels.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/thing/css/modal.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/thing/css/billing.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/thing/css/quickpanel.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/thing/css/labels.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/thing/css/styling.css">
  <title><?= $konten[0]->isi ?></title>

</head>
<script type="text/javascript" src="<?= $bu; ?>assets/kasir/frontend\js\rupiah.js"></script>
<script src="<?= base_url() ?>assets/thing/plugins/jquery/jquery-3.4.1.slim.min.js"></script>
<script src="<?= base_url() ?>assets/thing/plugins/bootstrap/popper.min.js"></script>
<script src="<?= base_url() ?>assets/thing/plugins/bootstrap/bootstrap.min.js"></script>
<script src="<?= base_url() ?>assets/thing/js/side_nav.js"></script>
<script src="<?= base_url() ?>assets/thing/js/app.js?id=d8438ffd1a08e8e93bf3"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/AlertifyJS/1.13.1/alertify.js" integrity="sha512-eOUPKZXJTfgptSYQqVilRmxUNYm0XVHwcRHD4mdtCLWf/fC9XWe98IT8H1xzBkLL4Mo9GL0xWMSJtgS5te9rQA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="<?= $bu; ?>assets/admin//build/js/custom.js"></script>
<script src="<?= $bu; ?>assets/admin/vendors/datatables.net/js/jquery.dataTables.js"></script>
<script src="<?= $bu; ?>assets/admin/vendors/datatables.net-responsive/js/dataTables.responsive.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<style>
  .nav-pills .nav-link.active,
  .nav-pills .show>.nav-link {
    background-color: #17A2B8;
  }

  .dropdown-menu {
    top: 39px;
    right: -9px;
    left: unset;
    width: 361px;
    box-shadow: 0px 5px 7px -1px #c1c1c1;
    padding-bottom: 0px;
    padding: 9px;
  }
  .head {
    padding: 5px 15px;
    border-radius: 3px 3px 0px 0px;
  }

  .footer {
    padding: 5px 15px;
    border-radius: 0px 0px 3px 3px;
  }

  .notification-box {
    padding: 10px 0px;
  }

  .bg-gray {
    background-color: #eee;
  }

  @media (max-width: 640px) {
    .dropdown-menu {
      top: 50px;
      left: -16px;
      width: 290px;
    }

    .nav {
      display: block;
    }

    .nav .nav-item,
    .nav .nav-item a {
      padding-left: 0px;
    }

    .message {
      font-size: 13px;
    }
  }
  .text-light {
    color: #00070d!important;
}
.fixed-bottom {
    position: fixed;
    right: 0;
    bottom: -7%!important;
    left: 0;
    z-index: 1030;
}

</style>

<body class="container-fluid p-0">
  <div id="app">

    <input type="hidden" id="totalHargaSemua">

    <nav class="navbar navbar-expand-lg top-nav p-2">
     
    </nav>

    <script>
      $(document).ready(function() {
        Notif()

        function Notif() {
          $.ajax({
            type: "post",
            url: "<?= $bu ?>/Kasir/getPesananBelumAda",
            data: {
              id_cabang: '<?= $id_cabang ?>'
            },
            dataType: "json",
            success: function(r) {
              var html = ''
              var newHtml = ''
              var total = r.data.length
              if (total > 0) {
                $('#totalNotif').html(total);
              }
              var lihatSemua = '';
              if(total>=5){
                 lihatSemua = ` <li class="footer bg-dark text-center allMenu">
                                <a href="" class="text-light allMenu">View All</a>
                              </li>
                              `;
              }
              $.map(r.data, function(e, i) {
                var classnya = 'notification-box'
                console.log(e.length);
                if(i%2){
                  classnya = 'notification-box bg-gray';
                }
                html += `
                    <a class="content keMenu" data-id="${e.id_transaksi}">
                      <div class="notification-item">
                        <h4 class="item-title text-center">${e.kode_transaksi}</h4>
                        <p class="item-info text-center">User : ${e.nama_user}</p>
                      </div>
                    </a>
                `
                newHtml += `
                <li class="${classnya}">
                  <div class="row keMenu" data-id="${e.id_transaksi}">
                    <div class="col-lg-3 col-sm-3 col-3 text-center keMenu" data-id="${e.id_transaksi}">
                    </div>
                    <div class="col-lg-8 col-sm-8 col-8">
                      <strong class="text-info">${e.kode_transaksi}</strong>
                      <div>
                        User : ${e.nama_user}
                      </div>
                      <small class="text-info">${convertToRupiah(e.harga_total)}</small>
                    </div>
                  </div>
                </li>

                `

              });
              $('.menuKebawah').append(html);

              $('.menuDibawahNew').append(newHtml);
              $('.menuDibawahNew').append(lihatSemua);
            }
          });
        }
        $('body').on('click', '.keMenu', function() {
          console.log($(this).data());
          var id_produk = $(this).data('id');
          var id_cabang = '<?= $id_cabang ?>'
          var harga = $(this).data('harga');
          var qty = 1;
          $('.btn-tawar').html('Tunggu...');
          $('.btn-tawar').prop('disabled', true);

          var id_transaksi = $(this).data('id_transaksi');
          link = "<?= base_url() ?>Kasir/tableorderan/"
          window.open(`${link}`);


        })
        $('body').on('click', '.allMenu', function() {
          link = "<?= base_url() ?>Kasir/tableorderan/"
          window.open(`${link}`);


        })

        function getMeja() {
          $.ajax({
            type: "post",
            url: "<?= $bu ?>/Kasir/getMeja",
            data: {
              id_cabang: 1
            },
            dataType: "json",
            success: function(r) {
              var html = ''
              $.map(r.data, function(e, i) {
                html += `          
                    <option value="${e.id_meja}">${e.id_meja}</option>
                    `
              });
              $('#meja_id').append(html);
            }
          });
        }
        getMeja()
        $('#hasan').click(function(e) {
          // e.preventDefault();
          // console.log('<?= $bu ?>/Kasir/NewAdd')
          // return false
          location.reload('<?= $bu ?>/Kasir');
        });

      })
    </script>