<div class="col-md-12 col-sm-12 ">
	<div class="x_panel">
		<div class="x_title">
			<?php
			$date = date("Y-m-d");
			?>
			<h2>Laporan Harian</h2>
			<style>
				#image {
					max-width: 100px;
					display: block;
				}
			</style>

			<div class="clearfix"></div>
		</div>
		<div class="x_content">
			<div class="row">

				<div class="col-sm-12 xs-12 md-12 xss-12 ">
					<div class="card">
						<div class="card-body">
							<h5 class="card-title text-center">Harian</h5>

							<div id="date-picker-example" class="md-form md-outline input-with-post-icon datepicker" inline="true">
								<div class="form-group  md-6 xl-6 x-6">
									<label for=""></label>
									<input type="date" class="form-control md-6 xl-6 x-6" name="date" id="date" aria-describedby="helpId" value="<?= $date ?>" placeholder="">
								</div>
							</div>
							<div id="date-picker-example" class="md-form md-outline input-with-post-icon datepicker" inline="true">

								<button type="button" name="" id="cari" class="btn btn-primary btn-block" btn-lg btn-block">Cari</button>
							</div>
							<table class="table thead-dark" id="detail">
								<thead>
									<tr>
										<th>No</th>
										<th>Nama Menu</th>
										<th>QTY</th>
										<th>Total Harga</th>
										<th>Waktu</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
					<button class="btn btn-primary btn-block cetakPrint" type="submit">Cetak</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	document.addEventListener("DOMContentLoaded", function(event) {

		$('#cari').click(function(e) {
			e.preventDefault();
			var date = $('#date').val();
			datatable.ajax.reload()
		});

		$('.cetakPrint').click(function(e) {
			var date = $('#date').val();
			window.location.href = '<?= base_url() ?>/Admin/laporan_menu_harian_pdf_cabang/?tanggal=' + date;

		});
		var bu = '<?= base_url() ?>'
		var datatable = $('#detail').DataTable({
				'lengthMenu': [
					[10, 25, 50, 100,200,300],
					[10, 25, 50, 100,200,300],
				],
				dom: "Bfrltip",
				'pageLength': 10,
				"responsive": true,
				"processing": true,
				"bProcessing": true,
				"autoWidth": false,
				"serverSide": true,

				"columnDefs": [{
						"targets": 0,
						"className": "dt-body-center dt-head-center",
						"width": "20px",
						"orderable": false
					},
					{
						"targets": 1,
						"className": "dt-head-center"
					},
					{
						"targets": 2,
						"className": "dt-head-center"
					},
				],
				"order": [
					[1, "desc"]
				],
				'ajax': {
					url: bu + 'CabangData/getMenuJualByDate',
					type: 'POST',
					"data": function(d) {
						d.date = $('#date').val();
						return d;
					}
				},

				
				
			});

	});
</script>