<html lang="en">

<head>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script type="text/javascript" async="" src="https://www.google-analytics.com/analytics.js"></script>
    <script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-158287329-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-158287329-1');
    </script>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="csrf-token" content="tfA9T1k6AcJ1NGKI2ZHeLYSghTmZdXxrC7SUMq2l">
    <script>
        'use strict';
        window.settings = {
            app_link: "http://pos.appsthing.com",
            csrfToken: "tfA9T1k6AcJ1NGKI2ZHeLYSghTmZdXxrC7SUMq2l",

            logged_in: "1",
            logged_in_user: "HKnbW4PuuGKZkCcqJDZ8jqHDh",
            access_token: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3RfdG9rZW4iLCJzdWIiOnsidXNlcl9pZCI6NSwidXNlcl9zbGFjayI6IkhLbmJXNFB1dUdLWmtDY3FKRFo4anFIRGgifSwiaWF0IjoxNjYxNzc3Mzk5LCJleHAiOjE2NjE4NjM3OTl9.l2sTU_XoDAEbwxbtwRpoxHFcEatrR3d6RhHtOowFF7U",
            logged_user_store_slack: "1zRPg6txi9xD5PA02zJQNFIJv",

            language: "en",
            menu_language: "",

            restaurant_mode: "1",
            currency_code: "USD"
        }
    </script>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no">

    <link rel="icon" type="image/png" sizes="32x32" href="/images/favicon_32_32.png">
    <link rel="apple-touch-icon" href="http://pos.appsthing.com/images/logo_apple_touch_icon.png">

    <link rel="stylesheet" href="<?= base_url() ?>assets/thing/css/font.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/thing/plugins/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/thing/plugins/datatables/datatables.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/thing/plugins/datatables/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/thing/plugins/fontawesome/all.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/thing/css/web.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/thing/css/nav.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/thing/css/tables.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/thing/css/form.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/thing/css/button.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/thing/css/labels.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/thing/css/modal.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/thing/css/billing.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/thing/css/quickpanel.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/thing/css/labels.css">
    <title>Appsthing POS</title>
    <style type="text/css">
        .vue-notification-group {
            display: block;
            position: fixed;
            z-index: 5000
        }

        .vue-notification-wrapper {
            display: block;
            overflow: hidden;
            width: 100%;
            margin: 0;
            padding: 0
        }

        .notification-title {
            font-weight: 600
        }

        .vue-notification-template {
            background: #fff
        }

        .vue-notification,
        .vue-notification-template {
            display: block;
            box-sizing: border-box;
            text-align: left
        }

        .vue-notification {
            font-size: 12px;
            padding: 10px;
            margin: 0 5px 5px;
            color: #fff;
            background: #44a4fc;
            border-left: 5px solid #187fe7
        }

        .vue-notification.warn {
            background: #ffb648;
            border-left-color: #f48a06
        }

        .vue-notification.error {
            background: #e54d42;
            border-left-color: #b82e24
        }

        .vue-notification.success {
            background: #68cd86;
            border-left-color: #42a85f
        }

        .vn-fade-enter-active,
        .vn-fade-leave-active,
        .vn-fade-move {
            transition: all .5s
        }

        .vn-fade-enter,
        .vn-fade-leave-to {
            opacity: 0
        }
    </style>
    <style type="text/css">
        .mx-icon-left:before,
        .mx-icon-right:before,
        .mx-icon-double-left:before,
        .mx-icon-double-right:before,
        .mx-icon-double-left:after,
        .mx-icon-double-right:after {
            content: "";
            position: relative;
            top: -1px;
            display: inline-block;
            width: 10px;
            height: 10px;
            vertical-align: middle;
            border-style: solid;
            border-color: currentColor;
            border-width: 2px 0 0 2px;
            border-radius: 1px;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            -webkit-transform-origin: center;
            transform-origin: center;
            -webkit-transform: rotate(-45deg) scale(0.7);
            transform: rotate(-45deg) scale(0.7)
        }

        .mx-icon-double-left:after {
            left: -4px
        }

        .mx-icon-double-right:before {
            left: 4px
        }

        .mx-icon-right:before,
        .mx-icon-double-right:before,
        .mx-icon-double-right:after {
            -webkit-transform: rotate(135deg) scale(0.7);
            transform: rotate(135deg) scale(0.7)
        }

        .mx-btn {
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            line-height: 1;
            font-size: 14px;
            font-weight: 500;
            padding: 7px 15px;
            margin: 0;
            cursor: pointer;
            background-color: transparent;
            outline: none;
            border: 1px solid rgba(0, 0, 0, .1);
            border-radius: 4px;
            color: #73879c;
            white-space: nowrap
        }

        .mx-btn:hover {
            border-color: #1284e7;
            color: #1284e7
        }

        .mx-btn-text {
            border: 0;
            padding: 0 4px;
            text-align: left;
            line-height: inherit
        }

        .mx-scrollbar {
            height: 100%
        }

        .mx-scrollbar:hover .mx-scrollbar-track {
            opacity: 1
        }

        .mx-scrollbar-wrap {
            height: 100%;
            overflow-x: hidden;
            overflow-y: auto
        }

        .mx-scrollbar-track {
            position: absolute;
            top: 2px;
            right: 2px;
            bottom: 2px;
            width: 6px;
            z-index: 1;
            border-radius: 4px;
            opacity: 0;
            -webkit-transition: opacity .24s ease-out;
            transition: opacity .24s ease-out
        }

        .mx-scrollbar-track .mx-scrollbar-thumb {
            position: absolute;
            width: 100%;
            height: 0;
            cursor: pointer;
            border-radius: inherit;
            background-color: rgba(144, 147, 153, .3);
            -webkit-transition: background-color .3s;
            transition: background-color .3s
        }

        .mx-zoom-in-down-enter-active,
        .mx-zoom-in-down-leave-active {
            opacity: 1;
            -webkit-transform: scaleY(1);
            transform: scaleY(1);
            -webkit-transition: opacity .3s cubic-bezier(0.23, 1, 0.32, 1), -webkit-transform .3s cubic-bezier(0.23, 1, 0.32, 1);
            transition: opacity .3s cubic-bezier(0.23, 1, 0.32, 1), -webkit-transform .3s cubic-bezier(0.23, 1, 0.32, 1);
            transition: transform .3s cubic-bezier(0.23, 1, 0.32, 1), opacity .3s cubic-bezier(0.23, 1, 0.32, 1);
            transition: transform .3s cubic-bezier(0.23, 1, 0.32, 1), opacity .3s cubic-bezier(0.23, 1, 0.32, 1), -webkit-transform .3s cubic-bezier(0.23, 1, 0.32, 1);
            -webkit-transform-origin: center top;
            transform-origin: center top
        }

        .mx-zoom-in-down-enter,
        .mx-zoom-in-down-leave-to {
            opacity: 0;
            -webkit-transform: scaleY(0);
            transform: scaleY(0)
        }

        .mx-datepicker {
            position: relative;
            display: inline-block;
            width: 210px
        }

        .mx-datepicker svg {
            width: 1em;
            height: 1em;
            vertical-align: -0.15em;
            fill: currentColor;
            overflow: hidden
        }

        .mx-datepicker-range {
            width: 320px
        }

        .mx-datepicker-inline {
            width: auto
        }

        .mx-input-wrapper {
            position: relative
        }

        .mx-input-wrapper .mx-icon-clear {
            display: none
        }

        .mx-input-wrapper:hover .mx-icon-clear {
            display: block
        }

        .mx-input-wrapper:hover .mx-icon-clear+.mx-icon-calendar {
            display: none
        }

        .mx-input {
            display: inline-block;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            width: 100%;
            height: 34px;
            padding: 6px 30px;
            padding-left: 10px;
            font-size: 14px;
            line-height: 1.4;
            color: #555;
            background-color: #fff;
            border: 1px solid #ccc;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075)
        }

        .mx-input:hover,
        .mx-input:focus {
            border-color: #409aff
        }

        .mx-input:disabled,
        .mx-input.disabled {
            color: #ccc;
            background-color: #f3f3f3;
            border-color: #ccc;
            cursor: not-allowed
        }

        .mx-input:focus {
            outline: none
        }

        .mx-input::-ms-clear {
            display: none
        }

        .mx-icon-calendar,
        .mx-icon-clear {
            position: absolute;
            top: 50%;
            right: 8px;
            -webkit-transform: translateY(-50%);
            transform: translateY(-50%);
            font-size: 16px;
            line-height: 1;
            color: rgba(0, 0, 0, .5);
            vertical-align: middle
        }

        .mx-icon-clear {
            cursor: pointer
        }

        .mx-icon-clear:hover {
            color: rgba(0, 0, 0, .8)
        }

        .mx-datepicker-main {
            font: 14px/1.5 "Helvetica Neue", Helvetica, Arial, "Microsoft Yahei", sans-serif;
            color: #73879c;
            background-color: #fff;
            border: 1px solid #e8e8e8
        }

        .mx-datepicker-popup {
            position: absolute;
            margin-top: 1px;
            margin-bottom: 1px;
            -webkit-box-shadow: 0 6px 12px rgba(0, 0, 0, .175);
            box-shadow: 0 6px 12px rgba(0, 0, 0, .175);
            z-index: 2001
        }

        .mx-datepicker-sidebar {
            float: left;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            width: 100px;
            padding: 6px;
            overflow: auto
        }

        .mx-datepicker-sidebar+.mx-datepicker-content {
            margin-left: 100px;
            border-left: 1px solid #e8e8e8
        }

        .mx-datepicker-body {
            position: relative;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none
        }

        .mx-btn-shortcut {
            display: block;
            padding: 0 6px;
            line-height: 24px
        }

        .mx-range-wrapper {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex
        }

        @media(max-width: 750px) {
            .mx-range-wrapper {
                -webkit-box-orient: vertical;
                -webkit-box-direction: normal;
                -ms-flex-direction: column;
                flex-direction: column
            }
        }

        .mx-datepicker-header {
            padding: 6px 8px;
            border-bottom: 1px solid #e8e8e8
        }

        .mx-datepicker-footer {
            padding: 6px 8px;
            text-align: right;
            border-top: 1px solid #e8e8e8
        }

        .mx-calendar {
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            width: 248px;
            padding: 6px 12px
        }

        .mx-calendar+.mx-calendar {
            border-left: 1px solid #e8e8e8
        }

        .mx-calendar-header,
        .mx-time-header {
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            height: 34px;
            line-height: 34px;
            text-align: center;
            overflow: hidden
        }

        .mx-btn-icon-left,
        .mx-btn-icon-double-left {
            float: left
        }

        .mx-btn-icon-right,
        .mx-btn-icon-double-right {
            float: right
        }

        .mx-calendar-header-label {
            font-size: 14px
        }

        .mx-calendar-decade-separator {
            margin: 0 2px
        }

        .mx-calendar-decade-separator:after {
            content: "~"
        }

        .mx-calendar-content {
            position: relative;
            height: 224px;
            -webkit-box-sizing: border-box;
            box-sizing: border-box
        }

        .mx-calendar-content .cell {
            cursor: pointer
        }

        .mx-calendar-content .cell:hover {
            color: #73879c;
            background-color: #f3f9fe
        }

        .mx-calendar-content .cell.active {
            color: #fff;
            background-color: #1284e7
        }

        .mx-calendar-content .cell.in-range {
            color: #73879c;
            background-color: #dbedfb
        }

        .mx-calendar-content .cell.disabled {
            cursor: not-allowed;
            color: #ccc;
            background-color: #f3f3f3
        }

        .mx-calendar-week-mode .mx-date-row {
            cursor: pointer
        }

        .mx-calendar-week-mode .mx-date-row:hover {
            background-color: #f3f9fe
        }

        .mx-calendar-week-mode .mx-date-row.mx-active-week {
            background-color: #dbedfb
        }

        .mx-calendar-week-mode .mx-date-row .cell:hover {
            color: inherit;
            background-color: transparent
        }

        .mx-calendar-week-mode .mx-date-row .cell.active {
            color: inherit;
            background-color: transparent
        }

        .mx-week-number {
            opacity: .5
        }

        .mx-table {
            table-layout: fixed;
            border-collapse: separate;
            border-spacing: 0;
            width: 100%;
            height: 100%;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            text-align: center;
            vertical-align: middle
        }

        .mx-table th {
            padding: 0;
            font-weight: 500
        }

        .mx-table td {
            padding: 0
        }

        .mx-table-date td,
        .mx-table-date th {
            height: 32px;
            font-size: 12px
        }

        .mx-table-date .today {
            color: #2a90e9
        }

        .mx-table-date .cell.not-current-month {
            color: #ccc
        }

        .mx-time {
            -webkit-box-flex: 1;
            -ms-flex: 1;
            flex: 1;
            width: 224px;
            background: #fff
        }

        .mx-time+.mx-time {
            border-left: 1px solid #e8e8e8
        }

        .mx-calendar-time {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%
        }

        .mx-time-header {
            border-bottom: 1px solid #e8e8e8
        }

        .mx-time-content {
            height: 224px;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            overflow: hidden
        }

        .mx-time-columns {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            width: 100%;
            height: 100%;
            overflow: hidden
        }

        .mx-time-column {
            -webkit-box-flex: 1;
            -ms-flex: 1;
            flex: 1;
            position: relative;
            border-left: 1px solid #e8e8e8;
            text-align: center
        }

        .mx-time-column:first-child {
            border-left: 0
        }

        .mx-time-column .mx-time-list {
            margin: 0;
            padding: 0;
            list-style: none
        }

        .mx-time-column .mx-time-list::after {
            content: "";
            display: block;
            height: 192px
        }

        .mx-time-column .mx-time-item {
            cursor: pointer;
            font-size: 12px;
            height: 32px;
            line-height: 32px
        }

        .mx-time-column .mx-time-item:hover {
            color: #73879c;
            background-color: #f3f9fe
        }

        .mx-time-column .mx-time-item.active {
            color: #1284e7;
            background-color: transparent;
            font-weight: 700
        }

        .mx-time-column .mx-time-item.disabled {
            cursor: not-allowed;
            color: #ccc;
            background-color: #f3f3f3
        }

        .mx-time-option {
            cursor: pointer;
            padding: 8px 10px;
            font-size: 14px;
            line-height: 20px
        }

        .mx-time-option:hover {
            color: #73879c;
            background-color: #f3f9fe
        }

        .mx-time-option.active {
            color: #1284e7;
            background-color: transparent;
            font-weight: 700
        }

        .mx-time-option.disabled {
            cursor: not-allowed;
            color: #ccc;
            background-color: #f3f3f3
        }
    </style>
    <style type="text/css">
        .IZ-select * {
            box-sizing: border-box
        }

        .IZ-select__input-wrap {
            display: flex;
            align-items: center;
            height: 100%
        }

        .IZ-select__input,
        .IZ-select__input input {
            height: 100%
        }

        .fade-leave-active {
            position: absolute
        }

        .fade-enter-active,
        .fade-leave,
        .fade-leave-to {
            transition: opacity .2s
        }

        .fade-enter,
        .fade-leave-to {
            opacity: 0
        }

        .IZ-select {
            position: relative;
            outline: none
        }

        .IZ-select .IZ-select__input {
            font-weight: 400;
            font-size: 1rem;
            line-height: 1.5
        }

        .IZ-select .IZ-select__input input {
            height: calc(1.5em + .75rem);
            padding: .375rem .75rem
        }

        .IZ-select--sm .IZ-select__input {
            border-radius: .2rem
        }

        .IZ-select--sm .IZ-select__input input {
            height: calc(1.5em + .5rem);
            padding: .25rem .5rem;
            font-size: .875rem;
            line-height: 1.5
        }

        .IZ-select--lg .IZ-select__input {
            border-radius: .3rem
        }

        .IZ-select--lg .IZ-select__input input {
            height: calc(1.5em + 1rem);
            padding: .5rem 1rem;
            font-size: 1.25rem;
            line-height: 1.5
        }

        .IZ-select__input {
            align-items: center;
            display: flex;
            flex: 1 1 auto;
            flex-wrap: wrap;
            width: 100%;
            color: #495057;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid #ced4da;
            border-radius: .25rem;
            transition: background-color .15s ease-in-out, border-color .15s ease-in-out, box-shadow .15s ease-in-out
        }

        .IZ-select__input.IZ-select__input--selection-slot {
            padding-left: .75rem
        }

        .IZ-select__input.IZ-select__input--selection-slot input {
            padding-left: 10px
        }

        .IZ-select__input.IZ-select__input--has-error {
            border: 1px solid #dc3545 !important;
            caret-color: #ff5252 !important
        }

        .IZ-select__input.IZ-select__input--has-error input {
            color: #ff5252 !important
        }

        .IZ-select__input.IZ-select__input--successful {
            border: 1px solid #28a745 !important;
            caret-color: #28c346 !important
        }

        .IZ-select__input.IZ-select__input--focused {
            border-color: #80bdff;
            outline: 0;
            box-shadow: 0 0 0 .2rem rgba(128, 189, 255, .5)
        }

        .IZ-select__input.IZ-select__input--focused.IZ-select__input--has-error {
            box-shadow: 0 0 0 .2rem rgba(220, 53, 69, .25) !important
        }

        .IZ-select__input.IZ-select__input--focused.IZ-select__input--successful {
            box-shadow: 0 0 0 .2rem rgba(40, 167, 69, .25) !important
        }

        .IZ-select__input.IZ-select__input--disabled {
            pointer-events: none;
            background-color: #e9ecef;
            opacity: 1
        }

        .IZ-select__input.IZ-select__input--disabled input {
            color: #6c737a !important
        }

        .IZ-select__input.IZ-select__input--disabled::-webkit-input-placeholder {
            color: #6c737a !important
        }

        .IZ-select__input.IZ-select__input--disabled::-moz-placeholder {
            color: #6c737a !important
        }

        .IZ-select__input.IZ-select__input--disabled:-ms-input-placeholder {
            color: #6c737a !important
        }

        .IZ-select__input.IZ-select__input--disabled::-ms-input-placeholder {
            color: #6c737a !important
        }

        .IZ-select__input.IZ-select__input--disabled::placeholder {
            color: #6c737a !important
        }

        .IZ-select__input input {
            font-size: 1rem;
            background-size: 25px 25px;
            background-position: right 10px center;
            background-repeat: no-repeat;
            color: #495057 !important;
            background-color: transparent;
            border-style: none;
            pointer-events: auto;
            flex: 1 1;
            margin-top: 0;
            min-width: 0;
            position: relative;
            line-height: 20px;
            max-width: 100%;
            width: 100%
        }

        .IZ-select__input input:focus {
            outline: none
        }

        .IZ-select__input input:disabled {
            pointer-events: none
        }

        .IZ-select__menu {
            opacity: 0;
            transition: opacity .1s;
            position: absolute;
            z-index: 8;
            transform-origin: left top 0;
            background-color: #fff;
            border: 1px solid #ced4da;
            border-radius: .25rem;
            box-shadow: 0 2px 11px -2px rgba(0, 0, 0, .19)
        }

        .IZ-select__menu--at-top {
            border-bottom: 0;
            border-bottom-left-radius: 0;
            border-bottom-right-radius: 0
        }

        .IZ-select__menu--at-top.IZ-select__menu--disable-search {
            border-bottom: 1px;
            border-bottom-left-radius: .25rem;
            border-bottom-right-radius: .25rem
        }

        .IZ-select__menu--at-bottom {
            border-top: 0;
            border-top-left-radius: 0;
            border-top-right-radius: 0
        }

        .IZ-select__menu--at-bottom.IZ-select__menu--disable-search {
            border-top: 1px;
            border-top-left-radius: .25rem;
            border-top-right-radius: .25rem
        }

        .IZ-select__menu .IZ-select__menu-items {
            overflow-y: auto;
            overflow-x: hidden
        }

        .IZ-select__menu .IZ-select__no-data {
            margin: 0 10px
        }

        .IZ-select__item {
            cursor: pointer;
            padding: 10px 14px;
            transition: .3s cubic-bezier(.25, .8, .5, 1)
        }

        .IZ-select__item:hover {
            background-color: #f2f2f2
        }

        .IZ-select__item.IZ-select__item--selected {
            color: #1976d2 !important
        }

        .IZ-select__error {
            margin-top: .55rem;
            font-size: 85%;
            color: #dc3545
        }
    </style>
    <style type="text/css">
        fieldset[disabled] .multiselect {
            pointer-events: none
        }

        .multiselect__spinner {
            position: absolute;
            right: 1px;
            top: 1px;
            width: 48px;
            height: 35px;
            background: #fff;
            display: block
        }

        .multiselect__spinner:after,
        .multiselect__spinner:before {
            position: absolute;
            content: "";
            top: 50%;
            left: 50%;
            margin: -8px 0 0 -8px;
            width: 16px;
            height: 16px;
            border-radius: 100%;
            border: 2px solid transparent;
            border-top-color: #41b883;
            box-shadow: 0 0 0 1px transparent
        }

        .multiselect__spinner:before {
            animation: spinning 2.4s cubic-bezier(.41, .26, .2, .62);
            animation-iteration-count: infinite
        }

        .multiselect__spinner:after {
            animation: spinning 2.4s cubic-bezier(.51, .09, .21, .8);
            animation-iteration-count: infinite
        }

        .multiselect__loading-enter-active,
        .multiselect__loading-leave-active {
            transition: opacity .4s ease-in-out;
            opacity: 1
        }

        .multiselect__loading-enter,
        .multiselect__loading-leave-active {
            opacity: 0
        }

        .multiselect,
        .multiselect__input,
        .multiselect__single {
            font-family: inherit;
            font-size: 16px;
            -ms-touch-action: manipulation;
            touch-action: manipulation
        }

        .multiselect {
            box-sizing: content-box;
            display: block;
            position: relative;
            width: 100%;
            min-height: 40px;
            text-align: left;
            color: #35495e
        }

        .multiselect * {
            box-sizing: border-box
        }

        .multiselect:focus {
            outline: none
        }

        .multiselect--disabled {
            background: #ededed;
            pointer-events: none;
            opacity: .6
        }

        .multiselect--active {
            z-index: 50
        }

        .multiselect--active:not(.multiselect--above) .multiselect__current,
        .multiselect--active:not(.multiselect--above) .multiselect__input,
        .multiselect--active:not(.multiselect--above) .multiselect__tags {
            border-bottom-left-radius: 0;
            border-bottom-right-radius: 0
        }

        .multiselect--active .multiselect__select {
            transform: rotate(180deg)
        }

        .multiselect--above.multiselect--active .multiselect__current,
        .multiselect--above.multiselect--active .multiselect__input,
        .multiselect--above.multiselect--active .multiselect__tags {
            border-top-left-radius: 0;
            border-top-right-radius: 0
        }

        .multiselect__input,
        .multiselect__single {
            position: relative;
            display: inline-block;
            min-height: 20px;
            line-height: 20px;
            border: none;
            border-radius: 5px;
            background: #fff;
            padding: 0 0 0 5px;
            width: 100%;
            transition: border .1s ease;
            box-sizing: border-box;
            margin-bottom: 8px;
            vertical-align: top
        }

        .multiselect__input:-ms-input-placeholder {
            color: #35495e
        }

        .multiselect__input::placeholder {
            color: #35495e
        }

        .multiselect__tag~.multiselect__input,
        .multiselect__tag~.multiselect__single {
            width: auto
        }

        .multiselect__input:hover,
        .multiselect__single:hover {
            border-color: #cfcfcf
        }

        .multiselect__input:focus,
        .multiselect__single:focus {
            border-color: #a8a8a8;
            outline: none
        }

        .multiselect__single {
            padding-left: 5px;
            margin-bottom: 8px
        }

        .multiselect__tags-wrap {
            display: inline
        }

        .multiselect__tags {
            min-height: 40px;
            display: block;
            padding: 8px 40px 0 8px;
            border-radius: 5px;
            border: 1px solid #e8e8e8;
            background: #fff;
            font-size: 14px
        }

        .multiselect__tag {
            position: relative;
            display: inline-block;
            padding: 4px 26px 4px 10px;
            border-radius: 5px;
            margin-right: 10px;
            color: #fff;
            line-height: 1;
            background: #41b883;
            margin-bottom: 5px;
            white-space: nowrap;
            overflow: hidden;
            max-width: 100%;
            text-overflow: ellipsis
        }

        .multiselect__tag-icon {
            cursor: pointer;
            margin-left: 7px;
            position: absolute;
            right: 0;
            top: 0;
            bottom: 0;
            font-weight: 700;
            font-style: normal;
            width: 22px;
            text-align: center;
            line-height: 22px;
            transition: all .2s ease;
            border-radius: 5px
        }

        .multiselect__tag-icon:after {
            content: "\D7";
            color: #266d4d;
            font-size: 14px
        }

        .multiselect__tag-icon:focus,
        .multiselect__tag-icon:hover {
            background: #369a6e
        }

        .multiselect__tag-icon:focus:after,
        .multiselect__tag-icon:hover:after {
            color: #fff
        }

        .multiselect__current {
            min-height: 40px;
            overflow: hidden;
            padding: 8px 30px 0 12px;
            white-space: nowrap;
            border-radius: 5px;
            border: 1px solid #e8e8e8
        }

        .multiselect__current,
        .multiselect__select {
            line-height: 16px;
            box-sizing: border-box;
            display: block;
            margin: 0;
            text-decoration: none;
            cursor: pointer
        }

        .multiselect__select {
            position: absolute;
            width: 40px;
            height: 38px;
            right: 1px;
            top: 1px;
            padding: 4px 8px;
            text-align: center;
            transition: transform .2s ease
        }

        .multiselect__select:before {
            position: relative;
            right: 0;
            top: 65%;
            color: #999;
            margin-top: 4px;
            border-color: #999 transparent transparent;
            border-style: solid;
            border-width: 5px 5px 0;
            content: ""
        }

        .multiselect__placeholder {
            color: #adadad;
            display: inline-block;
            margin-bottom: 10px;
            padding-top: 2px
        }

        .multiselect--active .multiselect__placeholder {
            display: none
        }

        .multiselect__content-wrapper {
            position: absolute;
            display: block;
            background: #fff;
            width: 100%;
            max-height: 240px;
            overflow: auto;
            border: 1px solid #e8e8e8;
            border-top: none;
            border-bottom-left-radius: 5px;
            border-bottom-right-radius: 5px;
            z-index: 50;
            -webkit-overflow-scrolling: touch
        }

        .multiselect__content {
            list-style: none;
            display: inline-block;
            padding: 0;
            margin: 0;
            min-width: 100%;
            vertical-align: top
        }

        .multiselect--above .multiselect__content-wrapper {
            bottom: 100%;
            border-bottom-left-radius: 0;
            border-bottom-right-radius: 0;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
            border-bottom: none;
            border-top: 1px solid #e8e8e8
        }

        .multiselect__content::webkit-scrollbar {
            display: none
        }

        .multiselect__element {
            display: block
        }

        .multiselect__option {
            display: block;
            padding: 12px;
            min-height: 40px;
            line-height: 16px;
            text-decoration: none;
            text-transform: none;
            vertical-align: middle;
            position: relative;
            cursor: pointer;
            white-space: nowrap
        }

        .multiselect__option:after {
            top: 0;
            right: 0;
            position: absolute;
            line-height: 40px;
            padding-right: 12px;
            padding-left: 20px;
            font-size: 13px
        }

        .multiselect__option--highlight {
            background: #41b883;
            outline: none;
            color: #fff
        }

        .multiselect__option--highlight:after {
            content: attr(data-select);
            background: #41b883;
            color: #fff
        }

        .multiselect__option--selected {
            background: #f3f3f3;
            color: #35495e;
            font-weight: 700
        }

        .multiselect__option--selected:after {
            content: attr(data-selected);
            color: silver
        }

        .multiselect__option--selected.multiselect__option--highlight {
            background: #ff6a6a;
            color: #fff
        }

        .multiselect__option--selected.multiselect__option--highlight:after {
            background: #ff6a6a;
            content: attr(data-deselect);
            color: #fff
        }

        .multiselect--disabled .multiselect__current,
        .multiselect--disabled .multiselect__select {
            background: #ededed;
            color: #a6a6a6
        }

        .multiselect__option--disabled {
            background: #ededed !important;
            color: #a6a6a6 !important;
            cursor: text;
            pointer-events: none
        }

        .multiselect__option--group {
            background: #ededed;
            color: #35495e
        }

        .multiselect__option--group.multiselect__option--highlight {
            background: #35495e;
            color: #fff
        }

        .multiselect__option--group.multiselect__option--highlight:after {
            background: #35495e
        }

        .multiselect__option--disabled.multiselect__option--highlight {
            background: #dedede
        }

        .multiselect__option--group-selected.multiselect__option--highlight {
            background: #ff6a6a;
            color: #fff
        }

        .multiselect__option--group-selected.multiselect__option--highlight:after {
            background: #ff6a6a;
            content: attr(data-deselect);
            color: #fff
        }

        .multiselect-enter-active,
        .multiselect-leave-active {
            transition: all .15s ease
        }

        .multiselect-enter,
        .multiselect-leave-active {
            opacity: 0
        }

        .multiselect__strong {
            margin-bottom: 8px;
            line-height: 20px;
            display: inline-block;
            vertical-align: top
        }

        [dir=rtl] .multiselect {
            text-align: right
        }

        [dir=rtl] .multiselect__select {
            right: auto;
            left: 1px
        }

        [dir=rtl] .multiselect__tags {
            padding: 8px 8px 0 40px
        }

        [dir=rtl] .multiselect__content {
            text-align: right
        }

        [dir=rtl] .multiselect__option:after {
            right: auto;
            left: 0
        }

        [dir=rtl] .multiselect__clear {
            right: auto;
            left: 12px
        }

        [dir=rtl] .multiselect__spinner {
            right: auto;
            left: 1px
        }

        @keyframes spinning {
            0% {
                transform: rotate(0)
            }

            to {
                transform: rotate(2turn)
            }
        }
    </style>
    <style type="text/css">
        /* classes attached to <body> */

        .fc-not-allowed,
        .fc-not-allowed .fc-event {
            /* override events' custom cursors */
            cursor: not-allowed;
        }

        .fc-unselectable {
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            -webkit-touch-callout: none;
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
        }

        .fc {
            /* layout of immediate children */
            display: flex;
            flex-direction: column;

            font-size: 1em
        }

        .fc,
        .fc *,
        .fc *:before,
        .fc *:after {
            box-sizing: border-box;
        }

        .fc table {
            border-collapse: collapse;
            border-spacing: 0;
            font-size: 1em;
            /* normalize cross-browser */
        }

        .fc th {
            text-align: center;
        }

        .fc th,
        .fc td {
            vertical-align: top;
            padding: 0;
        }

        .fc a[data-navlink] {
            cursor: pointer;
        }

        .fc a[data-navlink]:hover {
            text-decoration: underline;
        }

        .fc-direction-ltr {
            direction: ltr;
            text-align: left;
        }

        .fc-direction-rtl {
            direction: rtl;
            text-align: right;
        }

        .fc-theme-standard td,
        .fc-theme-standard th {
            border: 1px solid #ddd;
            border: 1px solid var(--fc-border-color, #ddd);
        }

        /* for FF, which doesn't expand a 100% div within a table cell. use absolute positioning */
        /* inner-wrappers are responsible for being absolute */
        /* TODO: best place for this? */
        .fc-liquid-hack td,
        .fc-liquid-hack th {
            position: relative;
        }

        @font-face {
            font-family: 'fcicons';
            src: url("data:application/x-font-ttf;charset=utf-8;base64,AAEAAAALAIAAAwAwT1MvMg8SBfAAAAC8AAAAYGNtYXAXVtKNAAABHAAAAFRnYXNwAAAAEAAAAXAAAAAIZ2x5ZgYydxIAAAF4AAAFNGhlYWQUJ7cIAAAGrAAAADZoaGVhB20DzAAABuQAAAAkaG10eCIABhQAAAcIAAAALGxvY2ED4AU6AAAHNAAAABhtYXhwAA8AjAAAB0wAAAAgbmFtZXsr690AAAdsAAABhnBvc3QAAwAAAAAI9AAAACAAAwPAAZAABQAAApkCzAAAAI8CmQLMAAAB6wAzAQkAAAAAAAAAAAAAAAAAAAABEAAAAAAAAAAAAAAAAAAAAABAAADpBgPA/8AAQAPAAEAAAAABAAAAAAAAAAAAAAAgAAAAAAADAAAAAwAAABwAAQADAAAAHAADAAEAAAAcAAQAOAAAAAoACAACAAIAAQAg6Qb//f//AAAAAAAg6QD//f//AAH/4xcEAAMAAQAAAAAAAAAAAAAAAQAB//8ADwABAAAAAAAAAAAAAgAANzkBAAAAAAEAAAAAAAAAAAACAAA3OQEAAAAAAQAAAAAAAAAAAAIAADc5AQAAAAABAWIAjQKeAskAEwAAJSc3NjQnJiIHAQYUFwEWMjc2NCcCnuLiDQ0MJAz/AA0NAQAMJAwNDcni4gwjDQwM/wANIwz/AA0NDCMNAAAAAQFiAI0CngLJABMAACUBNjQnASYiBwYUHwEHBhQXFjI3AZ4BAA0N/wAMJAwNDeLiDQ0MJAyNAQAMIw0BAAwMDSMM4uINIwwNDQAAAAIA4gC3Ax4CngATACcAACUnNzY0JyYiDwEGFB8BFjI3NjQnISc3NjQnJiIPAQYUHwEWMjc2NCcB87e3DQ0MIw3VDQ3VDSMMDQ0BK7e3DQ0MJAzVDQ3VDCQMDQ3zuLcMJAwNDdUNIwzWDAwNIwy4twwkDA0N1Q0jDNYMDA0jDAAAAgDiALcDHgKeABMAJwAAJTc2NC8BJiIHBhQfAQcGFBcWMjchNzY0LwEmIgcGFB8BBwYUFxYyNwJJ1Q0N1Q0jDA0Nt7cNDQwjDf7V1Q0N1QwkDA0Nt7cNDQwkDLfWDCMN1Q0NDCQMt7gMIw0MDNYMIw3VDQ0MJAy3uAwjDQwMAAADAFUAAAOrA1UAMwBoAHcAABMiBgcOAQcOAQcOARURFBYXHgEXHgEXHgEzITI2Nz4BNz4BNz4BNRE0JicuAScuAScuASMFITIWFx4BFx4BFx4BFREUBgcOAQcOAQcOASMhIiYnLgEnLgEnLgE1ETQ2Nz4BNz4BNz4BMxMhMjY1NCYjISIGFRQWM9UNGAwLFQkJDgUFBQUFBQ4JCRULDBgNAlYNGAwLFQkJDgUFBQUFBQ4JCRULDBgN/aoCVgQIBAQHAwMFAQIBAQIBBQMDBwQECAT9qgQIBAQHAwMFAQIBAQIBBQMDBwQECASAAVYRGRkR/qoRGRkRA1UFBAUOCQkVDAsZDf2rDRkLDBUJCA4FBQUFBQUOCQgVDAsZDQJVDRkLDBUJCQ4FBAVVAgECBQMCBwQECAX9qwQJAwQHAwMFAQICAgIBBQMDBwQDCQQCVQUIBAQHAgMFAgEC/oAZEhEZGRESGQAAAAADAFUAAAOrA1UAMwBoAIkAABMiBgcOAQcOAQcOARURFBYXHgEXHgEXHgEzITI2Nz4BNz4BNz4BNRE0JicuAScuAScuASMFITIWFx4BFx4BFx4BFREUBgcOAQcOAQcOASMhIiYnLgEnLgEnLgE1ETQ2Nz4BNz4BNz4BMxMzFRQWMzI2PQEzMjY1NCYrATU0JiMiBh0BIyIGFRQWM9UNGAwLFQkJDgUFBQUFBQ4JCRULDBgNAlYNGAwLFQkJDgUFBQUFBQ4JCRULDBgN/aoCVgQIBAQHAwMFAQIBAQIBBQMDBwQECAT9qgQIBAQHAwMFAQIBAQIBBQMDBwQECASAgBkSEhmAERkZEYAZEhIZgBEZGREDVQUEBQ4JCRUMCxkN/asNGQsMFQkIDgUFBQUFBQ4JCBUMCxkNAlUNGQsMFQkJDgUEBVUCAQIFAwIHBAQIBf2rBAkDBAcDAwUBAgICAgEFAwMHBAMJBAJVBQgEBAcCAwUCAQL+gIASGRkSgBkSERmAEhkZEoAZERIZAAABAOIAjQMeAskAIAAAExcHBhQXFjI/ARcWMjc2NC8BNzY0JyYiDwEnJiIHBhQX4uLiDQ0MJAzi4gwkDA0N4uINDQwkDOLiDCQMDQ0CjeLiDSMMDQ3h4Q0NDCMN4uIMIw0MDOLiDAwNIwwAAAABAAAAAQAAa5n0y18PPPUACwQAAAAAANivOVsAAAAA2K85WwAAAAADqwNVAAAACAACAAAAAAAAAAEAAAPA/8AAAAQAAAAAAAOrAAEAAAAAAAAAAAAAAAAAAAALBAAAAAAAAAAAAAAAAgAAAAQAAWIEAAFiBAAA4gQAAOIEAABVBAAAVQQAAOIAAAAAAAoAFAAeAEQAagCqAOoBngJkApoAAQAAAAsAigADAAAAAAACAAAAAAAAAAAAAAAAAAAAAAAAAA4ArgABAAAAAAABAAcAAAABAAAAAAACAAcAYAABAAAAAAADAAcANgABAAAAAAAEAAcAdQABAAAAAAAFAAsAFQABAAAAAAAGAAcASwABAAAAAAAKABoAigADAAEECQABAA4ABwADAAEECQACAA4AZwADAAEECQADAA4APQADAAEECQAEAA4AfAADAAEECQAFABYAIAADAAEECQAGAA4AUgADAAEECQAKADQApGZjaWNvbnMAZgBjAGkAYwBvAG4Ac1ZlcnNpb24gMS4wAFYAZQByAHMAaQBvAG4AIAAxAC4AMGZjaWNvbnMAZgBjAGkAYwBvAG4Ac2ZjaWNvbnMAZgBjAGkAYwBvAG4Ac1JlZ3VsYXIAUgBlAGcAdQBsAGEAcmZjaWNvbnMAZgBjAGkAYwBvAG4Ac0ZvbnQgZ2VuZXJhdGVkIGJ5IEljb01vb24uAEYAbwBuAHQAIABnAGUAbgBlAHIAYQB0AGUAZAAgAGIAeQAgAEkAYwBvAE0AbwBvAG4ALgAAAAMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=") format('truetype');
            font-weight: normal;
            font-style: normal;
        }

        .fc-icon {
            /* added for fc */
            display: inline-block;
            width: 1em;
            height: 1em;
            text-align: center;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;

            /* use !important to prevent issues with browser extensions that change fonts */
            font-family: 'fcicons' !important;
            speak: none;
            font-style: normal;
            font-weight: normal;
            font-variant: normal;
            text-transform: none;
            line-height: 1;

            /* Better Font Rendering =========== */
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
        }

        .fc-icon-chevron-left:before {
            content: "\E900";
        }

        .fc-icon-chevron-right:before {
            content: "\E901";
        }

        .fc-icon-chevrons-left:before {
            content: "\E902";
        }

        .fc-icon-chevrons-right:before {
            content: "\E903";
        }

        .fc-icon-minus-square:before {
            content: "\E904";
        }

        .fc-icon-plus-square:before {
            content: "\E905";
        }

        .fc-icon-x:before {
            content: "\E906";
        }

        /*
Lots taken from Flatly (MIT): https://bootswatch.com/4/flatly/bootstrap.css

These styles only apply when the standard-theme is activated.
When it's NOT activated, the fc-button classes won't even be in the DOM.
*/
        .fc {

            /* reset */

        }

        .fc .fc-button {
            border-radius: 0;
            overflow: visible;
            text-transform: none;
            margin: 0;
            font-family: inherit;
            font-size: inherit;
            line-height: inherit;
        }

        .fc .fc-button:focus {
            outline: 1px dotted;
            outline: 5px auto -webkit-focus-ring-color;
        }

        .fc .fc-button {
            -webkit-appearance: button;
        }

        .fc .fc-button:not(:disabled) {
            cursor: pointer;
        }

        .fc .fc-button::-moz-focus-inner {
            padding: 0;
            border-style: none;
        }

        .fc {

            /* theme */

        }

        .fc .fc-button {
            display: inline-block;
            font-weight: 400;
            text-align: center;
            vertical-align: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-color: transparent;
            border: 1px solid transparent;
            padding: 0.4em 0.65em;
            font-size: 1em;
            line-height: 1.5;
            border-radius: 0.25em;
        }

        .fc .fc-button:hover {
            text-decoration: none;
        }

        .fc .fc-button:focus {
            outline: 0;
            box-shadow: 0 0 0 0.2rem rgba(44, 62, 80, 0.25);
        }

        .fc .fc-button:disabled {
            opacity: 0.65;
        }

        .fc {

            /* "primary" coloring */

        }

        .fc .fc-button-primary {
            color: #fff;
            color: var(--fc-button-text-color, #fff);
            background-color: #2C3E50;
            background-color: var(--fc-button-bg-color, #2C3E50);
            border-color: #2C3E50;
            border-color: var(--fc-button-border-color, #2C3E50);
        }

        .fc .fc-button-primary:hover {
            color: #fff;
            color: var(--fc-button-text-color, #fff);
            background-color: #1e2b37;
            background-color: var(--fc-button-hover-bg-color, #1e2b37);
            border-color: #1a252f;
            border-color: var(--fc-button-hover-border-color, #1a252f);
        }

        .fc .fc-button-primary:disabled {
            /* not DRY */
            color: #fff;
            color: var(--fc-button-text-color, #fff);
            background-color: #2C3E50;
            background-color: var(--fc-button-bg-color, #2C3E50);
            border-color: #2C3E50;
            border-color: var(--fc-button-border-color, #2C3E50);
            /* overrides :hover */
        }

        .fc .fc-button-primary:focus {
            box-shadow: 0 0 0 0.2rem rgba(76, 91, 106, 0.5);
        }

        .fc .fc-button-primary:not(:disabled):active,
        .fc .fc-button-primary:not(:disabled).fc-button-active {
            color: #fff;
            color: var(--fc-button-text-color, #fff);
            background-color: #1a252f;
            background-color: var(--fc-button-active-bg-color, #1a252f);
            border-color: #151e27;
            border-color: var(--fc-button-active-border-color, #151e27);
        }

        .fc .fc-button-primary:not(:disabled):active:focus,
        .fc .fc-button-primary:not(:disabled).fc-button-active:focus {
            box-shadow: 0 0 0 0.2rem rgba(76, 91, 106, 0.5);
        }

        .fc {

            /* icons within buttons */

        }

        .fc .fc-button .fc-icon {
            vertical-align: middle;
            font-size: 1.5em;
            /* bump up the size (but don't make it bigger than line-height of button, which is 1.5em also) */
        }

        .fc .fc-button-group {
            position: relative;
            display: inline-flex;
            vertical-align: middle;
        }

        .fc .fc-button-group>.fc-button {
            position: relative;
            flex: 1 1 auto;
        }

        .fc .fc-button-group>.fc-button:hover {
            z-index: 1;
        }

        .fc .fc-button-group>.fc-button:focus,
        .fc .fc-button-group>.fc-button:active,
        .fc .fc-button-group>.fc-button.fc-button-active {
            z-index: 1;
        }

        .fc-direction-ltr .fc-button-group>.fc-button:not(:first-child) {
            margin-left: -1px;
            border-top-left-radius: 0;
            border-bottom-left-radius: 0;
        }

        .fc-direction-ltr .fc-button-group>.fc-button:not(:last-child) {
            border-top-right-radius: 0;
            border-bottom-right-radius: 0;
        }

        .fc-direction-rtl .fc-button-group>.fc-button:not(:first-child) {
            margin-right: -1px;
            border-top-right-radius: 0;
            border-bottom-right-radius: 0;
        }

        .fc-direction-rtl .fc-button-group>.fc-button:not(:last-child) {
            border-top-left-radius: 0;
            border-bottom-left-radius: 0;
        }

        .fc .fc-toolbar {
            display: flex;
            justify-content: space-between;
            align-items: center;
        }

        .fc .fc-toolbar.fc-header-toolbar {
            margin-bottom: 1.5em;
        }

        .fc .fc-toolbar.fc-footer-toolbar {
            margin-top: 1.5em;
        }

        .fc .fc-toolbar-title {
            font-size: 1.75em;
            margin: 0;
        }

        .fc-direction-ltr .fc-toolbar>*> :not(:first-child) {
            margin-left: .75em;
            /* space between */
        }

        .fc-direction-rtl .fc-toolbar>*> :not(:first-child) {
            margin-right: .75em;
            /* space between */
        }

        .fc-direction-rtl .fc-toolbar-ltr {
            /* when the toolbar-chunk positioning system is explicitly left-to-right */
            flex-direction: row-reverse;
        }

        .fc .fc-scroller {
            -webkit-overflow-scrolling: touch;
            position: relative;
            /* for abs-positioned elements within */
        }

        .fc .fc-scroller-liquid {
            height: 100%;
        }

        .fc .fc-scroller-liquid-absolute {
            position: absolute;
            top: 0;
            right: 0;
            left: 0;
            bottom: 0;
        }

        .fc .fc-scroller-harness {
            position: relative;
            overflow: hidden;
            direction: ltr;
            /* hack for chrome computing the scroller's right/left wrong for rtl. undone below... */
            /* TODO: demonstrate in codepen */
        }

        .fc .fc-scroller-harness-liquid {
            height: 100%;
        }

        .fc-direction-rtl .fc-scroller-harness>.fc-scroller {
            /* undo above hack */
            direction: rtl;
        }

        .fc-theme-standard .fc-scrollgrid {
            border: 1px solid #ddd;
            border: 1px solid var(--fc-border-color, #ddd);
            /* bootstrap does this. match */
        }

        .fc .fc-scrollgrid,
        .fc .fc-scrollgrid table {
            /* all tables (self included) */
            width: 100%;
            /* because tables don't normally do this */
            table-layout: fixed;
        }

        .fc .fc-scrollgrid table {
            /* inner tables */
            border-top-style: hidden;
            border-left-style: hidden;
            border-right-style: hidden;
        }

        .fc .fc-scrollgrid {

            border-collapse: separate;
            border-right-width: 0;
            border-bottom-width: 0;

        }

        .fc .fc-scrollgrid-liquid {
            height: 100%;
        }

        .fc .fc-scrollgrid-section {
            /* a <tr> */
            height: 1px
                /* better than 0, for firefox */

        }

        .fc .fc-scrollgrid-section>td {
            height: 1px;
            /* needs a height so inner div within grow. better than 0, for firefox */
        }

        .fc .fc-scrollgrid-section table {
            height: 1px;
            /* for most browsers, if a height isn't set on the table, can't do liquid-height within cells */
            /* serves as a min-height. harmless */
        }

        .fc .fc-scrollgrid-section-liquid {
            height: auto
        }

        .fc .fc-scrollgrid-section-liquid>td {
            height: 100%;
            /* better than `auto`, for firefox */
        }

        .fc .fc-scrollgrid-section>* {
            border-top-width: 0;
            border-left-width: 0;
        }

        .fc .fc-scrollgrid-section-header>*,
        .fc .fc-scrollgrid-section-footer>* {
            border-bottom-width: 0;
        }

        .fc .fc-scrollgrid-section-body table,
        .fc .fc-scrollgrid-section-footer table {
            border-bottom-style: hidden;
            /* head keeps its bottom border tho */
        }

        .fc {

            /* stickiness */

        }

        .fc .fc-scrollgrid-section-sticky>* {
            background: #fff;
            background: var(--fc-page-bg-color, #fff);
            position: -webkit-sticky;
            position: sticky;
            z-index: 2;
            /* TODO: var */
            /* TODO: box-shadow when sticking */
        }

        .fc .fc-scrollgrid-section-header.fc-scrollgrid-section-sticky>* {
            top: 0;
            /* because border-sharing causes a gap at the top */
            /* TODO: give safari -1. has bug */
        }

        .fc .fc-scrollgrid-section-footer.fc-scrollgrid-section-sticky>* {
            bottom: 0;
            /* known bug: bottom-stickiness doesn't work in safari */
        }

        .fc .fc-scrollgrid-sticky-shim {
            /* for horizontal scrollbar */
            height: 1px;
            /* needs height to create scrollbars */
            margin-bottom: -1px;
        }

        .fc-sticky {
            /* no .fc wrap because used as child of body */
            position: -webkit-sticky;
            position: sticky;
        }

        .fc .fc-view-harness {
            flex-grow: 1;
            /* because this harness is WITHIN the .fc's flexbox */
            position: relative;
        }

        .fc {

            /* when the harness controls the height, make the view liquid */

        }

        .fc .fc-view-harness-active>.fc-view {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }

        .fc .fc-col-header-cell-cushion {
            display: inline-block;
            /* x-browser for when sticky (when multi-tier header) */
            padding: 2px 4px;
        }

        .fc .fc-bg-event,
        .fc .fc-non-business,
        .fc .fc-highlight {
            /* will always have a harness with position:relative/absolute, so absolutely expand */
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
        }

        .fc .fc-non-business {
            background: rgba(215, 215, 215, 0.3);
            background: var(--fc-non-business-color, rgba(215, 215, 215, 0.3));
        }

        .fc .fc-bg-event {
            background: rgb(143, 223, 130);
            background: var(--fc-bg-event-color, rgb(143, 223, 130));
            opacity: 0.3;
            opacity: var(--fc-bg-event-opacity, 0.3)
        }

        .fc .fc-bg-event .fc-event-title {
            margin: .5em;
            font-size: .85em;
            font-size: var(--fc-small-font-size, .85em);
            font-style: italic;
        }

        .fc .fc-highlight {
            background: rgba(188, 232, 241, 0.3);
            background: var(--fc-highlight-color, rgba(188, 232, 241, 0.3));
        }

        .fc .fc-cell-shaded,
        .fc .fc-day-disabled {
            background: rgba(208, 208, 208, 0.3);
            background: var(--fc-neutral-bg-color, rgba(208, 208, 208, 0.3));
        }

        /* link resets */
        /* ---------------------------------------------------------------------------------------------------- */
        a.fc-event,
        a.fc-event:hover {
            text-decoration: none;
        }

        /* cursor */
        .fc-event[href],
        .fc-event.fc-event-draggable {
            cursor: pointer;
        }

        /* event text content */
        /* ---------------------------------------------------------------------------------------------------- */
        .fc-event .fc-event-main {
            position: relative;
            z-index: 2;
        }

        /* dragging */
        /* ---------------------------------------------------------------------------------------------------- */
        .fc-event-dragging:not(.fc-event-selected) {
            /* MOUSE */
            opacity: 0.75;
        }

        .fc-event-dragging.fc-event-selected {
            /* TOUCH */
            box-shadow: 0 2px 7px rgba(0, 0, 0, 0.3);
        }

        /* resizing */
        /* ---------------------------------------------------------------------------------------------------- */
        /* (subclasses should hone positioning for touch and non-touch) */
        .fc-event .fc-event-resizer {
            display: none;
            position: absolute;
            z-index: 4;
        }

        .fc-event:hover,
        /* MOUSE */
        .fc-event-selected {
            /* TOUCH */

        }

        .fc-event:hover .fc-event-resizer,
        .fc-event-selected .fc-event-resizer {
            display: block;
        }

        .fc-event-selected .fc-event-resizer {
            border-radius: 4px;
            border-radius: calc(var(--fc-event-resizer-dot-total-width, 8px) / 2);
            border-width: 1px;
            border-width: var(--fc-event-resizer-dot-border-width, 1px);
            width: 8px;
            width: var(--fc-event-resizer-dot-total-width, 8px);
            height: 8px;
            height: var(--fc-event-resizer-dot-total-width, 8px);
            border-style: solid;
            border-color: inherit;
            background: #fff;
            background: var(--fc-page-bg-color, #fff)
                /* expand hit area */

        }

        .fc-event-selected .fc-event-resizer:before {
            content: '';
            position: absolute;
            top: -20px;
            left: -20px;
            right: -20px;
            bottom: -20px;
        }

        /* selecting (always TOUCH) */
        /* ---------------------------------------------------------------------------------------------------- */
        .fc-event-selected {
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.2)
                /* expand hit area (subclasses should expand) */

        }

        .fc-event-selected:before {
            content: "";
            position: absolute;
            z-index: 3;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
        }

        .fc-event-selected {

            /* dimmer effect */

        }

        .fc-event-selected:after {
            content: "";
            background: rgba(0, 0, 0, 0.25);
            background: var(--fc-event-selected-overlay-color, rgba(0, 0, 0, 0.25));
            position: absolute;
            z-index: 1;

            /* assume there's a border on all sides. overcome it. */
            /* sometimes there's NOT a border, in which case the dimmer will go over */
            /* an adjacent border, which looks fine. */
            top: -1px;
            left: -1px;
            right: -1px;
            bottom: -1px;
        }

        /*
A HORIZONTAL event
*/
        .fc-h-event {
            /* allowed to be top-level */
            display: block;
            border: 1px solid #3788d8;
            border: 1px solid var(--fc-event-border-color, #3788d8);
            background-color: #3788d8;
            background-color: var(--fc-event-bg-color, #3788d8)
        }

        .fc-h-event .fc-event-main {
            color: #fff;
            color: var(--fc-event-text-color, #fff);
        }

        .fc-h-event .fc-event-main-frame {
            display: flex;
            /* for make fc-event-title-container expand */
        }

        .fc-h-event .fc-event-time {
            max-width: 100%;
            /* clip overflow on this element */
            overflow: hidden;
        }

        .fc-h-event .fc-event-title-container {
            /* serves as a container for the sticky cushion */
            flex-grow: 1;
            flex-shrink: 1;
            min-width: 0;
            /* important for allowing to shrink all the way */
        }

        .fc-h-event .fc-event-title {
            display: inline-block;
            /* need this to be sticky cross-browser */
            vertical-align: top;
            /* for not messing up line-height */
            left: 0;
            /* for sticky */
            right: 0;
            /* for sticky */
            max-width: 100%;
            /* clip overflow on this element */
            overflow: hidden;
        }

        .fc-h-event.fc-event-selected:before {
            /* expand hit area */
            top: -10px;
            bottom: -10px;
        }

        /* adjust border and border-radius (if there is any) for non-start/end */
        .fc-direction-ltr .fc-daygrid-block-event:not(.fc-event-start),
        .fc-direction-rtl .fc-daygrid-block-event:not(.fc-event-end) {
            border-top-left-radius: 0;
            border-bottom-left-radius: 0;
            border-left-width: 0;
        }

        .fc-direction-ltr .fc-daygrid-block-event:not(.fc-event-end),
        .fc-direction-rtl .fc-daygrid-block-event:not(.fc-event-start) {
            border-top-right-radius: 0;
            border-bottom-right-radius: 0;
            border-right-width: 0;
        }

        /* resizers */
        .fc-h-event:not(.fc-event-selected) .fc-event-resizer {
            top: 0;
            bottom: 0;
            width: 8px;
            width: var(--fc-event-resizer-thickness, 8px);
        }

        .fc-direction-ltr .fc-h-event:not(.fc-event-selected) .fc-event-resizer-start,
        .fc-direction-rtl .fc-h-event:not(.fc-event-selected) .fc-event-resizer-end {
            cursor: w-resize;
            left: -4px;
            left: calc(var(--fc-event-resizer-thickness, 8px) / -2);
        }

        .fc-direction-ltr .fc-h-event:not(.fc-event-selected) .fc-event-resizer-end,
        .fc-direction-rtl .fc-h-event:not(.fc-event-selected) .fc-event-resizer-start {
            cursor: e-resize;
            right: -4px;
            right: calc(var(--fc-event-resizer-thickness, 8px) / -2);
        }

        /* resizers for TOUCH */
        .fc-h-event.fc-event-selected .fc-event-resizer {
            top: 50%;
            margin-top: -4px;
            margin-top: calc(var(--fc-event-resizer-dot-total-width, 8px) / -2);
        }

        .fc-direction-ltr .fc-h-event.fc-event-selected .fc-event-resizer-start,
        .fc-direction-rtl .fc-h-event.fc-event-selected .fc-event-resizer-end {
            left: -4px;
            left: calc(var(--fc-event-resizer-dot-total-width, 8px) / -2);
        }

        .fc-direction-ltr .fc-h-event.fc-event-selected .fc-event-resizer-end,
        .fc-direction-rtl .fc-h-event.fc-event-selected .fc-event-resizer-start {
            right: -4px;
            right: calc(var(--fc-event-resizer-dot-total-width, 8px) / -2);
        }
    </style>
    <style type="text/css">
        :root {
            --fc-daygrid-event-dot-width: 8px;
        }

        .fc .fc-popover {
            position: fixed;
            top: 0;
            /* for when not positioned yet */
            box-shadow: 0 2px 6px rgba(0, 0, 0, .15);
        }

        .fc .fc-popover-header {
            display: flex;
            flex-direction: row;
            justify-content: space-between;
            align-items: center;
            padding: 3px 4px;
        }

        .fc .fc-popover-title {
            margin: 0 2px;
        }

        .fc .fc-popover-close {
            cursor: pointer;
            opacity: 0.65;
            font-size: 1.1em;
        }

        .fc-theme-standard .fc-popover {
            border: 1px solid #ddd;
            border: 1px solid var(--fc-border-color, #ddd);
            background: #fff;
            background: var(--fc-page-bg-color, #fff);
        }

        .fc-theme-standard .fc-popover-header {
            background: rgba(208, 208, 208, 0.3);
            background: var(--fc-neutral-bg-color, rgba(208, 208, 208, 0.3));
        }

        /* help things clear margins of inner content */
        .fc-daygrid-day-frame,
        .fc-daygrid-day-events,
        .fc-daygrid-event-harness {
            /* for event top/bottom margins */
        }

        .fc-daygrid-day-frame:before,
        .fc-daygrid-day-events:before,
        .fc-daygrid-event-harness:before {
            content: "";
            clear: both;
            display: table;
        }

        .fc-daygrid-day-frame:after,
        .fc-daygrid-day-events:after,
        .fc-daygrid-event-harness:after {
            content: "";
            clear: both;
            display: table;
        }

        .fc .fc-daygrid-body {
            /* a <div> that wraps the table */
            position: relative;
            z-index: 1;
            /* container inner z-index's because <tr>s can't do it */
        }

        .fc .fc-daygrid-day.fc-day-today {
            background-color: rgba(255, 220, 40, 0.15);
            background-color: var(--fc-today-bg-color, rgba(255, 220, 40, 0.15));
        }

        .fc .fc-daygrid-day-frame {
            position: relative;
            min-height: 100%;
            /* seems to work better than `height` because sets height after rows/cells naturally do it */
        }

        .fc {

            /* cell top */

        }

        .fc .fc-daygrid-day-top {
            display: flex;
            flex-direction: row-reverse;
        }

        .fc .fc-day-other .fc-daygrid-day-top {
            opacity: 0.3;
        }

        .fc {

            /* day number (within cell top) */

        }

        .fc .fc-daygrid-day-number {
            position: relative;
            z-index: 4;
            padding: 4px;
        }

        .fc {

            /* event container */

        }

        .fc .fc-daygrid-day-events {
            margin-top: 1px;
            /* needs to be margin, not padding, so that available cell height can be computed */
        }

        .fc {

            /* positioning for balanced vs natural */

        }

        .fc .fc-daygrid-body-balanced .fc-daygrid-day-events {
            position: absolute;
            left: 0;
            right: 0;
        }

        .fc .fc-daygrid-body-unbalanced .fc-daygrid-day-events {
            position: relative;
            /* for containing abs positioned event harnesses */
            min-height: 2em;
            /* in addition to being a min-height during natural height, equalizes the heights a little bit */
        }

        .fc .fc-daygrid-body-natural {
            /* can coexist with -unbalanced */
        }

        .fc .fc-daygrid-body-natural .fc-daygrid-day-events {
            margin-bottom: 1em;
        }

        .fc {

            /* event harness */

        }

        .fc .fc-daygrid-event-harness {
            position: relative;
        }

        .fc .fc-daygrid-event-harness-abs {
            position: absolute;
            top: 0;
            /* fallback coords for when cannot yet be computed */
            left: 0;
            /* */
            right: 0;
            /* */
        }

        .fc .fc-daygrid-bg-harness {
            position: absolute;
            top: 0;
            bottom: 0;
        }

        .fc {

            /* bg content */

        }

        .fc .fc-daygrid-day-bg .fc-non-business {
            z-index: 1
        }

        .fc .fc-daygrid-day-bg .fc-bg-event {
            z-index: 2
        }

        .fc .fc-daygrid-day-bg .fc-highlight {
            z-index: 3
        }

        .fc {

            /* events */

        }

        .fc .fc-daygrid-event {
            z-index: 6;
            margin-top: 1px;
        }

        .fc .fc-daygrid-event.fc-event-mirror {
            z-index: 7;
        }

        .fc {

            /* cell bottom (within day-events) */

        }

        .fc .fc-daygrid-day-bottom {
            font-size: .85em;
            margin: 2px 3px 0;
        }

        .fc .fc-daygrid-more-link {
            position: relative;
            z-index: 4;
            cursor: pointer;
        }

        .fc {

            /* week number (within frame) */

        }

        .fc .fc-daygrid-week-number {
            position: absolute;
            z-index: 5;
            top: 0;
            padding: 2px;
            min-width: 1.5em;
            text-align: center;
            background-color: rgba(208, 208, 208, 0.3);
            background-color: var(--fc-neutral-bg-color, rgba(208, 208, 208, 0.3));
            color: #808080;
            color: var(--fc-neutral-text-color, #808080);
        }

        .fc {

            /* popover */

        }

        .fc .fc-more-popover {
            z-index: 8;
        }

        .fc .fc-more-popover .fc-popover-body {
            min-width: 220px;
            padding: 10px;
        }

        .fc-direction-ltr .fc-daygrid-event.fc-event-start,
        .fc-direction-rtl .fc-daygrid-event.fc-event-end {
            margin-left: 2px;
        }

        .fc-direction-ltr .fc-daygrid-event.fc-event-end,
        .fc-direction-rtl .fc-daygrid-event.fc-event-start {
            margin-right: 2px;
        }

        .fc-direction-ltr .fc-daygrid-week-number {
            left: 0;
            border-radius: 0 0 3px 0;
        }

        .fc-direction-rtl .fc-daygrid-week-number {
            right: 0;
            border-radius: 0 0 0 3px;
        }

        .fc-liquid-hack .fc-daygrid-day-frame {
            position: static;
            /* will cause inner absolute stuff to expand to <td> */
        }

        .fc-daygrid-event {
            /* make root-level, because will be dragged-and-dropped outside of a component root */
            position: relative;
            /* for z-indexes assigned later */
            white-space: nowrap;
            border-radius: 3px;
            /* dot event needs this to when selected */
            font-size: .85em;
            font-size: var(--fc-small-font-size, .85em);
        }

        /* --- the rectangle ("block") style of event --- */
        .fc-daygrid-block-event .fc-event-time {
            font-weight: bold;
        }

        .fc-daygrid-block-event .fc-event-time,
        .fc-daygrid-block-event .fc-event-title {
            padding: 1px;
        }

        /* --- the dot style of event --- */
        .fc-daygrid-dot-event {
            display: flex;
            align-items: center;
            padding: 2px 0
        }

        .fc-daygrid-dot-event .fc-event-title {
            flex-grow: 1;
            flex-shrink: 1;
            min-width: 0;
            /* important for allowing to shrink all the way */
            overflow: hidden;
            font-weight: bold;
        }

        .fc-daygrid-dot-event:hover,
        .fc-daygrid-dot-event.fc-event-mirror {
            background: rgba(0, 0, 0, 0.1);
        }

        .fc-daygrid-dot-event.fc-event-selected:before {
            /* expand hit area */
            top: -10px;
            bottom: -10px;
        }

        .fc-daygrid-event-dot {
            /* the actual dot */
            margin: 0 4px;
            box-sizing: content-box;
            width: 0;
            height: 0;
            border: 4px solid #3788d8;
            border: calc(var(--fc-daygrid-event-dot-width, 8px) / 2) solid var(--fc-event-border-color, #3788d8);
            border-radius: 4px;
            border-radius: calc(var(--fc-daygrid-event-dot-width, 8px) / 2);
        }

        /* --- spacing between time and title --- */
        .fc-direction-ltr .fc-daygrid-event .fc-event-time {
            margin-right: 3px;
        }

        .fc-direction-rtl .fc-daygrid-event .fc-event-time {
            margin-left: 3px;
        }
    </style>
    <style type="text/css">
        /*
A VERTICAL event
*/

        .fc-v-event {
            /* allowed to be top-level */
            display: block;
            border: 1px solid #3788d8;
            border: 1px solid var(--fc-event-border-color, #3788d8);
            background-color: #3788d8;
            background-color: var(--fc-event-bg-color, #3788d8)
        }

        .fc-v-event .fc-event-main {
            color: #fff;
            color: var(--fc-event-text-color, #fff);
            height: 100%;
        }

        .fc-v-event .fc-event-main-frame {
            height: 100%;
            display: flex;
            flex-direction: column;
        }

        .fc-v-event .fc-event-time {
            flex-grow: 0;
            flex-shrink: 0;
            max-height: 100%;
            overflow: hidden;
        }

        .fc-v-event .fc-event-title-container {
            /* a container for the sticky cushion */
            flex-grow: 1;
            flex-shrink: 1;
            min-height: 0;
            /* important for allowing to shrink all the way */
        }

        .fc-v-event .fc-event-title {
            /* will have fc-sticky on it */
            top: 0;
            bottom: 0;
            max-height: 100%;
            /* clip overflow */
            overflow: hidden;
        }

        .fc-v-event:not(.fc-event-start) {
            border-top-width: 0;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }

        .fc-v-event:not(.fc-event-end) {
            border-bottom-width: 0;
            border-bottom-left-radius: 0;
            border-bottom-right-radius: 0;
        }

        .fc-v-event.fc-event-selected:before {
            /* expand hit area */
            left: -10px;
            right: -10px;
        }

        .fc-v-event {

            /* resizer (mouse AND touch) */

        }

        .fc-v-event .fc-event-resizer-start {
            cursor: n-resize;
        }

        .fc-v-event .fc-event-resizer-end {
            cursor: s-resize;
        }

        .fc-v-event {

            /* resizer for MOUSE */

        }

        .fc-v-event:not(.fc-event-selected) .fc-event-resizer {
            height: 8px;
            height: var(--fc-event-resizer-thickness, 8px);
            left: 0;
            right: 0;
        }

        .fc-v-event:not(.fc-event-selected) .fc-event-resizer-start {
            top: -4px;
            top: calc(var(--fc-event-resizer-thickness, 8px) / -2);
        }

        .fc-v-event:not(.fc-event-selected) .fc-event-resizer-end {
            bottom: -4px;
            bottom: calc(var(--fc-event-resizer-thickness, 8px) / -2);
        }

        .fc-v-event {

            /* resizer for TOUCH (when event is "selected") */

        }

        .fc-v-event.fc-event-selected .fc-event-resizer {
            left: 50%;
            margin-left: -4px;
            margin-left: calc(var(--fc-event-resizer-dot-total-width, 8px) / -2);
        }

        .fc-v-event.fc-event-selected .fc-event-resizer-start {
            top: -4px;
            top: calc(var(--fc-event-resizer-dot-total-width, 8px) / -2);
        }

        .fc-v-event.fc-event-selected .fc-event-resizer-end {
            bottom: -4px;
            bottom: calc(var(--fc-event-resizer-dot-total-width, 8px) / -2);
        }

        .fc .fc-timegrid .fc-daygrid-body {
            /* the all-day daygrid within the timegrid view */
            z-index: 2;
            /* put above the timegrid-body so that more-popover is above everything. TODO: better solution */
        }

        .fc .fc-timegrid-divider {
            padding: 0 0 2px;
            /* browsers get confused when you set height. use padding instead */
        }

        .fc .fc-timegrid-body {
            position: relative;
            z-index: 1;
            /* scope the z-indexes of slots and cols */
            min-height: 100%;
            /* fill height always, even when slat table doesn't grow */
        }

        .fc .fc-timegrid-axis-chunk {
            /* for advanced ScrollGrid */
            position: relative
                /* offset parent for now-indicator-container */

        }

        .fc .fc-timegrid-axis-chunk>table {
            position: relative;
            z-index: 1;
            /* above the now-indicator-container */
        }

        .fc .fc-timegrid-slots {
            position: relative;
            z-index: 1;
        }

        .fc .fc-timegrid-slot {
            /* a <td> */
            height: 1.5em;
            border-bottom: 0
                /* each cell owns its top border */
        }

        .fc .fc-timegrid-slot:empty:before {
            content: '\A0';
            /* make sure there's at least an empty space to create height for height syncing */
        }

        .fc .fc-timegrid-slot-minor {
            border-top-style: dotted;
        }

        .fc .fc-timegrid-slot-label-cushion {
            display: inline-block;
            white-space: nowrap;
        }

        .fc .fc-timegrid-slot-label {
            vertical-align: middle;
            /* vertical align the slots */
        }

        .fc {


            /* slots AND axis cells (top-left corner of view including the "all-day" text) */

        }

        .fc .fc-timegrid-axis-cushion,
        .fc .fc-timegrid-slot-label-cushion {
            padding: 0 4px;
        }

        .fc {


            /* axis cells (top-left corner of view including the "all-day" text) */
            /* vertical align is more complicated, uses flexbox */

        }

        .fc .fc-timegrid-axis-frame-liquid {
            height: 100%;
            /* will need liquid-hack in FF */
        }

        .fc .fc-timegrid-axis-frame {
            overflow: hidden;
            display: flex;
            align-items: center;
            /* vertical align */
            justify-content: flex-end;
            /* horizontal align. matches text-align below */
        }

        .fc .fc-timegrid-axis-cushion {
            max-width: 60px;
            /* limits the width of the "all-day" text */
            flex-shrink: 0;
            /* allows text to expand how it normally would, regardless of constrained width */
        }

        .fc-direction-ltr .fc-timegrid-slot-label-frame {
            text-align: right;
        }

        .fc-direction-rtl .fc-timegrid-slot-label-frame {
            text-align: left;
        }

        .fc-liquid-hack .fc-timegrid-axis-frame-liquid {
            height: auto;
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }

        .fc .fc-timegrid-col.fc-day-today {
            background-color: rgba(255, 220, 40, 0.15);
            background-color: var(--fc-today-bg-color, rgba(255, 220, 40, 0.15));
        }

        .fc .fc-timegrid-col-frame {
            min-height: 100%;
            /* liquid-hack is below */
            position: relative;
        }

        .fc-liquid-hack .fc-timegrid-col-frame {
            height: auto;
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }

        .fc-media-screen .fc-timegrid-cols {
            position: absolute;
            /* no z-index. children will decide and go above slots */
            top: 0;
            left: 0;
            right: 0;
            bottom: 0
        }

        .fc-media-screen .fc-timegrid-cols>table {
            height: 100%;
        }

        .fc-media-screen .fc-timegrid-col-bg,
        .fc-media-screen .fc-timegrid-col-events,
        .fc-media-screen .fc-timegrid-now-indicator-container {
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
        }

        .fc-media-screen .fc-timegrid-event-harness {
            position: absolute;
            /* top/left/right/bottom will all be set by JS */
        }

        .fc {

            /* bg */

        }

        .fc .fc-timegrid-col-bg {
            z-index: 2;
            /* TODO: kill */
        }

        .fc .fc-timegrid-col-bg .fc-non-business {
            z-index: 1
        }

        .fc .fc-timegrid-col-bg .fc-bg-event {
            z-index: 2
        }

        .fc .fc-timegrid-col-bg .fc-highlight {
            z-index: 3
        }

        .fc .fc-timegrid-bg-harness {
            position: absolute;
            /* top/bottom will be set by JS */
            left: 0;
            right: 0;
        }

        .fc {

            /* fg events */
            /* (the mirror segs are put into a separate container with same classname, */
            /* and they must be after the normal seg container to appear at a higher z-index) */

        }

        .fc .fc-timegrid-col-events {
            z-index: 3;
            /* child event segs have z-indexes that are scoped within this div */
        }

        .fc {

            /* now indicator */

        }

        .fc .fc-timegrid-now-indicator-container {
            bottom: 0;
            overflow: hidden;
            /* don't let overflow of lines/arrows cause unnecessary scrolling */
            /* z-index is set on the individual elements */
        }

        .fc-direction-ltr .fc-timegrid-col-events {
            margin: 0 2.5% 0 2px;
        }

        .fc-direction-rtl .fc-timegrid-col-events {
            margin: 0 2px 0 2.5%;
        }

        .fc-timegrid-event-harness-inset .fc-timegrid-event,
        .fc-timegrid-event.fc-event-mirror {
            box-shadow: 0px 0px 0px 1px #fff;
            box-shadow: 0px 0px 0px 1px var(--fc-page-bg-color, #fff);
        }

        .fc-timegrid-event {
            /* events need to be root */

            font-size: .85em;

            font-size: var(--fc-small-font-size, .85em);
            border-radius: 3px
        }

        .fc-timegrid-event .fc-event-main {
            padding: 1px 1px 0;
        }

        .fc-timegrid-event .fc-event-time {
            white-space: nowrap;
            font-size: .85em;
            font-size: var(--fc-small-font-size, .85em);
            margin-bottom: 1px;
        }

        .fc-timegrid-event-condensed .fc-event-main-frame {
            flex-direction: row;
            overflow: hidden;
        }

        .fc-timegrid-event-condensed .fc-event-time:after {
            content: '\A0-\A0';
            /* dash surrounded by non-breaking spaces */
        }

        .fc-timegrid-event-condensed .fc-event-title {
            font-size: .85em;
            font-size: var(--fc-small-font-size, .85em)
        }

        .fc-media-screen .fc-timegrid-event {
            position: absolute;
            /* absolute WITHIN the harness */
            top: 0;
            bottom: 1px;
            /* stay away from bottom slot line */
            left: 0;
            right: 0;
        }

        .fc {

            /* line */

        }

        .fc .fc-timegrid-now-indicator-line {
            position: absolute;
            z-index: 4;
            left: 0;
            right: 0;
            border-style: solid;
            border-color: red;
            border-color: var(--fc-now-indicator-color, red);
            border-width: 1px 0 0;
        }

        .fc {

            /* arrow */

        }

        .fc .fc-timegrid-now-indicator-arrow {
            position: absolute;
            z-index: 4;
            margin-top: -5px;
            /* vertically center on top coordinate */
            border-style: solid;
            border-color: red;
            border-color: var(--fc-now-indicator-color, red);
        }

        .fc-direction-ltr .fc-timegrid-now-indicator-arrow {
            left: 0;

            /* triangle pointing right. TODO: mixin */
            border-width: 5px 0 5px 6px;
            border-top-color: transparent;
            border-bottom-color: transparent;
        }

        .fc-direction-rtl .fc-timegrid-now-indicator-arrow {
            right: 0;

            /* triangle pointing left. TODO: mixin */
            border-width: 5px 6px 5px 0;
            border-top-color: transparent;
            border-bottom-color: transparent;
        }
    </style>
    <style type="text/css">
        :root {
            --fc-list-event-dot-width: 10px;
            --fc-list-event-hover-bg-color: #f5f5f5;
        }

        .fc-theme-standard .fc-list {
            border: 1px solid #ddd;
            border: 1px solid var(--fc-border-color, #ddd);
        }

        .fc {

            /* message when no events */

        }

        .fc .fc-list-empty {
            background-color: rgba(208, 208, 208, 0.3);
            background-color: var(--fc-neutral-bg-color, rgba(208, 208, 208, 0.3));
            height: 100%;
            display: flex;
            justify-content: center;
            align-items: center;
            /* vertically aligns fc-list-empty-inner */
        }

        .fc .fc-list-empty-cushion {
            margin: 5em 0;
        }

        .fc {

            /* table within the scroller */
            /* ---------------------------------------------------------------------------------------------------- */

        }

        .fc .fc-list-table {
            width: 100%;
            border-style: hidden;
            /* kill outer border on theme */
        }

        .fc .fc-list-table tr>* {
            border-left: 0;
            border-right: 0;
        }

        .fc .fc-list-sticky .fc-list-day>* {
            /* the cells */
            position: -webkit-sticky;
            position: sticky;
            top: 0;
            background: #fff;
            background: var(--fc-page-bg-color, #fff);
            /* for when headers are styled to be transparent and sticky */
        }

        .fc .fc-list-table th {
            padding: 0;
            /* uses an inner-wrapper instead... */
        }

        .fc .fc-list-table td,
        .fc .fc-list-day-cushion {
            padding: 8px 14px;
        }

        .fc {


            /* date heading rows */
            /* ---------------------------------------------------------------------------------------------------- */

        }

        .fc .fc-list-day-cushion:after {
            content: "";
            clear: both;
            display: table;
            /* clear floating */
        }

        .fc-theme-standard .fc-list-day-cushion {
            background-color: rgba(208, 208, 208, 0.3);
            background-color: var(--fc-neutral-bg-color, rgba(208, 208, 208, 0.3));
        }

        .fc-direction-ltr .fc-list-day-text,
        .fc-direction-rtl .fc-list-day-side-text {
            float: left;
        }

        .fc-direction-ltr .fc-list-day-side-text,
        .fc-direction-rtl .fc-list-day-text {
            float: right;
        }

        /* make the dot closer to the event title */
        .fc-direction-ltr .fc-list-table .fc-list-event-graphic {
            padding-right: 0
        }

        .fc-direction-rtl .fc-list-table .fc-list-event-graphic {
            padding-left: 0
        }

        .fc .fc-list-event.fc-event-forced-url {
            cursor: pointer;
            /* whole row will seem clickable */
        }

        .fc .fc-list-event:hover td {
            background-color: #f5f5f5;
            background-color: var(--fc-list-event-hover-bg-color, #f5f5f5);
        }

        .fc {

            /* shrink certain cols */

        }

        .fc .fc-list-event-graphic,
        .fc .fc-list-event-time {
            white-space: nowrap;
            width: 1px;
        }

        .fc .fc-list-event-dot {
            display: inline-block;
            box-sizing: content-box;
            width: 0;
            height: 0;
            border: 5px solid #3788d8;
            border: calc(var(--fc-list-event-dot-width, 10px) / 2) solid var(--fc-event-border-color, #3788d8);
            border-radius: 5px;
            border-radius: calc(var(--fc-list-event-dot-width, 10px) / 2);
        }

        .fc {

            /* reset <a> styling */

        }

        .fc .fc-list-event-title a {
            color: inherit;
            text-decoration: none;
        }

        .fc {

            /* underline link when hovering over any part of row */

        }

        .fc .fc-list-event.fc-event-forced-url:hover a {
            text-decoration: underline;
        }
    </style>
    <style type="text/css">
        .tippy-box[data-animation=fade][data-state=hidden] {
            opacity: 0
        }

        [data-tippy-root] {
            max-width: calc(100vw - 10px)
        }

        .tippy-box {
            position: relative;
            background-color: #333;
            color: #fff;
            border-radius: 4px;
            font-size: 14px;
            line-height: 1.4;
            outline: 0;
            transition-property: transform, visibility, opacity
        }

        .tippy-box[data-placement^=top]>.tippy-arrow {
            bottom: 0
        }

        .tippy-box[data-placement^=top]>.tippy-arrow:before {
            bottom: -7px;
            left: 0;
            border-width: 8px 8px 0;
            border-top-color: initial;
            transform-origin: center top
        }

        .tippy-box[data-placement^=bottom]>.tippy-arrow {
            top: 0
        }

        .tippy-box[data-placement^=bottom]>.tippy-arrow:before {
            top: -7px;
            left: 0;
            border-width: 0 8px 8px;
            border-bottom-color: initial;
            transform-origin: center bottom
        }

        .tippy-box[data-placement^=left]>.tippy-arrow {
            right: 0
        }

        .tippy-box[data-placement^=left]>.tippy-arrow:before {
            border-width: 8px 0 8px 8px;
            border-left-color: initial;
            right: -7px;
            transform-origin: center left
        }

        .tippy-box[data-placement^=right]>.tippy-arrow {
            left: 0
        }

        .tippy-box[data-placement^=right]>.tippy-arrow:before {
            left: -7px;
            border-width: 8px 8px 8px 0;
            border-right-color: initial;
            transform-origin: center right
        }

        .tippy-box[data-inertia][data-state=visible] {
            transition-timing-function: cubic-bezier(.54, 1.5, .38, 1.11)
        }

        .tippy-arrow {
            width: 16px;
            height: 16px;
            color: #333
        }

        .tippy-arrow:before {
            content: "";
            position: absolute;
            border-color: transparent;
            border-style: solid
        }

        .tippy-content {
            position: relative;
            padding: 5px 9px;
            z-index: 1
        }
    </style>
</head>

<body class="container-fluid p-0">
    <div id="app"><span class="bg-warning text-dark p-1 pl-3 pr-3 position-absolute demo-indicator"><span class="mx-auto">
                Demo Mode
            </span></span>
        <nav class="navbar navbar-expand-lg top-nav p-2">
            <div class="container-fluid"><a href="/" class="navbar-brand"><img src="/images/logo_small.png" alt="appsthing" class="d-inline-block align-top top-nav-logo  ml-lg-1 ml-sm-4"></a>
                <!----> <button type="button" data-toggle="collapse" data-target="#small_menu_toogler" aria-controls="small_menu_toogler" aria-expanded="false" aria-label="Toggle actions" class="navbar-toggler dropdown-toggle"><img src="<?= base_url() ?>assets/thing/images/profile_default.jpg" alt="" class="d-inline-block rounded-circle mr-2 top-nav-profile"></button>
                <div id="small_menu_toogler" class="collapse navbar-collapse">
                    <ul class="navbar-nav mt-lg-0 ml-md-5 pt-1">
                        <li class="nav-item text-right">
                            <div class="dropdown"><a href="#" id="user_store_dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link nav-link-dark dropdown-toggle text-bold"><span class="text-thin">Store</span> STORE1 - Appsthing Store 1
                                </a>
                                <div aria-labelledby="user_store_dropdown" class="dropdown-menu dropdown-menu-left store-selector-dropdown"><small class="dropdown-header">CHOOSE FROM 3 STORE(S)</small> <a href="#" value="1zRPg6txi9xD5PA02zJQNFIJv" class="dropdown-item mb-2 border-left border-primary bg-light"><span class="mr-2 text-primary"><i class="fas fa-check-circle"></i></span> <span class="text-bold">STORE1</span> - Appsthing Store 1 <br> <span class="text-muted">883 Mueller Road Suite 468
                                            New Itzel, DC 36148</span></a><a href="#" value="4PBJvWzr9cDLYW7Jj5Rua8O7I" class="dropdown-item mb-2">
                                        <!----> <span class="text-bold">STORE2</span> - Appsthing Store 2 <br> <span class="text-muted">753 Ruecker Loop Suite 845
                                            West Masonchester, NC 64911-4719</span>
                                    </a><a href="#" value="3XRjYn18fzkMEiNdJ4nshFKnC" class="dropdown-item mb-2">
                                        <!----> <span class="text-bold">STORE3</span> - Appsthing Store 3 <br> <span class="text-muted">728 Runolfsson Wall Apt. 356
                                            Thelmafort, FL 99764</span>
                                    </a></div>
                            </div>
                        </li>
                    </ul>
                    <ul class="navbar-nav ml-auto mt-lg-0 pt-1">
                        <li class="nav-item text-right pl-md-4 pl-lg-4 pl-xl-4">
                            <div class="dropdown"><a href="#" id="quicklink_dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link quick-link dropdown-toggle text-bold"><i class="fas fa-bolt"></i></a>
                                <div aria-labelledby="quicklink_dropdown" class="dropdown-menu dropdown-menu-right"><a href="http://pos.appsthing.com/add_order" class="dropdown-item">New Order</a><a href="http://pos.appsthing.com/add_transaction" class="dropdown-item">New Transaction</a><a href="http://pos.appsthing.com/add_invoice" class="dropdown-item">New Invoice</a><a href="http://pos.appsthing.com/add_booking" class="dropdown-item">New Booking</a></div>
                            </div>
                        </li>
                        <li class="nav-item text-right pl-md-4 pl-lg-4 pl-xl-4"><a href="/search" class="nav-link nav-link-dark text-bold"><i class="fas fa-search search-icon"></i> Search</a></li>
                        <li class="nav-item text-right pl-md-4 pl-lg-4 pl-xl-4"><a href="http://pos.appsthing.com/add_order" class="nav-link nav-link-dark text-bold">+ New Order</a></li>
                        <li class="nav-item text-right pl-md-4 pl-lg-4 pl-xl-4">
                            <div class="dropdown">
                                <a href="#" id="notification_dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link nav-link-dark dropdown-toggle text-bold">
                                    
                                <h6>Icon<span class="badge badge-danger">30</span></h6>
                                   
                                </a>
                                <div aria-labelledby="notification_dropdown" class="dropdown-menu dropdown-menu-right notification-dropdown pt-3 pb-3">
                                    <!----> <span class="dropdown-item-text">No Notifications</span>
                                    <div class="d-flex justify-content-between pl-4 pr-4"><span class="text-centered btn-label mt-2" style="display: none;">
                                            <!---->
                                            Load More
                                        </span> <span class="text-centered btn-label mt-2" style="display: none;">
                                            <!---->
                                            Mark All as Read
                                        </span> <span class="text-centered btn-label mt-2" style="display: none;">
                                            <!---->
                                            Remove All
                                        </span> <a href="http://pos.appsthing.com/notifications" class="text-centered btn-label mt-2" style="display: none;">View All</a></div>
                                </div>
                            </div>
                        </li>
                        <li class="nav-item text-right pl-md-4 pl-lg-4 pl-xl-4">
                            <div class="dropdown"><a href="#" id="user_menu_dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link nav-link-dark dropdown-toggle text-bold"><img src="<?= base_url() ?>assets/thing/images/profile_default.jpg" alt="" class="d-inline-block rounded-circle mr-2 top-nav-profile">
                                    Tanya O'Conner
                                </a>
                                <div aria-labelledby="user_menu_dropdown" class="dropdown-menu dropdown-menu-right"><a href="http://pos.appsthing.com/profile/HKnbW4PuuGKZkCcqJDZ8jqHDh" class="dropdown-item"> Profile</a> <a href="http://pos.appsthing.com/logout" class="dropdown-item">Logout</a></div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="vue-notification-group" style="width: 500px; top: 0px; right: 0px;"><span></span></div>
            </div>
        </nav>
        <div class="wrapper">
            <div class="content-order m-0 p-0">
                <div class="row m-0">
                    <div class="col-md-8 p-0 bg-white border-right">
                        <div class="p-0 border-bottom">
                            <div class="d-flex flex-nowrap justify-content-between p-3 horizontal-scroll hide-horizontal-scroll">
                                <div class="mr-auto"><span class="text-title">New Order</span></div> <button class="btn btn-primary btn-light ml-3"><i class="fas fa-keyboard"></i></button>
                                <div title="Bill Type" class="col-md-2 pr-0"><select name="billing_type" class="form-control form-control-custom custom-select">
                                        <option value="FINE_DINE">
                                            Fine Dine
                                        </option>
                                        <option value="QUICK_BILL">
                                            Quick Bill
                                        </option>
                                    </select></div> <button class="btn btn-primary btn-light ml-3"> Digital Menu Orders</button> <button class="btn btn-primary btn-light ml-3"> Running Orders</button> <button class="btn btn-primary btn-light ml-3"> Hold List</button> <button class="btn btn-danger ml-3"><i class="fas fa-cash-register"></i>&ensp;<span>Close Register</span></button>
                            </div>
                        </div>
                        <div class="d-flex flex-column p-3 border-bottom product-info-form">
                            <form>
                                <div class="form-row mb-2">
                                    <div class="form-group col-md-3"><label for="barcode">Barcode</label> <input type="text" name="barcode" placeholder="Scan Barcode" autocomplete="off" class="form-control form-control-lg"></div>
                                    <div class="form-group col-md-3"><label for="product_title">Product Title</label> <input type="text" name="product_title" placeholder="Product Title" autocomplete="off" class="form-control form-control-lg"></div>
                                    <div class="form-group col-md-3"><label for="category">Category</label> <select name="category" class="form-control custom-select custom-select-lg">
                                            <option value="">Filter by Category..</option>
                                            <option value="sarITS7eSS2apL8P6xWujabdX">
                                                Add-ons
                                            </option>
                                            <option value="RzrQKHyAuDmtJIZgiLWVsRIA5">
                                                Beverages
                                            </option>
                                            <option value="dtYQUwpMuZLXPvUmdTdFtbP7f">
                                                Burger Pizza
                                            </option>
                                            <option value="lgOEv0kNyxh1wZ8AdY1CPCr6o">
                                                Chicken
                                            </option>
                                            <option value="m28d38D1vs4phkQz2V2JdU8MA">
                                                Ingredients
                                            </option>
                                            <option value="uEa0eROaGj1pgFqpPXPfWt65i">
                                                Non Veg Pizzas
                                            </option>
                                            <option value="YVb1ZUrh7OnDIux87XD5LPm6s">
                                                Pasta
                                            </option>
                                            <option value="IyU3VESylARTypGYTfmKmPbWY">
                                                Side Orders
                                            </option>
                                            <option value="ei5HK9Iob4xTmtmlrEf9iEmjk">
                                                Veg Pizzas
                                            </option>
                                        </select></div>
                                    <div class="form-group col-md-3"><button type="submit" class="btn btn-primary btn-block btn-lg find-product-btn">
                                            <!----> Go
                                        </button></div>
                                </div>
                            </form>
                        </div>
                        <div class="d-flex flex-wrap mb-5 p-3 product-list bg-light">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="d-flex align-items-start flex-column p-1 mb-1 col-md-4 bg-light product">
                                        <div class="col-12 p-3 bg-white product-grid">
                                            <div class="d-flex flex-row-reverse"><img src="/storage/product/thumb_placeholder_image.png" alt="" class="rounded-circle product-image position-absolute d-none d-lg-block"></div>
                                            <div class="product-code"><span class="small text-secondary text-break">Product Code : 92M8</span></div>
                                            <div class="text-bold text-break overflow-hidden product-title">
                                                Veggie Paradise - Medium
                                                <span title="Customizable" class="ml-1 text-primary"><i class="far fa-plus-square"></i></span> <span title="Variants Available" class="ml-1 text-primary"><i class="far fa-clone"></i></span>
                                            </div>
                                            <div class="text-bold text-break overflow-hidden" style="display: none;"><span class="text-warning text-caption">Only 99999.00 stock(s) left</span></div>
                                            <div class="text-bold text-break overflow-hidden" style="display: none;"><span class="text-warning text-caption">Low on Ingredient stock</span></div>
                                            <div class="text-bold text-break overflow-hidden"><span class="text-success text-caption">Discount 5.00%</span></div>
                                            <div class="mt-auto ml-auto pt-3 text-break product-price"><span class="product-price-currency">USD</span> 9.00
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex align-items-start flex-column p-1 mb-1 col-md-4 bg-light product">
                                        <div class="col-12 p-3 bg-white product-grid">
                                            <div class="d-flex flex-row-reverse"><img src="/storage/product/thumb_placeholder_image.png" alt="" class="rounded-circle product-image position-absolute d-none d-lg-block"></div>
                                            <div class="product-code"><span class="small text-secondary text-break">Product Code : AQGP</span></div>
                                            <div class="text-bold text-break overflow-hidden product-title">
                                                Veg Extravaganza - Medium
                                                <span title="Customizable" class="ml-1 text-primary"><i class="far fa-plus-square"></i></span> <span title="Variants Available" class="ml-1 text-primary"><i class="far fa-clone"></i></span>
                                            </div>
                                            <div class="text-bold text-break overflow-hidden" style="display: none;"><span class="text-warning text-caption">Only 99999.00 stock(s) left</span></div>
                                            <div class="text-bold text-break overflow-hidden" style="display: none;"><span class="text-warning text-caption">Low on Ingredient stock</span></div>
                                            <div class="text-bold text-break overflow-hidden"><span class="text-success text-caption">Discount 5.00%</span></div>
                                            <div class="mt-auto ml-auto pt-3 text-break product-price"><span class="product-price-currency">USD</span> 8.00
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex align-items-start flex-column p-1 mb-1 col-md-4 bg-light product">
                                        <div class="col-12 p-3 bg-white product-grid">
                                            <div class="d-flex flex-row-reverse"><img src="/storage/product/thumb_placeholder_image.png" alt="" class="rounded-circle product-image position-absolute d-none d-lg-block"></div>
                                            <div class="product-code"><span class="small text-secondary text-break">Product Code : JR8Q</span></div>
                                            <div class="text-bold text-break overflow-hidden product-title">
                                                Chicken Meatballs Peri-Peri
                                                <span title="Customizable" class="ml-1 text-primary"><i class="far fa-plus-square"></i></span>
                                                <!---->
                                            </div>
                                            <div class="text-bold text-break overflow-hidden" style="display: none;"><span class="text-warning text-caption">Only 99999.00 stock(s) left</span></div>
                                            <div class="text-bold text-break overflow-hidden" style="display: none;"><span class="text-warning text-caption">Low on Ingredient stock</span></div>
                                            <div class="text-bold text-break overflow-hidden"><span class="text-success text-caption">Discount 5.00%</span></div>
                                            <div class="mt-auto ml-auto pt-3 text-break product-price"><span class="product-price-currency">USD</span> 15.50
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex align-items-start flex-column p-1 mb-1 col-md-4 bg-light product">
                                        <div class="col-12 p-3 bg-white product-grid">
                                            <div class="d-flex flex-row-reverse"><img src="/storage/product/thumb_placeholder_image.png" alt="" class="rounded-circle product-image position-absolute d-none d-lg-block"></div>
                                            <div class="product-code"><span class="small text-secondary text-break">Product Code : VE7C</span></div>
                                            <div class="text-bold text-break overflow-hidden product-title">
                                                Boneless Chicken Wings Peri-Peri
                                                <span title="Customizable" class="ml-1 text-primary"><i class="far fa-plus-square"></i></span>
                                                <!---->
                                            </div>
                                            <div class="text-bold text-break overflow-hidden" style="display: none;"><span class="text-warning text-caption">Only 99999.00 stock(s) left</span></div>
                                            <div class="text-bold text-break overflow-hidden" style="display: none;"><span class="text-warning text-caption">Low on Ingredient stock</span></div>
                                            <div class="text-bold text-break overflow-hidden"><span class="text-success text-caption">Discount 5.00%</span></div>
                                            <div class="mt-auto ml-auto pt-3 text-break product-price"><span class="product-price-currency">USD</span> 16.00
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex align-items-start flex-column p-1 mb-1 col-md-4 bg-light product">
                                        <div class="col-12 p-3 bg-white product-grid">
                                            <div class="d-flex flex-row-reverse"><img src="/storage/product/thumb_placeholder_image.png" alt="" class="rounded-circle product-image position-absolute d-none d-lg-block"></div>
                                            <div class="product-code"><span class="small text-secondary text-break">Product Code : KUEE</span></div>
                                            <div class="text-bold text-break overflow-hidden product-title">
                                                Peppy Paneer - Medium
                                                <span title="Customizable" class="ml-1 text-primary"><i class="far fa-plus-square"></i></span> <span title="Variants Available" class="ml-1 text-primary"><i class="far fa-clone"></i></span>
                                            </div>
                                            <div class="text-bold text-break overflow-hidden" style="display: none;"><span class="text-warning text-caption">Only 99999.00 stock(s) left</span></div>
                                            <div class="text-bold text-break overflow-hidden" style="display: none;"><span class="text-warning text-caption">Low on Ingredient stock</span></div>
                                            <div class="text-bold text-break overflow-hidden"><span class="text-success text-caption">Discount 5.00%</span></div>
                                            <div class="mt-auto ml-auto pt-3 text-break product-price"><span class="product-price-currency">USD</span> 9.00
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex align-items-start flex-column p-1 mb-1 col-md-4 bg-light product">
                                        <div class="col-12 p-3 bg-white product-grid">
                                            <div class="d-flex flex-row-reverse"><img src="/storage/product/thumb_placeholder_image.png" alt="" class="rounded-circle product-image position-absolute d-none d-lg-block"></div>
                                            <div class="product-code"><span class="small text-secondary text-break">Product Code : EEKT</span></div>
                                            <div class="text-bold text-break overflow-hidden product-title">
                                                Mexican Green Wave - Medium
                                                <span title="Customizable" class="ml-1 text-primary"><i class="far fa-plus-square"></i></span> <span title="Variants Available" class="ml-1 text-primary"><i class="far fa-clone"></i></span>
                                            </div>
                                            <div class="text-bold text-break overflow-hidden" style="display: none;"><span class="text-warning text-caption">Only 99999.00 stock(s) left</span></div>
                                            <div class="text-bold text-break overflow-hidden" style="display: none;"><span class="text-warning text-caption">Low on Ingredient stock</span></div>
                                            <div class="text-bold text-break overflow-hidden"><span class="text-success text-caption">Discount 5.00%</span></div>
                                            <div class="mt-auto ml-auto pt-3 text-break product-price"><span class="product-price-currency">USD</span> 15.00
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex align-items-start flex-column p-1 mb-1 col-md-4 bg-light product">
                                        <div class="col-12 p-3 bg-white product-grid">
                                            <div class="d-flex flex-row-reverse"><img src="/storage/product/thumb_placeholder_image.png" alt="" class="rounded-circle product-image position-absolute d-none d-lg-block"></div>
                                            <div class="product-code"><span class="small text-secondary text-break">Product Code : A81V</span></div>
                                            <div class="text-bold text-break overflow-hidden product-title">
                                                Lipton Ice Tea
                                                <span title="Customizable" class="ml-1 text-primary"><i class="far fa-plus-square"></i></span>
                                                <!---->
                                            </div>
                                            <div class="text-bold text-break overflow-hidden" style="display: none;"><span class="text-warning text-caption">Only 99999.00 stock(s) left</span></div>
                                            <div class="text-bold text-break overflow-hidden" style="display: none;"><span class="text-warning text-caption">Low on Ingredient stock</span></div>
                                            <div class="text-bold text-break overflow-hidden"><span class="text-success text-caption">Discount 5.00%</span></div>
                                            <div class="mt-auto ml-auto pt-3 text-break product-price"><span class="product-price-currency">USD</span> 4.00
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex align-items-start flex-column p-1 mb-1 col-md-4 bg-light product">
                                        <div class="col-12 p-3 bg-white product-grid">
                                            <div class="d-flex flex-row-reverse"><img src="/storage/product/thumb_placeholder_image.png" alt="" class="rounded-circle product-image position-absolute d-none d-lg-block"></div>
                                            <div class="product-code"><span class="small text-secondary text-break">Product Code : EJ5J</span></div>
                                            <div class="text-bold text-break overflow-hidden product-title">
                                                Sprite
                                                <span title="Customizable" class="ml-1 text-primary"><i class="far fa-plus-square"></i></span>
                                                <!---->
                                            </div>
                                            <div class="text-bold text-break overflow-hidden" style="display: none;"><span class="text-warning text-caption">Only 99999.00 stock(s) left</span></div>
                                            <div class="text-bold text-break overflow-hidden" style="display: none;"><span class="text-warning text-caption">Low on Ingredient stock</span></div>
                                            <div class="text-bold text-break overflow-hidden"><span class="text-success text-caption">Discount 5.00%</span></div>
                                            <div class="mt-auto ml-auto pt-3 text-break product-price"><span class="product-price-currency">USD</span> 6.00
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex align-items-start flex-column p-1 mb-1 col-md-4 bg-light product">
                                        <div class="col-12 p-3 bg-white product-grid">
                                            <div class="d-flex flex-row-reverse"><img src="/storage/product/thumb_placeholder_image.png" alt="" class="rounded-circle product-image position-absolute d-none d-lg-block"></div>
                                            <div class="product-code"><span class="small text-secondary text-break">Product Code : U2VM</span></div>
                                            <div class="text-bold text-break overflow-hidden product-title">
                                                Burger Pizza - Classic Non Veg
                                                <span title="Customizable" class="ml-1 text-primary"><i class="far fa-plus-square"></i></span>
                                                <!---->
                                            </div>
                                            <div class="text-bold text-break overflow-hidden" style="display: none;"><span class="text-warning text-caption">Only 99999.00 stock(s) left</span></div>
                                            <div class="text-bold text-break overflow-hidden" style="display: none;"><span class="text-warning text-caption">Low on Ingredient stock</span></div>
                                            <div class="text-bold text-break overflow-hidden"><span class="text-success text-caption">Discount 5.00%</span></div>
                                            <div class="mt-auto ml-auto pt-3 text-break product-price"><span class="product-price-currency">USD</span> 11.00
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex align-items-start flex-column p-1 mb-1 col-md-4 bg-light product">
                                        <div class="col-12 p-3 bg-white product-grid">
                                            <div class="d-flex flex-row-reverse"><img src="/storage/product/thumb_placeholder_image.png" alt="" class="rounded-circle product-image position-absolute d-none d-lg-block"></div>
                                            <div class="product-code"><span class="small text-secondary text-break">Product Code : KFBM</span></div>
                                            <div class="text-bold text-break overflow-hidden product-title">
                                                Veg Pasta Italiano White
                                                <span title="Customizable" class="ml-1 text-primary"><i class="far fa-plus-square"></i></span>
                                                <!---->
                                            </div>
                                            <div class="text-bold text-break overflow-hidden" style="display: none;"><span class="text-warning text-caption">Only 99999.00 stock(s) left</span></div>
                                            <div class="text-bold text-break overflow-hidden" style="display: none;"><span class="text-warning text-caption">Low on Ingredient stock</span></div>
                                            <div class="text-bold text-break overflow-hidden"><span class="text-success text-caption">Discount 5.00%</span></div>
                                            <div class="mt-auto ml-auto pt-3 text-break product-price"><span class="product-price-currency">USD</span> 6.00
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 p-0 full-height">
                        <div class="cart_form">
                            <div class="p-0 border-bottom">
                                <div class="d-flex flex-wrap p-3"><span class="mr-auto text-title text-black-50">Cart</span> <button class="btn btn-outline-primary"><i class="fas fa-user-edit"></i> Customer: Walkin Customer</button></div>
                            </div>
                            <div class="p-0 border-bottom">
                                <div class="d-flex flex-wrap justify-content-between p-3"><span>2 Items (2 Qty)</span> <span>Order Level Tax : 0.00</span> <span class="text-success">Order Level Discount : 0.00</span></div>
                            </div>
                            <div class="p-0 cart-list border-left">
                                <div value="5uznJoUKg7uF9TlASom9kKtgj" class="d-flex flex-column pl-3 pt-3 pb-3 border-bottom">
                                    <div class="d-flex mb-2"><span class="small text-secondary">Product Code : JR8Q</span> <button type="button" aria-label="Close" class="close cart-item-remove bg-light mr-2 ml-auto"><span aria-hidden="true">×</span></button></div>
                                    <div class="d-flex justify-content-between mb-2"><span class="text-bold text-break cart-item-title">
                                            Chicken Meatballs Peri-Peri
                                        </span> <input type="number" autocomplete="off" min="0" class="form-control form-control-custom cart-product-quantity mr-2 ml-3"></div>
                                    <div class="d-flex flex-row justify-content-between mr-2 cart-item-summary">
                                        <div>
                                            <div class="d-flex flex-column">
                                                <div class="text-success"><i class="fas fa-tags cart-discount-tag"></i> Discount Amount: 0.78</div>
                                                <div>Tax Amount: 0.00</div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="d-flex flex-column">
                                                <div class="text-right">Price: 1 x 15.50</div>
                                                <div class="text-right">Total: 15.50</div>
                                            </div>
                                        </div>
                                    </div>
                                    <!----> <button type="button" class="btn btn-link customize-btn text-bold mr-auto p-0 text-primary text-decoration-none">
                                        Customize
                                    </button>
                                </div>
                                <div value="DEEX8oVI2ZUwSqWNER9y0kaqy" class="d-flex flex-column pl-3 pt-3 pb-3 border-bottom">
                                    <div class="d-flex mb-2"><span class="small text-secondary">Product Code : LINT</span> <button type="button" aria-label="Close" class="close cart-item-remove bg-light mr-2 ml-auto"><span aria-hidden="true">×</span></button></div>
                                    <div class="d-flex justify-content-between mb-2"><span class="text-bold text-break cart-item-title">
                                            Veg Extravaganza - Regular
                                        </span> <input type="number" autocomplete="off" min="0" class="form-control form-control-custom cart-product-quantity mr-2 ml-3"></div>
                                    <div class="d-flex flex-row justify-content-between mr-2 cart-item-summary">
                                        <div>
                                            <div class="d-flex flex-column">
                                                <div class="text-success"><i class="fas fa-tags cart-discount-tag"></i> Discount Amount: 0.30</div>
                                                <div>Tax Amount: 0.00</div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="d-flex flex-column">
                                                <div class="text-right">Price: 1 x 6.00</div>
                                                <div class="text-right">Total: 6.00</div>
                                            </div>
                                        </div>
                                    </div>
                                    <!----> <button type="button" class="btn btn-link customize-btn text-bold mr-auto p-0 text-primary text-decoration-none">
                                        Customize
                                    </button>
                                </div>
                            </div>
                            <div class="d-flex flex-column p-3 ml-auto fixed-bottom col-md-4 border-top cart-summary">
                                <div class="d-flex justify-content-center show-more-billing-data bg-white cursor"><span class="show-more-billing-data-text"><span><i class="fas fa-angle-double-up show-more-billing-data-icon"></i> Show more</span></span></div>
                                <div style="display: none;">
                                    <div class="d-flex justify-content-between mb-2 cart-summary-label mt-0"><span>Sub total</span> <span>21.50</span></div>
                                    <div class="d-flex justify-content-between mb-2 cart-summary-label mt-0"><span>Discount</span> <span>1.08</span></div>
                                    <div class="d-flex justify-content-between mb-2 cart-summary-label mt-0"><span class="d-inline-flex">
                                            Addt'l Discount
                                            <input type="number" placeholder="Discount" min="0" max="100" class="form-control form-control-sm ml-3 mr-1 additional_discount">%
                                        </span> <span>0</span></div>
                                    <div class="d-flex justify-content-between mb-2 cart-summary-label mt-0"><span>Total After Discount</span> <span>20.42</span></div>
                                    <div class="d-flex justify-content-between mb-2 cart-summary-label"><span>Total Tax</span> <span>0.00</span></div>
                                </div>
                                <div class="d-flex justify-content-between mb-2 cart-total"><span>Total</span> <span>20.42</span></div>
                                <div>
                                    <div>
                                        <div class="mt-2">
                                            <!---->
                                            <div class="d-flex mt-2">
                                                <div class="mr-2"><button type="submit" class="btn btn-light btn-lg btn-block">
                                                        <!----> Hold Order
                                                    </button></div>
                                                <div class="flex-grow-1"><button type="submit" class="btn btn-primary btn-lg btn-block">
                                                        <!----> Close Order
                                                    </button></div>
                                            </div>
                                        </div>
                                        <!---->
                                    </div>
                                </div>
                                <!---->
                            </div>
                        </div>
                    </div>
                    <div style="display: none;">
                        <div class="modal-wrapper modal">
                            <div class="modal-dialog-centered">
                                <div class="modal-container">
                                    <div class="modal-header"><span class="modal-title">
                                            Provide Customer Details
                                        </span> <button type="button" aria-label="Close" class="close"><span aria-hidden="true">×</span></button></div>
                                    <div class="modal-body">
                                        <div>
                                            <div class="form-group"><label for="filter_customer">Search Customer</label>
                                                <div tabindex="-1" class="IZ-select" type="text" name="filter_customer" autocomplete="off">
                                                    <div class="IZ-select__input-wrap">
                                                        <div class="IZ-select__input">
                                                            <!----> <input placeholder="Name or Email or Contact No." tabindex="0" type="text" role="combobox" autocomplete="new-password" class="">
                                                        </div>
                                                    </div>
                                                    <div class="IZ-select__menu IZ-select__menu--at-top" style="pointer-events: none; visibility: hidden; opacity: 0;">
                                                        <div class="IZ-select__menu-items" style="max-height: 300px;">
                                                            <div style="height: 8px;"></div>
                                                            <div class="IZ-select__no-data">
                                                                No data available
                                                            </div>
                                                            <div style="height: 8px;"></div>
                                                        </div>
                                                        <div style="position: absolute; top: 0px; left: 0px; right: 0px;"></div>
                                                        <div style="position: absolute; bottom: 0px; left: 0px; right: 0px;"></div>
                                                    </div>
                                                    <div class="IZ-select__menu IZ-select__menu--at-bottom" style="pointer-events: none; visibility: hidden; opacity: 0;">
                                                        <div class="IZ-select__menu-items" style="max-height: 300px;">
                                                            <div style="height: 8px;"></div>
                                                            <div class="IZ-select__no-data">
                                                                No data available
                                                            </div>
                                                            <div style="height: 8px;"></div>
                                                        </div>
                                                        <div style="position: absolute; top: 0px; left: 0px; right: 0px;"></div>
                                                        <div style="position: absolute; bottom: 0px; left: 0px; right: 0px;"></div>
                                                    </div>
                                                    <div class="IZ-select__error" style="display: none;">

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="d-flex justify-content-center p-2"><span>Or</span></div>
                                            <div class="form-group"><label for="customer_name">Customer Name</label> <input type="text" name="customer_name" placeholder="Please provide Name" autocomplete="off" class="form-control" aria-required="false" aria-invalid="false"> <span class=""></span> <span class="small text-secondary text-break"><i class="fas fa-exclamation-circle text-primary"></i> Track only customer name</span></div>
                                            <div class="d-flex justify-content-center p-2"><span>Or</span></div>
                                            <div class="d-flex justify-content-center p-2"><span class="text-primary text-bold cursor"><i class="fas fa-user-plus"></i> Add New Customer</span></div>
                                            <div class="row mt-2 border-top" style="display: none;">
                                                <div class="col-md-12 mt-3">
                                                    <form class="mb-3">
                                                        <div class="d-flex flex-wrap mb-2">
                                                            <div class="mr-auto"><span class="text-subtitle">Add Customer</span></div>
                                                        </div>
                                                        <p class=""></p>
                                                        <div class="form-row mb-2">
                                                            <div class="form-group col-md-12"><label for="name">Fullname</label> <input type="text" name="name" placeholder="Please enter fullname" autocomplete="off" class="form-control form-control-custom" aria-required="true" aria-invalid="false"> <span class=""></span></div> <span class="small text-secondary text-break pl-1"><i class="fas fa-exclamation-circle text-primary"></i> Email or Contact No. is required</span>
                                                            <div class="form-group col-md-12"><label for="email">Email</label> <input type="text" name="email" placeholder="Please enter email" autocomplete="off" class="form-control form-control-custom" aria-required="true" aria-invalid="false"> <span class=""></span></div>
                                                            <div class="form-group col-md-12"><label for="phone">Contact No.&nbsp;<small class="text-muted">(With Country Code)</small></label> <input type="text" name="phone" placeholder="Please enter Contact Number" autocomplete="off" class="form-control form-control-custom" aria-required="true" aria-invalid="false"> <span class=""></span></div>
                                                            <div class="form-group col-md-12"><label for="dob">Date of Birth</label>
                                                                <div class="mx-datepicker" name="dob" autocomplete="off">
                                                                    <div class="mx-input-wrapper"><input placeholder="Please enter Date of Birth" name="date" type="text" autocomplete="off" class="form-control form-control-custom bg-white">
                                                                        <!----> <i class="mx-icon-calendar"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1024 1024">
                                                                                <path d="M940.218182 107.054545h-209.454546V46.545455h-65.163636v60.50909H363.054545V46.545455H297.890909v60.50909H83.781818c-18.618182 0-32.581818 13.963636-32.581818 32.581819v805.236363c0 18.618182 13.963636 32.581818 32.581818 32.581818h861.090909c18.618182 0 32.581818-13.963636 32.581818-32.581818V139.636364c-4.654545-18.618182-18.618182-32.581818-37.236363-32.581819zM297.890909 172.218182V232.727273h65.163636V172.218182h307.2V232.727273h65.163637V172.218182h176.872727v204.8H116.363636V172.218182h181.527273zM116.363636 912.290909V442.181818h795.927273v470.109091H116.363636z"></path>
                                                                            </svg></i>
                                                                    </div>
                                                                </div> <span class=""></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-row mb-2">
                                                            <div class="form-group col-md-12"><label for="address">Address</label> <textarea name="address" rows="5" placeholder="Enter Address" class="form-control form-control-custom" aria-required="false" aria-invalid="false"></textarea> <span class=""></span></div>
                                                        </div>
                                                        <div><button type="submit" class="btn btn-primary btn-block">
                                                                <!----> Save
                                                            </button></div>
                                                    </form>
                                                </div>
                                                <!---->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer"><button type="button" class="btn btn-light"> Skip</button> <button type="button" class="btn btn-primary">
                                            <!----> Proceed
                                        </button></div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-mask modal"></div>
                    </div>
                    <div style="display: none;">
                        <div class="modal-wrapper modal">
                            <div class="modal-dialog-centered">
                                <div class="modal-container modal-container-xl">
                                    <div class="modal-header"><span class="modal-title">
                                            Confirm
                                        </span> <button type="button" aria-label="Close" class="close"><span aria-hidden="true">×</span></button></div>
                                    <div class="modal-body">
                                        <p class="" style="display: none;"></p>
                                        <form data-vv-scope="confirmation_form">
                                            <!---->
                                            <div>
                                                <div class="form-group"><label for="restaurant_order_type">Order Type</label>
                                                    <div class="d-flex flex-wrap">
                                                        <div class="row flex-fill">
                                                            <div class="col-md-4"><input type="radio" name="restaurant_order_type" id="restaurant_order_type_check0" class="check d-none" value="DINEIN" aria-required="true" aria-invalid="false"> <label for="restaurant_order_type_check0" class="check-buttons w-100 text-truncate">Dine In</label></div>
                                                            <div class="col-md-4"><input type="radio" name="restaurant_order_type" id="restaurant_order_type_check1" class="check d-none" value="TAKEWAY" aria-required="true" aria-invalid="false"> <label for="restaurant_order_type_check1" class="check-buttons w-100 text-truncate">Take Away</label></div>
                                                            <div class="col-md-4"><input type="radio" name="restaurant_order_type" id="restaurant_order_type_check2" class="check d-none" value="DELIVERY" aria-required="true" aria-invalid="false"> <label for="restaurant_order_type_check2" class="check-buttons w-100 text-truncate">Delivery</label></div>
                                                        </div>
                                                    </div> <span class=""></span>
                                                </div>
                                                <!---->
                                                <div class="form-group col-md-12 p-0"><label for="waiter">Waiter</label>
                                                    <div class="d-flex flex-wrap">
                                                        <div class="row flex-fill">
                                                            <div class="col-md-3"><input type="radio" name="waiter" id="waiter0" class="check d-none" value="XoKUw38EfVqtVTKAeKmKlYDIA"> <label for="waiter0" class="check-buttons w-100 text-truncate">103 - Judson Carter</label></div>
                                                            <div class="col-md-3"><input type="radio" name="waiter" id="waiter1" class="check d-none" value="A1r7PmrxHOT3cf8ikZMxAPDeN"> <label for="waiter1" class="check-buttons w-100 text-truncate">104 - Javier Rice</label></div>
                                                            <div class="col-md-3"><input type="radio" name="waiter" id="waiter2" class="check d-none" value="OO8j0V4eKBvzkyNvifS87zIyH"> <label for="waiter2" class="check-buttons w-100 text-truncate">105 - Keeley Anderson</label></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!---->
                                            </div>
                                        </form>
                                        Are you sure you want to proceed?
                                    </div>
                                    <div class="modal-footer"><button type="button" class="btn btn-light">Cancel</button> <button type="button" class="btn btn-primary">
                                            <!----> Continue
                                        </button></div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-mask modal"></div>
                    </div>
                    <!---->
                    <!---->
                    <!---->
                    <div style="display: none;">
                        <div class="p-0 bg-white hidden quickpanel col-md-6 col-xl-3">
                            <div>
                                <div class="border-bottom p-0">
                                    <div class="d-flex justify-content-between p-3">
                                        <div class="mr-auto text-subtitle">
                                            Running Orders ()
                                        </div> <button type="button" aria-label="Close" class="close panel-close bg-light ml-auto"><span aria-hidden="true">×</span></button>
                                    </div>
                                </div>
                                <div class="p-0">
                                    <div class="d-flex flex-column p-3 mb-1">
                                        <!---->
                                        <div><input type="text" placeholder="Search by order, customer, table.." autocomplete="off" class="form-control form-control-custom mb-3">
                                            <div class="text-muted">There are 0 running orders</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-mask modal"></div>
                    </div>
                    <div style="display: none;">
                        <div class="p-0 bg-white hidden quickpanel col-md-6 col-xl-3">
                            <div>
                                <div class="border-bottom p-0">
                                    <div class="d-flex justify-content-between p-3">
                                        <div class="mr-auto text-subtitle">
                                            Hold orders from last 2 days (0)
                                        </div> <button type="button" aria-label="Close" class="close panel-close bg-light ml-auto"><span aria-hidden="true">×</span></button>
                                    </div>
                                </div>
                                <div class="p-0">
                                    <div class="d-flex flex-column p-3 mb-1">
                                        <!---->
                                        <div><input type="text" placeholder="Search by order, customer .." autocomplete="off" class="form-control form-control-custom mb-3">
                                            <div class="text-muted">There are 0 orders on hold.</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-mask modal"></div>
                    </div>
                    <div style="display: none;">
                        <div class="p-0 bg-white hidden quickpanel col-md-6 col-xl-3">
                            <div>
                                <div class="border-bottom p-0">
                                    <div class="d-flex justify-content-between p-3">
                                        <div class="mr-auto text-subtitle">
                                            Keyboard Shortcuts
                                        </div> <button type="button" aria-label="Close" class="close panel-close bg-light ml-auto"><span aria-hidden="true">×</span></button>
                                    </div>
                                </div>
                                <div class="p-0">
                                    <div class="d-flex flex-column p-3 mb-1">
                                        <div value="CLOSE_ORDER" class="d-flex border-bottom">
                                            <div class="p-2 col-4">Ctrl+Shift+m</div>
                                            <div class="p-2 col-8">Close Order</div>
                                        </div>
                                        <div value="HOLD_ORDER" class="d-flex border-bottom">
                                            <div class="p-2 col-4">Ctrl+Shift+n</div>
                                            <div class="p-2 col-8">Hold Order</div>
                                        </div>
                                        <div value="SEND_TO_KITCHEN" class="d-flex border-bottom">
                                            <div class="p-2 col-4">Ctrl+Shift+b</div>
                                            <div class="p-2 col-8">Send to Kitchen</div>
                                        </div>
                                        <div value="SKIP_CUSTOMER" class="d-flex border-bottom">
                                            <div class="p-2 col-4">Shift+z</div>
                                            <div class="p-2 col-8">Skip customer selection</div>
                                        </div>
                                        <div value="PROCEED_CUSTOMER" class="d-flex border-bottom">
                                            <div class="p-2 col-4">Shift+x</div>
                                            <div class="p-2 col-8">Proceed customer selection</div>
                                        </div>
                                        <div value="ARROW_LEFT" class="d-flex border-bottom">
                                            <div class="p-2 col-4">Arrow Left</div>
                                            <div class="p-2 col-8">Move left through POS products</div>
                                        </div>
                                        <div value="ARROW_RIGHT" class="d-flex border-bottom">
                                            <div class="p-2 col-4">Arrow Right</div>
                                            <div class="p-2 col-8">Move right through POS products</div>
                                        </div>
                                        <div value="CHOOSE_PRODUCT" class="d-flex border-bottom">
                                            <div class="p-2 col-4">Control</div>
                                            <div class="p-2 col-8">Choose POS product</div>
                                        </div>
                                        <div value="SCROLL_PAYMENT_METHODS" class="d-flex border-bottom">
                                            <div class="p-2 col-4">Shift+p</div>
                                            <div class="p-2 col-8">Scroll through payment methods</div>
                                        </div>
                                        <div value="CHOOSE_PAYMENT_METHOD" class="d-flex border-bottom">
                                            <div class="p-2 col-4">Shift+l</div>
                                            <div class="p-2 col-8">Choose payment method</div>
                                        </div>
                                        <div value="SCROLL_BUSINESS_ACCOUNTS" class="d-flex border-bottom">
                                            <div class="p-2 col-4">Shift+o</div>
                                            <div class="p-2 col-8">Scroll through business accounts</div>
                                        </div>
                                        <div value="CHOOSE_BUSINESS_ACCOUNT" class="d-flex border-bottom">
                                            <div class="p-2 col-4">Shift+k</div>
                                            <div class="p-2 col-8">Choose business accounts</div>
                                        </div>
                                        <div value="SCROLL_ORDER_TYPES" class="d-flex border-bottom">
                                            <div class="p-2 col-4">Shift+i</div>
                                            <div class="p-2 col-8">Scroll through order types</div>
                                        </div>
                                        <div value="CHOOSE_ORDER_TYPE" class="d-flex border-bottom">
                                            <div class="p-2 col-4">Shift+j</div>
                                            <div class="p-2 col-8">Choose order type</div>
                                        </div>
                                        <div value="SCROLL_RESTAURANT_TABLES" class="d-flex border-bottom">
                                            <div class="p-2 col-4">Shift+u</div>
                                            <div class="p-2 col-8">Scroll through restaurant tables</div>
                                        </div>
                                        <div value="CHOOSE_RESTAURANT_TABLE" class="d-flex border-bottom">
                                            <div class="p-2 col-4">Shift+h</div>
                                            <div class="p-2 col-8">Choose restaurant table</div>
                                        </div>
                                        <div value="CONTINUE" class="d-flex border-bottom">
                                            <div class="p-2 col-4">Shift+m</div>
                                            <div class="p-2 col-8">Continue</div>
                                        </div>
                                        <div value="CANCEL" class="d-flex border-bottom">
                                            <div class="p-2 col-4">Shift+n</div>
                                            <div class="p-2 col-8">Cancel</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-mask modal"></div>
                    </div>
                    <div style="display: none;">
                        <div class="p-0 bg-white hidden quickpanel col-md-6 col-xl-3">
                            <div>
                                <div class="border-bottom p-0">
                                    <div class="d-flex justify-content-between p-3">
                                        <div class="mr-auto text-subtitle">
                                            Digital Menu Orders (0)
                                        </div> <button type="button" aria-label="Close" class="close panel-close bg-light ml-auto"><span aria-hidden="true">×</span></button>
                                    </div>
                                </div>
                                <div class="p-0">
                                    <div class="d-flex flex-column p-3 mb-1">
                                        <!---->
                                        <div style=""><input type="text" placeholder="Search by order, customer, table.." autocomplete="off" class="form-control form-control-custom mb-3">
                                            <div class="text-muted">There are 0 digital orders</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-mask modal"></div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="container-fluid p-3 fixed-bottom">
            <div class="d-flex justify-content-between"><span>&nbsp;© 2022 - 2023 Appsthing · <span class="text-muted">v5.4</span></span>
                <div class="dropdown">
                    <div class="btn-group dropup"><button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static" class="btn btn-link text-decoration-none dropdown-toggle text-body"><i class="fas fa-globe mr-1 text-secondary"></i> English - en
                        </button>
                        <div class="dropdown-menu dropdown-menu-right mr-2"><button class="dropdown-item">Arabic - ar</button><button class="dropdown-item">Chinese - zh</button><button class="dropdown-item">Dutch - nl</button><button class="dropdown-item">English - en</button><button class="dropdown-item">French - fr</button><button class="dropdown-item">German - de</button><button class="dropdown-item">Italian - it</button><button class="dropdown-item">Malay - ms</button><button class="dropdown-item">Norwegian - no</button><button class="dropdown-item">Portuguese - pt</button><button class="dropdown-item">Spanish - es</button><button class="dropdown-item">Swedish - sv</button><button class="dropdown-item">Thai - th</button></div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <script src="<?= base_url() ?>assets/thing/plugins/jquery/jquery-3.4.1.slim.min.js"></script>
    <script src="<?= base_url() ?>assets/thing/plugins/bootstrap/popper.min.js"></script>
    <script src="<?= base_url() ?>assets/thing/plugins/bootstrap/bootstrap.min.js"></script>
    <script src="<?= base_url() ?>assets/thing/js/side_nav.js"></script>
    <script src="<?= base_url() ?>assets/thing/js/app.js?id=d8438ffd1a08e8e93bf3"></script>
    <!---->
</body>

</html>