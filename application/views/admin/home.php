<div class="col-md-12 col-sm-12 ">
	<div class="x_panel">
		<div class="x_title">
			<h2>Data Admin</h2>
			<div class="clearfix"></div>
		</div>
		<div class="x_content">
			<div class="row">
				<div class="container-fluid px-4">
					<h1 class="mt-4">Dashboard</h1>


					<div class="row">

						<div class="col-md-4 col-sm-4 ">
							<div class="x_panel tile fixed_height_320">
								<div class="">
									<h2>Menu</h2>
								</div>
								<div class="x_content">
									<canvas id="myChart"></canvas>
								</div>
							</div>
						</div>

						<div class="col-md-4 col-sm-4 ">
							<div class="x_panel tile fixed_height_320">
								<div class="">
									<h2>Top Menu</h2>
								</div>
								<div class="x_content">
									<canvas id="TopMenu"></canvas>
								</div>
							</div>
						</div>

						<div class="col-md-4 col-sm-4 ">
							<div class="x_panel tile fixed_height_320">
								<div class="">
									<h2>Transaksi Harian</h2>
								</div>
								<div class="x_content">
									<canvas id="TransaksiHarian"></canvas>
								</div>
							</div>
						</div>

						
					</div>

					<div class="row">
						<div class="col-xl-3 col-md-6">
							<div class="card bg-primary text-white mb-4">
								<div class="card-body">Jual Transaksi Hari Ini</div>
								<div class="card-footer d-flex align-items-center justify-content-between">
									<h5 class="small text-white stretched-link" href="#"><?= $hariIni ?></h5>
								</div>
							</div>
						</div>
						<div class="col-xl-3 col-md-6">
							<div class="card bg-primary text-white mb-4">
								<div class="card-body">Jual Transaksi Kemarin</div>
								<div class="card-footer d-flex align-items-center justify-content-between">
									<h5 class="small text-white stretched-link" href="#"><?= $kemarin ?></h5>
								</div>
							</div>
						</div>
						<div class="col-xl-3 col-md-6">
							<div class="card bg-primary text-white mb-4">
								<div class="card-body">Jual Menu Hari Ini</div>
								<div class="card-footer d-flex align-items-center justify-content-between">
									<h5 class="small text-white stretched-link" href="#"><?= $menuHariIni ?></h5>
								</div>
							</div>
						</div>
						<div class="col-xl-3 col-md-6">
							<div class="card bg-primary text-white mb-4">
								<div class="card-body">Jual Menu Kemarin </div>
								<div class="card-footer d-flex align-items-center justify-content-between">
									<h5 class="small text-white stretched-link" href="#"><?= $menuKemarin ?></h5>
								</div>
							</div>
						</div>
					</div>


				</div>
			</div>
		</div>
	</div>
</div>
<!-- <link href="<?= $bu; ?>assets/admin/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"> -->


<!-- <script src="<?= $bu; ?>assets/admin/vendors/nprogress/nprogress.js"></script> -->

<script src="<?= $bu; ?>assets/admin/vendors/Chart.js/dist/Chart.min.js"></script>
<script src="<?= $bu; ?>assets/util/fungsi.js"></script>
<?php 
// var_dump($graph);
// die;
?>

<script type="text/javascript">


		chartFunc('myChart','doughnut',<?= json_encode($graph) ?>,'nama_menu','stok','Produk',)

		
		chartFunc('TopMenu','doughnut',<?= json_encode($menuItem) ?>,'nama_menu','qty','Top Menu')
		
		chartFunc('TransaksiHarian','bar',<?= json_encode($TransaksiHarian) ?>,'dayName','harga_total','Transaksi Harian',)
	


	

</script>
