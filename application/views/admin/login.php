<?php
defined('BASEPATH') or exit('No direct script access allowed');
// $title = array_key_exists('title', $header) ? $header['title'] : $ci->namaAplikasi();
// $page = array_key_exists('page', $header) ? $header['page'] : 'master_admin';
// $js = array_key_exists('js', $header) ? $header['js'] : array();
$bu = base_url();
?>

<!doctype html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://preview.colorlib.com/theme/bootstrap/login-form-07/A.fonts,,_icomoon,,_style.css+css,,_owl.carousel.min.css+css,,_bootstrap.min.css+css,,_style.css,Mcc.Gajl4v2LrE.css.pagespeed.cf.MrWHX3xQRZ.css" />


  <title>Login Admin</title>
  <script nonce="e1ebdffb-6c15-40b8-9ed7-97f0f2541b37">
    (function(w, d) {
      ! function(a, e, t, r) {
        a.zarazData = a.zarazData || {};
        a.zarazData.executed = [];
        a.zaraz = {
          deferred: []
        };
        a.zaraz.q = [];
        a.zaraz._f = function(e) {
          return function() {
            var t = Array.prototype.slice.call(arguments);
            a.zaraz.q.push({
              m: e,
              a: t
            })
          }
        };
        for (const e of ["track", "set", "ecommerce", "debug"]) a.zaraz[e] = a.zaraz._f(e);
        a.zaraz.init = () => {
          var t = e.getElementsByTagName(r)[0],
            z = e.createElement(r),
            n = e.getElementsByTagName("title")[0];
          n && (a.zarazData.t = e.getElementsByTagName("title")[0].text);
          a.zarazData.x = Math.random();
          a.zarazData.w = a.screen.width;
          a.zarazData.h = a.screen.height;
          a.zarazData.j = a.innerHeight;
          a.zarazData.e = a.innerWidth;
          a.zarazData.l = a.location.href;
          a.zarazData.r = e.referrer;
          a.zarazData.k = a.screen.colorDepth;
          a.zarazData.n = e.characterSet;
          a.zarazData.o = (new Date).getTimezoneOffset();
          a.zarazData.q = [];
          for (; a.zaraz.q.length;) {
            const e = a.zaraz.q.shift();
            a.zarazData.q.push(e)
          }
          z.defer = !0;
          for (const e of [localStorage, sessionStorage]) Object.keys(e || {}).filter((a => a.startsWith("_zaraz_"))).forEach((t => {
            try {
              a.zarazData["z_" + t.slice(7)] = JSON.parse(e.getItem(t))
            } catch {
              a.zarazData["z_" + t.slice(7)] = e.getItem(t)
            }
          }));
          z.referrerPolicy = "origin";
          z.src = "/cdn-cgi/zaraz/s.js?z=" + btoa(encodeURIComponent(JSON.stringify(a.zarazData)));
          t.parentNode.insertBefore(z, t)
        };
        ["complete", "interactive"].includes(e.readyState) ? zaraz.init() : a.addEventListener("DOMContentLoaded", zaraz.init)
      }(w, d, 0, "script");
    })(window, document);
  </script>
</head>

<body>
  <div class="content">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <script data-pagespeed-no-defer>
            //<![CDATA[
            (function() {
              for (var g = "function" == typeof Object.defineProperties ? Object.defineProperty : function(b, c, a) {
                  if (a.get || a.set) throw new TypeError("ES3 does not support getters and setters.");
                  b != Array.prototype && b != Object.prototype && (b[c] = a.value)
                }, h = "undefined" != typeof window && window === this ? this : "undefined" != typeof global && null != global ? global : this, k = ["String", "prototype", "repeat"], l = 0; l < k.length - 1; l++) {
                var m = k[l];
                m in h || (h[m] = {});
                h = h[m]
              }
              var n = k[k.length - 1],
                p = h[n],
                q = p ? p : function(b) {
                  var c;
                  if (null == this) throw new TypeError("The 'this' value for String.prototype.repeat must not be null or undefined");
                  c = this + "";
                  if (0 > b || 1342177279 < b) throw new RangeError("Invalid count value");
                  b |= 0;
                  for (var a = ""; b;)
                    if (b & 1 && (a += c), b >>>= 1) c += c;
                  return a
                };
              q != p && null != q && g(h, n, {
                configurable: !0,
                writable: !0,
                value: q
              });
              var t = this;

              function u(b, c) {
                var a = b.split("."),
                  d = t;
                a[0] in d || !d.execScript || d.execScript("var " + a[0]);
                for (var e; a.length && (e = a.shift());) a.length || void 0 === c ? d[e] ? d = d[e] : d = d[e] = {} : d[e] = c
              };

              function v(b) {
                var c = b.length;
                if (0 < c) {
                  for (var a = Array(c), d = 0; d < c; d++) a[d] = b[d];
                  return a
                }
                return []
              };

              function w(b) {
                var c = window;
                if (c.addEventListener) c.addEventListener("load", b, !1);
                else if (c.attachEvent) c.attachEvent("onload", b);
                else {
                  var a = c.onload;
                  c.onload = function() {
                    b.call(this);
                    a && a.call(this)
                  }
                }
              };
              var x;

              function y(b, c, a, d, e) {
                this.h = b;
                this.j = c;
                this.l = a;
                this.f = e;
                this.g = {
                  height: window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight,
                  width: window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth
                };
                this.i = d;
                this.b = {};
                this.a = [];
                this.c = {}
              }

              function z(b, c) {
                var a, d, e = c.getAttribute("data-pagespeed-url-hash");
                if (a = e && !(e in b.c))
                  if (0 >= c.offsetWidth && 0 >= c.offsetHeight) a = !1;
                  else {
                    d = c.getBoundingClientRect();
                    var f = document.body;
                    a = d.top + ("pageYOffset" in window ? window.pageYOffset : (document.documentElement || f.parentNode || f).scrollTop);
                    d = d.left + ("pageXOffset" in window ? window.pageXOffset : (document.documentElement || f.parentNode || f).scrollLeft);
                    f = a.toString() + "," + d;
                    b.b.hasOwnProperty(f) ? a = !1 : (b.b[f] = !0, a = a <= b.g.height && d <= b.g.width)
                  } a && (b.a.push(e),
                  b.c[e] = !0)
              }
              y.prototype.checkImageForCriticality = function(b) {
                b.getBoundingClientRect && z(this, b)
              };
              u("pagespeed.CriticalImages.checkImageForCriticality", function(b) {
                x.checkImageForCriticality(b)
              });
              u("pagespeed.CriticalImages.checkCriticalImages", function() {
                A(x)
              });

              function A(b) {
                b.b = {};
                for (var c = ["IMG", "INPUT"], a = [], d = 0; d < c.length; ++d) a = a.concat(v(document.getElementsByTagName(c[d])));
                if (a.length && a[0].getBoundingClientRect) {
                  for (d = 0; c = a[d]; ++d) z(b, c);
                  a = "oh=" + b.l;
                  b.f && (a += "&n=" + b.f);
                  if (c = !!b.a.length)
                    for (a += "&ci=" + encodeURIComponent(b.a[0]), d = 1; d < b.a.length; ++d) {
                      var e = "," + encodeURIComponent(b.a[d]);
                      131072 >= a.length + e.length && (a += e)
                    }
                  b.i && (e = "&rd=" + encodeURIComponent(JSON.stringify(B())), 131072 >= a.length + e.length && (a += e), c = !0);
                  C = a;
                  if (c) {
                    d = b.h;
                    b = b.j;
                    var f;
                    if (window.XMLHttpRequest) f =
                      new XMLHttpRequest;
                    else if (window.ActiveXObject) try {
                      f = new ActiveXObject("Msxml2.XMLHTTP")
                    } catch (r) {
                      try {
                        f = new ActiveXObject("Microsoft.XMLHTTP")
                      } catch (D) {}
                    }
                    f && (f.open("POST", d + (-1 == d.indexOf("?") ? "?" : "&") + "url=" + encodeURIComponent(b)), f.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"), f.send(a))
                  }
                }
              }

              function B() {
                var b = {},
                  c;
                c = document.getElementsByTagName("IMG");
                if (!c.length) return {};
                var a = c[0];
                if (!("naturalWidth" in a && "naturalHeight" in a)) return {};
                for (var d = 0; a = c[d]; ++d) {
                  var e = a.getAttribute("data-pagespeed-url-hash");
                  e && (!(e in b) && 0 < a.width && 0 < a.height && 0 < a.naturalWidth && 0 < a.naturalHeight || e in b && a.width >= b[e].o && a.height >= b[e].m) && (b[e] = {
                    rw: a.width,
                    rh: a.height,
                    ow: a.naturalWidth,
                    oh: a.naturalHeight
                  })
                }
                return b
              }
              var C = "";
              u("pagespeed.CriticalImages.getBeaconData", function() {
                return C
              });
              u("pagespeed.CriticalImages.Run", function(b, c, a, d, e, f) {
                var r = new y(b, c, a, e, f);
                x = r;
                d && w(function() {
                  window.setTimeout(function() {
                    A(r)
                  }, 0)
                })
              });
            })();

            pagespeed.CriticalImages.Run('/mod_pagespeed_beacon', 'https://preview.colorlib.com/theme/bootstrap/login-form-07/', '-ilGEe-FWC', true, false, 'wiBGWA7VhJs');
            //]]>
          </script><img src="https://preview.colorlib.com/theme/bootstrap/login-form-07/images/undraw_remotely_2j6y.svg" alt="Image" class="img-fluid" data-pagespeed-url-hash="4289692523" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
        </div>
        <div class="col-md-6 contents">
          <div class="row justify-content-center">
            <div class="col-md-8">
              <div class="mb-4">
                <h3>Sign In</h3>
                <p class="mb-4">Login Admin.</p>
              </div>
                <div class="form-group first">
                  <label for="username">Username</label>
                    <input type="text" class="form-control" placeholder="Username" id="username" name="username" required="" />                  
                </div>
                <div class="form-group last mb-4">
                  <label for="password">Password</label>
                    <input type="password" class="form-control" placeholder="Password" id="password" name="password" required="" />                  
                </div>
                <div class="d-flex mb-5 align-items-center">
                  <label class="control control--checkbox mb-0"><span class="caption">Remember me</span>
                    <input type="checkbox" checked />
                    <div class="control__indicator"></div>
                  </label>
                </div>
                <input type="submit" value="Log In"  id="loginBtn" class="btn btn-block btn-primary">

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script src="https://preview.colorlib.com/theme/bootstrap/login-form-07/js/jquery-3.3.1.min.js"></script>
  <script src="https://preview.colorlib.com/theme/bootstrap/login-form-07/js/popper.min.js+bootstrap.min.js+main.js.pagespeed.jc.9rL6_qf-nt.js"></script>
  <script>
    eval(mod_pagespeed_$$cYUBAGVu);
  </script>
  <script>
    eval(mod_pagespeed_MR4O_w3mta);
  </script>
  <script>
    eval(mod_pagespeed_DPOuBfjpPE);
  </script>
  <script defer src="https://static.cloudflareinsights.com/beacon.min.js/v652eace1692a40cfa3763df669d7439c1639079717194" integrity="sha512-Gi7xpJR8tSkrpF7aordPZQlW2DLtzUlZcumS8dMQjwDHEnw9I7ZLyiOj/6tZStRBGtGgN6ceN6cMH8z7etPGlw==" data-cf-beacon='{"rayId":"73923e6fefbf4202","token":"cd0b4b3a733644fc843ef0b185f98241","version":"2022.6.0","si":100}' crossorigin="anonymous"></script>
</body>
<script src="<?= $bu; ?>assets/admin//vendors/jquery/dist/jquery.min.js"></script>
  <!-- Bootstrap -->
  <script src="<?= $bu; ?>assets/admin//vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <!-- FastClick -->
  <script src="<?= $bu; ?>assets/admin//vendors/fastclick/lib/fastclick.js"></script>
  <!-- NProgress -->
  <script src="<?= $bu; ?>assets/admin//vendors/nprogress/nprogress.js"></script>

  <!-- Custom Theme Scripts -->
  <script src="<?= $bu; ?>assets/admin//build/js/custom.min.js"></script>
  <script src="<?= $bu; ?>assets/admin/vendors/datatables.net/js/jquery.dataTables.js"></script>
  <script src="<?= $bu; ?>assets/admin/vendors/datatables.net-responsive/js/dataTables.responsive.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>

</html>
<script type="text/javascript">
  document.addEventListener("DOMContentLoaded", function(event) {

    $('#loginBtn').on('click', function() {

      var username = $('#username').val();
      var password = $('#password').val();
      var win = '<?= $bu ?>Admin'
      // console.log(username,password)
      // return false

      if (username.length < 1 || password.length < 1) {
        var message = 'Silahkan ketikkan username dan password Anda.';
        $('#alertNotif').html('<div class="alert alert-danger alert-dismissible show" role="alert"><span>' + message + '</span><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');

      } else {
        $('#loginBtn').html('<i class="fas fa-spinner fa-spin"></i> Tunggu..');
        // alert("gagal-")
        $.ajax({
          type: "POST",
          dataType: 'json',
          url: '<?= $bu; ?>/LoginAdmin/login_proses',
          data: {
            username: username,
            password: password,
          },
        }).done(function(e) {
          if (e.status) {
            Swal.fire(
              'Login Success!',
              e.message,
              'success'
            )
            setTimeout(() => {
              window.location = win;
            }, 1000);

          } else {
            
            Swal.fire(
              'Login Gagal!',
              e.message,
              'error'
            )
            
          }
        }).fail(function(e) {
          Swal.fire(
            'Login Gagal!',
            e.message,
            'error'
          )
        });
      }
      // return false;;
    });
  });
</script>