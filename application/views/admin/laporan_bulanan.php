<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?= $title;?></title>
        <style>
            #table {
                font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
                border-collapse: collapse;
                width: 100%;
            }

            #table td, #table th {
                border: 1px solid #ddd;
                padding: 8px;
            }

            #table tr:nth-child(even){background-color: #f2f2f2;}

            #table tr:hover {background-color: #ddd;}

            #table th {
                padding-top: 10px;
                padding-bottom: 10px;
                text-align: left;
                background-color: #4CAF50;
                color: white;
            }
        </style>
    </head>
    <body>
        <div style="text-align:center">
					<h5 class="card-title text-center">Bulanan</h5>
					<h6 class="card-title text-center"><?= $bulan ?>/<?= $tahun?></h6>
        </div>
        <table id="table">
            <thead>
                	<tr>
										<th>No</th>
										<th>Nama Menu</th>
										<th>Harga</th>
										<th>QTY</th>
										<th>Total</th>
									</tr>
            </thead>
            <tbody>
							<?php 
								$no =1;
								$grand =0;
								foreach ($data as $key => $value) {		
									$grand +=$value->total;
								?>
									<tr>
										<td><?= $no++  ?></td>
										<td><?= $value->nama_menu ?></td>
										<td><?= convert_to_rupiah($value->harga) ?></td>
										<td><?= $value->qty ?></td>
										<td><?= convert_to_rupiah($value->total) ?></td>
									</tr>
									<?php } ?>

								</tbody>
								
							</table>
							<h3>Total Keseluruhan : <?= convert_to_rupiah($grand) ?></h3>
    </body>
</html>
