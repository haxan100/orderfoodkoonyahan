            <?php
			defined('BASEPATH') or exit('No direct script access allowed');
			// $title = array_key_exists('title', $header) ? $header['title'] : $ci->namaAplikasi();
			// $page = array_key_exists('page', $header) ? $header['page'] : 'master_admin';
			// $js = array_key_exists('js', $header) ? $header['js'] : array();
			$bu = base_url();
			$role = $this->AdminModel->getRoleAll($this->session->userdata('id_admin'))->row();
			$logo = $this->AdminModel->getLogo()->row()->logo;
			// var_dump($logo);die;
			$cabang = $this->SemuaModel->getCabang();
			$ownerLogin = $_SESSION['owner_session'];



			$o = 'Owner';
			if(!$ownerLogin) $o = '';
			$nama_admin = $_SESSION['nama'];
			$link = 'Admin';
			if ($owner) $link  = 'Owner';
			?>

            <div class="col-md-3 left_col">
            	<div class="left_col scroll-view">
            		<div class="navbar nav_title" style="border: 0;">
            			<a href="<?= base_url() . $link ?>" class="site_title">
            				<img src="<?= base_url() ?>assets/images/admin/<?= $logo?>" class="img-fluid ${3|rounded-top,rounded-right,rounded-bottom,rounded-left,rounded-circle,|}" alt="">
            			</a>
            		</div>

            		<div class="clearfix"></div>

            		<!-- menu profile quick info -->
            		<div class="profile clearfix">
            			<div class="profile_info">
            				<span>Welcome, <?= $o ?>  </span>
            				<h2><?= $nama_admin  ?></h2>
            			</div>
            			<div class="clearfix"></div>
            		</div>
            		<!-- /menu profile quick info -->

            		<br />

            		<!-- sidebar menu -->
            		<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            			<div class="menu_section">
            				<h3>Menu</h3>
            				<ul class="nav side-menu">
            					<?php
								if ($role->data_admin == 1 || $role->data_kasir == 1 || $role->master_menu == 1 || $role->master_transaksi == 1) {

								?>
            						<li><a><i class="fa fa-home"></i> Master <span class="fa fa-chevron-down"></span></a>
            							<ul class="nav child_menu">
            								<?php
											if ($role->data_admin == 1) {
											?>
            									<li><a href="<?= $bu . $link ?>/master_admin">Master Admin</a></li>
            									<li><a href="<?= $bu . $link ?>/master_cabang">Master Cabang</a></li>

            								<?php 	}
											if ($role->data_kasir == 1) {
											?>
            									<li><a href="<?= $bu . $link ?>/master_kasir">Master Kasir</a></li>
            								<?php 	}

											if ($role->master_menu == 1) {
											?>
            									<!-- <li><a href="<?= $bu .$link?>/master_kategori">Master Kategori</a></li> -->
            									<li><a href="<?= $bu . $link ?>/master_item">Master Item</a></li>
            									<li><a href="<?= $bu . $link ?>/master_meja">Master Meja</a></li>
            								<?php 	}											
											?>
            							</ul>
            						</li>

            					<?php 	}




								if ($role->seeting == 1 || $role->data_admin == 1 || $role->histori == 1) {
								?>
            						<li><a><i class="fa fa-home"></i> Seting <span class="fa fa-chevron-down"></span></a>
            							<ul class="nav child_menu">
            								<?php
											if ($role->seeting == 1) {
											?>
            									<li><a href="<?= $bu . $link ?>/setting">Setting App</a></li>
            								<?php 	}
											if ($role->data_admin == 1) {
											?>
            									<li><a href="<?= $bu . $link ?>/master_role">Role</a></li>
            								<?php 	}
											if ($role->histori == 1) {
											?>
            									<li><a href="<?= $bu . $link ?>/master_histori">Histori</a></li>
            								<?php 	}

											if ($role->data_admin == 1) {
											?>
            									<li><a href="<?= $bu . $link ?>/master_slider">Slider Menu</a></li>

            									<li><a href="<?= $bu . $link ?>/logo">Logo</a></li>
            								<?php 	}
											?>
            							</ul>
            						</li>
            					<?php 	}
								
									?>


            					<!-- <li> <a href="<?= $bu . $link ?>/master_laporan"><i class="fa fa-home"></i>Laporan</a></li> -->

								<li>
									
									<?php 
								if ($role->master_transaksi == 1 && !$ownerLogin) {
											?>
								<a><i class="fa fa-home"></i> Laporan <span class="fa fa-chevron-down"></span></a>
								
									<ul class="nav child_menu">
            									<li><a href="<?= $bu . $link ?>/master_transaksi">Transaksi Online</a></li>
            									<li><a href="<?= $bu . $link ?>/master_transaksi_offline">Transaksi Offline</a></li>
															</ul>
            								<?php 	}
											?>
								</li>



								
            					<!-- <li> <a href="<?= $bu . $link ?>/master_penjualan_menu"><i class="fa fa-home"></i>Laporan Menu Terjual</a></li> -->
								<?php if($owner){ ?>
            					<li class=""><a><i class="fa fa-home"></i> Transk Online Cabang<span class="fa fa-chevron-down"></span></a>
											<ul class="nav child_menu" style="display: none;">
										<?php 
										foreach ($cabang as $key => $v) {
											?>
            									<li><a href="<?= $bu . $link ?>/master_transaksi/<?= $v->id_cabang ?>"><?= $v->cabang ?></a></li>
											<?php
										}
										?>
            						</ul>
            					</li>
								<?php
								?>
            					<li class=""><a><i class="fa fa-home"></i> Transak Ofline Cabang<span class="fa fa-chevron-down"></span></a>
            						<ul class="nav child_menu" style="display: none;">
									<?php 
										foreach ($cabang as $key => $v) {
											?>
            						<li><a href="<?= $bu . $link ?>/master_transaksi_offline/<?= $v->id_cabang ?>"><?= $v->cabang ?></a></li>
											<?php
										}
										?>
            						</ul>
            					</li>
								<?php }?>
            				</ul>
            			</div>
            		</div>
            	</div>
            </div>
            <!-- top navigation -->
            <div class="top_nav">
            	<div class="nav_menu">
            		<div class="nav toggle">
            			<a id="menu_toggle"><i class="fa fa-bars"></i></a>
            		</div>
            		<nav class="nav navbar-nav">
            			<ul class=" navbar-right">
            				<li class="nav-item dropdown open" style="padding-left: 15px;">
            					<a href="javascript:;" class="user-profile dropdown-toggle" aria-haspopup="true" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
            						<img src="<?= base_url() ?>assets/images/admin/admin.png" alt=""><?= $nama_admin  ?>
            					</a>
            					<div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
            						<a class="dropdown-item" id="keluar"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
            					</div>
            				</li>

            			</ul>
            		</nav>
            	</div>
            </div>
            <!-- /top navigation -->
