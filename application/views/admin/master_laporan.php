<div class="col-md-12 col-sm-12 ">
	<div class="x_panel">
		<div class="x_title">
			<h2>Data Laporan</h2>
			<style>
				#image {
					max-width: 100px;
					display: block;
				}
			</style>

			<div class="clearfix"></div>
		</div>
		<div class="x_content">
			<div class="row">

				<div class="col-sm-12 xs-12 md-12 xss-12 ">
					<div class="card">
						<div class="card-body">
							<?php							
							echo	form_open('Admin/lihat_laporan_harian');
							
							?>
							<h5 class="card-title text-center">Harian</h5>
							<div class="row">
								<div class="col-sm-4">
									<label for="input" class="col-sm-4 control-label">Tanggal:</label>
										<select name="tanggal" id="tanggal" class="form-control" required="required">
											<?php
											for ($i=1; $i <=31 ; $i++) { 
												?>
												<option value=<?= $i ?>><?= $i ?></option>
												<?php
											}
											?>
										</select>
								</div>
								<div class="col-sm-4">
										<label for="input" class="col-sm-4 control-label">Bulan:</label>
										<select name="bulan" id="bulan" class="form-control" required="required">
											<?php
											for ($i=1; $i <=12 ; $i++) { 
												?>
												<option value=<?= $i ?>><?= $i ?></option>
												<?php
											}
											?>
										</select>
								</div>
								<div class="col-sm-4">
										<label for="input" class="col-sm-4 control-label">Tahun:</label>
										<select name="tahun" id="tahun" class="form-control" required="required">
										<?php
											for ($i=2022; $i <=2025 ; $i++) { 
												?>
												<option value=<?= $i ?>><?= $i ?></option>
												<?php
											}
											?>
											
										</select>
									</div>
							</div>
						</div>
					</div>
					<button class="btn btn-primary btn-block" type="submit">Cari Data</button>
				</div>
				<?php
				echo form_close();

				?>

				<div class="col-sm-12 xs-12 md-12 xss-12 ">
					<div class="card">
						<div class="card-body">
							<?php							
							echo	form_open('Admin/lihat_laporan_bulanan');
							
							?>
							<h5 class="card-title text-center">Bulanan</h5>
							<div class="row">
								<div class="col-sm-6">
										<label for="input" class="col-sm-4 control-label">Bulan:</label>
										<select name="bulan" id="bulan" class="form-control" required="required">
											<?php
											for ($i=1; $i <=12 ; $i++) { 
												?>
												<option value=<?= $i ?>><?= $i ?></option>
												<?php
											}
											?>
										</select>
								</div>
								<div class="col-sm-6">
										<label for="input" class="col-sm-4 control-label">Tahun:</label>
										<select name="tahun" id="tahun" class="form-control" required="required">
										<?php
											for ($i=2022; $i <=2025 ; $i++) { 
												?>
												<option value=<?= $i ?>><?= $i ?></option>
												<?php
											}
											?>
											
										</select>
									</div>
							</div>
						</div>
					</div>
					<button class="btn btn-primary btn-block" type="submit">Cari Data</button>
				</div>
				<?php
				echo form_close();

				?>

				<div class="col-sm-12 xs-12 md-12 xss-12 ">
					<div class="card">
						<div class="card-body">
							<?php							
							echo	form_open('Admin/lihat_laporan_tahunan');
							
							?>
							<h5 class="card-title text-center">Tahunan</h5>
							<div class="row">
								<div class="col-sm-12">
										<label for="input" class="col-sm-4 control-label">Tahun:</label>
										<select name="tahun" id="tahun" class="form-control" required="required">
										<?php
											for ($i=2022; $i <=2025 ; $i++) { 
												?>
												<option value=<?= $i ?>><?= $i ?></option>
												<?php
											}
											?>
											
										</select>
									</div>
							</div>
						</div>
					</div>
					<button class="btn btn-primary btn-block" type="submit">Cari Data</button>
				</div>


			</div>
		</div>
	</div>
</div>
<?php
echo form_close();

?>
<script type="text/javascript">
	$(document).ready(function () {

	});
</script>
