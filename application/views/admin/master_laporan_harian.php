<div class="col-md-12 col-sm-12 ">
	<div class="x_panel">
		<div class="x_title">
			<h2>Laporan Harian</h2>
			<style>
				#image {
					max-width: 100px;
					display: block;
				}
			</style>

			<div class="clearfix"></div>
		</div>
		<div class="x_content">
			<div class="row">

				<div class="col-sm-12 xs-12 md-12 xss-12 ">
					<div class="card">
						<div class="card-body">
							<h5 class="card-title text-center">Harian</h5>
							<h6 class="card-title text-center"><?= $tanggal ?>/<?= $bulan ?>/<?= $tahun?></h6>
							<table class="table thead-dark">
								<tbody>
									<tr>
										<td>No</td>
										<td>Cabang</td>
										<td>Nama Menu</td>
										<td>Harga</td>
										<td>QTY</td>
										<td>Total</td>
									</tr>
								<?php 
								$no =1;
								$grand =0;
								foreach ($data as $key => $value) {	
									$grand +=$value->total;
								?>
									<tr>
										<th><?= $no++  ?></th>
										<th><?= $value->cabang ?></th>
										<th><?= $value->nama_menu ?></th>
										<th><?= convert_to_rupiah($value->harga) ?></th>
										<th><?= $value->qty ?></th>
										<th><?= convert_to_rupiah($value->total) ?></th>
									</tr>
									<?php } ?>

								</tbody>
							</table>
							
							<h3>Total Keseluruhan : <?= convert_to_rupiah($grand) ?></h3>

						</div>
					</div>
					<button class="btn btn-primary btn-block cetakPrint"  type="submit">Cetak</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	document.addEventListener("DOMContentLoaded", function(event) {
		
		console.log("firstdddd")
		$('.cetakPrint').click(function (e) { 

			link = '<?= base_url() ?>/Admin/laporan_harian_pdf/?tanggal=<?= $tanggal ?>&bulan=<?=$bulan?>&tahun=<?=$tahun?>'; 
				window.open(`${link}`);
			
		});
	});
</script>
