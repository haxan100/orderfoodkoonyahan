<!doctype html>
<html lang="en">

<?php
    defined('BASEPATH') or exit('No direct script access allowed');
    // $title = array_key_exists('title', $header) ? $header['title'] : $ci->namaAplikasi();
    // $page = array_key_exists('page', $header) ? $header['page'] : 'master_admin';
    // $js = array_key_exists('js', $header) ? $header['js'] : array();
    $bu = base_url();
    ?>		
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://preview.colorlib.com/theme/bootstrap/login-form-02/fonts,_icomoon,_style.css+css,_owl.carousel.min.css+css,_bootstrap.min.css+css,_style.css.pagespeed.cc.12uEfYIPIx.css" />


  <title>Login #2</title>
  <script nonce="e2bc025a-8470-4660-8f57-6b8bed75c54b">
    (function(w, d) {
      ! function(a, e, t, r) {
        a.zarazData = a.zarazData || {};
        a.zarazData.executed = [];
        a.zaraz = {
          deferred: []
        };
        a.zaraz.q = [];
        a.zaraz._f = function(e) {
          return function() {
            var t = Array.prototype.slice.call(arguments);
            a.zaraz.q.push({
              m: e,
              a: t
            })
          }
        };
        for (const e of ["track", "set", "ecommerce", "debug"]) a.zaraz[e] = a.zaraz._f(e);
        a.zaraz.init = () => {
          var t = e.getElementsByTagName(r)[0],
            z = e.createElement(r),
            n = e.getElementsByTagName("title")[0];
          n && (a.zarazData.t = e.getElementsByTagName("title")[0].text);
          a.zarazData.x = Math.random();
          a.zarazData.w = a.screen.width;
          a.zarazData.h = a.screen.height;
          a.zarazData.j = a.innerHeight;
          a.zarazData.e = a.innerWidth;
          a.zarazData.l = a.location.href;
          a.zarazData.r = e.referrer;
          a.zarazData.k = a.screen.colorDepth;
          a.zarazData.n = e.characterSet;
          a.zarazData.o = (new Date).getTimezoneOffset();
          a.zarazData.q = [];
          for (; a.zaraz.q.length;) {
            const e = a.zaraz.q.shift();
            a.zarazData.q.push(e)
          }
          z.defer = !0;
          for (const e of [localStorage, sessionStorage]) Object.keys(e || {}).filter((a => a.startsWith("_zaraz_"))).forEach((t => {
            try {
              a.zarazData["z_" + t.slice(7)] = JSON.parse(e.getItem(t))
            } catch {
              a.zarazData["z_" + t.slice(7)] = e.getItem(t)
            }
          }));
          z.referrerPolicy = "origin";
          z.src = "/cdn-cgi/zaraz/s.js?z=" + btoa(encodeURIComponent(JSON.stringify(a.zarazData)));
          t.parentNode.insertBefore(z, t)
        };
        ["complete", "interactive"].includes(e.readyState) ? zaraz.init() : a.addEventListener("DOMContentLoaded", zaraz.init)
      }(w, d, 0, "script");
    })(window, document);
  </script>
</head>

<body>
  <div class="d-lg-flex half">
    <div class="bg order-1 order-md-2" style="background-image:url(https://media.istockphoto.com/vectors/female-small-business-owner-business-concept-vector-illustration-vector-id1326771590?k=20&m=1326771590&s=612x612&w=0&h=e-eMG2sN35XPjQwXTA9Tf2THopT3GuHPSwupt1ajVe0=)"></div>
    <div class="contents order-2 order-md-1">
      <div class="container">
        <div class="row align-items-center justify-content-center">
          <div class="col-md-7">
            <h3> <strong>Login</strong></h3>
            <p class="mb-4">Owner.</p>
              <div class="form-group first">
                <label for="username">Username</label>
                <input type="text" class="form-control" placeholder="your-email@gmail.com" id="username">
              </div>
              <div class="form-group last mb-3">
                <label for="password">Password</label>
                <input type="password" class="form-control" placeholder="Your Password" id="password">
              </div>
              <div class="d-flex mb-5 align-items-center">
                <label class="control control--checkbox mb-0"><span class="caption">Remember me</span>
                  <input type="checkbox" checked />
                  <div class="control__indicator"></div>
                </label>
              </div>
              <input type="submit" value="Log In" id="loginBtn" class="btn btn-block btn-primary">
          </div>
        </div>
      </div>
    </div>
  </div>
      <!-- jQuery -->
	<script src="<?= $bu; ?>assets/admin//vendors/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap -->
	<script src="<?= $bu; ?>assets/admin//vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
	<!-- FastClick -->
	<script src="<?= $bu; ?>assets/admin//vendors/fastclick/lib/fastclick.js"></script>
	<!-- NProgress -->
	<script src="<?= $bu; ?>assets/admin//vendors/nprogress/nprogress.js"></script>

	<!-- Custom Theme Scripts -->
	<script src="<?= $bu; ?>assets/admin//build/js/custom.min.js"></script>
	<script src="<?= $bu; ?>assets/admin/vendors/datatables.net/js/jquery.dataTables.js"></script>
	<script src="<?= $bu; ?>assets/admin/vendors/datatables.net-responsive/js/dataTables.responsive.js"></script>
	<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
  <script>
    eval(mod_pagespeed_U0drreBoo3);
  </script>
  <script>
    eval(mod_pagespeed_ECLkrTsSNR);
  </script>
  <script>
    eval(mod_pagespeed_muSzTF93jG);
  </script>
  <script defer src="https://static.cloudflareinsights.com/beacon.min.js/v652eace1692a40cfa3763df669d7439c1639079717194" integrity="sha512-Gi7xpJR8tSkrpF7aordPZQlW2DLtzUlZcumS8dMQjwDHEnw9I7ZLyiOj/6tZStRBGtGgN6ceN6cMH8z7etPGlw==" data-cf-beacon='{"rayId":"739254d7ef0273c3","token":"cd0b4b3a733644fc843ef0b185f98241","version":"2022.6.0","si":100}' crossorigin="anonymous"></script>
</body>

</html><script type="text/javascript">
	document.addEventListener("DOMContentLoaded", function(event) { 

    $('#loginBtn').on('click', function() {
			login()
		});

		$('#password').keypress(function (e) {
		if (e.which == 13) {
			login()
			
			return false;  
		}
	});

		function login() {
			
        var username = $('#username').val();
        var password = $('#password').val();
        var win = '<?= $bu ?>Owner'
        // console.log(username,password)
        // return false

        if (username.length < 1 || password.length < 1) {
            var message = 'Silahkan ketikkan username dan password Anda.';
            $('#alertNotif').html('<div class="alert alert-danger alert-dismissible show" role="alert"><span>' + message + '</span><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');

        } else {
            $('#loginBtn').html('<i class="fas fa-spinner fa-spin"></i> Tunggu..');
            $('#loginBtn').prop('disabled', true);
            // alert("gagal-")
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: '<?= $bu; ?>/LoginOwner/login_proses',
                data: {
                    username: username,
                    password: password,
                },
            }).done(function(e) {
                   
                if (e.status) {
                    $('#username').val('');
                    $('#password').val('');
                    $('#captcha').val('');
                    $('#alertNotif').html('<div class="alert alert-success alert-dismissible show" role="alert"><span>' + e.message + '</span><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');

                    Swal.fire(
											'Login Success!',
											e.message,
											'success'
										)

                    setTimeout(() => {
                        window.location = win ;
                    }, 1000);
                    
                } else {
                   
									 Swal.fire(
											'Login Gagal!',
											e.message,
											'error'
										);
								}
            }).fail(function(e) {
                
                    Swal.fire(
						'Login Gagal!',
						e.message,
						'error'
					)
                var message = 'Terjadi Kesalahan.';
                $('#alertNotif').html('<div class="alert alert-danger alert-dismissible show" role="alert"><span>' + message + '</span><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            }).always(function() {
                toAlert();
                resetButton()
            });
        }
	}

    });

</script>
