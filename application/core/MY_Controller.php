<?php
defined('BASEPATH') or exit('No direct script access allowed');

// var_dump(APPPATH . 'third_party/vendor/autoload.php');die;
require_once(APPPATH . 'third_party/vendor/autoload.php');
// require_once(APPPATH . 'third_party/fpdf/fpdf.php');require_once(APPPATH . 'third_party/fpdi/fpdi.php');
include 'vendor/autoload.php';



use setasign\Fpdi\Fpdi;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class MY_Controller extends CI_Controller
{
		public function qrcode($filename,$data)
	{
		$this->load->library('ciqrcode');
		$config['cacheable'] = true; //boolean, the default is true
		$config['cachedir'] = './uploads/'; //string, the default is application/cache/
		$config['errorlog'] = './uploads/'; //string, the default is application/logs/
		$config['imagedir'] = './uploads/images/'; //direktori penyimpanan qr code
		$config['quality'] = true; //boolean, the default is true
		$config['size'] = '1024'; //interger, the default is 1024
		$config['black'] = array(224, 255, 255); // array, default is array(255,255,255)
		$config['white'] = array(70, 130, 180); // array, default is array(0,0,0)
		$this->ciqrcode->initialize($config);

		$image_name = "$filename" . '.png'; //buat name dari qr code sesuai dengan nim

		$params['data'] = "$data"; //data yang akan di jadikan QR CODE
		$params['level'] = 'H'; //H=High
		$params['size'] = 10;
		$params['savename'] = FCPATH . $config['imagedir'] . $image_name; //simpan image QR CODE ke folder assets/images/
		$this->ciqrcode->generate($params); // fungsi untuk generate QR CODE

	}

	function generateRandomString($length = 10)
{
	$characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$charactersLength = strlen($characters);
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, $charactersLength - 1)];
	}
	$tgl = date('Y');

	// var_dump($tgl);die;


	return "TR_" . $tgl . $randomString;
}
	function generateRandomMeja($length = 10)
{
	$characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$charactersLength = strlen($characters);
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, $charactersLength - 1)];
	}
	$tgl = date('y');
	return "MJ_" . $tgl . $randomString;
}
	
}
