<?php 

defined('BASEPATH') OR exit('No direct script access allowed');
                        
class MejaModel extends CI_Model {

	public function data($post)
	{
		$columns = array(
			'cabang',	
			'nomor_meja',	
			'link',
		);		
		$columnsSearch = array(
			'nomor_meja',		
			'link',
			'qr_code'
		);
		// gunakan join disini
		$from = 'meja m';
		// custom SQL
		$sql = "SELECT m.*,c.cabang FROM {$from} 
		left join cabang c on m.id_cabang = c.id_cabang";

		$where = "";
		$whereTemp = "";

		if (isset($post['cabang']) && $post['cabang'] != 'default') {
			if ($where != "") $where .= "AND";
			$where .= " (m.id_cabang='" . $post['cabang'] . "')";
		}


		if ($whereTemp != '' && $where != '') $where .= " AND (" . $whereTemp . ")";
		else if ($whereTemp != '') $where .= $whereTemp;
		// search
		if (isset($post['search']['value']) && $post['search']['value'] != '') {
			$search = $post['search']['value'];
			// create parameter pencarian kesemua kolom yang tertulis
			// di $columns
			$whereTemp = "";
			for ($i = 0; $i < count($columnsSearch); $i++) {
				$whereTemp .= $columnsSearch[$i] . ' LIKE "%' . $search . '%"';
				// agar tidak menambahkan 'OR' diakhir Looping
				if ($i < count($columnsSearch) - 1) {
					$whereTemp .= ' OR ';
				}
			}
			if ($where != '') $where .= " AND (" . $whereTemp . ")";
			else $where .= $whereTemp;
		}
		if ($where != '') $sql .= ' and (' . $where . ')';
		//SORT Kolom
		$sortColumn = isset($post['order'][0]['column']) ? $post['order'][0]['column'] : 1;
		$sortDir    = isset($post['order'][0]['dir']) ? $post['order'][0]['dir'] : 'asc';
		$sortColumn = $columns[$sortColumn - 1];
		$sql .= " ORDER BY {$sortColumn} {$sortDir}";
		$count = $this->db->query($sql);
		$totaldata = $count->num_rows();
		$start  = isset($post['start']) ? $post['start'] : 0;
		$length = isset($post['length']) ? $post['length'] : 10;
		$sql .= " LIMIT {$start}, {$length}";
		// echo $this->db->last_query();die;
		$data  = $this->db->query($sql);

		return array(
			'totalData' => $totaldata,
			'data' => $data,
		);
	}  
	public function dataCabang($post)
	{
		$id_cabang = $this->session->userdata('id_cabang');
		$columns = array(
			'nomor_meja',	
			'link',
			'qrcode'
		);		
		$columnsSearch = array(
			'nomor_meja',		
			'link',
			'qrcode'
		);
		// gunakan join disini
		$from = 'meja m';
		// custom SQL
		$sql = "SELECT* FROM {$from} where id_cabang = '$id_cabang' ";
		$where = "";
		$whereTemp = "";




		if ($whereTemp != '' && $where != '') $where .= " AND (" . $whereTemp . ")";
		else if ($whereTemp != '') $where .= $whereTemp;
		// search
		if (isset($post['search']['value']) && $post['search']['value'] != '') {
			$search = $post['search']['value'];
			// create parameter pencarian kesemua kolom yang tertulis
			// di $columns
			$whereTemp = "";
			for ($i = 0; $i < count($columnsSearch); $i++) {
				$whereTemp .= $columnsSearch[$i] . ' LIKE "%' . $search . '%"';
				// agar tidak menambahkan 'OR' diakhir Looping
				if ($i < count($columnsSearch) - 1) {
					$whereTemp .= ' OR ';
				}
			}
			if ($where != '') $where .= " AND (" . $whereTemp . ")";
			else $where .= $whereTemp;
		}
		if ($where != '') $sql .= ' And (' . $where . ')';
		//SORT Kolom
		$sortColumn = isset($post['order'][0]['column']) ? $post['order'][0]['column'] : 1;
		$sortDir    = isset($post['order'][0]['dir']) ? $post['order'][0]['dir'] : 'asc';
		$sortColumn = $columns[$sortColumn - 1];
		$sql .= " ORDER BY {$sortColumn} {$sortDir}";
		$count = $this->db->query($sql);
		$totaldata = $count->num_rows();
		$start  = isset($post['start']) ? $post['start'] : 0;
		$length = isset($post['length']) ? $post['length'] : 10;
		$sql .= " LIMIT {$start}, {$length}";
		// echo $this->db->last_query();die;
		$data  = $this->db->query($sql);

		return array(
			'totalData' => $totaldata,
			'data' => $data,
		);
	}  
                        
}
                        
/* End of file MenuModel.php */
    
                        