<?php 

defined('BASEPATH') OR exit('No direct script access allowed');
                        
class ProdukModel extends CI_Model {

	public function getProdukByIdTipeProduk($id_kategori,$page,$id_cabang,$prodduk='')
	{
		// var_dump($prodduk=='');die;
		$perHal = 8;
		$start = ($page - 1) * $perHal;
		$length =  $start + $perHal;
		$total  = $this->getProdukByIdTipeProdukCount($id_kategori,$id_cabang,$prodduk);
		$pages = ceil($total / $perHal);
		$this->db->select("m.*,kategori.id_kategori,kategori.nama_kategori")
			->from('menu m');
		$this->db->join('kategori', 'kategori.id_kategori = m.id_kategori', 'left')
		->where('m.stok > 0')
		->where('m.id_cabang', $id_cabang);		
		if ($id_kategori != 0) {
			$this->db->where('m.id_kategori', $id_kategori);
		}
		if ($prodduk != '') {
			$this->db->like('m.nama_menu', $prodduk);
		}
		$this->db->limit($perHal, $start);
		$query = $this->db->get()->result();
		$pagination = array(
			'total_halaman' => $pages,
			'halaman' => $page,
			'total_data' => $total, 
			'jumlah' => count($query)
		);
		// echo $this->db->last_query(); die;
		$output = array(
			'data' => $query,
            'page' => $pagination,
		);
		return $output;
	}
	public function getProdukByIdTipeProdukCount($id_kategori,$id_cabang,$prodduk='')
	{

		$this->db->select("*")
			->from('menu m');
		$this->db->join('kategori', 'kategori.id_kategori = m.id_kategori', 'left');
		$this->db->where('m.stok > 0')
		->where('m.id_cabang', $id_cabang);

		

		if ($id_kategori != 0) {
			$this->db->where('m.id_kategori', $id_kategori);
		}
		if ($prodduk != '') {
			$this->db->where('m.nama_menu', $prodduk);
		}
		$query = $this->db->get()->result();
		return count($query);
	}     
		public function getCountProdukById($id_produk)
	{
		$this->db->where('id_menu', $id_produk);
		$this->db->from('menu');
		$sql = $this->db->get();
		return $sql->num_rows();
		
		
	}    
		public function getProdukById($id_produk)
	{
		$this->db->where('id_menu', $id_produk);
		$this->db->from('menu');
		$sql = $this->db->get();
		return $sql->row();	
		
	}    
                            
                        
}
                        
/* End of file ProdukModel.php */
    
                        