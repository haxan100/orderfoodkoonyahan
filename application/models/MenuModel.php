<?php

defined('BASEPATH') or exit('No direct script access allowed');

class MenuModel extends CI_Model
{

	public function data_AllMenu($post)
	{
		$columns = array(
			'nama_menu',
			'cabang',
			'nama_kategori',
			'harga',
			'stok'

		);
		$columnsSearch = array(
			'nama_menu',
			'k.id_kategori',
			'harga',
			'm.foto'

		);
		// gunakan join disini
		$from = 'menu m';
		// custom SQL
		$sql = "SELECT  m.*,k.nama_kategori,c.cabang FROM {$from} 
		  join kategori k on k.id_kategori=m.id_kategori
		  left join cabang c on c.id_cabang=m.id_cabang

		";
		$where = "";

		if (isset($post['kategori']) && $post['kategori'] != 'default') {
			if ($where != "") $where .= "AND";
			// die("t");
			$where .= " (k.id_kategori='" . $post['kategori'] . "')";
		}
		$whereTemp = "";
		if ($whereTemp != '' && $where != '') $where .= " AND (" . $whereTemp . ")";
		else if ($whereTemp != '') $where .= $whereTemp;
		// search
		if (isset($post['search']['value']) && $post['search']['value'] != '') {
			$search = $post['search']['value'];
			// create parameter pencarian kesemua kolom yang tertulis
			// di $columns
			$whereTemp = "";
			for ($i = 0; $i < count($columnsSearch); $i++) {
				$whereTemp .= $columnsSearch[$i] . ' LIKE "%' . $search . '%"';
				// agar tidak menambahkan 'OR' diakhir Looping
				if ($i < count($columnsSearch) - 1) {
					$whereTemp .= ' OR ';
				}
			}
			if ($where != '') $where .= " AND (" . $whereTemp . ")";
			else $where .= $whereTemp;
		}
		if ($where != '') $sql .= ' WHERE (' . $where . ')';
		//SORT Kolom
		$sortColumn = isset($post['order'][0]['column']) ? $post['order'][0]['column'] : 1;
		$sortDir    = isset($post['order'][0]['dir']) ? $post['order'][0]['dir'] : 'asc';
		$sortColumn = $columns[$sortColumn - 1];
		$sql .= " ORDER BY {$sortColumn} {$sortDir}";
		$count = $this->db->query($sql);
		$totaldata = $count->num_rows();
		$start  = isset($post['start']) ? $post['start'] : 0;
		$length = isset($post['length']) ? $post['length'] : 10;
		$sql .= " LIMIT {$start}, {$length}";
		// echo $this->db->last_query();die;
		$data  = $this->db->query($sql);

		return array(
			'totalData' => $totaldata,
			'data' => $data,
		);
	}
	public function getKategori()
	{
		$this->db->from('kategori');
		$sql = $this->db->get();
		return $sql->result();

		# code...
	}
	public function data_AllMenuDetailLog($post)
	{
		$columns = array(
			'nama_menu',
			'qty',
			'total',
		);
		$columnsSearch = array(
			'nama_menu',
			'qty',
			'total',
		);
		// gunakan join disini
		$from = 'transaksi_detail dt';
		// custom SQL
		$sql = "SELECT* FROM {$from} join menu m on m.id_menu =dt.id_menu
		where dt.id_menu = {$post['id_menu']}";
		$where = "";
		$whereTemp = "";
		if ($whereTemp != '' && $where != '') $where .= " AND (" . $whereTemp . ")";
		else if ($whereTemp != '') $where .= $whereTemp;
		// search
		if (isset($post['search']['value']) && $post['search']['value'] != '') {
			$search = $post['search']['value'];
			// create parameter pencarian kesemua kolom yang tertulis
			// di $columns
			$whereTemp = "";
			for ($i = 0; $i < count($columnsSearch); $i++) {
				$whereTemp .= $columnsSearch[$i] . ' LIKE "%' . $search . '%"';
				// agar tidak menambahkan 'OR' diakhir Looping
				if ($i < count($columnsSearch) - 1) {
					$whereTemp .= ' OR ';
				}
			}
			if ($where != '') $where .= " AND (" . $whereTemp . ")";
			else $where .= $whereTemp;
		}
		if ($where != '') $sql .= ' WHERE (' . $where . ')';
		//SORT Kolom
		$sortColumn = isset($post['order'][0]['column']) ? $post['order'][0]['column'] : 1;
		$sortDir    = isset($post['order'][0]['dir']) ? $post['order'][0]['dir'] : 'asc';
		$sortColumn = $columns[$sortColumn - 1];
		$sql .= " ORDER BY {$sortColumn} {$sortDir}";
		$count = $this->db->query($sql);
		$totaldata = $count->num_rows();
		$start  = isset($post['start']) ? $post['start'] : 0;
		$length = isset($post['length']) ? $post['length'] : 10;
		$sql .= " LIMIT {$start}, {$length}";
		// echo $this->db->last_query();die;
		$data  = $this->db->query($sql);

		return array(
			'totalData' => $totaldata,
			'data' => $data,
		);
	}
	public function data_AllMenuByDate($post)
	{
		$columns = array(
			'nama_menu',
			'qty',
			'total',
		);
		$columnsSearch = array(
			'nama_menu',
		);
		// gunakan join disini
		$from = 'transaksi_detail dt';
		// custom SQL
		$sql = "SELECT dt.*,m.nama_menu FROM {$from} join menu m on m.id_menu =dt.id_menu
		where  dt.created_at like '%{$post['date']}%' ";
		$where = "";
		$whereTemp = "";
		if ($whereTemp != '' && $where != '') $where .= " AND (" . $whereTemp . ")";
		else if ($whereTemp != '') $where .= $whereTemp;
		// search
		if (isset($post['search']['value']) && $post['search']['value'] != '') {
			$search = $post['search']['value'];
			// create parameter pencarian kesemua kolom yang tertulis
			// di $columns
			$whereTemp = "";
			for ($i = 0; $i < count($columnsSearch); $i++) {
				$whereTemp .= $columnsSearch[$i] . ' LIKE "%' . $search . '%"';
				// agar tidak menambahkan 'OR' diakhir Looping
				if ($i < count($columnsSearch) - 1) {
					$whereTemp .= ' OR ';
				}
			}
			if ($where != '') $where .= " AND (" . $whereTemp . ")";
			else $where .= $whereTemp;
		}
		if ($where != '') $sql .= ' and (' . $where . ')';
		//SORT Kolom
		$sortColumn = isset($post['order'][0]['column']) ? $post['order'][0]['column'] : 1;
		$sortDir    = isset($post['order'][0]['dir']) ? $post['order'][0]['dir'] : 'asc';
		$sortColumn = $columns[$sortColumn - 1];
		$sql .= " ORDER BY {$sortColumn} {$sortDir}";
		$count = $this->db->query($sql);
		$totaldata = $count->num_rows();
		$start  = isset($post['start']) ? $post['start'] : 0;
		$length = isset($post['length']) ? $post['length'] : 10;
		$sql .= " LIMIT {$start}, {$length}";
		// echo $this->db->last_query();die;
		$data  = $this->db->query($sql);

		return array(
			'totalData' => $totaldata,
			'data' => $data,
		);
	}
	public function data_AllMenuCabang($post)
	{

		$id_cabang = $_SESSION['id_cabang'];

		$columns = array(
			'nama_menu',
			'k.id_kategori',
			'harga',
			'm.foto'
		);
		$columnsSearch = array(
			'nama_menu',
			'k.id_kategori',
			'harga',
			'm.foto'
		);
		// gunakan join disini
		$from = 'menu m';
		// custom SQL
		$sql = "SELECT m.*,k.nama_kategori FROM {$from}   
		join kategori k on k.id_kategori=m.id_kategori
		where  m.id_cabang = '{$id_cabang}' ";
		$where = "";
		// var_dump(
		// 		$post['kategori'],
		// 		isset($post['ketegori']) ,  
		// 		$post['ketegori'] != 'default'
		// );die;

		if (isset($post['kategori']) && $post['kategori'] != 'default') {
			if ($where != "") $where .= "AND";
			// die("t");
			$where .= " (k.id_kategori='" . $post['kategori'] . "')";
		}
		$whereTemp = "";
		if ($whereTemp != '' && $where != '') $where .= " AND (" . $whereTemp . ")";
		else if ($whereTemp != '') $where .= $whereTemp;
		// search
		if (isset($post['search']['value']) && $post['search']['value'] != '') {
			$search = $post['search']['value'];
			// create parameter pencarian kesemua kolom yang tertulis
			// di $columns
			$whereTemp = "";
			for ($i = 0; $i < count($columnsSearch); $i++) {
				$whereTemp .= $columnsSearch[$i] . ' LIKE "%' . $search . '%"';
				// agar tidak menambahkan 'OR' diakhir Looping
				if ($i < count($columnsSearch) - 1) {
					$whereTemp .= ' OR ';
				}
			}
			if ($where != '') $where .= " AND (" . $whereTemp . ")";
			else $where .= $whereTemp;
		}
		if ($where != '') $sql .= ' and (' . $where . ')';
		//SORT Kolom
		$sortColumn = isset($post['order'][0]['column']) ? $post['order'][0]['column'] : 1;
		$sortDir    = isset($post['order'][0]['dir']) ? $post['order'][0]['dir'] : 'asc';
		$sortColumn = $columns[$sortColumn - 1];
		$sql .= " ORDER BY {$sortColumn} {$sortDir}";
		$count = $this->db->query($sql);
		$totaldata = $count->num_rows();
		$start  = isset($post['start']) ? $post['start'] : 0;
		$length = isset($post['length']) ? $post['length'] : 10;
		$sql .= " LIMIT {$start}, {$length}";
		// echo $this->db->last_query();die;
		$data  = $this->db->query($sql);

		return array(
			'totalData' => $totaldata,
			'data' => $data,
		);
	}
	public function data_AllMenuByDateCabang($post)
	{
		$id_cabang = $_SESSION['id_cabang'];
		$columns = array(
			'nama_menu',
			'qty',
			'total',
		);		
		$columnsSearch = array(
			'nama_menu',
		);
		// gunakan join disini
		$from = 'transaksi_detail dt';
		// custom SQL
		$sql = "SELECT dt.*,m.nama_menu FROM {$from} 
			join menu m on m.id_menu =dt.id_menu
		where  dt.created_at like '%{$post['date']}%'
		and dt.id_cabang = $id_cabang
		 ";
		$where = "";
		$whereTemp = "";
		if ($whereTemp != '' && $where != '') $where .= " AND (" . $whereTemp . ")";
		else if ($whereTemp != '') $where .= $whereTemp;
		// search
		if (isset($post['search']['value']) && $post['search']['value'] != '') {
			$search = $post['search']['value'];
			// create parameter pencarian kesemua kolom yang tertulis
			// di $columns
			$whereTemp = "";
			for ($i = 0; $i < count($columnsSearch); $i++) {
				$whereTemp .= $columnsSearch[$i] . ' LIKE "%' . $search . '%"';
				// agar tidak menambahkan 'OR' diakhir Looping
				if ($i < count($columnsSearch) - 1) {
					$whereTemp .= ' OR ';
				}
			}
			if ($where != '') $where .= " AND (" . $whereTemp . ")";
			else $where .= $whereTemp;
		}
		if ($where != '') $sql .= ' and (' . $where . ')';
		//SORT Kolom
		$sortColumn = isset($post['order'][0]['column']) ? $post['order'][0]['column'] : 1;
		$sortDir    = isset($post['order'][0]['dir']) ? $post['order'][0]['dir'] : 'asc';
		$sortColumn = $columns[$sortColumn - 1];
		$sql .= " ORDER BY {$sortColumn} {$sortDir}";
		$count = $this->db->query($sql);
		$totaldata = $count->num_rows();
		$start  = isset($post['start']) ? $post['start'] : 0;
		$length = isset($post['length']) ? $post['length'] : 10;
		$sql .= " LIMIT {$start}, {$length}";
		// echo $this->db->last_query();die;
		$data  = $this->db->query($sql);

		return array(
			'totalData' => $totaldata,
			'data' => $data,
		);
	}   
	public function getDataMejas($id_cabang='1')
	{
		// var_dump($id_cabang);die;
		$this->db->from('cabang');
		$sql = $this->db->get();
		return $sql->result();

		// return $this->db->where('id_cabang', $id_cabang)
		// ->get('meja')
		// ->result();		  
	}
	public function getData()
	{
		
		return $this->db->from('tcompany')->get()->result();
		
		# code...
	}
	public function Add($data)
	{
		return $this->db->insert('tanswer', $data);
	}
}
                        
/* End of file MenuModel.php */
