<?php 

defined('BASEPATH') OR exit('No direct script access allowed');
                        
class TransaksiModel extends CI_Model {
                        
public function login(){
                        
                                
}
	public function dt_transaksi($post)
	{
		$from = 'transaksi t';
		// untuk sort
		$columns = array(
			't.created_at',
			't.kode_transaksi',
			't.nama_user',
			'k.nama_kasir',
			't.tipe_pesan',
			't.harga_total',
		);
		
		// untuk search
		$columnsSearch = array(
			'k.nama_kasir',
			't.created_at',
			't.kode_transaksi',
			't.nama_user',
			't.tipe_pesan',
			't.harga_total',
		);


		// custom SQL
		$sql = "SELECT t.*,k.nama_kasir,k.id_kasir 
		from transaksi t 
		join kasir k on k.id_kasir=t.id_user
		where type_penjualan_offline=0";


		$whereIdCabang = "";
		if (isset($post['id_cabang']) && $post['id_cabang'] != 'default') $whereIdCabang .= " (t.id_cabang='" . $post['id_cabang'] . "')";

		$where = "";
		if (isset($post['status']) && $post['status'] != 'default') $where .= " (t.status='" . $post['status'] . "')";

		$whereMet = "";
		if (isset($post['metode']) && $post['metode'] != 'default') $whereMet .= " (t.metode_pembayaran='" . $post['metode'] . "')";

		$whereTemp = "";
		if (isset($post['date']) && $post['date'] != '') {
			$date = explode(' / ', $post['date']);
			$selectDate = 't.created_at';
			if (count($date) == 1) {
				$whereTemp .= "(" . $selectDate . "  LIKE '%" . $post['date'] . "%')";
			} else {
				// $whereTemp .= "(created_at BETWEEN '".$date[0]."' AND '".$date[1]."')";
				$whereTemp .= "(date_format($selectDate, \"%Y-%m-%d\") >='$date[0]' AND date_format($selectDate, \"%Y-%m-%d\") <= '$date[1]')";
				// $whereTemp .= "(date_format($selectDate, \"%Y-%m-%d %H:%i:%s\") >='$date[0]' AND date_format($selectDate, \"%Y-%m-%d %H:%i:%s\") <= '$date[1]')
			}
		}
		if ($whereTemp != '' && $where != ''
		) $where .= " where (" . $whereTemp . ")";
		else if ($whereTemp != '') $where .= $whereTemp;

		if ($whereMet != '' && $where != ''
		) $where .= " where (" . $whereMet . ")";
		else if ($whereMet != '') $where .= $whereMet;

		if ($whereIdCabang != '' && $where != ''
		) $where .= " and (" . $whereIdCabang . ")";
		else if ($whereIdCabang != '') $where .= $whereIdCabang;

		// search
		$status_search = isset($post['search']['value']) && $post['search']['value'] != '';
		if ($status_search) {
			$search = $post['search']['value'];
			$whereTemp = "";
			for ($i = 0; $i < count($columnsSearch); $i++) {
				$whereTemp .= $columnsSearch[$i] . ' LIKE "%' . $search . '%"';

				// agar tidak menambahkan 'OR' diakhir Looping
				if ($i < count($columnsSearch) - 1) {
					$whereTemp .= ' OR ';
				}
			}
			if ($where != ''
			) $where .= " AND (" . $whereTemp . ")";
			else $where .= $whereTemp;
		}
		//filter
		// if($status_search)

		// if($where == '') $sql .= ' WHERE ('.$where.')';
		if ($where != '') $sql .= " AND (" . $where . ")";
		else $sql .= $where;


		//SORT Kolom
		$sortColumn = isset($post['order'][0]['column']) ? $post['order'][0]['column'] : 1;
		$sortDir    = isset($post['order'][0]['dir']) ? $post['order'][0]['dir'] : 'asc';

		$sortColumn = $columns[$sortColumn - 1];

		$sql .= " ORDER BY {$sortColumn} {$sortDir}";

		$count = $this->db->query($sql);
		// hitung semua data
		$totaldata = $count->num_rows();

		// memberi Limit
		$start  = isset($post['start']) ? $post['start'] : 0;
		$length = isset($post['length']) ? $post['length'] : 10;


		$sql .= " LIMIT {$start}, {$length}";


		$data  = $this->db->query($sql);
		// var_dump(json_encode($this->db->last_query()));die();

		return array(
			'totalData' => $totaldata,
			'data' => $data,
		);
	}
	public function dt_transaksi_offline($post)
	{
		$from = 'transaksi t';
		// untuk sort
		$columns = array(
			't.nama_user',
			't.kode_transaksi',
			't.tipe_pesan',
			't.nomor_meja',
			't.harga_total',
			't.catatan',
		);

		// untuk search
		$columnsSearch = array(
			't.created_at',
			't.kode_transaksi',
			't.nama_user',
			't.tipe_pesan',
			't.harga_total',
		);


		// custom SQL
		$sql = "SELECT t.* from transaksi t where type_penjualan_offline=1";

		$whereIdCabang = "";
		if (isset($post['id_cabang']) && $post['id_cabang'] != 'default') $whereIdCabang .= " (t.id_cabang='" . $post['id_cabang'] . "')";


		$where = "";
		if (isset($post['status']) && $post['status'] != 'default') $where .= " (t.status='" . $post['status'] . "')";

		$whereMet = "";
		if (isset($post['metode']) && $post['metode'] != 'default') $whereMet .= " (t.metode_pembayaran='" . $post['metode'] . "')";

		$whereTemp = "";
		if (isset($post['date']) && $post['date'] != '') {
			$date = explode(' / ', $post['date']);
			$selectDate = 'created_at';
			if (count($date) == 1) {
				$whereTemp .= "(" . $selectDate . "  LIKE '%" . $post['date'] . "%')";
			} else {
				// $whereTemp .= "(created_at BETWEEN '".$date[0]."' AND '".$date[1]."')";
				$whereTemp .= "(date_format($selectDate, \"%Y-%m-%d\") >='$date[0]' AND date_format($selectDate, \"%Y-%m-%d\") <= '$date[1]')";
				// $whereTemp .= "(date_format($selectDate, \"%Y-%m-%d %H:%i:%s\") >='$date[0]' AND date_format($selectDate, \"%Y-%m-%d %H:%i:%s\") <= '$date[1]')
			}
		}
		if ($whereTemp != '' && $where != ''
		) $where .= " where (" . $whereTemp . ")";
		else if ($whereTemp != '') $where .= $whereTemp;

		if ($whereMet != '' && $where != ''
		) $where .= " where (" . $whereMet . ")";
		else if ($whereMet != '') $where .= $whereMet;

		
		if ($whereIdCabang != '' && $where != ''
		) $where .= " and (" . $whereIdCabang . ")";
		else if ($whereIdCabang != '') $where .= $whereIdCabang;


		// search
		$status_search = isset($post['search']['value']) && $post['search']['value'] != '';
		if ($status_search) {
			$search = $post['search']['value'];
			$whereTemp = "";
			for ($i = 0; $i < count($columnsSearch); $i++) {
				$whereTemp .= $columnsSearch[$i] . ' LIKE "%' . $search . '%"';

				// agar tidak menambahkan 'OR' diakhir Looping
				if ($i < count($columnsSearch) - 1) {
					$whereTemp .= ' OR ';
				}
			}
			if ($where != ''
			) $where .= " AND (" . $whereTemp . ")";
			else $where .= $whereTemp;
		}
		//filter
		// if($status_search)

		// if($where == '') $sql .= ' WHERE ('.$where.')';
		if ($where != '') $sql .= " AND (" . $where . ")";
		else $sql .= $where;


		//SORT Kolom
		$sortColumn = isset($post['order'][0]['column']) ? $post['order'][0]['column'] : 1;
		$sortDir    = isset($post['order'][0]['dir']) ? $post['order'][0]['dir'] : 'asc';

		$sortColumn = $columns[$sortColumn - 1];

		$sql .= " ORDER BY {$sortColumn} {$sortDir}";

		$count = $this->db->query($sql);
		// hitung semua data
		$totaldata = $count->num_rows();

		// memberi Limit
		$start  = isset($post['start']) ? $post['start'] : 0;
		$length = isset($post['length']) ? $post['length'] : 10;


		$sql .= " LIMIT {$start}, {$length}";


		$data  = $this->db->query($sql);
		// var_dump(json_encode($this->db->last_query()));die();

		return array(
			'totalData' => $totaldata,
			'data' => $data,
		);
	}
	public function dt_transaksi_detail($post)
	{
		$from = 'transaksi_detail t';
		// untuk sort
		$columns = array(

			't.id_transaksi',
			't.id_transaksi_detail',
			't.id_menu',
			't.qty',
			't.total',
			// 't.created_at',
			// 't.kode_transaksi',
			// 't.nama_user',
			// 't.tipe_pesan',
			// 't.harga_total',
		);

		// untuk search
		$columnsSearch = array(
			't.id_transaksi',
			't.id_transaksi_detail',
			't.id_menu',
			't.qty',
			't.total',
		);


		// custom SQL
		$sql = "SELECT * from transaksi_detail t 
		join menu m on m.id_menu = t.id_menu
		";


		$where = "";
		if (isset($post['status']) && $post['status'] != 'default') $where .= " (t.status='" . $post['status'] . "')";

		if (isset($post['id']) && $post['id'] != 'default') $where .= " (t.id_transaksi='" . $post['id'] . "')";

		$whereMet = "";
		if (isset($post['metode']) && $post['metode'] != 'default') $whereMet .= " (t.metode_pembayaran='" . $post['metode'] . "')";

		$whereTemp = "";
		if (isset($post['date']) && $post['date'] != '') {
			$date = explode(' / ',
				$post['date']
			);
			$selectDate = 'created_at';
			if (count($date) == 1) {
				$whereTemp .= "(" . $selectDate . "  LIKE '%" . $post['date'] . "%')";
			} else {
				// $whereTemp .= "(created_at BETWEEN '".$date[0]."' AND '".$date[1]."')";
				$whereTemp .= "(date_format($selectDate, \"%Y-%m-%d\") >='$date[0]' AND date_format($selectDate, \"%Y-%m-%d\") <= '$date[1]')";
				// $whereTemp .= "(date_format($selectDate, \"%Y-%m-%d %H:%i:%s\") >='$date[0]' AND date_format($selectDate, \"%Y-%m-%d %H:%i:%s\") <= '$date[1]')
			}
		}
		if (
			$whereTemp != '' && $where != ''
		) $where .= " where (" . $whereTemp . ")";
		else if ($whereTemp != '') $where .= $whereTemp;

		if (
			$whereMet != '' && $where != ''
		) $where .= " where (" . $whereMet . ")";
		else if ($whereMet != '') $where .= $whereMet;

		// search
		$status_search = isset($post['search']['value']) && $post['search']['value'] != '';
		if ($status_search) {
			$search = $post['search']['value'];
			$whereTemp = "";
			for ($i = 0; $i < count($columnsSearch); $i++) {
				$whereTemp .= $columnsSearch[$i] . ' LIKE "%' . $search . '%"';

				// agar tidak menambahkan 'OR' diakhir Looping
				if ($i < count($columnsSearch) - 1) {
					$whereTemp .= ' OR ';
				}
			}
			if (
				$where != ''
			) $where .= " AND (" . $whereTemp . ")";
			else $where .= $whereTemp;
		}
		//filter
		// if($status_search)

		// if($where == '') $sql .= ' WHERE ('.$where.')';
		if ($where != '') $sql .= " where (" . $where . ")";
		else $sql .= $where;


		//SORT Kolom
		$sortColumn = isset($post['order'][0]['column']) ? $post['order'][0]['column'] : 1;
		$sortDir    = isset($post['order'][0]['dir']) ? $post['order'][0]['dir'] : 'asc';

		$sortColumn = $columns[$sortColumn - 1];

		$sql .= " ORDER BY {$sortColumn} {$sortDir}";

		$count = $this->db->query($sql);
		// hitung semua data
		$totaldata = $count->num_rows();

		// memberi Limit
		$start  = isset($post['start']) ? $post['start'] : 0;
		$length = isset($post['length']) ? $post['length'] : 10;


		$sql .= " LIMIT {$start}, {$length}";


		$data  = $this->db->query($sql);
		// var_dump(json_encode($this->db->last_query()));die();

		return array(
			'totalData' => $totaldata,
			'data' => $data,
		);
	}


	public function get_transaksi($status, $date,$id_cabang=null)
	{
		$selectDate = 't.created_at';
		$this->db->select('t.*,k.*');
		$this->db->from('transaksi as t');
		$this->db->where('type_penjualan_offline', 0);
		if($id_cabang!=null) $this->db->where('t.id_cabang', $id_cabang);
		$this->db->join('kasir  as k', 'k.id_kasir = t.id_user');
		$from = 't.';
		if ($date != ''
		) {
			$date = explode('/ ', $date);
			if (count($date) == 1) {
				$whereTemp = array('date_format(' . $selectDate . ', "%Y-%m-%d") =' => $date[0]);
				$this->db->where($whereTemp);

			} else {
				$whereTemp = array('date_format(' . $selectDate . ', "%Y-%m-%d") >=' => $date[0], 'date_format(' . $selectDate . ', "%Y-%m-%d") <=' => $date[1]);
				$this->db->where($whereTemp);
			}
		}
		$data = $this->db->get();
		// echo $this->db->last_query();
		// die;
		
		return $data->result();
	}
	public function get_transaksi_offline($status, $date,$id_cabang=null)
	{
		$selectDate = 't.created_at';
		$this->db->select('t.*');
		$this->db->from('transaksi as t');
		$this->db->where('type_penjualan_offline', 1);
		if($id_cabang!=null) $this->db->where('t.id_cabang', $id_cabang);

		$from = 't.';
		if ($date != ''
		) {
			$date = explode('/ ', $date);
			if (count($date) == 1) {
				$whereTemp = array('date_format(' . $selectDate . ', "%Y-%m-%d") =' => $date[0]);
				$this->db->where($whereTemp);

			} else {
				$whereTemp = array('date_format(' . $selectDate . ', "%Y-%m-%d") >=' => $date[0], 'date_format(' . $selectDate . ', "%Y-%m-%d") <=' => $date[1]);
				$this->db->where($whereTemp);
			}
		}
		$data = $this->db->get();
		// echo $this->db->last_query();
		// die;
		
		return $data->result();
	}

	public function getLaporanHarian($tanggal,$bulan,$tahun){
		// var_dump("s");die;
		$selectDate = 't.created_at';
		$this->db->select('t.*,dt.qty,dt.total,m.nama_menu,m.harga,c.id_cabang,c.cabang');
		$this->db->from('transaksi as t');
		$this->db->join('transaksi_detail dt', 'dt.id_transaksi = t.id_transaksi', 'left');
		$this->db->join('cabang c', 'c.id_cabang = t.id_cabang', 'left');		
		$this->db->join('menu m', 'm.id_menu = dt.id_menu', 'left');
		$this->db->where('DAY(t.created_at)', $tanggal);
		$this->db->where('MONTH(t.created_at)', $bulan);
		$this->db->where('YEAR(t.created_at)', $tahun);
		// var_dump( $this->db->last_query());die;
		
		return $this->db->get()->result();

	}
	public function getLaporanHarianCabang($tanggal,$bulan,$tahun){
		$id_cabang = $this->session->userdata('id_cabang');
		$selectDate = 't.created_at';
		$this->db->where('t.id_cabang', $id_cabang);
		
		$this->db->where('DAY(t.created_at)', $tanggal);
		$this->db->where('MONTH(t.created_at)', $bulan);
		$this->db->where('YEAR(t.created_at)', $tahun);
		$this->db->select('t.*,dt.qty,dt.total,m.nama_menu,m.harga');
		$this->db->from('transaksi as t');
		$this->db->join('transaksi_detail dt', 'dt.id_transaksi = t.id_transaksi', 'left');
		$this->db->join('menu m', 'm.id_menu = dt.id_menu', 'left');

		return $this->db->get()->result();

	}
	public function getLaporanBulanan($bulan,$tahun){
		$selectDate = 't.created_at';
		$this->db->select('t.*,dt.qty,dt.total,m.nama_menu,m.harga');
		$this->db->from('transaksi as t');
		$this->db->join('transaksi_detail dt', 'dt.id_transaksi = t.id_transaksi', 'left');
		$this->db->join('menu m', 'm.id_menu = dt.id_menu', 'left');
		
		$this->db->where('MONTH(t.created_at)', $bulan);
		$this->db->where('YEAR(t.created_at)', $tahun);

		return $this->db->get()->result();

	}
	public function getLaporanBulananCabang($bulan,$tahun){
			$id_cabang = $this->session->userdata('id_cabang');
		$selectDate = 't.created_at';
		$this->db->where('t.id_cabang', $id_cabang);
		$this->db->select('t.*,dt.qty,dt.total,m.nama_menu,m.harga');
		$this->db->from('transaksi as t');
		$this->db->join('transaksi_detail dt', 'dt.id_transaksi = t.id_transaksi', 'left');
		$this->db->join('menu m', 'm.id_menu = dt.id_menu', 'left');
		
		$this->db->where('MONTH(t.created_at)', $bulan);
		$this->db->where('YEAR(t.created_at)', $tahun);

		return $this->db->get()->result();

	}
	public function getLaporanTahunan($tahun){
		$selectDate = 't.created_at';
		$this->db->select('t.*,dt.qty,dt.total,m.nama_menu,m.harga');
		$this->db->from('transaksi as t');
		$this->db->join('transaksi_detail dt', 'dt.id_transaksi = t.id_transaksi', 'left');
		$this->db->join('menu m', 'm.id_menu = dt.id_menu', 'left');		
		$this->db->where('YEAR(t.created_at)', $tahun);

		return $this->db->get()->result();

	}
	public function getLaporanTahunanCabang($tahun){
		
			$id_cabang = $this->session->userdata('id_cabang');
		$selectDate = 't.created_at';
		$this->db->where('t.id_cabang', $id_cabang);
		$selectDate = 't.created_at';
		$this->db->select('t.*,dt.qty,dt.total,m.nama_menu,m.harga');
		$this->db->from('transaksi as t');
		$this->db->join('transaksi_detail dt', 'dt.id_transaksi = t.id_transaksi', 'left');
		$this->db->join('menu m', 'm.id_menu = dt.id_menu', 'left');		
		$this->db->where('YEAR(t.created_at)', $tahun);

		return $this->db->get()->result();

	}
           
	public function getMenuLaporanHarian($tanggal){
		$this->db->select('dt.*,m.nama_menu,m.harga');
		$this->db->from('transaksi_detail as dt');
		$this->db->join('menu m', 'm.id_menu = dt.id_menu', 'left');		
		$this->db->like('dt.created_at', $tanggal);
		

		return $this->db->get()->result();

	}
           
	public function getMenuLaporanHarianCabang($tanggal){
		$this->db->where('dt.id_cabang', $_SESSION['id_cabang']);		
		$this->db->select('dt.*,m.nama_menu,m.harga');
		$this->db->from('transaksi_detail as dt');
		$this->db->join('menu m', 'm.id_menu = dt.id_menu', 'left');		
		$this->db->like('dt.created_at', $tanggal);
		return $this->db->get()->result();
	}
           
	public function getCountTransaksiHarian($tanggal){
		$this->db->from('transaksi dt');
		$this->db->like('dt.created_at', $tanggal);
		return $this->db->count_all_results();
		return $this->db->get()->num_rows();
	}
	public function getCountMenuHarian($tanggal){
		$this->db->select('sum(qty) as total');		
		$this->db->from('transaksi_detail dt');
		$this->db->like('dt.created_at', $tanggal);
		
		// return $this->db->count_all_results();
		return $this->db->get()->row()->total;
		
	}
         
	public function dt_transaksiCabang($post)
	{
		$id_cabang = $_SESSION['id_cabang'];
		$from = 'transaksi t';
		// untuk sort
		$columns = array(
			't.created_at',
			't.kode_transaksi',
			't.nama_user',
			'k.nama_kasir',
			't.tipe_pesan',
			't.harga_total',
		);
		
		// untuk search
		$columnsSearch = array(
			'k.nama_kasir',
			't.created_at',
			't.kode_transaksi',
			't.nama_user',
			't.tipe_pesan',
			't.harga_total',
		);


		// custom SQL
		$sql = "SELECT t.*,k.nama_kasir,k.id_kasir 
		from transaksi t 
		left join kasir k on k.id_kasir=t.id_user
		where type_penjualan_offline=0
		and t.id_cabang = $id_cabang
		";

		$where = "";
		if (isset($post['status']) && $post['status'] != 'default') $where .= " (t.status='" . $post['status'] . "')";

		if (isset($post['tipe_pesanan']) && $post['tipe_pesanan'] != 'default') {
			if ($where != "") $where .= "AND";
			$where .= " (t.tipe_pesan='" . $post['tipe_pesanan'] . "')";
		}

		$whereMet = "";
		if (isset($post['metode']) && $post['metode'] != 'default') $whereMet .= " (t.metode_pembayaran='" . $post['metode'] . "')";

		$whereTemp = "";
		if (isset($post['date']) && $post['date'] != '') {
			$date = explode(' / ', $post['date']);
			$selectDate = 't.created_at';
			if (count($date) == 1) {
				$whereTemp .= "(" . $selectDate . "  LIKE '%" . $post['date'] . "%')";
			} else {
				// $whereTemp .= "(created_at BETWEEN '".$date[0]."' AND '".$date[1]."')";
				$whereTemp .= "(date_format($selectDate, \"%Y-%m-%d\") >='$date[0]' AND date_format($selectDate, \"%Y-%m-%d\") <= '$date[1]')";
				// $whereTemp .= "(date_format($selectDate, \"%Y-%m-%d %H:%i:%s\") >='$date[0]' AND date_format($selectDate, \"%Y-%m-%d %H:%i:%s\") <= '$date[1]')
			}
		}
		if ($whereTemp != '' && $where != ''
		) $where .= " where (" . $whereTemp . ")";
		else if ($whereTemp != '') $where .= $whereTemp;

		if ($whereMet != '' && $where != ''
		) $where .= " where (" . $whereMet . ")";
		else if ($whereMet != '') $where .= $whereMet;

		// search
		$status_search = isset($post['search']['value']) && $post['search']['value'] != '';
		if ($status_search) {
			$search = $post['search']['value'];
			$whereTemp = "";
			for ($i = 0; $i < count($columnsSearch); $i++) {
				$whereTemp .= $columnsSearch[$i] . ' LIKE "%' . $search . '%"';

				// agar tidak menambahkan 'OR' diakhir Looping
				if ($i < count($columnsSearch) - 1) {
					$whereTemp .= ' OR ';
				}
			}
			if ($where != ''
			) $where .= " AND (" . $whereTemp . ")";
			else $where .= $whereTemp;
		}
		//filter
		// if($status_search)

		// if($where == '') $sql .= ' WHERE ('.$where.')';
		if ($where != '') $sql .= " AND (" . $where . ")";
		else $sql .= $where;


		//SORT Kolom
		$sortColumn = isset($post['order'][0]['column']) ? $post['order'][0]['column'] : 1;
		$sortDir    = isset($post['order'][0]['dir']) ? $post['order'][0]['dir'] : 'asc';

		$sortColumn = $columns[$sortColumn - 1];

		$sql .= " ORDER BY {$sortColumn} {$sortDir}";

		$count = $this->db->query($sql);
		// hitung semua data
		$totaldata = $count->num_rows();

		// memberi Limit
		$start  = isset($post['start']) ? $post['start'] : 0;
		$length = isset($post['length']) ? $post['length'] : 10;


		$sql .= " LIMIT {$start}, {$length}";


		$data  = $this->db->query($sql);
		// var_dump(json_encode($this->db->last_query()));die();

		return array(
			'totalData' => $totaldata,
			'data' => $data,
		);
	}
	public function dt_transaksi_offlineCabang($post)
	{
		$id_cabang = $this->session->userdata('id_cabang');

		$from = 'transaksi t';
		// untuk sort
		$columns = array(
			't.nama_user',
			't.kode_transaksi',
			't.tipe_pesan',
			't.nomor_meja',
			't.harga_total',
			't.catatan',
		);

		// untuk search
		$columnsSearch = array(
			't.created_at',
			't.kode_transaksi',
			't.nama_user',
			't.tipe_pesan',
			't.harga_total',
		);


		// custom SQL
		$sql = "SELECT t.* from transaksi t 
		where type_penjualan_offline=1 
		and t.id_cabang = $id_cabang
		

		";


		$where = "";
		if (isset($post['status']) && $post['status'] != 'default') $where .= " (t.status='" . $post['status'] . "')";

		$whereMet = "";
		if (isset($post['metode']) && $post['metode'] != 'default') $whereMet .= " (t.metode_pembayaran='" . $post['metode'] . "')";

		$whereTemp = "";
		if (isset($post['date']) && $post['date'] != '') {
			$date = explode(' / ', $post['date']);
			$selectDate = 'created_at';
			if (count($date) == 1) {
				$whereTemp .= "(" . $selectDate . "  LIKE '%" . $post['date'] . "%')";
			} else {
				// $whereTemp .= "(created_at BETWEEN '".$date[0]."' AND '".$date[1]."')";
				$whereTemp .= "(date_format($selectDate, \"%Y-%m-%d\") >='$date[0]' AND date_format($selectDate, \"%Y-%m-%d\") <= '$date[1]')";
				// $whereTemp .= "(date_format($selectDate, \"%Y-%m-%d %H:%i:%s\") >='$date[0]' AND date_format($selectDate, \"%Y-%m-%d %H:%i:%s\") <= '$date[1]')
			}
		}
		if ($whereTemp != '' && $where != ''
		) $where .= " where (" . $whereTemp . ")";
		else if ($whereTemp != '') $where .= $whereTemp;

		if ($whereMet != '' && $where != ''
		) $where .= " where (" . $whereMet . ")";
		else if ($whereMet != '') $where .= $whereMet;


		// search
		$status_search = isset($post['search']['value']) && $post['search']['value'] != '';
		if ($status_search) {
			$search = $post['search']['value'];
			$whereTemp = "";
			for ($i = 0; $i < count($columnsSearch); $i++) {
				$whereTemp .= $columnsSearch[$i] . ' LIKE "%' . $search . '%"';

				// agar tidak menambahkan 'OR' diakhir Looping
				if ($i < count($columnsSearch) - 1) {
					$whereTemp .= ' OR ';
				}
			}
			if ($where != ''
			) $where .= " AND (" . $whereTemp . ")";
			else $where .= $whereTemp;
		}
		//filter
		// if($status_search)

		// if($where == '') $sql .= ' WHERE ('.$where.')';
		if ($where != '') $sql .= " AND (" . $where . ")";
		else $sql .= $where;


		//SORT Kolom
		$sortColumn = isset($post['order'][0]['column']) ? $post['order'][0]['column'] : 1;
		$sortDir    = isset($post['order'][0]['dir']) ? $post['order'][0]['dir'] : 'asc';

		$sortColumn = $columns[$sortColumn - 1];

		$sql .= " ORDER BY {$sortColumn} {$sortDir}";

		$count = $this->db->query($sql);
		// hitung semua data
		$totaldata = $count->num_rows();

		// memberi Limit
		$start  = isset($post['start']) ? $post['start'] : 0;
		$length = isset($post['length']) ? $post['length'] : 10;


		$sql .= " LIMIT {$start}, {$length}";


		$data  = $this->db->query($sql);
		// var_dump(json_encode($this->db->last_query()));die();

		return array(
			'totalData' => $totaldata,
			'data' => $data,
		);
	}
              
	public function graph($id=0)
	{		
		$id_cabang =  $this->session->userdata('id_cabang');
		if($id_cabang==null) $id_cabang =$id;
		if($id_cabang==0){
			$data = $this->db->query("SELECT * from menu limit 5");
		} else{
			$data = $this->db->query("SELECT * from menu where id_cabang = $id_cabang  limit 10");
		}
		
		return $data->result();
	}
	public function graphTransaksiDetMenu($id=0)
	{		
		$id_cabang =  $this->session->userdata('id_cabang');
		if($id_cabang==null) $id_cabang =$id;
		if($id_cabang==0){
			$data = $this->db->query("SELECT * FROM `transaksi_detail` as dt JOIN menu m on m.id_menu=dt.id_menu GROUP by dt.id_menu ");
		}else{
			$data = $this->db->query("SELECT * FROM `transaksi_detail` as dt JOIN menu m on m.id_menu=dt.id_menu where dt.id_cabang = $id_cabang GROUP by dt.id_menu ");

		}

		return $data->result();
	}
	public function graphTransaksiHarian($id=0)
	{	
		$id_cabang =  $this->session->userdata('id_cabang');
		if($id_cabang==null) $id_cabang =$id;
		if($id_cabang==0){
			$data = $this->db->query(" select DATE_FORMAT(t.created_at ,'%b %d') as dayName, sum(t.harga_total) harga_total from `transaksi` t group by dayName  order by t.created_at asc  ");
		}else{
			$data = $this->db->query(" select DATE_FORMAT(t.created_at ,'%b %d') as dayName, sum(t.harga_total) harga_total from `transaksi` t where t.id_cabang = $id_cabang group by dayName  order by t.created_at asc  ");
		}
		return $data->result();
	}
	public function dt_transaksi_all($post)
	{
		$from = 'transaksi t';
		// untuk sort
		$columns = array(
			't.nama_user',
			't.kode_transaksi',
			't.tipe_pesan',
			't.harga_total',
			't.created_at',
			't.status',
		);
		
		// untuk search
		$columnsSearch = array(
			'k.nama_kasir',
			't.created_at',
			't.kode_transaksi',
			't.nama_user',
			't.tipe_pesan',
			't.harga_total',
		);


		// custom SQL
		$sql = "SELECT t.*,k.nama_kasir,k.id_kasir 
		from transaksi t 
		join kasir k on k.id_kasir=t.id_user
		";


		$whereIdCabang = "";
		if (isset($post['id_cabang']) && $post['id_cabang'] != 'default') $whereIdCabang .= " (t.id_cabang='" . $post['id_cabang'] . "')";

		$where = "";
		if (isset($post['status']) && $post['status'] != 'default') $where .= " (t.status='" . $post['status'] . "')";

		$whereMet = "";
		if (isset($post['metode']) && $post['metode'] != 'default') $whereMet .= " (t.metode_pembayaran='" . $post['metode'] . "')";

		$whereTemp = "";
		if (isset($post['date']) && $post['date'] != '') {
			$date = explode(' / ', $post['date']);
			$selectDate = 't.created_at';
			if (count($date) == 1) {
				$whereTemp .= "(" . $selectDate . "  LIKE '%" . $post['date'] . "%')";
			} else {
				// $whereTemp .= "(created_at BETWEEN '".$date[0]."' AND '".$date[1]."')";
				$whereTemp .= "(date_format($selectDate, \"%Y-%m-%d\") >='$date[0]' AND date_format($selectDate, \"%Y-%m-%d\") <= '$date[1]')";
				// $whereTemp .= "(date_format($selectDate, \"%Y-%m-%d %H:%i:%s\") >='$date[0]' AND date_format($selectDate, \"%Y-%m-%d %H:%i:%s\") <= '$date[1]')
			}
		}
		if ($whereTemp != '' && $where != ''
		) $where .= " where (" . $whereTemp . ")";
		else if ($whereTemp != '') $where .= $whereTemp;

		if ($whereMet != '' && $where != ''
		) $where .= " where (" . $whereMet . ")";
		else if ($whereMet != '') $where .= $whereMet;

		if ($whereIdCabang != '' && $where != ''
		) $where .= " and (" . $whereIdCabang . ")";
		else if ($whereIdCabang != '') $where .= $whereIdCabang;

		// search
		$status_search = isset($post['search']['value']) && $post['search']['value'] != '';
		if ($status_search) {
			$search = $post['search']['value'];
			$whereTemp = "";
			for ($i = 0; $i < count($columnsSearch); $i++) {
				$whereTemp .= $columnsSearch[$i] . ' LIKE "%' . $search . '%"';

				// agar tidak menambahkan 'OR' diakhir Looping
				if ($i < count($columnsSearch) - 1) {
					$whereTemp .= ' OR ';
				}
			}
			if ($where != ''
			) $where .= " AND (" . $whereTemp . ")";
			else $where .= $whereTemp;
		}
		//filter
		// if($status_search)

		// if($where == '') $sql .= ' WHERE ('.$where.')';
		if ($where != '') $sql .= " AND (" . $where . ")";
		else $sql .= $where;


		//SORT Kolom
		$sortColumn = isset($post['order'][0]['column']) ? $post['order'][0]['column'] : 1;
		$sortDir    = isset($post['order'][0]['dir']) ? $post['order'][0]['dir'] : 'asc';

		$sortColumn = $columns[$sortColumn - 1];

		$sql .= " ORDER BY {$sortColumn} {$sortDir}";

		$count = $this->db->query($sql);
		// hitung semua data
		$totaldata = $count->num_rows();

		// memberi Limit
		$start  = isset($post['start']) ? $post['start'] : 0;
		$length = isset($post['length']) ? $post['length'] : 10;


		$sql .= " LIMIT {$start}, {$length}";


		$data  = $this->db->query($sql);
		// var_dump(json_encode($this->db->last_query()));die();

		return array(
			'totalData' => $totaldata,
			'data' => $data,
		);
	}
}
                        
/* End of file TransaksiModel.php */
    
                        