<?php

defined('BASEPATH') or exit('No direct script access allowed');

class SemuaModel extends CI_Model
{

	public function login()
	{
	}
	public function Tambah($nama, $data)
	{
		return $this->db->insert($nama, $data);
	}
	public function AddTransaksi($data)
	{
		$this->db->insert('transaksi', $data);
		return $this->db->insert_id();
	}
	public function getDataById($namaTable, $nama_id, $id_Dicari)
	{
		$this->db->select('*');
		$this->db->where($nama_id, $id_Dicari);
		$query = $this->db->get($namaTable);
		return $query->result();
	}
	public function getDataByIdRow($namaTable, $nama_id, $id_Dicari)
	{
		$this->db->select('*');
		$this->db->where($nama_id, $id_Dicari);
		$query = $this->db->get($namaTable);
		return $query->row();
	}
	public function getData($namaTable,$where=[],$select='*')
	{
		$this->db->select($select);
		$this->db->where($where);
		$query = $this->db->get($namaTable);
		return $query->result();
	}
	public function HapusData($NamaTabel, $nama_id, $IdNya)
	{
		$this->db->where($nama_id, $IdNya);
		$this->db->delete($NamaTabel);
		$query = $this->db->get($NamaTabel);
		return $query->result();
	}
	public function HapusDataMore($NamaTabel, $where=[])
	{
		$this->db->where($where);
		$this->db->delete($NamaTabel);
		$query = $this->db->get($NamaTabel);
		return $query->result();
	}
	public function EditData($NamaTabel, $nama_id, $in, $nisn)
	{
		$this->db->where($nama_id, $nisn);
		return $this->db->update($NamaTabel, $in);
	}
	public function getAllRole()
	{
		$this->db->from('role');
		$sql = $this->db->get();
		return $sql->result();

		# code...
	}
	public function getSeting()
	{
		$this->db->from('setting');
		$sql = $this->db->get();
		return $sql->result();

		# code...
	}
	public function data_AllSetting($post)
	{
		$id_cabang = $_SESSION['id_cabang'];
		$columns = array(
			'cabang',
			'alamat_cabang',
			'nomor_telp',
			'facebook',
			'instagram',
			'twitter',
			'youtube',

		);
		$columnsSearch = array(
			'cabang',
			'alamat_cabang',
			'nomor_telp',
			'facebook',
			'instagram',
			'twitter',
			'youtube',
		);
		// gunakan join disini
		$from = "cabang s
		 where id_cabang = $id_cabang
		";
		// custom SQL
		$sql = "SELECT* FROM {$from}   
		";
		$where = "";
		$whereTemp = "";
		if ($whereTemp != '' && $where != '') $where .= " AND (" . $whereTemp . ")";
		else if ($whereTemp != '') $where .= $whereTemp;
		// search
		if (isset($post['search']['value']) && $post['search']['value'] != '') {
			$search = $post['search']['value'];
			// create parameter pencarian kesemua kolom yang tertulis
			// di $columns
			$whereTemp = "";
			for ($i = 0; $i < count($columnsSearch); $i++) {
				$whereTemp .= $columnsSearch[$i] . ' LIKE "%' . $search . '%"';
				// agar tidak menambahkan 'OR' diakhir Looping
				if ($i < count($columnsSearch) - 1) {
					$whereTemp .= ' OR ';
				}
			}
			if ($where != '') $where .= " AND (" . $whereTemp . ")";
			else $where .= $whereTemp;
		}
		if ($where != '') $sql .= ' And (' . $where . ')';
		//SORT Kolom
		$sortColumn = isset($post['order'][0]['column']) ? $post['order'][0]['column'] : 1;
		$sortDir    = isset($post['order'][0]['dir']) ? $post['order'][0]['dir'] : 'asc';
		$sortColumn = $columns[$sortColumn - 1];
		$sql .= " ORDER BY {$sortColumn} {$sortDir}";
		$count = $this->db->query($sql);
		$totaldata = $count->num_rows();
		$start  = isset($post['start']) ? $post['start'] : 0;
		$length = isset($post['length']) ? $post['length'] : 10;
		$sql .= " LIMIT {$start}, {$length}";
		$data  = $this->db->query($sql);
		return array(
			'totalData' => $totaldata,
			'data' => $data,
		);
	}
	public function data_Setting($post)
	{
		$columns = array(
			'cabang',
			'alamat_cabang',
			'nomor_telp',
			'facebook',
			'instagram',
			'twitter',
			'youtube',

		);
		$columnsSearch = array(
			'cabang',
			'alamat_cabang',
			'nomor_telp',
			'facebook',
			'instagram',
			'twitter',
			'youtube',
		);
		// gunakan join disini
		$from = "cabang s
		";
		// custom SQL
		$sql = "SELECT* FROM {$from}   
		";
		$where = "";
		$whereTemp = "";
		if ($whereTemp != '' && $where != '') $where .= " AND (" . $whereTemp . ")";
		else if ($whereTemp != '') $where .= $whereTemp;
		// search
		if (isset($post['search']['value']) && $post['search']['value'] != '') {
			$search = $post['search']['value'];
			// create parameter pencarian kesemua kolom yang tertulis
			// di $columns
			$whereTemp = "";
			for ($i = 0; $i < count($columnsSearch); $i++) {
				$whereTemp .= $columnsSearch[$i] . ' LIKE "%' . $search . '%"';
				// agar tidak menambahkan 'OR' diakhir Looping
				if ($i < count($columnsSearch) - 1) {
					$whereTemp .= ' OR ';
				}
			}
			if ($where != '') $where .= " AND (" . $whereTemp . ")";
			else $where .= $whereTemp;
		}
		if ($where != '') $sql .= ' Where (' . $where . ')';
		//SORT Kolom
		$sortColumn = isset($post['order'][0]['column']) ? $post['order'][0]['column'] : 1;
		$sortDir    = isset($post['order'][0]['dir']) ? $post['order'][0]['dir'] : 'asc';
		$sortColumn = $columns[$sortColumn - 1];
		$sql .= " ORDER BY {$sortColumn} {$sortDir}";
		$count = $this->db->query($sql);
		$totaldata = $count->num_rows();
		$start  = isset($post['start']) ? $post['start'] : 0;
		$length = isset($post['length']) ? $post['length'] : 10;
		$sql .= " LIMIT {$start}, {$length}";
		$data  = $this->db->query($sql);
		return array(
			'totalData' => $totaldata,
			'data' => $data,
		);
	}
	public function HapusCartSatu($id_user, $id_menu)
	{
		$this->db->where('id_produk', $id_menu);
		$this->db->where('id_user', $id_user);
		$this->db->delete('keranjang');
		$query = $this->db->get('keranjang');
		return $query->result();
	}
	public function HapusCartMeja($id_user, $id_menu)
	{
		// var_dump($id_user, $id_menu);die;
		$this->db->where('id_produk', $id_menu);
		$this->db->where('id_meja', $id_user);
		$this->db->delete('keranjang_meja_user');
		$query = $this->db->get('keranjang_meja_user');
		// var_dump($this->db->last_query()
		// );die;
		return $query->result();
	}
	public function HapusCartByIdUser($id_user)
	{
		$this->db->where('id_user', $id_user);
		$this->db->delete('keranjang');
		$query = $this->db->get('keranjang');
		return $query->result();
	}
	public function getDataTransaksiById($id)
	{
		$this->db->where('id_transaksi', $id);
		$this->db->from('transaksi');		
		$sql = $this->db->get()->row();
		return $sql;
	}
	public function getDataFromDetTranAndPro($id)
	{
		$this->db->where('id_transaksi', $id);
		$this->db->from('transaksi_detail td');
		$this->db->join('menu m', 'm.id_menu = td.id_menu', 'left');

		$sql = $this->db->get()->result();
		return $sql;
	}
	public function getKasirByUNandPW($username, $password)
	{
		$this->db->from('kasir');
		$this->db->where('username', $username);
		$this->db->where('password', $password);
		$sql = $this->db->get();
		return $sql->row();
	}
	public function data_AllSlider($post)
	{
		$columns = array(
			'nama_foto',
			'foto',
		);
		$columnsSearch = array(
			'nama_foto',
			'foto',
		);
		// gunakan join disini
		$from = 'slider s';
		// custom SQL
		$sql = "SELECT* FROM {$from}   
		";
		$where = "";
		$whereTemp = "";
		if (
			$whereTemp != '' && $where != ''
		) $where .= " AND (" . $whereTemp . ")";
		else if ($whereTemp != '') $where .= $whereTemp;
		// search
		if (isset($post['search']['value']) && $post['search']['value'] != '') {
			$search = $post['search']['value'];
			// create parameter pencarian kesemua kolom yang tertulis
			// di $columns
			$whereTemp = "";
			for ($i = 0; $i < count($columnsSearch); $i++) {
				$whereTemp .= $columnsSearch[$i] . ' LIKE "%' . $search . '%"';
				// agar tidak menambahkan 'OR' diakhir Looping
				if ($i < count($columnsSearch) - 1) {
					$whereTemp .= ' OR ';
				}
			}
			if ($where != '') $where .= " AND (" . $whereTemp . ")";
			else $where .= $whereTemp;
		}
		if ($where != '') $sql .= ' WHERE (' . $where . ')';
		//SORT Kolom
		$sortColumn = isset($post['order'][0]['column']) ? $post['order'][0]['column'] : 1;
		$sortDir    = isset($post['order'][0]['dir']) ? $post['order'][0]['dir'] : 'asc';
		$sortColumn = $columns[$sortColumn - 1];
		$sql .= " ORDER BY {$sortColumn} {$sortDir}";
		$count = $this->db->query($sql);
		$totaldata = $count->num_rows();
		$start  = isset($post['start']) ? $post['start'] : 0;
		$length = isset($post['length']) ? $post['length'] : 10;
		$sql .= " LIMIT {$start}, {$length}";
		$data  = $this->db->query($sql);
		return array(
			'totalData' => $totaldata,
			'data' => $data,
		);
	}
	public function getFotoSliderById($id)
	{
		$this->db->from('slider');
		$this->db->where('id_slider', $id);
		$sql = $this->db->get();
		return $sql->row();



		# code...
	}
	public function getSlider()
	{
		$this->db->from('slider');
		$sql = $this->db->get();
		return $sql->result();

		# code...
	}
	public function minus($nama_tabel,$nama_colomn,$nama_id,$id)
{
    $this->db->set($nama_colomn, "$nama_colomn-1", false);
    $this->db->where($nama_id , $id);
    $this->db->update($nama_tabel);
}
public function getCabang()
{
	$this->db->from('cabang');
	$sql = $this->db->get();
	return $sql->result();
	# code...
}
public function data_AllSliderCabang($post)
{

	$id_cabang = $this->session->userdata('id_cabang');
	$columns = array(
		'nama_foto',
		'foto',
	);
	$columnsSearch = array(
		'nama_foto',
		'foto',
	);
	// gunakan join disini
	$from = 'slider s';
	// custom SQL
	$sql = "SELECT* FROM {$from}   
	where id_cabang = '$id_cabang'
	";
	$where = "";
	$whereTemp = "";
	if (
		$whereTemp != '' && $where != ''
	) $where .= " AND (" . $whereTemp . ")";
	else if ($whereTemp != '') $where .= $whereTemp;
	// search
	if (isset($post['search']['value']) && $post['search']['value'] != '') {
		$search = $post['search']['value'];
		// create parameter pencarian kesemua kolom yang tertulis
		// di $columns
		$whereTemp = "";
		for ($i = 0; $i < count($columnsSearch); $i++) {
			$whereTemp .= $columnsSearch[$i] . ' LIKE "%' . $search . '%"';
			// agar tidak menambahkan 'OR' diakhir Looping
			if ($i < count($columnsSearch) - 1) {
				$whereTemp .= ' OR ';
			}
		}
		if ($where != '') $where .= " AND (" . $whereTemp . ")";
		else $where .= $whereTemp;
	}
	if ($where != '') $sql .= ' WHERE (' . $where . ')';
	//SORT Kolom
	$sortColumn = isset($post['order'][0]['column']) ? $post['order'][0]['column'] : 1;
	$sortDir    = isset($post['order'][0]['dir']) ? $post['order'][0]['dir'] : 'asc';
	$sortColumn = $columns[$sortColumn - 1];
	$sql .= " ORDER BY {$sortColumn} {$sortDir}";
	$count = $this->db->query($sql);
	$totaldata = $count->num_rows();
	$start  = isset($post['start']) ? $post['start'] : 0;
	$length = isset($post['length']) ? $post['length'] : 10;
	$sql .= " LIMIT {$start}, {$length}";
	$data  = $this->db->query($sql);
	return array(
		'totalData' => $totaldata,
		'data' => $data,
	);
}
	public function getDataMeja($id_cabang='1')
	{
		// var_dump($id_cabang);die;
		// $this->db->from('cabang');
		// $sql = $this->db->get();
		// return $sql->result();

		return $this->db->where('id_cabang', $id_cabang)
		->get('meja')
		->result();		  
	}
	public function getDataTransaksiLimit($namaTable,$where=[],$select='*',$limit=false)
	{
		$this->db->select($select);
		if($limit!=false)$this->db->limit($limit);
		
		$this->db->order_by('id_transaksi', 'desc');
		
		
		$this->db->where($where);
		$query = $this->db->get($namaTable);
		return $query->result();
	}
	public function data_AllLogo($post)
	{
		$columns = array(
			'id_logo',
			'logo',
		);
		$columnsSearch = array(
			'id_logo',
			'logo',
		);
		// gunakan join disini
		$from = 'logo';
		// custom SQL
		$sql = "SELECT* FROM {$from}   
		";
		$where = "";
		$whereTemp = "";
		if (
			$whereTemp != '' && $where != ''
		) $where .= " AND (" . $whereTemp . ")";
		else if ($whereTemp != '') $where .= $whereTemp;
		// search
		if (isset($post['search']['value']) && $post['search']['value'] != '') {
			$search = $post['search']['value'];
			// create parameter pencarian kesemua kolom yang tertulis
			// di $columns
			$whereTemp = "";
			for ($i = 0; $i < count($columnsSearch); $i++) {
				$whereTemp .= $columnsSearch[$i] . ' LIKE "%' . $search . '%"';
				// agar tidak menambahkan 'OR' diakhir Looping
				if ($i < count($columnsSearch) - 1) {
					$whereTemp .= ' OR ';
				}
			}
			if ($where != '') $where .= " AND (" . $whereTemp . ")";
			else $where .= $whereTemp;
		}
		if ($where != '') $sql .= ' WHERE (' . $where . ')';
		//SORT Kolom
		$sortColumn = isset($post['order'][0]['column']) ? $post['order'][0]['column'] : 1;
		$sortDir    = isset($post['order'][0]['dir']) ? $post['order'][0]['dir'] : 'asc';
		$sortColumn = $columns[$sortColumn - 1];
		$sql .= " ORDER BY {$sortColumn} {$sortDir}";
		$count = $this->db->query($sql);
		$totaldata = $count->num_rows();
		$start  = isset($post['start']) ? $post['start'] : 0;
		$length = isset($post['length']) ? $post['length'] : 10;
		$sql .= " LIMIT {$start}, {$length}";
		$data  = $this->db->query($sql);
		return array(
			'totalData' => $totaldata,
			'data' => $data,
		);
	}
}
                        
/* End of file SemuaModel.php */
