<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Owner extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('MenuModel');
		$this->load->model('SemuaModel');
		$this->load->model('TransaksiModel');
		$this->load->model('AdminModel');
		$this->load->helper('url');
	}

	public function index()
	{
		$this->cekLoginOwner();
		$date = date("Y-m-d");
		$dateYesterday = date("Y-m-d", strtotime("-1 days"));
		$id_cabang = 0;
		$obj['graph'] = $this->TransaksiModel->graph($id_cabang);
		$obj['menuItem'] = $this->TransaksiModel->graphTransaksiDetMenu($id_cabang);
		$obj['TransaksiHarian'] = $this->TransaksiModel->graphTransaksiHarian($id_cabang);

		$obj['hariIni'] = $this->TransaksiModel->getCountTransaksiHarian($date);
		$obj['kemarin'] = $this->TransaksiModel->getCountTransaksiHarian($dateYesterday);
		$obj['menuHariIni'] = ($this->TransaksiModel->getCountMenuHarian($date) == null) ? 0 : $this->TransaksiModel->getCountMenuHarian($date);
		$obj['menuKemarin'] = ($this->TransaksiModel->getCountMenuHarian($dateYesterday) == null) ? 0 : $this->TransaksiModel->getCountMenuHarian($dateYesterday);
		$obj['ci'] = $this;
		$obj['header'] = $this->SemuaModel->getSeting();
		$obj['content'] =  "admin/home";
		$obj['owner'] = true;
		$this->load->view('admin/templates/index', $obj);
	}
	public function master_admin()
	{
		$this->cekLoginOwner();
		$obj['header'] = $this->SemuaModel->getSeting();
		$obj['cabang'] = $this->SemuaModel->getCabang();
		$obj['owner'] = true;
		$obj['ci'] = $this;
		$obj['listKategori'] = $this->SemuaModel->getAllRole();
		$obj['content'] = 'admin/master_admin';
		$this->load->view('admin/templates/index', $obj);
	}
	function cekLoginOwner()
	{
		if (!$this->isLoggedInOwner()) {
			$this->session->set_flashdata(
				'notifikasi',
				array(
					'alert' => 'alert-danger',
					'message' => 'Silahkan Login terlebih dahulu.',
				)
			);
			redirect('LoginOwner');
		}
	}
	public function isLoggedInOwner()
	{
		if ($this->session->userdata('owner_session'))
			return true; // sudah login
		else
			return false; // belum login
	}
	public function master_cabang()
	{
		$id_admin = $this->session->userdata('id_admin');
		$role = $this->AdminModel->getRole($id_admin, 'data_admin')->r;
		$obj['header'] = $this->SemuaModel->getSeting();
		$obj['owner'] = true;
		if ($role == 1) {
			$obj['listKategori'] = $this->SemuaModel->getAllRole();
			$obj['content'] = 'admin/master_cabang';
			$this->load->view('admin/templates/index', $obj);
		} elseif ($role == 0) {
			$obj['ci'] = $this;
			$obj['content'] =  "admin/blank";

			$this->load->view('admin/templates/index', $obj);
		}
	}

	public function master_item()
	{
		$this->cekLoginOwner();
		$id_admin = $this->session->userdata('id_admin');
		$obj['header'] = $this->SemuaModel->getSeting();
		$role = $this->AdminModel->getRole($id_admin, 'master_menu')->r;
		$obj['owner'] = true;
		if ($role == 1) {
			$obj['listKategori'] = $this->MenuModel->getKategori();
			$obj['ci'] = $this;
			$obj['content'] = 'admin/master_item';
			$this->load->view('admin/templates/index', $obj);
		} elseif ($role == 0) {
			$obj['ci'] = $this;
			$obj['content'] =  "admin/blank";
			$this->load->view('admin/templates/index', $obj);
		}
	}
	public function master_kasir()
	{
		$obj['header'] = $this->SemuaModel->getSeting();
		$obj['cabang'] = $this->SemuaModel->getCabang();
		$this->cekLoginOwner();
		$obj['owner'] = true;

		$id_admin = $this->session->userdata('id_admin');
		$role = $this->AdminModel->getRole($id_admin, 'data_kasir')->r;
		if ($role == 1) {
			$obj['ci'] = $this;
			$obj['content'] = 'admin/master_kasir';
			$this->load->view('admin/templates/index', $obj);
		} elseif ($role == 0) {
			$obj['ci'] = $this;
			$obj['content'] =  "admin/blank";
			$this->load->view('admin/templates/index', $obj);
		}
	}
	public function setting()
	{
		$obj['header'] = $this->SemuaModel->getSeting();
		$obj['owner'] = true;

		$this->cekLoginOwner();
		$id_admin = $this->session->userdata('id_admin');
		$role = $this->AdminModel->getRole($id_admin, 'seeting')->r;
		if ($role == 1) {
			$obj['ci'] = $this;
			$obj['content'] = 'admin/setting';
			$this->load->view('admin/templates/index', $obj);
		} elseif ($role == 0) {
			$obj['ci'] = $this;
			$obj['content'] =  "admin/blank";
			$this->load->view('admin/templates/index', $obj);
		}
	}
	public function master_transaksi($id_cabang=null)
	{
		if($id_cabang==null) redirect('Owner','refresh');
		
		$obj['header'] = $this->SemuaModel->getSeting();
		$obj['owner'] = true;
		$obj['id_cabang'] = $id_cabang;

		$this->cekLoginOwner();

		$id_admin = $this->session->userdata('id_admin');
		$role = $this->AdminModel->getRole($id_admin, 'master_transaksi')->r;
		if ($role == 1) {
			$obj['ci'] = $this;
			$obj['listKategori'] = $this->SemuaModel->getAllRole();
			$obj['content'] = 'admin/master_transaksi';
			$this->load->view('admin/templates/index', $obj);
		} elseif ($role == 0) {
			$obj['ci'] = $this;
			$obj['content'] =  "admin/blank";
			$this->load->view('admin/templates/index', $obj);
		}
	}
	public function master_role()
	{
		$obj['header'] = $this->SemuaModel->getSeting();
		$obj['owner'] = true;


		$this->cekLoginOwner();
		$id_admin = $this->session->userdata('id_admin');
		$role = $this->AdminModel->getRole($id_admin, 'data_admin')->r;
		if ($role == 1) {
			$obj['ci'] = $this;
			$obj['content'] = 'admin/master_role';
			$this->load->view('admin/templates/index', $obj);
		} elseif ($role == 0) {
			$obj['ci'] = $this;
			$obj['content'] =  "admin/blank";
			$this->load->view('admin/templates/index', $obj);
		}


		# code...
	}
	public function master_histori()
	{
		$obj['header'] = $this->SemuaModel->getSeting();
		$obj['owner'] = true;
		$obj['cabang'] = $this->SemuaModel->getCabang();

		$this->cekLoginOwner();
		$id_admin = $this->session->userdata('id_admin');
		$role = $this->AdminModel->getRole($id_admin, 'histori')->r;
		if ($role == 1) {
			$obj['ci'] = $this;

			$obj['listkategorilog'] = $this->AdminModel->getAllKategorilogadmin();
			$obj['content'] = 'admin/histori_admin';
			$this->load->view('admin/templates/index', $obj);
		} elseif ($role == 0) {
			$obj['ci'] = $this;
			$obj['content'] =  "admin/blank";
			$this->load->view('admin/templates/index', $obj);
		}


		# code...
	}
	public function master_slider()
	{
		$id_admin = $this->session->userdata('id_admin');
		$role = $this->AdminModel->getRole($id_admin, 'data_admin')->r;
		$obj['owner'] = true;

		if ($role == 1) {
			$obj['listKategori'] = $this->SemuaModel->getAllRole();
			$obj['content'] = 'admin/master_slider';
			$this->load->view('admin/templates/index', $obj);
		} elseif ($role == 0) {
			$obj['ci'] = $this;
			$obj['content'] =  "admin/blank";

			$this->load->view('admin/templates/index', $obj);
		}
	}
	public function master_kategori()
	{
		$obj['header'] = $this->SemuaModel->getSeting();
		$obj['owner'] = true;

		$this->cekLoginOwner();
		$id_admin = $this->session->userdata('id_admin');
		$role = $this->AdminModel->getRole($id_admin, 'master_kategori')->r;
		if ($role == 1) {

			$obj['listKategori'] = $this->MenuModel->getKategori();
			$obj['ci'] = $this;
			$obj['content'] = 'admin/master_kategori';
			$this->load->view('admin/templates/index', $obj);
		} elseif ($role == 0) {
			$obj['ci'] = $this;
			$obj['content'] =  "admin/blank";
			$this->load->view('admin/templates/index', $obj);
		}
	}

	public function master_laporan()
	{
		$obj['header'] = $this->SemuaModel->getSeting();
		$obj['owner'] = true;

		$this->load->helper('form');

		$id_admin = $this->session->userdata('id_admin');
		$role = $this->AdminModel->getRole($id_admin, 'data_admin')->r;
		if ($role == 1) {
			$obj['listKategori'] = $this->SemuaModel->getAllRole();
			$obj['content'] = 'admin/master_laporan';
			$this->load->view('admin/templates/index', $obj);
		} elseif ($role == 0) {
			$obj['ci'] = $this;
			$obj['content'] =  "admin/blank";
			$this->load->view('admin/templates/index', $obj);
		}
	}
	public function lihat_laporan_harian()
	{
		$tanggal = $this->input->get_post('tanggal');
		$bulan = $this->input->get_post('bulan');
		$obj['header'] = $this->SemuaModel->getSeting();
		$obj['owner'] = true;

		$tahun = $this->input->get_post('tahun');
		$obj['data'] = $this->TransaksiModel->getLaporanHarian($tanggal, $bulan, $tahun);
		$obj['tanggal'] = $tanggal;
		$obj['bulan'] = $bulan;
		$obj['tahun'] = $tahun;
		$id_admin = $this->session->userdata('id_admin');
		$role = $this->AdminModel->getRole($id_admin, 'data_admin')->r;
		if ($role == 1) {
			$obj['listKategori'] = $this->SemuaModel->getAllRole();
			$obj['content'] = 'admin/master_laporan_harian';
			$this->load->view('admin/templates/index', $obj);
		} elseif ($role == 0) {
			$obj['ci'] = $this;
			$obj['content'] =  "admin/blank";
			$this->load->view('admin/templates/index', $obj);
		}
	}
	public function lihat_laporan_bulanan()
	{
		$obj['header'] = $this->SemuaModel->getSeting();
		$bulan = $this->input->get_post('bulan');
		$tahun = $this->input->get_post('tahun');
		$obj['owner'] = true;

		$obj['data'] = $this->TransaksiModel->getLaporanBulanan($bulan, $tahun);
		$obj['bulan'] = $bulan;
		$obj['tahun'] = $tahun;
		$id_admin = $this->session->userdata('id_admin');
		$role = $this->AdminModel->getRole($id_admin, 'data_admin')->r;
		if ($role == 1) {
			$obj['listKategori'] = $this->SemuaModel->getAllRole();
			$obj['content'] = 'admin/master_laporan_bulanan';
			$this->load->view('admin/templates/index', $obj);
		} elseif ($role == 0) {
			$obj['ci'] = $this;
			$obj['content'] =  "admin/blank";
			$this->load->view('admin/templates/index', $obj);
		}
	}
	public function lihat_laporan_tahunan()
	{
		$tahun = $this->input->get_post('tahun');
		$obj['owner'] = true;

		$obj['data'] = $this->TransaksiModel->getLaporanTahunan($tahun);
		$obj['tahun'] = $tahun;
		$id_admin = $this->session->userdata('id_admin');
		$role = $this->AdminModel->getRole($id_admin, 'data_admin')->r;
		if ($role == 1) {
			$obj['listKategori'] = $this->SemuaModel->getAllRole();
			$obj['content'] = 'admin/master_laporan_tahunan';
			$this->load->view('admin/templates/index', $obj);
		} elseif ($role == 0) {
			$obj['ci'] = $this;
			$obj['content'] =  "admin/blank";
			$this->load->view('admin/templates/index', $obj);
		}
	}
	public function laporan_harian_pdf()
	{

		$this->load->library('pdf');
		$obj['owner'] = true;

		$tanggal = $this->input->get_post('tanggal');
		$bulan = $this->input->get_post('bulan');
		$tahun = $this->input->get_post('tahun');
		$obj['data'] = $this->TransaksiModel->getLaporanHarian($tanggal, $bulan, $tahun);
		$obj['tanggal'] = $tanggal;
		$obj['bulan'] = $bulan;
		$obj['tahun'] = $tahun;
		$obj['ci'] = $this;
		$obj['bu'] = base_url();

		$this->pdf->setPaper('A4', 'potrait');
		$this->pdf->set_base_path(base_url() . "assets/admin/vendors/bootstrap/dist/css/bootstrap.min.css");

		$obj['title'] = "laporan_harian_$tanggal-$bulan-$tahun.pdf";
		$this->pdf->load_view('admin/laporan_harian', $obj);
		// $this->load->view('admin/laporan_pdf', $obj);

	}
	public function laporan_bulanan_pdf()
	{

		$this->load->library('pdf');
		$bulan = $this->input->get_post('bulan');
		$tahun = $this->input->get_post('tahun');
		$obj['owner'] = true;

		$obj['data'] = $this->TransaksiModel->getLaporanBulanan($bulan, $tahun);
		$obj['bulan'] = $bulan;
		$obj['tahun'] = $tahun;
		$obj['ci'] = $this;
		$obj['bu'] = base_url();

		$this->pdf->setPaper('A4', 'potrait');
		$this->pdf->set_base_path(base_url() . "assets/admin/vendors/bootstrap/dist/css/bootstrap.min.css");

		$obj['title'] = "laporan_bulanan_$bulan-$tahun.pdf";
		$this->pdf->load_view('admin/laporan_bulanan', $obj);
		// $this->load->view('admin/laporan_pdf', $obj);

	}
	public function laporan_tahunan_pdf()
	{

		$this->load->library('pdf');
		$tahun = $this->input->get_post('tahun');
		$obj['owner'] = true;

		$obj['data'] = $this->TransaksiModel->getLaporanTahunan($tahun);
		$obj['tahun'] = $tahun;
		$obj['ci'] = $this;
		$obj['bu'] = base_url();

		$this->pdf->setPaper('A4', 'potrait');
		$this->pdf->set_base_path(base_url() . "assets/admin/vendors/bootstrap/dist/css/bootstrap.min.css");

		$obj['title'] = "laporan_tahunan_-$tahun.pdf";
		$this->pdf->load_view('admin/laporan_tahunan', $obj);
		// $this->load->view('admin/laporan_pdf', $obj);

	}
	public function master_meja()
	{
		$this->cekLoginOwner();
		$obj['header'] = $this->SemuaModel->getSeting();
		$obj['owner'] = true;

		$id_admin = $this->session->userdata('id_admin');
		$role = $this->AdminModel->getRole($id_admin, 'master_menu')->r;
		if ($role == 1) {

			$obj['listKategori'] = $this->MenuModel->getKategori();
			$obj['ci'] = $this;
			$obj['content'] = 'admin/master_meja';
			$this->load->view('admin/templates/index', $obj);
		} elseif ($role == 0) {
			$obj['ci'] = $this;
			$obj['content'] =  "admin/blank";
			$this->load->view('admin/templates/index', $obj);
		}
	}

	public function master_penjualan_menu()
	{
		$obj['header'] = $this->SemuaModel->getSeting();
		$obj['owner'] = true;

		$tanggal = $this->input->get_post('tanggal');
		$bulan = $this->input->get_post('bulan');
		$tahun = $this->input->get_post('tahun');
		$obj['data'] = $this->TransaksiModel->getLaporanHarian($tanggal, $bulan, $tahun);
		$obj['tanggal'] = $tanggal;
		$obj['bulan'] = $bulan;
		$obj['tahun'] = $tahun;
		$id_admin = $this->session->userdata('id_admin');
		$role = $this->AdminModel->getRole($id_admin, 'data_admin')->r;
		if ($role == 1) {
			$obj['listKategori'] = $this->SemuaModel->getAllRole();
			$obj['content'] = 'admin/penjualan_harian';
			$this->load->view('admin/templates/index', $obj);
		} elseif ($role == 0) {
			$obj['ci'] = $this;
			$obj['content'] =  "admin/blank";
			$this->load->view('admin/templates/index', $obj);
		}
	}
	public function laporan_menu_harian_pdf()
	{
		$obj['header'] = $this->SemuaModel->getSeting();
		$obj['owner'] = true;

		$this->load->library('pdf');
		$tanggal = $this->input->get_post('tanggal');
		$obj['data'] = $this->TransaksiModel->getMenuLaporanHarian($tanggal);
		$obj['tanggal'] = $tanggal;
		$obj['ci'] = $this;
		$obj['bu'] = base_url();

		$this->pdf->setPaper('A4', 'potrait');
		$this->pdf->set_base_path(base_url() . "assets/admin/vendors/bootstrap/dist/css/bootstrap.min.css");

		$obj['title'] = "laporan_harian_$tanggal.pdf";
		$this->pdf->load_view('admin/laporan_menu_harian', $obj);
		// $this->load->view('admin/laporan_pdf', $obj);

	}
	public function master_transaksi_offline($id_cabang=null)
	{
		if($id_cabang==null) redirect('Owner','refresh');
		$obj['id_cabang'] = $id_cabang;
		$obj['header'] = $this->SemuaModel->getSeting();
		$obj['owner'] = true;

		$this->cekLoginOwner();

		$id_admin = $this->session->userdata('id_admin');
		$role = $this->AdminModel->getRole($id_admin, 'master_transaksi')->r;
		if ($role == 1) {
			$obj['ci'] = $this;
			$obj['listKategori'] = $this->SemuaModel->getAllRole();
			$obj['content'] = 'admin/master_transaksi_offline';
			$this->load->view('admin/templates/index', $obj);
		} elseif ($role == 0) {
			$obj['ci'] = $this;
			$obj['content'] =  "admin/blank";
			$this->load->view('admin/templates/index', $obj);
		}
	}
	public function logo()
	{
		$id_admin = $this->session->userdata('id_admin');
		$role = $this->AdminModel->getRole($id_admin, 'data_admin')->r;
		$obj['owner'] = true;
		$obj['header'] = $this->SemuaModel->getSeting();

		if ($role == 1) {
			$obj['content'] = 'admin/master_logo';
			$this->load->view('admin/templates/index', $obj);
		} elseif ($role == 0) {
			$obj['ci'] = $this;
			$obj['content'] =  "admin/blank";

			$this->load->view('admin/templates/index', $obj);
		}
	}
}
