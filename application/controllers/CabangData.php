<?php

defined('BASEPATH') or exit('No direct script access allowed');

class CabangData extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('MenuModel');
        $this->load->model('KasirModel');
        $this->load->model('AdminModel');
        $this->load->model('SemuaModel');
        $this->load->model('TransaksiModel');
        $this->load->model('KategoriModel');
        $this->load->model('MejaModel');
        $this->load->helper('url');
        $this->load->helper('rupial');

    }

    public function getAllMenuItem()
    {
        $bu = base_url();
        $dt = $this->MenuModel->data_AllMenuCabang($_POST);
        $datatable['draw'] = isset($_POST['draw']) ? $_POST['draw'] : 1;
        $datatable['recordsTotal'] = $dt['totalData'];
        $datatable['recordsFiltered'] = $dt['totalData'];
        $datatable['data'] = array();
        $start = isset($_POST['start']) ? $_POST['start'] : 0;
        // var_dump($dt['data']->result());die();
        $no = $start + 1;
        foreach ($dt['data']->result() as $row) {
            $fields = array($no++);

            $pajak = '';
            if($row->pajak!=null){
                $pajak = "$row->pajak %";
            }

            $btnDetail = '
			<button class="btn btn-info detailInfo"
			data-id_menu="' . $row->id_menu . '"
			nama_menu="' . $row->nama_menu . '"
			>Detail Log</button> ';

            $btnHapus = '
			<button class="btn btn-danger hapus" data-id_menu="' . $row->id_menu . '" nama_menu="' . $row->nama_menu . '"
			>Hapus</button>
			';
            $btnUbah = '
			<button class="btn btn-info btn_edit"  data-toggle="modal" data-target=".bs-example-modal-lg"
			data-id_menu="' . $row->id_menu . '"
			data-stok="' . $row->stok . '"
			data-nama_menu="' . $row->nama_menu . '"
			data-id_kategori="' . $row->id_kategori . '"
			data-harga="' . $row->harga . '"
			data-foto="' . $row->foto . '"
			></i> Ubah</button>  ';

            $fields[] = $row->nama_menu . '<br>';
            $fields[] = $row->nama_kategori . '<br>';
            $fields[] = $row->harga . '<br>';
            $fields[] = $row->promo . '<br>';
            // $fields[] = $pajak . '<br>';
            $fields[] = $row->stok . '<br>';
            $fields[] = '<img src="../assets/images/foods/' . $row->foto . '" id="image" alt="image"><br>';
            $fields[] = "$btnUbah $btnHapus $btnDetail";

            $datatable['data'][] = $fields;
        }

        echo json_encode($datatable);

        exit();
    }
    public function getAllKasir()
    {
        $bu = base_url();
        $dt = $this->KasirModel->data_Cabang($_POST);
        $datatable['draw'] = isset($_POST['draw']) ? $_POST['draw'] : 1;
        $datatable['recordsTotal'] = $dt['totalData'];
        $datatable['recordsFiltered'] = $dt['totalData'];
        $datatable['data'] = array();
        $start = isset($_POST['start']) ? $_POST['start'] : 0;
        // var_dump($dt['data']->result());die();
        $no = $start + 1;
        foreach ($dt['data']->result() as $row) {
            $fields = array($no++);
            // $fields[] = $row->id_menu . '<br>';
            $fields[] = $row->cabang . '<br>';
            $fields[] = $row->nama_kasir . '<br>';
            $fields[] = $row->username . '<br>';
            $fields[] = $row->last_login;
            $fields[] = '

			<button class="btn btn-round btn-info btn_edit"  data-toggle="modal" data-target=".bs-example-modal-lg"
			 data-id_kasir="' . $row->id_kasir . '" data-nama_kasir="' . $row->nama_kasir . '"
			data-username="' . $row->username . '"
			data-password="' . $row->password . '"
			data-id_cabang="' . $row->id_cabang . '"
			></i> Ubah</button>

        <button class="btn btn-round btn-danger hapus" data-id_kasir="' . $row->id_kasir . '" data-nama_kasir="' . $row->nama_kasir . '"
        >Hapus</button>

        ';
            $datatable['data'][] = $fields;
        }

        echo json_encode($datatable);

        exit();
    }
    public function getAllAdmin()
    {
        $bu = base_url();
        $dt = $this->AdminModel->data_All($_POST);
        $datatable['draw'] = isset($_POST['draw']) ? $_POST['draw'] : 1;
        $datatable['recordsTotal'] = $dt['totalData'];
        $datatable['recordsFiltered'] = $dt['totalData'];
        $datatable['data'] = array();
        $start = isset($_POST['start']) ? $_POST['start'] : 0;
        // var_dump($dt['data']->result());die();
        $no = $start + 1;
        foreach ($dt['data']->result() as $row) {
            $fields = array($no++);
            // $fields[] = $row->id_menu . '<br>';
            $fields[] = $row->cabang . '<br>';
            $fields[] = $row->nama_admin . '<br>';
            $fields[] = $row->username . '<br>';
            $fields[] = $row->nama_role . '<br>';
            $fields[] = '

			<button class="btn btn-round btn-info btn_edit"  data-toggle="modal" data-target=".bs-example-modal-lg"

			data-id_admin="' . $row->id_admin . '"
			data-nama_admin="' . $row->nama_admin . '"
			data-id_cabang="' . $row->id_cabang . '"
			data-username="' . $row->username . '"
			data-password="' . $row->password . '"
			data-id_role="' . $row->id_role . '"
			></i> Ubah</button>

        <button class="btn btn-round btn-danger hapus"
				data-id_admin="' . $row->id_admin . '"
				data-nama_admin="' . $row->nama_admin . '"
        >Hapus</button>

        ';
            $datatable['data'][] = $fields;
        }
        echo json_encode($datatable);
        exit();
    }
    public function getAllSetting()
    {
        $bu = base_url();
        $dt = $this->SemuaModel->data_AllSetting($_POST);
        $datatable['draw'] = isset($_POST['draw']) ? $_POST['draw'] : 1;
        $datatable['recordsTotal'] = $dt['totalData'];
        $datatable['recordsFiltered'] = $dt['totalData'];
        $datatable['data'] = array();
        $start = isset($_POST['start']) ? $_POST['start'] : 0;
        // var_dump($dt['data']->result());die();
        $no = $start + 1;
        foreach ($dt['data']->result() as $row) {
            $fields = array($no++);
            $fields[] = $row->cabang . '<br>';
            $fields[] = $row->alamat_cabang . '<br>';
            $fields[] = $row->nomor_telp . '<br>';
            $fields[] = $row->facebook . '<br>';
            $fields[] = $row->instagram . '<br>';
            $fields[] = $row->twitter . '<br>';
            $fields[] = $row->youtube . '<br>';
            $fields[] = '
			<button class="btn  btn-info btn_edit"  
            data-toggle="modal" data-target=".bs-example-modal-lg"
			data-id_cabang="' . $row->id_cabang . '" 
            data-cabang="' . $row->cabang . '"
            data-alamat_cabang="' . $row->alamat_cabang . '"
            data-nomor_telp="' . $row->nomor_telp . '"
            data-facebook="' . $row->facebook . '"
            data-instagram="' . $row->instagram . '"
            data-twitter="' . $row->twitter . '"
            data-youtube="' . $row->youtube . '"
			></i> Ubah</button>


        ';
            $datatable['data'][] = $fields;
        }
        echo json_encode($datatable);
        exit();
    }
    public function getAllTransaksi()
    {
        $bu = base_url();
        $dt = $this->TransaksiModel->dt_transaksiCabang($_POST);
        $datatable['draw'] = isset($_POST['draw']) ? $_POST['draw'] : 1;
        $datatable['recordsTotal'] = $dt['totalData'];
        $datatable['recordsFiltered'] = $dt['totalData'];
        $datatable['data'] = array();
        $start = isset($_POST['start']) ? $_POST['start'] : 0;
        // var_dump($dt['data']->result());die();
        $no = $start + 1;
        foreach ($dt['data']->result() as $row) {
            $fields = array($no++);
            $pesanan = strtoupper(str_replace('_', ' ', $row->tipe_pesan));
            $tombolDetail = '
			<button class="btn  btn-info btn_edit"  data-toggle="modal" data-target=".bs-example-modal-lg"
			 data-id_transaksi="' . $row->id_transaksi . '" data-kode_transaksi="' . $row->kode_transaksi . '"
			><i class="fa fa-info" aria-hidden="true"></i>
			Detail</button>
			';
            $tombolPrint = '
			<button class="btn btn-info btn_print"
				data-toggle="modal"
				data-target=".bs-example-modal-lg"
			 	data-id_transaksi="' . $row->id_transaksi . '"
				data-kode_transaksi="' . $row->kode_transaksi . '"
			> <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
			Print</button>
			';

            $tombolHapus = '
			<button class="btn btn-round btn-danger hapus" data-id_transaksi="' . $row->id_transaksi . '"
			>Hapus</button>
			';
            $fields[] = $row->nama_kasir . '<br>';
            $fields[] = $row->kode_transaksi . '<br>';
            $fields[] = $pesanan . '<br>';
            $fields[] = $row->nama_user . '<br>';

            $fields[] = rupiah($row->harga_total) . '<br>';
            $fields[] = 'Selesai <br>';
            $fields[] = $tombolDetail . $tombolPrint;
            $datatable['data'][] = $fields;
        }
        echo json_encode($datatable);

        exit();
    }
    public function getAllTransaksi_offline()
    {

        $bu = base_url();
        $dt = $this->TransaksiModel->dt_transaksi_offlineCabang($_POST);
        $datatable['draw'] = isset($_POST['draw']) ? $_POST['draw'] : 1;
        $datatable['recordsTotal'] = $dt['totalData'];
        $datatable['recordsFiltered'] = $dt['totalData'];
        $datatable['data'] = array();
        $start = isset($_POST['start']) ? $_POST['start'] : 0;
        // var_dump($dt['data']->result());die();
        $no = $start + 1;
        foreach ($dt['data']->result() as $row) {
            $pesanan = strtoupper(str_replace('_', ' ', $row->tipe_pesan));
            $fields = array($no++);
            $tombolDetail = '
			<button class="btn  btn-info btn_edit"  data-toggle="modal" data-target=".bs-example-modal-lg"
			 data-id_transaksi="' . $row->id_transaksi . '" data-kode_transaksi="' . $row->kode_transaksi . '"
			><i class="fa fa-info" aria-hidden="true"></i>
			Detail</button>
			';
            $tombolPrint = '
			<button class="btn btn-info btn_print"
				data-toggle="modal"
				data-target=".bs-example-modal-lg"
			 	data-id_transaksi="' . $row->id_transaksi . '"
				data-kode_transaksi="' . $row->kode_transaksi . '"
			> <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
			Print</button>
			';

            $tombolHapus = '
			<button class="btn btn-round btn-danger hapus" data-id_transaksi="' . $row->id_transaksi . '"
			>Hapus</button>
			';
            $fields[] = $row->nama_user . '<br>';
            $fields[] = $row->kode_transaksi . '<br>';
            $fields[] = $pesanan . '<br>';
            $fields[] = $row->nomor_meja . '<br>';

            $fields[] = rupiah($row->harga_total) . '<br>';
            $fields[] = $row->catatan . '<br>';
            $fields[] = 'Selesai <br>';
            $fields[] = $tombolDetail . $tombolPrint;
            $datatable['data'][] = $fields;
        }
        echo json_encode($datatable);

        exit();
    }
    public function getAllTransaksiDetail()
    {
        $bu = base_url();
        $dt = $this->TransaksiModel->dt_transaksi_detail($_POST);
        $datatable['draw'] = isset($_POST['draw']) ? $_POST['draw'] : 1;
        $datatable['recordsTotal'] = $dt['totalData'];
        $datatable['recordsFiltered'] = $dt['totalData'];
        $datatable['data'] = array();
        $start = isset($_POST['start']) ? $_POST['start'] : 0;
        // var_dump($dt['data']->result());die();
        $no = $start + 1;
        foreach ($dt['data']->result() as $row) {
            $fields = array($no++);
            // $fields[] = $row->id_menu . '<br>';
            $fields[] = $row->nama_menu . '<br>';
            $fields[] = $row->qty . '<br>';
            $fields[] = rupiah($row->harga) . '<br>';
            $fields[] = rupiah($row->total) . '<br>';

            $datatable['data'][] = $fields;
        }

        echo json_encode($datatable);

        exit();
    }
    public function getAllAdminRole()
    {
        $dt = $this->AdminModel->dt_admin_role($_POST);
        $datatable['draw'] = isset($_POST['draw']) ? $_POST['draw'] : 1;
        $datatable['recordsTotal'] = $dt['totalData'];
        $datatable['recordsFiltered'] = $dt['totalData'];
        $datatable['data'] = array();
        $start = isset($_POST['start']) ? $_POST['start'] : 0;
        $no = $start + 1;
        foreach ($dt['data']->result() as $row) {

            // var_dump($row->daftarBidderLihat);die;
            $fields = array($no++);
            $fields[] = $row->nama_role;
            $fields[] = '
        <button class="btn btn-warning my-1  btn-blocks btnEditAdmin  text-white"
          data-id_role="' . $row->id_role . '"
          data-nama_role="' . $row->nama_role . '"
          data-data_admin="' . $row->data_admin . '"
          data-data_kasir="' . $row->data_kasir . '"
          data-master_menu="' . $row->master_menu . '"

          data-master_transaksi="' . $row->master_transaksi . '"
          data-histori="' . $row->histori . '"
          data-seeting="' . $row->seeting . '"
		 ><i class="fa fa-edit"></i> Ubah</button>

        <button class="btn btn-danger my-1 btn-blocks  btnHapus text-white"

          data-id_role="' . $row->id_role . '" data-nama_role="' . $row->nama_role . '"

        ><i class="fa fa-trash"></i> Hapus</button>

        ';

            $datatable['data'][] = $fields;
        }

        echo json_encode($datatable);

        exit();
    }
    public function getHistoryAdmin()
    {
        $kategori = $this->input->post('id_spek', true);
        // var_dump($kategori);die;
        $date = $this->input->post('date', true);
        $selectDate = $this->input->post('selectDate', true);
        $id_user = $this->input->post('id_user');
        $dt = $this->AdminModel->history_admin_cabang($_POST);
        // var_dump($dt['data']->result());die();
        $datatable['draw'] = isset($_POST['draw']) ? $_POST['draw'] : 1;
        $datatable['recordsTotal'] = $dt['totalData'];
        $datatable['recordsFiltered'] = $dt['totalData'];
        $datatable['data'] = array();
        $start = isset($_POST['start']) ? $_POST['start'] : 0;
        $no = $start + 1;
        foreach ($dt['data']->result() as $row) {
            // var_dump(($row));die();
            $date = strtotime($row->created_at);
            $date2 = date('d-m-Y H:i:s', $date);
            $fields = array($no++);
            $fields[] = $date2;
            $fields[] = $row->nama_admin;
            $fields[] = $row->aksi;
            $datatable['data'][] = $fields;
        }
        echo json_encode($datatable);
        exit();
    }
    public function getAllSlider()
    {
        $bu = base_url();
        $dt = $this->SemuaModel->data_AllSliderCabang($_POST);
        $datatable['draw'] = isset($_POST['draw']) ? $_POST['draw'] : 1;
        $datatable['recordsTotal'] = $dt['totalData'];
        $datatable['recordsFiltered'] = $dt['totalData'];
        $datatable['data'] = array();
        $start = isset($_POST['start']) ? $_POST['start'] : 0;
        // var_dump($dt['data']->result());die();
        $no = $start + 1;
        foreach ($dt['data']->result() as $row) {

            $btnHapus = '<button class="btn  btn-danger hapus"
			 	data-id_slider=' . $row->id_slider . '
				data-nama_foto="' . $row->nama_foto . '"
				data-foto="' . $row->foto . '"
				></i> Hapus</button>';

            $btnEdit = '<button class="btn  btn-info btn_edit"
			 	data-id_slider=' . $row->id_slider . '
				data-nama_foto="' . $row->nama_foto . '"
				data-foto="' . $row->foto . '"
				></i> Ubah</button>';

            $fields = array($no++);
            $fields[] = $row->nama_foto . '<br>';
            $fields[] = '<img src="../assets/images/slider/' . $row->foto . '" id="image" alt="image"><br>';

            $fields[] = $btnEdit . $btnHapus;
            $datatable['data'][] = $fields;
        }
        echo json_encode($datatable);
        exit();
    }
    public function getAllKategori()
    {
        $bu = base_url();
        $dt = $this->KategoriModel->data_All_cabang($_POST);
        $datatable['draw'] = isset($_POST['draw']) ? $_POST['draw'] : 1;
        $datatable['recordsTotal'] = $dt['totalData'];
        $datatable['recordsFiltered'] = $dt['totalData'];
        $datatable['data'] = array();
        $start = isset($_POST['start']) ? $_POST['start'] : 0;
        $no = $start + 1;
        foreach ($dt['data']->result() as $row) {

            $fields = array($no++);
            $fields[] = $row->nama_kategori . '<br>';
            // $fields[] = '<img src="../assets/images/kategori/' . $row->foto . '" id="image" alt="image"><br>';
            $fields[] = '
			<button class="btn btn-round btn-info btn_edit"  data-toggle="modal" data-target=".bs-example-modal-lg"
			data-id_kategori="' . $row->id_kategori . '"
			data-nama_kategori="' . $row->nama_kategori . '"
			data-foto="' . $row->foto . '"
			></i> Ubah</button>

        <button class="btn btn-round btn-danger hapus" data-id_kategori="' . $row->id_kategori . '"
		nama_kategori="' . $row->nama_kategori . '"
        >Hapus</button>

        ';
            $datatable['data'][] = $fields;
        }
        echo json_encode($datatable);
        exit();
    }
    public function getAllMeja()
    {
        $bu = base_url();
        $dt = $this->MejaModel->dataCabang($_POST);
        $datatable['draw'] = isset($_POST['draw']) ? $_POST['draw'] : 1;
        $datatable['recordsTotal'] = $dt['totalData'];
        $datatable['recordsFiltered'] = $dt['totalData'];
        $datatable['data'] = array();
        $start = isset($_POST['start']) ? $_POST['start'] : 0;
        // var_dump($dt['data']->result());die();
        $no = $start + 1;
        foreach ($dt['data']->result() as $row) {
            $hapus = '
			<button class="btn btn-round btn-danger hapus"
			data-id_meja="' . $row->id_meja . '"
			nomor_meja="' . $row->nomor_meja . '"
			>Hapus</button>';

            $edit = '
			<button class="btn btn-round btn-info btn_edit"  data-toggle="modal" data-target=".bs-example-modal-lg"
			data-id_meja="' . $row->id_meja . '"
			data-nomor_meja="' . $row->nomor_meja . '"
			data-link="' . $row->link . '"
			data-qrcode="' . $row->qrcode . '"
			></i> Ubah</button>';

            $fields = array($no++);
            $fields[] = $row->nomor_meja . '<br>';
            $fields[] = $row->link . '<br>';
            $fields[] = '<img src="../uploads/images/' . $row->qrcode . '" id="image" alt="image"><br>';
            $fields[] = $hapus;
            $datatable['data'][] = $fields;
        }

        echo json_encode($datatable);

        exit();
    }
    public function getAllLogMenu()
    {

        $bu = base_url();
        $dt = $this->MenuModel->data_AllMenuDetailLog($_POST);
        $datatable['draw'] = isset($_POST['draw']) ? $_POST['draw'] : 1;
        $datatable['recordsTotal'] = $dt['totalData'];
        $datatable['recordsFiltered'] = $dt['totalData'];
        $datatable['data'] = array();
        $start = isset($_POST['start']) ? $_POST['start'] : 0;
        // var_dump($dt['data']->result());die();
        $no = $start + 1;
        foreach ($dt['data']->result() as $row) {
            $fields = array($no++);
            $fields[] = $row->nama_menu . '<br>';
            $fields[] = $row->qty . '<br>';
            $fields[] = rupiah($row->total) . '<br>';
            $fields[] = $row->created_at . '<br>';

            $datatable['data'][] = $fields;
        }

        echo json_encode($datatable);

        exit();
    }
    public function getMenuJualByDate()
    {
        $bu = base_url();
        $dt = $this->MenuModel->data_AllMenuByDateCabang($_POST);
        $datatable['draw'] = isset($_POST['draw']) ? $_POST['draw'] : 1;
        $datatable['recordsTotal'] = $dt['totalData'];
        $datatable['recordsFiltered'] = $dt['totalData'];
        $datatable['data'] = array();
        $start = isset($_POST['start']) ? $_POST['start'] : 0;
        $no = $start + 1;
        foreach ($dt['data']->result() as $row) {
            $newDate = date("Y-m-d H:m:s", strtotime($row->created_at));
            $fields = array($no++);
            $fields[] = $row->nama_menu . '<br>';
            $fields[] = $row->qty . '<br>';
            $fields[] = rupiah($row->total) . '<br>';
            $fields[] = $newDate . '<br>';

            $datatable['data'][] = $fields;
        }

        echo json_encode($datatable);

        exit();
    }
    public function getAllCabang()
    {
        $bu = base_url();
        $dt = $this->AdminModel->data_Cabang($_POST);
        $datatable['draw'] = isset($_POST['draw']) ? $_POST['draw'] : 1;
        $datatable['recordsTotal'] = $dt['totalData'];
        $datatable['recordsFiltered'] = $dt['totalData'];
        $datatable['data'] = array();
        $start = isset($_POST['start']) ? $_POST['start'] : 0;
        $no = $start + 1;
        foreach ($dt['data']->result() as $row) {
            $fields = array($no++);
            $fields[] = $row->cabang . '<br>';
            $fields[] = $row->alamat_cabang . '<br>';
            $fields[] = '

			<button class="btn btn-round btn-info btn_edit"
			data-toggle="modal" data-target=".bs-example-modal-lg"
			data-id_cabang="' . $row->id_cabang . '"
			data-cabang="' . $row->cabang . '"
			data-alamat="' . $row->alamat_cabang . '"
			></i> Ubah</button>

      <button class="btn btn-round btn-danger hapus"
			data-id_cabang="' . $row->id_cabang . '"
			data-cabang="' . $row->cabang . '"
      >Hapus</button>

        ';
            $datatable['data'][] = $fields;
        }

        echo json_encode($datatable);

        exit();
    }
		public function hapusSlider()
		{
			$id_slider = $this->input->post('id_slider', TRUE);
			$data = $this->SemuaModel->getDataByIdRow('slider', 'id_slider', $id_slider);
			$status = false;
			$message = 'Gagal menghapus Data!';
			if ($data == null) {
				$message .= '<br>Tidak terdapat Data yang dimaksud.';
			} else {
				$this->SemuaModel->HapusData('slider', 'id_slider', $id_slider);

				$id_admin = $this->session->userdata('id_admin');
				$aksi = 'Hapus slider ' . $data->nama_foto;
				$id_kategori = 6;
				$this->AdminModel->log($id_admin, $id_kategori, $aksi);
				$status = true;
				$message = 'Berhasil menghapus Data: <b>' . $data->nama_foto . '</b>';
			}
			echo json_encode(array(
				'status' => $status,
				'message' => $message,
			));
		}
}

/* End of file  Data.php */
