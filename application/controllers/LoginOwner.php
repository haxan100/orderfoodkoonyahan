<?php 
        
defined('BASEPATH') OR exit('No direct script access allowed');
        
class LoginOwner extends CI_Controller {
    public function __construct()
	{		
		ini_set('max_execution_time', 0);
		ini_set('memory_limit', '2048M');
		parent::__construct();
		$this->load->library(array('form_validation')); 
		//memanggil recapca
		$this->load->model('AdminModel');
	}


  public function index()
  {
      $obj['ci'] = $this;
      $this->load->view('owner/login', $obj);
  }

  public function login_proses()
  {       
    $this->load->library('form_validation');
    $username = $this->input->post('username', true);
    $password = $this->input->post('password', true);    
		$data = $this->AdminModel->loginOwnerAct($password,$username);
      if($data =='null'||$data==null){
          $status = false;
          $message = 'Username & password tidak cocok!';
      }else{
          $session = array(
              'owner_session' => true, // Buat session authenticated dengan value true
              'id_admin' => $data->id_owner, // Buat session authenticated
              'nama' => $data->nama, // Buat session authenticated
            );
            $this->session->set_userdata($session);          
          $status = true;
          $message = 'Selamat datang <span class="font-weight-bold">' . $data->nama . '</span>, sedang mengalihkan..';
      }    
    echo json_encode(array(
      'status' => $status,
      'message' => $message,
    ));
  }

        
}
        
    /* End of file  Login.php */
        
                            