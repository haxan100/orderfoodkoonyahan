<?php

use FontLib\Table\Type\post;

defined('BASEPATH') OR exit('No direct script access allowed');
        
class Cart extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('MenuModel');
		$this->load->model('SemuaModel');
		$this->load->model('ProdukModel');
		$this->load->model('CartModel');
		// $this->load->model('SekolahModel');
		$this->load->helper('url');
	}

public function indexOld()
{
		$sess =  $this->session->userdata('user_session');
		if ($sess == null) {

			redirect('Kasir/Login', 'refresh');
		}
			$data['konten'] = $this->SemuaModel->getSeting();
		$id_user = $this->session->userdata('id_kasir');

			// $id_user = 1 ;
			$cart = $this->CartModel->getCart($id_user);
			$data['totalcart'] = count($cart);
			$d = 0;
			// echo json_encode($cart);die;
			// var_dump($cart);die;
			foreach ($cart as $da) {
				$d += $da->total;
			}
			$totalHarga = $d;
			$data['totalHarga'] = $d;
		$data['cart_content']      = $cart;
		$this->load->view('Kasir/headers',$data);
		$this->load->view('Keranjang/all');
			            
}
public function checkout()
{
	$sess =  $this->session->userdata('user_session');
	$nama_kasir = $this->session->userdata('nama_kasir');
	$id_cabang = $this->session->userdata('id_cabang');
	$data['dataKategori']  = $this->SemuaModel->getDataById('kategori','id_cabang',$id_cabang);

	$data['id_cabang'] = $id_cabang;
	if ($sess == null) {
		redirect('Kasir/Login', 'refresh');
	}
	$data['konten'] = $this->SemuaModel->getSeting();
	$where = array('id_cabang' => $id_cabang );
	$data['meja'] = $this->SemuaModel->getData('meja',$where,'nomor_meja');
	$id_user = $this->session->userdata('id_kasir');
	$cart = $this->CartModel->getCart($id_user);
	$data['cart_content']      = $cart;
	$data['totalcart'] = count($cart);
	$d = 0;
	foreach ($cart as $da) {
		$d += $da->total;		
		$dataLog = array(
			'id_kategori' => 1,
			'id_menu' => $da->id_produk,
			'qty' => $da->qty,
			'created_by' => 'kasir',
			'nama' => $nama_kasir,
		);
		$this->SemuaModel->Tambah('log_menu',$dataLog);
		$this->SemuaModel->minus('menu','stok','id_menu',$da->id_produk);
	}
	$totalHarga = $d;
	$data['totalHarga'] = $d;
	$this->load->view('Kasir/headers',$data);
	$this->load->view('Keranjang/cekout',$data);
	
}
public function hapusSatuKeranjang()
{
		$id_user = $this->session->userdata('id_kasir');
	
	// var_dump($_POST);die;
	$id_produk = $this->input->post('id', TRUE);
	$msg = "Gagal Hapus";
	$status = false;
	if($this->SemuaModel->HapusCartSatu($id_user, $id_produk)){

		$msg = "Item Berhasil di Tambah Ke Keranjang";
		$status = true;

	}

		$data = array(
			'status' => $status,
			'msg' => $msg,
		);
		echo json_encode($data);


	# code...
}
public function updateCart()
{
	$id_user = $this->session->userdata('id_kasir');
	$id_produk = $this->input->post('id', TRUE);
	$catatan = $this->input->post('catatan', TRUE);
	$qty = $this->input->post('qty', TRUE);
	if($catatan!=""){
		$in = array(
			'qty' => $qty ,
			'catatan' => $catatan
		);

	}else{
		$in = array(
			'qty' => $qty ,
		);
		
	}
	$nama_kasir = $this->session->userdata('nama_kasir');
	$msg = "Gagal Update";
	$status = false;
	$getData = $this->CartModel->getAllCartByUser($id_user);
	$TotalgetData = count($this->CartModel->getAllCartByUser($id_user));
	$cart= $this->CartModel->getCart($id_user);
	$totalHarga =	" ";
	if($this->CartModel->updateCartByIDprodAndUser($in,$id_produk,$id_user)){
		$status = true;
		$msg = " berhasil merubah";		

		$cart = $this->CartModel->getCart($id_user);
			$data['totalcart'] = count($cart);
			$d = 0;
			foreach ($cart as $da) {
				$d += $da->total;
			}
			$totalHarga 			= $d;
			$data['totalHarga'] 	= $d;
			$data['cart_content'] 	= $cart;
			$totalHarga				= $d;	
	}
	$data = array(
		'status' => $status,
		'msg' => $msg,
		'data' => $getData,
		'harga' => $totalHarga,
	);
	echo json_encode($data);
}
public function updateCartonlyCatatan()
{
	$id_user = $this->session->userdata('id_kasir');
	$id_produk = $this->input->post('id', TRUE);
	$catatan = $this->input->post('catatan', TRUE);
	$in = array(
		'catatan' => $catatan
	);
	$nama_kasir = $this->session->userdata('nama_kasir');
	$msg = "Gagal Update";
	$status = false;
	$getData = $this->CartModel->getAllCartByUser($id_user);
	$TotalgetData = count($this->CartModel->getAllCartByUser($id_user));
	$cart= $this->CartModel->getCart($id_user);
	$totalHarga =	" ";
	if($this->CartModel->updateCartByIDprodAndUser($in,$id_produk,$id_user)){
		$status = true;
		$msg = " berhasil merubah";		

		$cart = $this->CartModel->getCart($id_user);
			$data['totalcart'] = count($cart);
			$d = 0;
			foreach ($cart as $da) {
				$d += $da->total;
			}
			$totalHarga 			= $d;
			$data['totalHarga'] 	= $d;
			$data['cart_content'] 	= $cart;
			$totalHarga				= $d;	
	}
	$data = array(
		'status' => $status,
		'msg' => $msg,
		'data' => $getData,
		'harga' => $totalHarga,
	);
	echo json_encode($data);
}
public function updateCartonlyCatatanUser()
{
	$id_produk = $this->input->post('id', TRUE);
	$id_meja = $this->input->post('id_meja', TRUE);
	$catatan = $this->input->post('catatan', TRUE);
	$in = array(
		'catatan' => $catatan
	);
	$msg = "Gagal Update";
	$status = false;
	$getData = $this->CartModel->getAllCartByIdMeja($id_meja);
	$TotalgetData = count($this->CartModel->getAllCartByIdMeja($id_meja));
	$cart= $this->CartModel->getCartMejaUser($id_meja);
	$totalHarga =	" ";
	if($this->CartModel->updateCartMejaByIdProdukAndIdMeja($in,$id_produk,$id_meja)){
		$status = true;
		$msg = " berhasil merubah";		

		$cart = $this->CartModel->getCartMejaUser($id_meja);
			$data['totalcart'] = count($cart);
			$d = 0;
			foreach ($cart as $da) {
				$d += $da->total;
			}
			$totalHarga 			= $d;
			$data['totalHarga'] 	= $d;
			$data['cart_content'] 	= $cart;
			$totalHarga				= $d;	
	}
	$data = array(
		'status' => $status,
		'msg' => $msg,
		'data' => $getData,
		'harga' => $totalHarga,
	);
	echo json_encode($data);
}
public function updateCartonlyCatatanUserqty()
{
	$id_produk = $this->input->post('id', TRUE);
	$id_meja = $this->input->post('id_meja', TRUE);
	$qty = $this->input->post('qty', TRUE);
	$in = array(
		'qty' => $qty ,
	);
	$msg = "Gagal Update";
	$status = false;
	$getData = $this->CartModel->getAllCartByIdMeja($id_meja);
	$TotalgetData = count($this->CartModel->getAllCartByIdMeja($id_meja));
	$cart= $this->CartModel->getCartMejaUser($id_meja);
	$totalHarga =	" ";
	if($this->CartModel->updateCartMejaByIdProdukAndIdMeja($in,$id_produk,$id_meja)){
		$status = true;
		$msg = " berhasil merubah";		

		$cart = $this->CartModel->getCartMejaUser($id_meja);
			$data['totalcart'] = count($cart);
			$d = 0;
			foreach ($cart as $da) {
				$d += $da->total;
			}
			$totalHarga 			= $d;
			$data['totalHarga'] 	= $d;
			$data['cart_content'] 	= $cart;
			$totalHarga				= $d;	
	}
	$data = array(
		'status' => $status,
		'msg' => $msg,
		'data' => $getData,
		'harga' => $totalHarga,
	);
	echo json_encode($data);
}
public function konfirmasi()
{
	// var_dump($_POST);
	$id_cabang = $this->input->post('id_cabang');
	$nama = $this->input->post('nama');
	$pilihtempat = $this->input->post('pilihtempat');
	$meja_id = $this->input->post('meja_id');
	$totalharga = $this->input->post('totalharga');
	$id_user = $this->session->userdata('id_kasir');
	$orderan = $this->input->post('orderan'); // grabfood dll
	$jenis = $this->input->post('jenis'); // 0 online || 1 offline
	$catatan = $this->input->post('catatan'); // 0 online || 1 offline

	if($totalharga==0||$totalharga==''||empty($totalharga)){
		$data = array(
			'status' => false,
			'msg' => 'Maaf, Data Tidak Valid, Coba Lagi Nanti',
			'id'=> 0,
		);
		echo json_encode($data);
		die();
	}

	$now = date('Y-m-d H:i:s');

	$cart = $this->CartModel->getCart($id_user);
	// var_dump($cart);die;
	$status = false;
	$msg ="";
	$id = 0;

	$data['totalcart'] = count($cart);
	$d = 0;
	if(intval($jenis)==0){
		$pesan = $orderan;
	}else{
		$pesan = $pilihtempat;
	}

	$ran = $this->generateRandomString(5);

	$inToTrans = array(
		'kode_transaksi' => $ran , 
		'nama_user' => $nama , 
		'id_cabang' => $id_cabang , 
		'tipe_pesan' => $pesan , 
		'type_penjualan_offline' => $jenis , 
		'nomor_meja' => $meja_id , 
		'harga_total' => $totalharga ,
		'id_user' => $id_user ,
		'created_at' => $now ,
		'catatan' => $catatan ,
		);
		// var_dump($inToTrans);die;
	$id_transaksi = $this->SemuaModel->AddTransaksi($inToTrans);

	foreach ($cart as $da) {
		$inTodetTrans = array(
			'id_cabang' =>$id_cabang,
			'id_transaksi' =>$id_transaksi,
			'id_menu' =>$da->id_produk,
			'qty' =>$da->qty,
			'total' =>$da->total,
			'catatan' =>$da->catatan,
	 	);
		 $this->SemuaModel->Tambah('transaksi_detail',$inTodetTrans);
	}
	if($id_transaksi!=null){
			$status = true;
			$msg = "Berhasil";
			$id = $id_transaksi;
			$this->SemuaModel->HapusCartByIdUser($id_user);
	}
		$data = array(
			'status' => $status,
			'msg' => $msg,
			'id'=> $id,
		);
		echo json_encode($data);
		die();
}
function generateRandomString($length = 10)
{
	$characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$charactersLength = strlen($characters);
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, $charactersLength - 1)];
	}
	$tgl = date('Y');

	// var_dump($tgl);die;


	return "TR_" . $tgl . $randomString;
}
public function selesai()
{

	$sess =  $this->session->userdata('user_session');
	$id_cabang =  $this->session->userdata('id_cabang');
	$data['dataKategori']  = $this->SemuaModel->getDataById('kategori','id_cabang',$id_cabang);

	// var_dump($sess);die;
	if ($sess == null) {
		redirect('Kasir/Login', 'refresh');
	}
	$id = $_GET['id'];
	$data['id'] = $id;
	$data['Data'] = $this->SemuaModel->getDataTransaksiById($id);
	$data['getData']=$this->SemuaModel->getDataFromDetTranAndPro($id);
	$data['konten'] = $this->SemuaModel->getSeting();
	$data['print_transaksi'] = true;

		// $this->load->view('keranjang/all');
		// $this->load->view('keranjang/cekoutOLD');
		$this->load->view('Kasir/headers', $data);
		// var_dump($Data);die;
		// $this->load->view('Keranjang/selesai');
		$this->load->view('Keranjang/selesai',$data);

		// $this->load->view('Kasir/footer', $data);

	
	# code...
}
public function getCartByIDUser()
{
	$id_cabang =  $this->input->post('id_cabang',true);
	$sess =  $this->session->userdata('user_session');
	if ($sess == null) {
		die("Tidak Ada Akses!");
	}
	$id_user = $this->session->userdata('id_kasir');	
	$cart = $this->CartModel->getCart($id_user);
	$data = count($cart);

	echo json_encode(array(
		'data' => $cart,
	));

}
public function getCartByIDUserMeja()
{
	$id_cabang =  $this->input->post('id_cabang',true);
	$id_meja =  $this->input->post('id_meja');
	$cart = $this->CartModel->getCartMejaUser($id_meja);
	$data = count($cart);

	echo json_encode(array(
		'data' => $cart,
	));

}
public function index()
{
	$sess =  $this->session->userdata('user_session');
	$id_cabang =  $this->session->userdata('id_cabang');
	$data['dataKategori']  = $this->SemuaModel->getDataById('kategori','id_cabang',$id_cabang);
        // var_dump($data);die;
	if ($sess == null) {

		redirect('Kasir/Login', 'refresh');
	}
	$data['konten'] = $this->SemuaModel->getSeting();
	$id_user = $this->session->userdata('id_kasir');


	$cart = $this->CartModel->getCart($id_user);
	$data['totalcart'] = count($cart);
	$d = 0;
	foreach ($cart as $da) {
		$d += $da->total;
	}
	$totalHarga = $d;
	$data['totalHarga'] = $d;
	$data['cart_content']      = $cart;
	$this->load->view('Kasir/headers',$data);
	$this->load->view('User/contohCart');
}
public function hapusSatuKeranjangUser()
{
	$id_produk = $this->input->post('id', TRUE);
	$id_meja = $this->input->post('id_meja', TRUE);
	$msg = "Item Berhasil di Tambah Ke Keranjang#";
		$status = true;
	// var_dump($this->SemuaModel->HapusCartMeja($id_meja, $id_produk));die;
	if($this->SemuaModel->HapusCartMeja($id_meja, $id_produk)){
		$msg = "Item Berhasil di Tambah Ke Keranjang";
		$status = true;
	}
	
		$data = array(
			'status' => $status,
			'msg' => $msg,
		);
		echo json_encode($data);
}
        
}
        
    /* End of file  Cart.php */
        
                            