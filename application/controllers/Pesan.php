<?php 
        
defined('BASEPATH') OR exit('No direct script access allowed');
        
class Pesan extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('MenuModel');
		$this->load->model('KasirModel');
		$this->load->model('AdminModel');
		$this->load->model('SemuaModel');
		$this->load->model('CartModel');
		$this->load->model('ProdukModel');
		$this->load->helper('url');
	}
public function index()
{
                
}
public function setBid()
{
	$id_produk = $this->input->post('id_produk', TRUE);
	$harga = $this->input->post('harga', TRUE);
	$id_cabang = $this->input->post('id_cabang', TRUE);
	$qty = $this->input->post('qty', TRUE);
	$now = date('Y-m-d H:i:s');
	$id_user = $this->session->userdata('id_kasir');
	$nama_kasir = $this->session->userdata('nama_kasir');
	$totalBarang = $this->ProdukModel->getProdukById($id_produk);
	if($totalBarang->stok <1){
		$data = array(
			'status' => false,
			'msg' => "Maaf!,Produk Kosong",
			'data' => [],
			'total' => [],
			'harga' => [],
		);
		echo json_encode($data);
		die();
	}

	$tgl = $now;
	$keranjangOld = $this->CartModel->getCartByIdUserAndProduk($id_user, $id_produk);
	$getHargaByIdProd = $this->CartModel->getProdByIdProd($id_produk)->harga;
	$getData = $this->CartModel->getAllCartByUser($id_user);
	$TotalgetData = count($this->CartModel->getAllCartByUser($id_user));
	$cart= $this->CartModel->getCart($id_user);	
	$d = 0 ;	
	foreach ($cart as $da ) {
		$d += $da->total;
	}	
	$totalHarga = $d;
	$hargaSemua = $totalHarga + $getHargaByIdProd; 
	$totalsemua = $TotalgetData + 1;
	if (count($keranjangOld) >= 1) {  // jika di keranjang ada produk , maka di updte qty nya
		// var_dump($keranjangOld);die;
		$qtyOld = $keranjangOld[0]->qty;
		$id_keranjang = $keranjangOld[0]->id_keranjang;
		$qtyNew = $qtyOld + $qty;
		$upd  = array(
			'qty' => $qtyNew,
		);
		$this->CartModel->updateCart($upd, $id_keranjang);			
		$msg = "Item Berhasil di Tambah Ke Keranjang";
		$status = true;
	} else {
		// var_dump("s");die;
		$data2 = array(
			'id_produk' => $id_produk,
			'id_cabang' => $id_cabang,
			'id_user' => $id_user,
			'qty' => $qty,
			'created_at' => $tgl,
		);

		if ($this->CartModel->AddCart($data2)) {
			$msg = "Item Berhasil di Tambah Ke Keranjang";
			$status = true;
		} else {
			$msg = "Item Gagal di Tambah Ke Keranjang";
			$status = false;
		}
	}

	if($status){
	}

	$data = array(
		'status' => $status,
		'msg' => $msg,
		'data' => $getData,
		'total' => $totalsemua,
		'harga' => $hargaSemua,
	);
	echo json_encode($data);
}

        
}
                            