<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Kasir extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('MenuModel');
        $this->load->model('SemuaModel');
        $this->load->model('ProdukModel');
        $this->load->model('CartModel');
        // $this->load->model('SekolahModel');
        $this->load->helper('url');
        $this->load->library('session');

        // require_once APPPATH . 'third_party/vendor/mike42/autoloader.php';
        require APPPATH . 'third_party/vendor/autoload.php';
    }
    public function getKategoriByIdCabang($id_cabang)
	{
		// $id_cabang = $this->input->post('id_cabang',true);
		$data = $this->SemuaModel->getDataById('kategori','id_cabang',$id_cabang);
        return $data;
	}
    public function transaksi()
    {
        $sess = $this->session->userdata('user_session');
        if ($sess == null) {
            redirect('Kasir/Login', 'refresh');
        }
        $id_user = $this->session->userdata('id_kasir');
        $data['konten'] = $this->SemuaModel->getSeting();
        $data['slider'] = $this->SemuaModel->getSlider();
        $data['kasir_login_slider'] = true;
        $id_cabang = $this->session->userdata('id_cabang');
        $data['dataKategori']  =$this->getKategoriByIdCabang($id_cabang);
        // var_dump($dataKategori);die;
		$data['id_cabang'] = $id_cabang;
        $cart = $this->CartModel->getCart($id_user);
        $data['totalcart'] = count($cart);
        $d = 0;
        foreach ($cart as $da) {
            $d += $da->total;
        }
        $totalHarga = $d;
        $data['totalHarga'] = $d;
        $this->load->view('Kasir/headersTransaksi', $data);
        // $this->load->view('Kasir/headers', $data);
        $this->load->view('Kasir/indexTransaksi', $data);
        $this->load->view('Kasir/footerTransaksi', $data);
    }
    public function index()
    {
        $sess = $this->session->userdata('user_session');
		$id_user = $this->session->userdata('id_kasir');
        // var_dump($id_user);die;
        if ($sess == null||$id_user==null) {
            redirect('Kasir/Login', 'refresh');
        }
        $data['konten'] = $this->SemuaModel->getSeting();
        $data['slider'] = $this->SemuaModel->getSlider();
        $data['kasir_login_slider'] = true;
        $id_cabang = $this->session->userdata('id_cabang');
        $data['dataKategori']  =$this->getKategoriByIdCabang($id_cabang);
		$data['id_cabang'] = $id_cabang;
        $cart = $this->CartModel->getCart($id_user);   
        // var_dump($cart);die;
        
		$data['totalcart'] = count($cart);
        $d = 0;
        foreach ($cart as $da) {
            $d += $da->total;
        }
        $totalHarga = $d;
        $data['totalHarga'] = $d;
        // $data['meja'] = $this->SemuaModel->getDataMeja(1);
        // var_dump($data);die;
        // var_dump($data);die;

        $this->load->view('newTampilan2', $data);
        // $this->load->view('Kasir/headers', $data);
        $this->load->view('Kasir/newOrder', $data);
        // $this->load->view('Kasir/footer', $data);
    }
    public function getProduk()
    {
        $id_kategori = $this->input->post('id_kategori', true);
        $id_cabang = $this->input->post('id_cabang', true);
        $page = $this->input->post('page', true);
        $produk = $this->input->post('produk', true);

        $hasil = "";
        $hasil = $this->ProdukModel->getProdukByIdTipeProduk($id_kategori, $page,$id_cabang,$produk);
        $data = array();
        $status = false;
        if (count($hasil) > 0) {
            $status = true;
            $data = $hasil;
        }
        echo json_encode(array(
            'status' => $status,
            'data' => $data,
        ));
    }
    public function login()
    {
        // var_dump($_SESSION);die;
        $this->load->view('Kasir/Login');
    }
    public function LoginAct()
    {
        $now = date("Y-m-d h:m:s");
        // var_dump($now);die;
        $username = $this->input->post('username', true);
        $password = $this->input->post('password', true);
        $data = $this->SemuaModel->getKasirByUNandPW($username, $password);
        if ($data == null) {
            $data = null;
            $pesan = "Username Dan Password salah!";
            $error = true;
        } else {
            $data = $data;
            $pesan = "Selamat Datang Kasir";
            $error = false;

            $session = array(
                'user_session' => true,
                'kasir_session' => true,
                'id_kasir' => $data->id_kasir,
                'id_cabang' => $data->id_cabang,
                'nama_kasir' => $data->nama_kasir,
            );
            $upd = array('last_login' => $now);
            $this->SemuaModel->EditData('kasir', 'id_kasir', $upd, $data->id_kasir);
            $this->session->set_userdata($session);
        }
        echo json_encode(array(
            'error' => $error,
            'data' => $data,
            'pesan' => $pesan,
        ));

        # code...
    }
    public function logout()
    {

        $this->session->sess_destroy();

        $error = false;
        $pesan = " berhasil kerluar";

        echo json_encode(array(
            'error' => $error,
            'pesan' => $pesan,
        ));
    }
    function print($id = 0) {
        $data['Data'] = $this->SemuaModel->getDataTransaksiById($id);
        $data['getData'] = $this->SemuaModel->getDataFromDetTranAndPro($id);
        $data['konten'] = $this->SemuaModel->getSeting();
        $kodeTransaksi = $data['Data']->kode_transaksi;

        // $this->load->view('Kasir/headers', $data);
        // $this->load->view('keranjang/selesai');

        $this->load->library('Pdf');
        $this->pdf->setPaper('A4', 'potrait');
        $this->pdf->filename = "Prin$kodeTransaksi.pdf";

        // $this->pdf->load_view('Kasir/Print', $data);
        $this->load->view('Kasir/Print', $data);
    }
    public function printPos($id = 0)
    {
        $data['Data'] = $this->SemuaModel->getDataTransaksiById($id);
        $data['getData'] = $this->SemuaModel->getDataFromDetTranAndPro($id);
        $data['konten'] = $this->SemuaModel->getSeting();
        // $this->load->view('Kasir/headers', $data);
        // $this->load->view('keranjang/selesai');
        $kodeTransaksi = $data['Data']->kode_transaksi;
        $this->load->library('Pdf');
        // $this->pdf->setPaper('A', 'potrait');
        $this->pdf->filename = "PrinPos$kodeTransaksi.pdf";
        $this->pdf->load_view('Kasir/PrintPos', $data);
        // $this->load->view('Kasir/printPos', $data);
    }
    public function printesc($id = 0)
    {
        $this->load->library('escpos');

        // membuat connector printer ke shared printer bernama "printer_a" (yang telah disetting sebelumnya)
        $connector = new Escpos\PrintConnectors\WindowsPrintConnector("hasan");

        // membuat objek $printer agar dapat di lakukan fungsinya
        $printer = new Escpos\Printer($connector);

        /* ---------------------------------------------------------
         * Teks biasa | text()
         */
        $printer->initialize();
        $printer->text("Ini teks biasa \n");
        $printer->text("\n");

        /* ---------------------------------------------------------
         * Select print mode | selectPrintMode()
         */
        // Printer::MODE_FONT_A
        $printer->initialize();
        $printer->selectPrintMode(Escpos\Printer::MODE_FONT_A);
        $printer->text("teks dengan MODE_FONT_A \n");
        $printer->text("\n");

        // Printer::MODE_FONT_B
        $printer->initialize();
        $printer->selectPrintMode(Escpos\Printer::MODE_FONT_B);
        $printer->text("teks dengan MODE_FONT_B \n");
        $printer->text("\n");

        // Printer::MODE_EMPHASIZED
        $printer->initialize();
        $printer->selectPrintMode(Escpos\Printer::MODE_EMPHASIZED);
        $printer->text("teks dengan MODE_EMPHASIZED \n");
        $printer->text("\n");

        // Printer::MODE_DOUBLE_HEIGHT
        $printer->initialize();
        $printer->selectPrintMode(Escpos\Printer::MODE_DOUBLE_HEIGHT);
        $printer->text("teks dengan MODE_DOUBLE_HEIGHT \n");
        $printer->text("\n");

        // Printer::MODE_DOUBLE_WIDTH
        $printer->initialize();
        $printer->selectPrintMode(Escpos\Printer::MODE_DOUBLE_WIDTH);
        $printer->text("teks dengan MODE_DOUBLE_WIDTH \n");
        $printer->text("\n");

        // Printer::MODE_UNDERLINE
        $printer->initialize();
        $printer->selectPrintMode(Escpos\Printer::MODE_UNDERLINE);
        $printer->text("teks dengan MODE_UNDERLINE \n");
        $printer->text("\n");

        /* ---------------------------------------------------------
         * Teks dengan garis bawah  | setUnderline()
         */
        $printer->initialize();
        $printer->setUnderline(Escpos\Printer::UNDERLINE_DOUBLE);
        $printer->text("Ini teks dengan garis bawah \n");
        $printer->text("\n");

        /* ---------------------------------------------------------
         * Rata kiri, tengah, dan kanan (JUSTIFICATION) | setJustification()
         */
        // Teks rata kiri JUSTIFY_LEFT
        $printer->initialize();
        $printer->setJustification(Escpos\Printer::JUSTIFY_LEFT);
        $printer->text("Ini teks rata kiri \n");
        $printer->text("\n");

        // Teks rata tengah JUSTIFY_CENTER
        $printer->initialize();
        $printer->setJustification(Escpos\Printer::JUSTIFY_CENTER);
        $printer->text("Ini teks rata tengah \n");
        $printer->text("\n");

        // Teks rata kanan JUSTIFY_RIGHT
        $printer->initialize();
        $printer->setJustification(Escpos\Printer::JUSTIFY_RIGHT);
        $printer->text("Ini teks rata kanan \n");
        $printer->text("\n");

        /* ---------------------------------------------------------
         * Font A, B dan C | setFont()
         */
        // Teks dengan font A
        $printer->initialize();
        $printer->setFont(Escpos\Printer::FONT_A);
        $printer->text("Ini teks dengan font A \n");
        $printer->text("\n");

        // Teks dengan font B
        $printer->initialize();
        $printer->setFont(Escpos\Printer::FONT_B);
        $printer->text("Ini teks dengan font B \n");
        $printer->text("\n");

        // Teks dengan font C
        $printer->initialize();
        $printer->setFont(Escpos\Printer::FONT_C);
        $printer->text("Ini teks dengan font C \n");
        $printer->text("\n");

        /* ---------------------------------------------------------
         * Jarak perbaris 40 (linespace) | setLineSpacing()
         */
        $printer->initialize();
        $printer->setLineSpacing(40);
        $printer->text("Ini paragraf dengan \nline spacing sebesar 40 \ndi printer dotmatrix \n");
        $printer->text("\n");

        /* ---------------------------------------------------------
         * Jarak dari kiri (Margin Left) | setPrintLeftMargin()
         */
        $printer->initialize();
        $printer->setPrintLeftMargin(10);
        $printer->text("Ini teks berjarak 10 dari kiri (Margin left) \n");
        $printer->text("\n");

        /* ---------------------------------------------------------
         * membalik warna teks (background menjadi hitam) | setReverseColors()
         */
        $printer->initialize();
        $printer->setReverseColors(true);
        $printer->text("Warna Teks ini terbalik \n");
        $printer->text("\n");

        /* ---------------------------------------------------------
         * Menyelesaikan printer
         */
        $printer->feed(4); // mencetak 2 baris kosong, agar kertas terangkat ke atas
        $printer->cut();
        $printer->close();
    }
    public function buatBaris1Kolom($kolom1)
    {
        // Mengatur lebar setiap kolom (dalam satuan karakter)
        $lebar_kolom_1 = 35;

        // Melakukan wordwrap(), jadi jika karakter teks melebihi lebar kolom, ditambahkan \n
        $kolom1 = wordwrap($kolom1, $lebar_kolom_1, "\n", true);

        // Merubah hasil wordwrap menjadi array, kolom yang memiliki 2 index array berarti memiliki 2 baris (kena wordwrap)
        $kolom1Array = explode("\n", $kolom1);

        // Mengambil jumlah baris terbanyak dari kolom-kolom untuk dijadikan titik akhir perulangan
        $jmlBarisTerbanyak = count($kolom1Array);

        // Mendeklarasikan variabel untuk menampung kolom yang sudah di edit
        $hasilBaris = array();

        // Melakukan perulangan setiap baris (yang dibentuk wordwrap), untuk menggabungkan setiap kolom menjadi 1 baris
        for ($i = 0; $i < $jmlBarisTerbanyak; $i++) {

            // memberikan spasi di setiap cell berdasarkan lebar kolom yang ditentukan,
            $hasilKolom1 = str_pad((isset($kolom1Array[$i]) ? $kolom1Array[$i] : ""), $lebar_kolom_1, " ");

            // Menggabungkan kolom tersebut menjadi 1 baris dan ditampung ke variabel hasil (ada 1 spasi disetiap kolom)
            $hasilBaris[] = $hasilKolom1;
        }

        // Hasil yang berupa array, disatukan kembali menjadi string dan tambahkan \n disetiap barisnya.
        return implode($hasilBaris, "\n") . "\n";
    }
    public function buatBaris3Kolom($kolom1, $kolom2, $kolom3)
    {
        // Mengatur lebar setiap kolom (dalam satuan karakter)
        $lebar_kolom_1 = 11;
        $lebar_kolom_2 = 11;
        $lebar_kolom_3 = 11;

        // Melakukan wordwrap(), jadi jika karakter teks melebihi lebar kolom, ditambahkan \n
        $kolom1 = wordwrap($kolom1, $lebar_kolom_1, "\n", true);
        $kolom2 = wordwrap($kolom2, $lebar_kolom_2, "\n", true);
        $kolom3 = wordwrap($kolom3, $lebar_kolom_3, "\n", true);

        // Merubah hasil wordwrap menjadi array, kolom yang memiliki 2 index array berarti memiliki 2 baris (kena wordwrap)
        $kolom1Array = explode("\n", $kolom1);
        $kolom2Array = explode("\n", $kolom2);
        $kolom3Array = explode("\n", $kolom3);

        // Mengambil jumlah baris terbanyak dari kolom-kolom untuk dijadikan titik akhir perulangan
        $jmlBarisTerbanyak = max(count($kolom1Array), count($kolom2Array), count($kolom3Array));

        // Mendeklarasikan variabel untuk menampung kolom yang sudah di edit
        $hasilBaris = array();

        // Melakukan perulangan setiap baris (yang dibentuk wordwrap), untuk menggabungkan setiap kolom menjadi 1 baris
        for ($i = 0; $i < $jmlBarisTerbanyak; $i++) {

            // memberikan spasi di setiap cell berdasarkan lebar kolom yang ditentukan,
            $hasilKolom1 = str_pad((isset($kolom1Array[$i]) ? $kolom1Array[$i] : ""), $lebar_kolom_1, " ");
            // memberikan rata kanan pada kolom 3 dan 4 karena akan kita gunakan untuk harga dan total harga
            $hasilKolom2 = str_pad((isset($kolom2Array[$i]) ? $kolom2Array[$i] : ""), $lebar_kolom_2, " ", STR_PAD_LEFT);

            $hasilKolom3 = str_pad((isset($kolom3Array[$i]) ? $kolom3Array[$i] : ""), $lebar_kolom_3, " ", STR_PAD_LEFT);

            // Menggabungkan kolom tersebut menjadi 1 baris dan ditampung ke variabel hasil (ada 1 spasi disetiap kolom)
            $hasilBaris[] = $hasilKolom1 . " " . $hasilKolom2 . " " . $hasilKolom3;
        }

        // Hasil yang berupa array, disatukan kembali menjadi string dan tambahkan \n disetiap barisnya.
        return implode($hasilBaris, "\n") . "\n";
    }
    public function buatBaris4Kolom($kolom1, $kolom2, $kolom3, $kolom4)
    {
        // Mengatur lebar setiap kolom (dalam satuan karakter)
        $lebar_kolom_1 = 10;
        $lebar_kolom_2 = 4;
        $lebar_kolom_3 = 6;
        $lebar_kolom_4 = 8;

        // Melakukan wordwrap(), jadi jika karakter teks melebihi lebar kolom, ditambahkan \n
        $kolom1 = wordwrap($kolom1, $lebar_kolom_1, "\n", true);
        $kolom2 = wordwrap($kolom2, $lebar_kolom_2, "\n", true);
        $kolom3 = wordwrap($kolom3, $lebar_kolom_3, "\n", true);
        $kolom4 = wordwrap($kolom4, $lebar_kolom_4, "\n", true);

        // Merubah hasil wordwrap menjadi array, kolom yang memiliki 2 index array berarti memiliki 2 baris (kena wordwrap)
        $kolom1Array = explode("\n", $kolom1);
        $kolom2Array = explode("\n", $kolom2);
        $kolom3Array = explode("\n", $kolom3);
        $kolom4Array = explode("\n", $kolom4);

        // Mengambil jumlah baris terbanyak dari kolom-kolom untuk dijadikan titik akhir perulangan
        $jmlBarisTerbanyak = max(count($kolom1Array), count($kolom2Array), count($kolom3Array), count($kolom4Array));

        // Mendeklarasikan variabel untuk menampung kolom yang sudah di edit
        $hasilBaris = array();

        // Melakukan perulangan setiap baris (yang dibentuk wordwrap), untuk menggabungkan setiap kolom menjadi 1 baris
        for ($i = 0; $i < $jmlBarisTerbanyak; $i++) {

            // memberikan spasi di setiap cell berdasarkan lebar kolom yang ditentukan,
            $hasilKolom1 = str_pad((isset($kolom1Array[$i]) ? $kolom1Array[$i] : ""), $lebar_kolom_1, " ");
            $hasilKolom2 = str_pad((isset($kolom2Array[$i]) ? $kolom2Array[$i] : ""), $lebar_kolom_2, " ");

            // memberikan rata kanan pada kolom 3 dan 4 karena akan kita gunakan untuk harga dan total harga
            $hasilKolom3 = str_pad((isset($kolom3Array[$i]) ? $kolom3Array[$i] : ""), $lebar_kolom_3, " ", STR_PAD_LEFT);
            $hasilKolom4 = str_pad((isset($kolom4Array[$i]) ? $kolom4Array[$i] : ""), $lebar_kolom_4, " ", STR_PAD_LEFT);

            // Menggabungkan kolom tersebut menjadi 1 baris dan ditampung ke variabel hasil (ada 1 spasi disetiap kolom)
            $hasilBaris[] = $hasilKolom1 . " " . $hasilKolom2 . " " . $hasilKolom3 . " " . $hasilKolom4;
        }

        // Hasil yang berupa array, disatukan kembali menjadi string dan tambahkan \n disetiap barisnya.
        return implode($hasilBaris, "\n") . "\n";
    }

    public function printContoh($id = 0)
    {
        $data['Data'] = $this->SemuaModel->getDataTransaksiById($id);
        $data['getData'] = $this->SemuaModel->getDataFromDetTranAndPro($id);

        $data['konten'] = $this->SemuaModel->getSeting();
        $kodeTransaksi = $data['Data']->kode_transaksi;

        $nama_toko = $data['konten'][0]->isi;
        $email = $data['konten'][1]->isi;
        $telp = $data['konten'][2]->isi;
        $waktu = $data['Data']->created_at;
        $harga_total = $data['Data']->harga_total;

        $this->load->library('escpos');
        $connector = new Escpos\PrintConnectors\WindowsPrintConnector("pos58");
        $printer = new Escpos\Printer($connector);

        $printer->initialize();
        $printer->selectPrintMode(Escpos\Printer::MODE_DOUBLE_HEIGHT); // Setting teks menjadi lebih besar
        $printer->setJustification(Escpos\Printer::JUSTIFY_CENTER); // Setting teks menjadi rata tengah
        $printer->text("$nama_toko\n");
        $printer->text("\n");

        // Data transaksi
        $printer->initialize();
        $printer->text("Kode : $kodeTransaksi\n");
        $printer->text("Alamat : $email\n");
        $printer->text("Telp. : $telp\n");
        $printer->text("Waktu : $waktu\n");

        // Membuat tabel
        $printer->initialize(); // Reset bentuk/jenis teks
        $printer->text("----------------------------\n");
        $printer->text($this->buatBaris4Kolom("Barang", "qty", "Harga", "Subtotal"));
        foreach ($data['getData'] as $key => $v) {

            $printer->text($this->buatBaris4Kolom("$v->nama_menu", $v->qty, $v->harga, $v->total));

        }
        $printer->text("----------------------------\n");

        $printer->text("----------------------------\n");
        $printer->text($this->buatBaris4Kolom('Total', '', "", $harga_total));
        $printer->text("\n");

        // Pesan penutup
        $printer->initialize();
        $printer->setJustification(Escpos\Printer::JUSTIFY_CENTER);
        $printer->text("Terima kasih telah berbelanja\n");
        $printer->text("-Start System Group-\n");

        $printer->feed(5); // mencetak 5 baris kosong agar terangkat (pemotong kertas saya memiliki jarak 5 baris dari toner)
        $printer->close();

    }
    public function qrcode($data, $text = "textnya")
    {
        $this->load->library('ciqrcode');

        // $params['data'] = base_url()."Kasir/User/meja".$text;
        // $params['level'] = 'H';
        // $params['size'] = 100;
        // $params['savename'] = FCPATH . 'tes.png';
        $this->ciqrcode->generate($data);

        echo '<img src="' . base_url() . 'tes.png" />';

    }
    public function getProdukForUser()
    {
        $id_kategori = $this->input->post('id_kategori', true);
        $id_cabang = $this->input->post('id_cabang', true);
        $page = $this->input->post('page', true);

        $hasil = "";
        $hasil = $this->ProdukModel->getProdukByIdTipeProduk($id_kategori, $page,$id_cabang);
        $data = array();
        $status = false;
        if (count($hasil) > 0) {
            $status = true;
            $data = $hasil;
        }
        echo json_encode(array(
            'status' => $status,
            'data' => $data,
        ));
    }
    public function getPesananBelumAda()
    {
        $id_cabang =  $this->input->post('id_cabang',true);
        $cari = array(
            'id_cabang' => $id_cabang,
            'status' => 'created',
         );
        $semua = $this->SemuaModel->getDataTransaksiLimit('transaksi',$cari,'*',false,'asc');
        $data = $this->SemuaModel->getDataTransaksiLimit('transaksi',$cari,'*',5,'asc');
        echo json_encode(array(
            'data' => $data,
            'semua' => $semua,
        ));
    }
    public function newAdd()
    {
		
        $sess = $this->session->userdata('user_session');
		$id_user = $this->session->userdata('id_kasir');
		$data['konten'] = $this->SemuaModel->getSeting();
		    if ($sess == null||$id_user==null) {
            redirect('Kasir/Login', 'refresh');
        }
		$data['slider'] = $this->SemuaModel->getSlider();
		$data['kasir_login_slider'] = true;
		$id_cabang = $this->session->userdata('id_cabang');
		$data['dataKategori'] = $this->getKategoriByIdCabang($id_cabang);
		// var_dump($data['dataKategori']);die;
		$data['id_cabang'] = $id_cabang;
        
        $where = array('id_cabang' => $id_cabang );
        $data['meja'] = $this->SemuaModel->getData('meja',$where,'nomor_meja');
		$cart = $this->CartModel->getCart($id_user);
		$data['totalcart'] = count($cart);
		$d = 0;
		foreach ($cart as $da) {
			$d += $da->total;
		}
		$totalHarga = $d;
		$data['totalHarga'] = $d;

        $this->load->view('newTampilan2',$data);
        
    }
	public function getTotalHarga()
	{
		$id_user = $this->session->userdata('id_kasir');
		$cart = $this->CartModel->getCart($id_user);
		$d = 0;
		foreach ($cart as $da) {
			$d += $da->total;
		}
		$totalHarga = $d;
		  echo json_encode(array(
            'data' => $totalHarga,
        ));
		return $totalHarga;
		# code...
	}
    public function CancleOrder()
    {
        $id_cabang =  $this->input->post('id_cabang',true);
        $id_user =  $this->input->post('id_user',true);
        $cari = array('id_cabang' =>$id_cabang ,'id_user'=>$id_user);
        $data = $this->SemuaModel->getData('keranjang',$cari);
        if(count($data)<1){
            $pesan = 'Tidak Ada Data!';
            $status = false;
        }else{
            $this->SemuaModel->HapusDataMore('keranjang',$cari);
            $pesan = 'Berhasil Menghapus!';
            $status = true;            
        }
        echo json_encode(array(
            'pesan' => $pesan,
            'status' => $status,
        ));
    }
    public function orderan($id=-99)
    {
        $sess = $this->session->userdata('user_session');
		$id_user = $this->session->userdata('id_kasir');
        if ($sess == null||$id_user==null) {
            redirect('Kasir/Login', 'refresh');
        }
        $data['konten'] = $this->SemuaModel->getSeting();
        $data['slider'] = $this->SemuaModel->getSlider();
        $data['kasir_login_slider'] = true;
        $id_cabang = $this->session->userdata('id_cabang');
        $data['dataKategori']  =$this->getKategoriByIdCabang($id_cabang);
        // var_dump($dataKategori);die;
		$data['id_cabang'] = $id_cabang;
        $cart = $this->CartModel->getCart($id_user);
        $data['totalcart'] = count($cart);
        $data['id'] = $id;
        $data['Data'] = $this->SemuaModel->getDataTransaksiById($id);
        $data['getData']=$this->SemuaModel->getDataFromDetTranAndPro($id);
        $data['konten'] = $this->SemuaModel->getSeting();
        $data['print_transaksi'] = true;    
        $this->load->view('newTampilan2', $data);
        $this->load->view('Kasir/newSelesai', $data);
    }
    public function tableorderan($id=-99)
    {
        $sess = $this->session->userdata('user_session');
		$id_user = $this->session->userdata('id_kasir');
        if ($sess == null||$id_user==null) {
            redirect('Kasir/Login', 'refresh');
        }
        $data['konten'] = $this->SemuaModel->getSeting();
        $data['slider'] = $this->SemuaModel->getSlider();
        $data['kasir_login_slider'] = true;
        $id_cabang = $this->session->userdata('id_cabang');
        $data['dataKategori']  =$this->getKategoriByIdCabang($id_cabang);
        // var_dump($dataKategori);die;
		$data['id_cabang'] = $id_cabang;
        $cart = $this->CartModel->getCart($id_user);
        $data['totalcart'] = count($cart);
        $data['id'] = $id;
        $data['Data'] = $this->SemuaModel->getDataTransaksiById($id);
        $data['getData']=$this->SemuaModel->getDataFromDetTranAndPro($id);
        $data['konten'] = $this->SemuaModel->getSeting();
        $data['print_transaksi'] = true;    
        $this->load->view('newTampilan2', $data);
        $this->load->view('Kasir/TableOrderan', $data);
    }
    public function orderandetail($id=-99)
    {
        $sess = $this->session->userdata('user_session');
		$id_user = $this->session->userdata('id_kasir');
        if ($sess == null||$id_user==null) {
            redirect('Kasir/Login', 'refresh');
        }
        $data['konten'] = $this->SemuaModel->getSeting();
        $data['slider'] = $this->SemuaModel->getSlider();
        $data['kasir_login_slider'] = true;
        $id_cabang = $this->session->userdata('id_cabang');
        $data['dataKategori']  =$this->getKategoriByIdCabang($id_cabang);
        // var_dump($dataKategori);die;
		$data['id_cabang'] = $id_cabang;
        $cart = $this->CartModel->getCart($id_user);
        $data['totalcart'] = count($cart);
        $data['id'] = $id;
        $data['Data'] = $this->SemuaModel->getDataTransaksiById($id);
        $data['getData']=$this->SemuaModel->getDataFromDetTranAndPro($id);
        $data['konten'] = $this->SemuaModel->getSeting();
        $data['print_transaksi'] = true;    
        $data['id'] = $id;    
        $this->load->view('newTampilan2', $data);
        $this->load->view('Kasir/DetailOrderan', $data);
    }
    public function getMeja()
    {
        $data = $this->SemuaModel->getDataMeja(1);
        echo json_encode(array(
            'data' => $data,
        ));
    }
    public function SelsaiOrder()
    {
        $id = $this->input->post('id', true);

        $hasil = "";
        $in = array(
			'status' =>'finish' ,
		);
        $hasil = $this->SemuaModel->EditData('transaksi', 'id_transaksi',$in,$id);
        echo json_encode(array(
            'status' => $hasil,
        ));
    }
    public function selesai()
    {
        $sess =  $this->session->userdata('user_session');
        $id_cabang =  $this->session->userdata('id_cabang');
        $data['dataKategori']  = $this->SemuaModel->getDataById('kategori','id_cabang',$id_cabang);    
        // var_dump($sess);die;
        if ($sess == null) {
            redirect('Kasir/Login', 'refresh');
        }
        $id = $_GET['id'];
        $data['id'] = $id;
        $data['Data'] = $this->SemuaModel->getDataTransaksiById($id);
        $data['getData']=$this->SemuaModel->getDataFromDetTranAndPro($id);
        $data['konten'] = $this->SemuaModel->getSeting();
        $data['print_transaksi'] = true;
		$data['id_cabang'] = $id_cabang;
    
        $this->load->view('newTampilan2', $data);
        $this->load->view('Kasir/Selesai', $data);

        // $this->load->view('Kasir/headers', $data);
        // $this->load->view('Keranjang/selesai',$data);
        # code...
    }
     // get data 
//  $inf = get_info_motor("G 2222 UT");
//  $inf = filter_data($inf);
//  $inf = html_to_json($inf);
 
//  // 
//  header('Content-Type: application/json');
//  echo ($inf);

public function cek()
{
     $inf = $this->get_info_motor("G 2222 UT");
    $inf = $this->filter_data($inf);
    $inf = $this->html_to_json($inf);
    var_dump(100000);die;
    echo ($inf);
}
 
// get info
function get_info_motor($nomor_full){
	// break to three 
	$np = explode(" ", $nomor_full);
	
	// var 
	$nomora = $np[0];
	$nomorb = $np[1];
	$nomorc = $np[2];
 
	// curl
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,"http://dppad.jatengprov.go.id/info-pajak-kendaraan/");
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS,"nomora=$nomora&nomorb=$nomorb&nomorc=$nomorc");  //Post Fields
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	$headers = [
		'content-type: application/x-www-form-urlencoded',
		'host: dppad.jatengprov.go.id',
		'origin: http://dppad.jatengprov.go.id',
		'referer: http://dppad.jatengprov.go.id/info-pajak-kendaraan/',
		'user-agent: Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36',
		'connection: keep-alive',
		'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
		'content-length: 30'
	];

	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

	$server_output = curl_exec ($ch);

	curl_close ($ch);
	
	
	// return
	return  $server_output;
}

function filter_data($data_mentah){
	
	$data = $data_mentah;
	
	// explode by <div id="infohasil">
	$d1 = explode('<div id="infohasil">', $data);
	$d2 = $d1[1];
	
	// explode by </table>
	$d3 = explode('</table>', $d2);
	$d4 = $d3[0].'</table>';
	
	// delete outer from table 
	$d5 = explode('<div class="inforesult">', $d4);
	$d6 = explode(' <div id="head-ipk" class="tit-ipk"><b>INFO PAJAK KENDARAAN</b></div>', $d5[1]);
	$d7 = $d6[1];
	
	// delete <td>:</td>
	$d7 = str_replace('<td>:</td>','',$d7);
	$d7 = str_replace('<td width="20px">:</td>','',$d7);
	$d7 = str_replace(' class="result-pajak"','',$d7);
	$d7 = str_replace('<span style="color:red">*</span>','',$d7);
	
	$data = $d7;
	
	return $data;
}


// html dom to json 
function html_to_json($html_s){
	$html = str_get_html($html_s);
	$row_count=0;
	$json = array();
	foreach ($html->find('tr') as $row) {
			$info = $row->find('td',0)->innertext;
			$value = $row->find('td',1)->innertext;

			$json[$info]=$value;
		}
    return json_encode($json);
}


}

/* End of file  Kasir.php */
