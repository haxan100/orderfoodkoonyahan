<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Cabang extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('MenuModel');
		$this->load->model('SemuaModel');
		$this->load->model('TransaksiModel');
		$this->load->model('AdminModel');
		$this->load->helper('url');
	}

	public function index()
	{
	}

	public function isLoggedInCabang()
	{
		if ($this->session->userdata('cabang_session'))
			return true;
		else
			return false;
	}
	function cekLogin_cabang()
	{
		if (!$this->isLoggedInCabang()) {
			$this->session->set_flashdata(
				'notifikasi',
				array(
					'alert' => 'alert-danger',
					'message' => 'Silahkan Login terlebih dahulu.',
				)
			);
			redirect('LoginAdmin');
		}
	}
	public function master_kasir()
	{
		$obj['header'] = $this->SemuaModel->getSeting();
		$obj['cabang'] = $this->SemuaModel->getCabang();
		$this->cekLogin_cabang();
		$id_admin = $this->session->userdata('id_admin');
		$id_cabang = $this->session->userdata('id_cabang');
		$obj['id_cabang'] = $id_cabang;
		$role = $this->AdminModel->getRole($id_admin, 'data_kasir')->r;
		if ($role == 1) {
			$obj['ci'] = $this;
			$obj['content'] = 'cabang/master_kasir';
			$this->load->view('admin/templates/index', $obj);
		} elseif ($role == 0) {
			$obj['ci'] = $this;
			$obj['content'] =  "admin/blank";
			$this->load->view('admin/templates/index', $obj);
		}
	}
	public function master_meja()
	{
		$this->cekLogin_cabang();
		$obj['header'] = $this->SemuaModel->getSeting();

		$id_admin = $this->session->userdata('id_admin');
		$role = $this->AdminModel->getRole($id_admin, 'master_menu')->r;
		$id_cabang = $this->session->userdata('id_cabang');
		$obj['id_cabang'] = $id_cabang;
		$role = 1;

		if ($role == 1) {
			$obj['listKategori'] = $this->MenuModel->getKategori();
			$obj['ci'] = $this;
			$obj['content'] = 'admin/master_meja';
			$this->load->view('admin/templates/index', $obj);
		} elseif ($role == 0) {
			$obj['ci'] = $this;
			$obj['content'] =  "admin/blank";
			$this->load->view('admin/templates/index', $obj);
		}
	}
	public function lihat_laporan_tahunan()
	{
		$tahun = $this->input->get_post('tahun');
		$obj['data'] = $this->TransaksiModel->getLaporanTahunanCabang($tahun);
		$obj['tahun'] = $tahun;
		$id_admin = $this->session->userdata('id_admin');
		$role = $this->AdminModel->getRole($id_admin, 'data_admin')->r;
		$role = 1;

		if ($role == 1) {
			$obj['listKategori'] = $this->SemuaModel->getAllRole();
			$obj['content'] = 'cabang/master_laporan_tahunan';
			$this->load->view('admin/templates/index', $obj);
		} elseif ($role == 0) {
			$obj['ci'] = $this;
			$obj['content'] =  "admin/blank";
			$this->load->view('admin/templates/index', $obj);
		}
	}
	public function lihat_laporan_bulanan()
	{
		$obj['header'] = $this->SemuaModel->getSeting();
		$bulan = $this->input->get_post('bulan');
		$tahun = $this->input->get_post('tahun');
		$obj['data'] = $this->TransaksiModel->getLaporanBulananCabang($bulan, $tahun);
		$obj['bulan'] = $bulan;
		$obj['tahun'] = $tahun;
		$id_admin = $this->session->userdata('id_admin');
		$role = $this->AdminModel->getRole($id_admin, 'data_admin')->r;
		$role = 1;

		if ($role == 1) {
			$obj['listKategori'] = $this->SemuaModel->getAllRole();
			$obj['content'] = 'cabang/master_laporan_bulanan';
			$this->load->view('admin/templates/index', $obj);
		} elseif ($role == 0) {
			$obj['ci'] = $this;
			$obj['content'] =  "admin/blank";
			$this->load->view('admin/templates/index', $obj);
		}
	}
	public function lihat_laporan_harian()
	{
		$tanggal = $this->input->get_post('tanggal');
		$bulan = $this->input->get_post('bulan');
		$obj['header'] = $this->SemuaModel->getSeting();

		$obj['id_cabang'] = $this->session->userdata('id_cabang');


		$tahun = $this->input->get_post('tahun');
		$obj['data'] = $this->TransaksiModel->getLaporanHarianCabang($tanggal, $bulan, $tahun);
		$obj['tanggal'] = $tanggal;
		$obj['bulan'] = $bulan;
		$obj['tahun'] = $tahun;
		$id_admin = $this->session->userdata('id_admin');
		$role = $this->AdminModel->getRole($id_admin, 'data_admin')->r;
		$role = 1;
		if ($role == 1) {
			$obj['listKategori'] = $this->SemuaModel->getAllRole();
			$obj['content'] = 'admin/master_laporan_harian';
			$this->load->view('admin/templates/index', $obj);
		} elseif ($role == 0) {
			$obj['ci'] = $this;
			$obj['content'] =  "admin/blank";
			$this->load->view('admin/templates/index', $obj);
		}
	}
	public function ubah_setting_proses()
	{
		$id_cabang = $this->input->post('id_cabang', TRUE);
		$nomor_telp = $this->input->post('nomor_telp', TRUE);
		$facebook = $this->input->post('facebook', TRUE);
		$youtube = $this->input->post('youtube', TRUE);
		$twitter = $this->input->post('twitter', TRUE);
		$instagram = $this->input->post('instagram', TRUE);
		$alamat_cabang = $this->input->post('alamat_cabang', TRUE);

		$message = 'Gagal mengedit data !<br>Silahkan lengkapi data yang diperlukan.';
		$errorInputs = array();
		$status = true;
		$in = array(
			'nomor_telp' => $nomor_telp,
			'facebook' => $facebook,
			'youtube' => $youtube,
			'twitter' => $twitter,
			'instagram' => $instagram,
			'alamat_cabang' => $alamat_cabang,
		);
		if (empty($nomor_telp)) {
			$status = false;
			$errorInputs[] = array('#isi', 'Silahkan Isi ');
		}

		if ($status) {

			if ($this->SemuaModel->EditData('cabang', 'id_cabang', $in, $id_cabang)) {
				$status = true;
				$message = "Berhasil Mengubah Data #1";
			}
		} else {
			$status = false;
			$message = "Gagal Mengubah Data #1";
		}



		echo json_encode(array(
			'status' => $status,
			'message' => $message,
			'errorInputs' => $errorInputs
		));
	}
}
