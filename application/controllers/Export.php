<?php
defined('BASEPATH') or exit('No direct script access allowed');

// use PhpOffice\PhpSpreadsheet\Spreadsheet;
// use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
// require 'vendor/autoload.php';
require APPPATH . '/third_party/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Export extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('MenuModel');
		$this->load->model('KasirModel');
		$this->load->model('AdminModel');
		$this->load->model('SemuaModel');
		$this->load->model('TransaksiModel');
		$this->load->helper('url');
	}

	public function master_list_transaksi_export()
	{
		// var_dump($_GET);die;
		$status = $this->input->get('status', TRUE);
		$id_cabang = $this->input->get('id_cabang', TRUE);
		$selectDate = $this->input->get('selectDate', TRUE);
		$date = $this->input->get('date', TRUE);
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$lisUser = $this->TransaksiModel->get_transaksi($status, $date,$id_cabang);
		$spreadsheet->setActiveSheetIndex(0)
			->setCellValue('A1', 'Id Transaksi')
			->setCellValue('B1', 'Tanggal Transaksi')
			->setCellValue('C1', 'Kode Transaksi')
			->setCellValue('D1', 'Nama Kasir')
			->setCellValue('E1', 'Harga Total')
			->setCellValue('F1', 'Tipe Pesanan')
			->setCellValue('G1', 'Nama User');
		$i = 1;
		foreach ($lisUser as $row) {
			$i++;
			$sheet->setCellValue('A' . $i, $row->id_transaksi);
			$sheet->setCellValue('B' . $i, $row->created_at);
			$sheet->setCellValue('C' . $i, $row->kode_transaksi);
			$sheet->setCellValue('D' . $i, $row->nama_kasir);
			$sheet->setCellValue('E' . $i, $row->harga_total);
			$sheet->setCellValue('F' . $i, strtoupper(str_replace('_', ' ', $row->tipe_pesan)));
			$sheet->setCellValue('G' . $i, $row->nama_user);
		}

		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(11);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(17);
	
		$writer = new Xlsx($spreadsheet);
		$filename = 'List_Transaksi_Online';

		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="' . $filename . '' . $date . '.xlsx"');
		header('Cache-Control: max-age=0');

		$writer->save('php://output'); // download file 


	}
	public function master_list_transaksi_export_offline()
	{
		$status = $this->input->get('status', TRUE);
		$id_cabang = $this->input->get('id_cabang', true);
		// var_dump($_GET);die;
		$selectDate = $this->input->get('selectDate', TRUE);
		$date = $this->input->get('date', TRUE);
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$lisUser = $this->TransaksiModel->get_transaksi_offline($status, $date,$id_cabang);
		$spreadsheet->setActiveSheetIndex(0)
			->setCellValue('A1', 'Id Transaksi')
			->setCellValue('B1', 'Nama User')
			->setCellValue('c1', 'Tanggal Transaksi')
			->setCellValue('D1', 'Kode Transaksi')
			->setCellValue('E1', 'Tipe Pesan')
			->setCellValue('F1', 'Nomor Meja (Jika Dine In)')
			->setCellValue('G1', 'Jual')
			->setCellValue('H1', 'Catatan');
		$i = 1;
		foreach ($lisUser as $row) {
			$i++;
			// var_dump($row);die;
			$pesanan=strtoupper(str_replace('_', ' ', $row->tipe_pesan));

			$sheet->setCellValue('A' . $i, $row->id_transaksi);
			$sheet->setCellValue('B' . $i, $row->nama_user);
			$sheet->setCellValue('C' . $i, $row->created_at);
			$sheet->setCellValue('D' . $i, $row->kode_transaksi);
			$sheet->setCellValue('E' . $i, $pesanan);
			$sheet->setCellValue('F' . $i, $row->nomor_meja);
			$sheet->setCellValue('G' . $i, $row->harga_total);
			$sheet->setCellValue('H' . $i, $row->catatan);
		}

		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(11);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(17);
	
		$writer = new Xlsx($spreadsheet);
		$filename = 'List_Transaksi_Offline';

		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="' . $filename . '' . $date . '.xlsx"');
		header('Cache-Control: max-age=0');

		$writer->save('php://output'); // download file 


	}
	public function master_list_transaksi_export_offlineCabang()
	{
		$status = $this->input->get('status', TRUE);
		// $id_cabang = $this->input->get('id_cabang', true);
		$id_cabang = $_SESSION['id_cabang'];
		$selectDate = $this->input->get('selectDate', TRUE);
		$date = $this->input->get('date', TRUE);
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$lisUser = $this->TransaksiModel->get_transaksi_offline($status, $date,$id_cabang);
		$spreadsheet->setActiveSheetIndex(0)
			->setCellValue('A1', 'Id Transaksi')
			->setCellValue('B1', 'Nama User')
			->setCellValue('c1', 'Tanggal Transaksi')
			->setCellValue('D1', 'Kode Transaksi')
			->setCellValue('E1', 'Tipe Pesan')
			->setCellValue('F1', 'Nomor Meja (Jika Dine In)')
			->setCellValue('G1', 'Jual')
			->setCellValue('H1', 'Catatan');
		$i = 1;
		foreach ($lisUser as $row) {
			$i++;
			// var_dump($row);die;
			$pesanan=strtoupper(str_replace('_', ' ', $row->tipe_pesan));

			$sheet->setCellValue('A' . $i, $row->id_transaksi);
			$sheet->setCellValue('B' . $i, $row->nama_user);
			$sheet->setCellValue('C' . $i, $row->created_at);
			$sheet->setCellValue('D' . $i, $row->kode_transaksi);
			$sheet->setCellValue('E' . $i, $pesanan);
			$sheet->setCellValue('F' . $i, $row->nomor_meja);
			$sheet->setCellValue('G' . $i, $row->harga_total);
			$sheet->setCellValue('H' . $i, $row->catatan);
		}

		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(11);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(17);
	
		$writer = new Xlsx($spreadsheet);
		$filename = 'List_Transaksi_Offline';

		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="' . $filename . '' . $date . '.xlsx"');
		header('Cache-Control: max-age=0');

		$writer->save('php://output'); // download file 


	}
}
