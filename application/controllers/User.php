<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('MenuModel');
        $this->load->model('SemuaModel');
        $this->load->model('ProdukModel');
        $this->load->model('CartModel');
        // $this->load->model('SekolahModel');
        $this->load->helper('url');
        $this->load->library('session');

        // require_once APPPATH . 'third_party/vendor/mike42/autoloader.php';
        require APPPATH . 'third_party/vendor/autoload.php';
    }
    public function getKategoriByIdCabang($id_cabang)
	{
		$data = $this->SemuaModel->getDataById('kategori','id_cabang',$id_cabang);
        return $data;
	}
    public function mejaOld($no = null)
    {
        // echo current_url();

        if ($no == null) {
            die('Tidak Ada Meja!');
        } else {
            $url = current_url();
            $dataMeja = $this->SemuaModel->getDataByIdRow('meja','random',$no);
            // var_dump($dataMeja);die;
            $id_meja = $dataMeja->id_meja;
            $random = $dataMeja->random;
            $id_cabang = $dataMeja->id_cabang;
            $data['dataKategori']  =$this->getKategoriByIdCabang($id_cabang);
            $data['kasir_login_slider'] = false;

            $id_user = $no;
            $data['konten'] = $this->SemuaModel->getSeting();
            $data['slider'] = $this->SemuaModel->getSlider();
            $data['id_cabang'] = $id_cabang;
            $data['id_meja'] = $id_meja;
            $data['random'] = $random;

            $cart = $this->CartModel->getCartMejaUser($id_meja);
            $data['totalcart'] = count($cart);
            $d = 0;
            foreach ($cart as $da) {
                $d += $da->total;
            }
            $totalHarga = $d;
            $data['totalHarga'] = $d;
            $this->load->view('user/headers', $data);
            $this->load->view('Kasir/index', $data);
            $this->load->view('user/footer', $data);
        }
    }
    public function setBid()
    {
        // var_dump($_POST);die;
        $id_cabang = $this->input->post('id_cabang', true);
        $id_produk = $this->input->post('id_produk', true);
        $id_meja = $this->input->post('id_meja', true);
        $harga = $this->input->post('harga', true);
        $qty = $this->input->post('qty', true);
        $now = date('Y-m-d H:i:s');
        $totalBarang = $this->ProdukModel->getProdukById($id_produk);

        if ($totalBarang->stok < 1) {
            $data = array(
                'status' => false,
                'msg' => "Maaf!,Produk Kosong",
                'data' => [],
                'total' => [],
                'harga' => [],
            );
            echo json_encode($data);
            die();
        }

        $tgl = $now;
        $keranjangOld = $this->CartModel->getCartByIdMejaAndProduk($id_meja, $id_produk,$id_cabang);
        $getHargaByIdProd = $this->CartModel->getProdByIdProd($id_produk)->harga;

        $getData = $this->CartModel->getAllCartByIdMeja($id_meja);

        $TotalgetData = count($this->CartModel->getAllCartByIdMeja($id_meja));
        $cart = $this->CartModel->getCartMejaUser($id_meja);
        $d = 0;
        foreach ($cart as $da) {
            $d += $da->total;
        }

        $totalHarga = $d;

        // var_dump($totalHarga);die;

        $hargaSemua = $totalHarga + $getHargaByIdProd;
        $totalsemua = $TotalgetData + 1;
        if (count($keranjangOld) >= 1) { // jika di keranjang ada produk , maka di updte qty nya
            // die("a");
            $qtyOld = $keranjangOld[0]->qty;
            $qtyNew = $qtyOld + $qty;
            $upd = array(
                'qty' => $qtyNew,
            );
            // var_dump($upd);die;
            $this->CartModel->updateCartMejaCabangUser($upd, $id_produk,$id_cabang,$id_meja);
            $msg = "Item Berhasil di Tambah Ke Keranjang";
            $status = true;
        } else {
            $data2 = array(
                'id_cabang' => $id_cabang,
                'id_produk' => $id_produk,
                'id_meja' => $id_meja,
                'qty' => $qty,
                'created_at' => $tgl,
            );

            if ($this->CartModel->AddCartMejaUser($data2)) {
                $msg = "Item Berhasil di Tambah Ke Keranjang";
                $status = true;
            } else {
                $msg = "Item Gagal di Tambah Ke Keranjang";
                $status = false;
            }
        }
        $data = array(
            'status' => $status,
            'msg' => $msg,
            'data' => $getData,
            'total' => $totalsemua,
            'harga' => $hargaSemua,
        );
        echo json_encode($data);
    }
    public function cart($random)
    {
        // $dataM  = $this->SemuaModel->getDataByIdRow('kategori','id_cabang',1);
        
        
        $dataMeja = $this->SemuaModel->getDataByIdRow('meja','random',$random);
        $id_cabang = $dataMeja->id_cabang;
        $id_meja = $dataMeja->id_meja;


        $data['dataKategori']  = $this->SemuaModel->getDataById('kategori','id_cabang',$id_cabang);
        // var_dump($data);die;

        $data['konten'] = $this->SemuaModel->getSeting();
        $data['random'] = $random;
        $data['id_meja'] = $id_meja;
        $cart = $this->CartModel->getCartMejaUser($id_meja);
        $data['totalcart'] = count($cart);
        $d = 0;
        foreach ($cart as $da) {
            $d += $da->total;
        }
        $totalHarga = $d;
        $data['totalHarga'] = $d;
        $data['cart_content'] = $cart;
        $data['id_meja'] = $id_meja;

        $this->load->view('user/headers', $data);
        $this->load->view('user/CartMeja');

        # code...
    }
	public function hapusCart()
	{
        $id_user = $this->input->post('id_meja', true);
		$id_produk = $this->input->post('id', TRUE);
		$msg = "Gagal Hapus";
		$status = false;
		if($this->SemuaModel->HapusCartMeja($id_user, $id_produk)){

			$msg = "Item Berhasil di Hapus Dari Keranjang";
			$status = true;

		}

			$data = array(
				'status' => $status,
				'msg' => $msg,
			);
			echo json_encode($data);


		# code...
	}
    public function updateCart()
    {
        $id_user = $this->input->post('id_meja', true);
        $id_produk = $this->input->post('id', true);
        $qty = $this->input->post('qty', true);
        $in = array('qty' => $qty);
        $msg = "Gagal Update";
        $status = false;
        $getData = $this->CartModel->getAllCartByIdMeja($id_user);
        $cart = $this->CartModel->getCartMejaUser($id_user);
        $totalHarga = " ";

        if ($this->CartModel->updateCartMejaByIdProdukAndIdMeja($in, $id_produk, $id_user)) {
            $status = true;
            $msg = " berhasil merubah";
            $cart = $this->CartModel->getCartMejaUser($id_user);
            $data['totalcart'] = count($cart);
            $d = 0;
            foreach ($cart as $da) {
                $d += $da->total;
            }
            $totalHarga = $d;
            $data['totalHarga'] = $d;
            $data['cart_content'] = $cart;
            $totalHarga = $d;
        }

        $data = array(
            'status' => $status,
            'msg' => $msg,
            'data' => $getData,
            'harga' => $totalHarga,
        );
        echo json_encode($data);

        # code...
    }
    public function updateCartonlyCatatan()
    {
        $id_user = $this->input->post('id_meja', true);
        $id_produk = $this->input->post('id', true);
        $qty = $this->input->post('qty', true);
	    $catatan = $this->input->post('catatan', TRUE);

        $in = array(
            'catatan' => $catatan
        );
        $msg = "Gagal Update";
        $status = false;
        $getData = $this->CartModel->getAllCartByIdMeja($id_user);
        $cart = $this->CartModel->getCartMejaUser($id_user);
        $totalHarga = " ";

        if ($this->CartModel->updateCartMejaByIdProdukAndIdMeja($in, $id_produk, $id_user)) {
            $status = true;
            $msg = " berhasil merubah";
            $cart = $this->CartModel->getCartMejaUser($id_user);
            $data['totalcart'] = count($cart);
            $d = 0;
            foreach ($cart as $da) {
                $d += $da->total;
            }
            $totalHarga = $d;
            $data['totalHarga'] = $d;
            $data['cart_content'] = $cart;
            $totalHarga = $d;
        }

        $data = array(
            'status' => $status,
            'msg' => $msg,
            'data' => $getData,
            'harga' => $totalHarga,
        );
        echo json_encode($data);

        # code...
    }
    public function checkout($id_meja)
    {
        $dataMeja = $this->SemuaModel->getDataByIdRow('meja','random',$id_meja);
        // var_dump($dataMeja);die;
        
	    $data['dataKategori']  = $this->SemuaModel->getDataById('kategori','id_cabang',$dataMeja->id_cabang);

        $id_meja = $dataMeja->id_meja;
        $random = $dataMeja->random;
        $data['konten'] = $this->SemuaModel->getSeting();
        $data['random'] = $random;
        $id_user = $id_meja;
        $cart = $this->CartModel->getCartMejaUser($id_user);
        $data['cart_content'] = $cart;
        $data['id_meja'] = $id_meja;
        $data['totalcart'] = count($cart);
        $d = 0;
        foreach ($cart as $da) {
            $d += $da->total;
        }
        $totalHarga = $d;
        $data['totalHarga'] = $d;
        $this->load->view('user/headers', $data);
        $this->load->view('user/cekout');
    }
    public function konfirmasi()
    {
        $nama = $this->input->post('nama');
        $pilihtempat = 'dine_in';
        $meja_id = $this->input->post('meja_id');
        $dataMeja = $this->SemuaModel->getDataByIdRow('meja','random',$meja_id);
        $meja_id = $dataMeja->id_meja;
        $id_cabang = $dataMeja->id_cabang;
        // var_dump($id_cabang);die;
        $totalharga = $this->input->post('totalharga');
        $orderan = ''; // grabfood dll
        $jenis = 1; // 0 online || 1 offline
        $catatan = $this->input->post('catatan'); // 0 online || 1 offline

        if ($totalharga == 0 || $totalharga == '' || empty($totalharga)) {
            $data = array(
                'status' => false,
                'msg' => 'Maaf, Data Tidak Valid, Coba Lagi Nanti',
                'id' => 0,
            );
            echo json_encode($data);
            die();
        }

        $now = date('Y-m-d H:i:s');

        $cart = $this->CartModel->getCartMejaUser($meja_id);
        $status = false;
        $msg = "";
        $id = 0;

        $data['totalcart'] = count($cart);
        $d = 0;
        if (intval($jenis) == 0) {
            $pesan = $orderan;
        } else {
            $pesan = $pilihtempat;
        }

        $ran = $this->generateRandomString();

        $inToTrans = array(
            'kode_transaksi' => $ran,
            'nama_user' => $nama,
            'tipe_pesan' => $pesan,
            'type_penjualan_offline' => $jenis,
            'nomor_meja' => $meja_id,
            'harga_total' => $totalharga,
            'id_user' => 0,
            'by_user' => 1,
            'created_at' => $now,
            'catatan' => $catatan,
            'id_cabang' => $id_cabang,
        );
        $id_transaksi = $this->SemuaModel->AddTransaksi($inToTrans);

        foreach ($cart as $da) {
            $inTodetTrans = array(
                'id_transaksi' => $id_transaksi,
                'id_menu' => $da->id_produk,
                'qty' => $da->qty,
                'total' => $da->total,
            );
            $this->SemuaModel->Tambah('transaksi_detail', $inTodetTrans);
        }
        if ($id_transaksi != null) {
            $status = true;
            $msg = "Berhasil";
            $id = $id_transaksi;
            $this->CartModel->HapusCartByIdMeja($meja_id);
        }

        $data = array(
            'status' => $status,
            'msg' => $msg,
            'id' => $id,
        );
        echo json_encode($data);
        die();
    }
    public function selesai()
    {
        $id = $_GET['id'];
        $data['id'] = $id;
        $data['id_meja'] = $_GET['id_meja'];
        $data['print_transaksi'] = false;
        $data['Data'] = $this->SemuaModel->getDataTransaksiById($id);
        $data['getData'] = $this->SemuaModel->getDataFromDetTranAndPro($id);
        $data['konten'] = $this->SemuaModel->getSeting();
        $this->load->view('user/headersSelesai', $data);
        $this->load->view('Keranjang/selesai',$data);

        # code...
    }
    public function meja($no = null)
    {
        // echo current_url();

        if ($no == null) {
            die('Tidak Ada Meja!');
        } else {
            $url = current_url();
            $dataMeja = $this->SemuaModel->getDataByIdRow('meja','random',$no);
            $id_meja = $dataMeja->id_meja;
            $random = $dataMeja->random;
            $id_cabang = $dataMeja->id_cabang;
            $data['dataKategori']  =$this->getKategoriByIdCabang($id_cabang);
            $data['kasir_login_slider'] = false;
            
            $id_user = $no;
            $data['konten'] = $this->SemuaModel->getSeting();
            $data['slider'] = $this->SemuaModel->getSlider();
            $data['id_cabang'] = $id_cabang;
            $data['id_meja'] = $id_meja;
            $data['random'] = $random;
            
            $cart = $this->CartModel->getCartMejaUser($id_meja);
            $data['totalcart'] = count($cart);
            $d = 0;
            foreach ($cart as $da) {
                $d += $da->total;
            }
            $totalHarga = $d;
            $data['totalHarga'] = $d;
            
            $data['id_cabang'] = $id_cabang; 

            $this->load->view('userTampilan', $data);
            // $this->load->view('Kasir/headers', $data);
            $this->load->view('user/newOrder', $data);
        }
    }
    public function getTotalHarga()
	{
        $id_meja = $this->input->post('id_meja', TRUE);

		$cart = $this->CartModel->getCartMejaUser($id_meja);
		$d = 0;
		foreach ($cart as $da) {
			$d += $da->total;
		}
		$totalHarga = $d;
		  echo json_encode(array(
            'data' => $totalHarga,
        ));
		return $totalHarga;
		# code...
	}
    public function konfirmasiUser()
    {
        $nama = $this->input->post('nama');
        $pilihtempat = 'dine_in';
        $meja_id = $this->input->post('meja_id');
        $id_cabang = $this->input->post('id_cabang');
        $totalharga = $this->input->post('totalharga');
        $catatan = $this->input->post('catatan'); // 0 online || 1 offline

        if ($totalharga == 0 || $totalharga == '' || empty($totalharga)) {
            $data = array(
                'status' => false,
                'msg' => 'Maaf, Data Tidak Valid, Coba Lagi Nanti',
                'id' => 0,
            );
            echo json_encode($data);
            die();
        }

        $now = date('Y-m-d H:i:s');

        $cart = $this->CartModel->getCartMejaUser($meja_id);
        // var_dump($cart);die;
        $status = false;
        $msg = "";
        $id = 0;

        $data['totalcart'] = count($cart);
        $d = 0;
        $ran = $this->generateRandomString();
        $inToTrans = array(
            'kode_transaksi' => $ran,
            'nama_user' => $nama,
            'tipe_pesan' => 'dine_in',
            'type_penjualan_offline' => 1,
            'nomor_meja' => $meja_id,
            'harga_total' => $totalharga,
            'id_user' => 1,
            'by_user' => 1,
            'created_at' => $now,
            'catatan' => $catatan,
            'id_cabang' => $id_cabang,
        );
        $id_transaksi = $this->SemuaModel->AddTransaksi($inToTrans);

        if($catatan==null) $catatan = '';
        foreach ($cart as $da) {
            $inTodetTrans = array(
                'id_transaksi' => $id_transaksi,
                'id_menu' => $da->id_produk,
                'qty' => intval($da->qty),
                'total' =>intval($da->total),            
                'catatan' => $da->catatan,
                'id_cabang' => $id_cabang,
            );
            $this->SemuaModel->Tambah('transaksi_detail', $inTodetTrans);
        }
        if ($id_transaksi != null) {
            $status = true;
            $msg = "Berhasil";
            $id = $id_transaksi;
            $this->CartModel->HapusCartByIdMeja($meja_id);
        }

        $data = array(
            'status' => $status,
            'msg' => $msg,
            'id' => $id,
        );
        echo json_encode($data);
        die();
    }
    function print($id = 0) {
        $data['Data'] = $this->SemuaModel->getDataTransaksiById($id);
        $data['getData'] = $this->SemuaModel->getDataFromDetTranAndPro($id);
        $data['konten'] = $this->SemuaModel->getSeting();
        $kodeTransaksi = $data['Data']->kode_transaksi;

        // $this->load->view('Kasir/headers', $data);
        // $this->load->view('keranjang/selesai');

        $this->load->library('Pdf');
        $this->pdf->setPaper('A4', 'potrait');
        $this->pdf->filename = "Prin$kodeTransaksi.pdf";

        // $this->pdf->load_view('Kasir/Print', $data);
        $this->load->view('Kasir/Print', $data);
    }
}
