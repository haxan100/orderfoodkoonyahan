<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('MenuModel');
		$this->load->model('SemuaModel');
		$this->load->model('TransaksiModel');
		$this->load->model('AdminModel');
		$this->load->helper('url');
	}

	function cekLogin()
	{
		if (!$this->isLoggedInAdmin()) {
			$this->session->set_flashdata(
				'notifikasi',
				array(
					'alert' => 'alert-danger',
					'message' => 'Silahkan Login terlebih dahulu.',
				)
			);
			redirect('LoginAdmin');
		}
	}
	public function index()
	{
		$id_cabang = $this->session->userdata('id_cabang');

		$this->cekLogin();
		$date = date("Y-m-d");
		$dateYesterday = date("Y-m-d", strtotime("-1 days"));

		$obj['graph'] = $this->TransaksiModel->graph($id_cabang);
		$obj['menuItem'] = $this->TransaksiModel->graphTransaksiDetMenu($id_cabang);
		$obj['TransaksiHarian'] = $this->TransaksiModel->graphTransaksiHarian($id_cabang);

		$obj['hariIni'] = $this->TransaksiModel->getCountTransaksiHarian($date);
		$obj['kemarin'] = $this->TransaksiModel->getCountTransaksiHarian($dateYesterday);
		$obj['menuHariIni'] = ($this->TransaksiModel->getCountMenuHarian($date) == null) ? 0 : $this->TransaksiModel->getCountMenuHarian($date);
		$obj['menuKemarin'] = ($this->TransaksiModel->getCountMenuHarian($dateYesterday) == null) ? 0 : $this->TransaksiModel->getCountMenuHarian($dateYesterday);
		$obj['ci'] = $this;
		$obj['header'] = $this->SemuaModel->getSeting();
		$obj['content'] =  "admin/home";

		$this->load->view('admin/templates/index', $obj);
	}
	public function master_cabang()
	{
		$id_admin = $this->session->userdata('id_admin');
		$role = $this->AdminModel->getRole($id_admin, 'data_admin')->r;
		$obj['header'] = $this->SemuaModel->getSeting();

		if ($role == 1) {
			$obj['listKategori'] = $this->SemuaModel->getAllRole();
			$obj['content'] = 'admin/master_cabang';
			$this->load->view('admin/templates/index', $obj);
		} elseif ($role == 0) {
			$obj['ci'] = $this;
			$obj['content'] =  "admin/blank";

			$this->load->view('admin/templates/index', $obj);
		}
	}
	public function master_admin()
	{
		// var_dump($_SESSION);die;
		$id_cabang = $this->session->userdata('id_cabang');
		$id_admin = $this->session->userdata('id_admin');
		$role = $this->AdminModel->getRole($id_admin, 'data_admin')->r;
		$obj['header'] = $this->SemuaModel->getSeting();
		$obj['cabang'] = $this->SemuaModel->getCabang();
		$obj['owner'] = false;

		if ($role == 1) {
			$obj['listKategori'] = $this->SemuaModel->getAllRole();
			$obj['content'] = 'admin/master_admin';
			$this->load->view('admin/templates/index', $obj);
		} elseif ($role == 0) {
			$obj['ci'] = $this;
			$obj['content'] =  "admin/blank";

			$this->load->view('admin/templates/index', $obj);
		}
	}
	public function master_item()
	{
		// var_dump($_SESSION);die;
		$this->cekLogin();
		$id_admin = $this->session->userdata('id_admin');
		$obj['header'] = $this->SemuaModel->getSeting();

		$role = $this->AdminModel->getRole($id_admin, 'master_menu')->r;
		if ($role == 1) {

			$obj['listKategori'] = $this->MenuModel->getKategori();
			$obj['ci'] = $this;
			$obj['content'] = 'cabang/master_item';
			$this->load->view('admin/templates/index', $obj);
		} elseif ($role == 0) {
			$obj['ci'] = $this;
			$obj['content'] =  "admin/blank";
			$this->load->view('admin/templates/index', $obj);
		}
	}
	public function tambah_menu_proses()
	{
		$id_cabang = $this->session->userdata('id_cabang');

		$pajak = $this->input->post('pajak', TRUE);
		$promo = $this->input->post('promo', TRUE);
		$nama = $this->input->post('nama', TRUE);

		$stok = $this->input->post('stok', TRUE);
		$id_kategori = $this->input->post('id_kategori', TRUE);
		$harga = $this->input->post('harga', TRUE);
		// var_dump($_FILES);die;
		$status = true;

		if ($nama == null) {
			$status = false;
			$message = "Harap Masukan Nama!";
		}
		if ($id_kategori == null) {
			$status = false;
			$message = "Harap Pilih Kategori!";
		}
		if ($harga == null) {
			$status = false;
			$message = "Harap Masukan Harga!";
		}
		if ($stok == null) {
			$status = false;
			$message = "Harap Masukan Stok!";
		}

		$cekFoto = empty($_FILES['foto']['name'][0]) || $_FILES['foto']['name'][0] == '';
		if (!$cekFoto) {

			$_FILES['f']['name']     = $_FILES['foto']['name'];
			$_FILES['f']['type']     = $_FILES['foto']['type'];
			$_FILES['f']['tmp_name'] = $_FILES['foto']['tmp_name'];
			$_FILES['f']['error']     = $_FILES['foto']['error'];
			$_FILES['f']['size']     = $_FILES['foto']['size'];
			$config['upload_path']          = './assets/images/foods';
			// $config['allowed_types']        = 'jpg|jpeg|png|gif|JPG|jpg|JPEG|PNG';
			$config["allowed_types"] = "*";
			$config['max_size']             = 3 * 1024;
			$config['max_width']            = 10 * 1024;
			$config['max_height']           = 10 * 1024;
			$config['file_name'] = $nama . "-" . date("Y-m-d-H-i-s") . ".jpg";
			$this->load->library('image_lib');
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$this->image_lib->resize();

			if (!$this->upload->do_upload('f')) {
				$errorUpload = $this->upload->display_errors() . '<br>';
				$status = false;
				$message = $errorUpload;
				$errorInputs[] = array('#foto', $errorUpload);
			} else {
				// var_dump($status);die;
				if ($status) {
					// Uploaded file data
					$fileName = $this->upload->data()["file_name"];
					$foto = array(
						'foto' => $fileName,
					);
					$in = array(
						'foto' => $fileName,
						'nama_menu' => $nama,
						'id_cabang' => $id_cabang,
						'id_kategori' => $id_kategori,
						'harga' => $harga,
						'promo' => $promo,
						'pajak' => $pajak,
						'stok' => $stok,
						'created_at' => date("Y-m-d H:i:s"),
					);
					$this->SemuaModel->Tambah('menu', $in);
					$message = "Berhasil Menambah  #1";
					$id_admin = $this->session->userdata('id_admin');
					$aksi = 'Tambah menu ' . $nama;
					$id_kategori = 7;
					$this->AdminModel->log($id_admin, $id_kategori, $aksi);
				} else {
					$message = "Gagal!  #1";
				}
			}
		} else {
			$message = "Harap Upload Foto!";
			$status = false;
		}

		echo json_encode(array(
			'status' => $status,
			'message' => $message,
		));
	}
	public function hapusMenu()
	{
		$id_menu = $this->input->post('id_menu', TRUE);
		$data = $this->SemuaModel->getDataById('menu', 'id_menu', $id_menu);
		$status = false;

		$message = 'Gagal menghapus Data!';
		if (count($data) == 0) {
			$message .= '<br>Tidak terdapat Data yang dimaksud.';
		} else {
			$this->SemuaModel->HapusData('menu', 'id_menu', $id_menu);

			$id_admin = $this->session->userdata('id_admin');
			$aksi = 'Hapus menu ' . $data[0]->nama_menu;
			$id_kategori = 9;
			$this->AdminModel->log($id_admin, $id_kategori, $aksi);
			$status = true;
			$message = 'Berhasil menghapus Data: <b>' . $data[0]->nama_menu . '</b>';
		}
		echo json_encode(array(
			'status' => $status,
			'message' => $message,
		));
	}
	public function ubah_menu_proses()
	{
		$id_menu = $this->input->post('id_menu', TRUE);
		$stok = $this->input->post('stok', TRUE);
		$nama = $this->input->post('nama', TRUE);
		$id_kategori = $this->input->post('id_kategori', TRUE);
		$harga = $this->input->post('harga', TRUE);
		$promo = $this->input->post('promo', TRUE);
		$message = 'Gagal mengedit data !<br>Silahkan lengkapi data yang diperlukan.';
		$errorInputs = array();
		$status = true;

		$in = array(
			'nama_menu' => $nama,
			'id_kategori' => $id_kategori,
			'harga' => $harga,
			'stok' => $stok,
			'promo' => $promo,
		);
		$maxFoto = 5;
		$getNamaFotoOld = $this->SemuaModel->getDataById('menu', 'id_menu', $id_menu)[0]->foto;
		if (empty($nama)) {
			$status = false;
			$errorInputs[] = array('#nama', 'Silahkan Isi Nama');
		}
		if (empty($harga)) {
			$status = false;
			$errorInputs[] = array('#harga', 'Silahkan pilih Kelas');
		}
		// var_dump($_FILES['foto']['name']);die();tunggu

		if ($status) {

			if ($this->SemuaModel->EditData('menu', 'id_menu', $in, $id_menu)) {

				$message = "Berhasil Mengubah Data #1";
				$cekFoto = empty($_FILES['foto']['name'][0]) || $_FILES['foto']['name'][0] == '';
				// var_dump(!$cekFoto);die;
				if (!$cekFoto) {

					$_FILES['f']['name']     = $_FILES['foto']['name'];
					$_FILES['f']['type']     = $_FILES['foto']['type'];
					$_FILES['f']['tmp_name'] = $_FILES['foto']['tmp_name'];
					$_FILES['f']['error']     = $_FILES['foto']['error'];
					$_FILES['f']['size']     = $_FILES['foto']['size'];

					$config['upload_path']          = './assets/images/foods/';
					$config['allowed_types']        = 'jpg|jpeg|png|gif';
					$config['max_size']             = 3 * 1024; // kByte
					$config['max_width']            = 10 * 1024;
					$config['max_height']           = 10 * 1024;
					$config['file_name'] = $id_menu . "-" . date("Y-m-d-H-i-s") . ".jpg";
					$this->load->library('image_lib');
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					$this->image_lib->resize();
					// var_dump(!$this->upload->do_upload('f'));die;
					// Upload file to server

					if (!$this->upload->do_upload('f')) {
						$errorUpload = $this->upload->display_errors() . '<br>';
					} else {
						// Uploaded file data
						$fileName = $this->upload->data()["file_name"];
						$foto = array(
							'foto' => $fileName,
						);
						$this->SemuaModel->EditData('menu', 'id_menu', $foto, $id_menu);

						$id_admin = $this->session->userdata('id_admin');
						$aksi = 'Edit menu ' . $nama;
						$id_kategori = 8;
						$this->AdminModel->log($id_admin, $id_kategori, $aksi);
						$message = "Berhasil Mengubah Data #1";
					}
				}
			}
		} else {
			$message = "Gagal Mengubah  #1";
		}



		echo json_encode(array(
			'status' => $status,
			'message' => $message,
			'errorInputs' => $errorInputs
		));
	}
	public function master_kasir()
	{
		// var_dump($_SESSION);die;
		$obj['header'] = $this->SemuaModel->getSeting();
		$obj['cabang'] = $this->SemuaModel->getCabang();
		$this->cekLogin();
		$obj['owner'] = false;
		$obj['id_cabang'] = $_SESSION['id_cabang'];

		$id_admin = $this->session->userdata('id_admin');
		$role = $this->AdminModel->getRole($id_admin, 'data_kasir')->r;
		if ($role == 1) {
			$obj['ci'] = $this;
			$obj['content'] = 'cabang/master_kasir';
			$this->load->view('admin/templates/index', $obj);
		} elseif ($role == 0) {
			$obj['ci'] = $this;
			$obj['content'] =  "admin/blank";
			$this->load->view('admin/templates/index', $obj);
		}
	}
	public function ubah_kasir_proses()
	{
		$id_kasir = $this->input->post('id_kasir', TRUE);
		$nama = $this->input->post('nama', TRUE);
		$username = $this->input->post('username', TRUE);
		$password = $this->input->post('password', TRUE);

		$message = 'Gagal mengedit data !<br>Silahkan lengkapi data yang diperlukan.';
		$errorInputs = array();
		$status = true;

		$in = array(
			'nama_kasir' => $nama,
			'username' => $username,
			'password' => $password,
		);
		if (empty($nama)) {
			$status = false;
			$errorInputs[] = array('#nama', 'Silahkan Isi Nama');
		}
		if (empty($username)) {
			$status = false;
			$errorInputs[] = array('#username', 'Silahkan pilih Kelas');
		}
		// var_dump($status);die;

		if ($status) {

			if ($this->SemuaModel->EditData('kasir', 'id_kasir', $in, $id_kasir)) {
				$status = true;

				$message = "Berhasil Mengubah Data #1";

				$id_admin = $this->session->userdata('id_admin');
				$aksi = 'Edit Kasir ' . $nama;
				$id_kategori = 5;
				$this->AdminModel->log($id_admin, $id_kategori, $aksi);
			}
		} else {
			$status = false;
			$message = "Gagal Mengubah Data #1";
		}



		echo json_encode(array(
			'status' => $status,
			'message' => $message,
			'errorInputs' => $errorInputs
		));
	}
	public function tambah_kasir_proses()
	{
		$nama = $this->input->post('nama', TRUE);
		$det_cabang = $this->input->post('det_cabang', TRUE);
		$username = $this->input->post('username', TRUE);
		$password = $this->input->post('password', TRUE);

		$status = true;

		if ($det_cabang == 'default') {
			$status = false;
			$message = "Harap Pilih Cabang!";
			// die("sss");
		}
		if ($nama == null) {
			$status = false;
			$message = "Harap Masukan Nama!";
			// die("sss");
		}
		if ($username == null) {
			$status = false;
			$message = "Harap Isi username!";
			// die("sss");
		}
		if ($password == null) {
			$status = false;
			$message = "Harap Masukan Password!";
			// die("sss");
		}
		if ($status) {
			$status = true;

			$in = array(
				'id_cabang' => $det_cabang,
				'nama_kasir' => $nama,
				'username' => $username,
				'password' => $password,
				'created_at' => date("Y-m-d H:i:s"),
			);
			$this->SemuaModel->Tambah('kasir', $in);
			$id_admin = $this->session->userdata('id_admin');
			$aksi = 'Tambah Kasir ' . $nama;
			$id_kategori = 4;
			$this->AdminModel->log($id_admin, $id_kategori, $aksi);

			$message = "Berhasil Menambah  #1";
		} else {
			$status = false;
			$message = "Gagal Menambah Data";
		}



		echo json_encode(array(
			'status' => $status,
			'message' => $message,
		));



		# code...
	}
	public function hapusKasir()
	{
		$id_kasir = $this->input->post('id_kasir', TRUE);
		$data = $this->SemuaModel->getDataById('kasir', 'id_kasir', $id_kasir);

		$status = false;

		$message = 'Gagal menghapus Data!';
		if (count($data) == 0) {
			$message .= '<br>Tidak terdapat Data yang dimaksud.';
		} else {
			$this->SemuaModel->HapusData('kasir', 'id_kasir', $id_kasir);

			$id_admin = $this->session->userdata('id_admin');
			$aksi = 'Hapus Kasir ' . $data[0]->nama_kasir;
			$id_kategori = 6;
			$this->AdminModel->log($id_admin, $id_kategori, $aksi);
			$status = true;
			$message = 'Berhasil menghapus Data: <b>' . $data[0]->nama_kasir . '</b>';
		}
		echo json_encode(array(
			'status' => $status,
			'message' => $message,
		));
	}
	public function tambah_admin_proses()
	{
		$det_cabang = $this->input->post('det_cabang', true);
		$nama = $this->input->post('nama', TRUE);
		$username = $this->input->post('username', TRUE);
		$password = $this->input->post('password', TRUE);
		$role = $this->input->post('role', TRUE);
		$status = true;

		if ($nama == null) {
			$status = false;
			$message = "Harap Masukan Nama!";
			// die("sss");
		}
		if ($det_cabang == 'default') {
			$status = false;
			$message = "Harap Pilih Cabang!";
		}
		if ($username == null) {
			$status = false;
			$message = "Harap Isi username!";
			// die("sss");
		}
		if ($password == null) {
			$status = false;
			$message = "Harap Masukan Password!";
			// die("sss");
		}
		if ($role == null) {
			$status = false;
			$message = "Harap Masukan Role!";
			// die("sss");
		}
		if ($status) {
			$status = true;

			$in = array(
				'id_cabang' => $det_cabang,
				'nama_admin' => $nama,
				'username' => $username,
				'password' => $password,
				'id_role' => $role,
				'created_at' => date("Y-m-d H:i:s"),
			);
			$this->SemuaModel->Tambah('admin', $in);
			$message = "Berhasil Menambah  #1";
			$id_admin = $this->session->userdata('id_admin');
			$aksi = 'Tambah Admin ' . $nama;
			$id_kategori = 1;
			$this->AdminModel->log($id_admin, $id_kategori, $aksi);
		} else {
			$status = false;
			$message = "Gagal Menambah Data";
		}



		echo json_encode(array(
			'status' => $status,
			'message' => $message,
		));



		# code...
	}
	public function ubah_admin_proses()
	{
		// var_dump($_POST);die;
		$det_cabang = $this->input->post('det_cabang', true);
		$id_admin = $this->input->post('id_admin', TRUE);
		$nama = $this->input->post('nama', TRUE);
		$username = $this->input->post('username', TRUE);
		$password = $this->input->post('password', TRUE);
		$role = $this->input->post('role', TRUE);

		$message = 'Gagal mengedit data !<br>Silahkan lengkapi data yang diperlukan.';
		$errorInputs = array();
		$status = true;

		$in = array(
			'id_cabang' => $det_cabang,
			'nama_admin' => $nama,
			'username' => $username,
			'password' => $password,
			'id_role' => $role,
		);
		if (empty($nama)) {
			$status = false;
			$errorInputs[] = array('#nama', 'Silahkan Isi Nama');
		}
		if ($det_cabang == 'default') {
			$status = false;
			$message = "Harap Pilih Cabang!";
		}
		if (empty($username)) {
			$status = false;
			$errorInputs[] = array('#username', 'Silahkan pilih Kelas');
		}
		// var_dump($status);die;

		if ($status) {

			if ($this->SemuaModel->EditData('admin', 'id_admin', $in, $id_admin)) {
				$status = true;

				$message = "Berhasil Mengubah Data #1";

				$id_admin = $this->session->userdata('id_admin');
				$aksi = 'Edit Admin ' . $nama;
				$id_kategori = 2;
				$this->AdminModel->log($id_admin, $id_kategori, $aksi);
			}
		} else {
			$status = false;
			$message = "Gagal Mengubah Data #1";
		}



		echo json_encode(array(
			'status' => $status,
			'message' => $message,
			'errorInputs' => $errorInputs
		));
	}
	public function hapusAdmin()
	{
		$id_admin = $this->input->post('id_admin', TRUE);
		$data = $this->SemuaModel->getDataById('admin', 'id_admin', $id_admin);

		$status = false;

		$message = 'Gagal menghapus Data!';
		if (count($data) == 0) {
			$message .= '<br>Tidak terdapat Data yang dimaksud.';
		} else {
			$this->SemuaModel->HapusData('admin', 'id_admin', $id_admin);

			$id_admin = $this->session->userdata('id_admin');
			$aksi = 'Hapus Admin ' .  $data[0]->nama_admin;
			$id_kategori = 3;
			$this->AdminModel->log($id_admin, $id_kategori, $aksi);

			$status = true;
			$message = 'Berhasil menghapus Data: <b>' . $data[0]->nama_admin . '</b>';
		}
		echo json_encode(array(
			'status' => $status,
			'message' => $message,
		));
	}
	public function setting()
	{
		$obj['header'] = $this->SemuaModel->getSeting();

		$this->cekLogin();
		$id_admin = $this->session->userdata('id_admin');
		$role = $this->AdminModel->getRole($id_admin, 'seeting')->r;
		if ($role == 1) {
			$obj['ci'] = $this;
			$obj['content'] = 'cabang/setting';
			$this->load->view('admin/templates/index', $obj);
		} elseif ($role == 0) {
			$obj['ci'] = $this;
			$obj['content'] =  "admin/blank";
			$this->load->view('admin/templates/index', $obj);
		}
	}
	public function ubah_setting_proses()
	{
		$id_setting = $this->input->post('id_setting', TRUE);
		$isi = $this->input->post('isi', TRUE);
		$konten = $this->input->post('konten', TRUE);

		$message = 'Gagal mengedit data !<br>Silahkan lengkapi data yang diperlukan.';
		$errorInputs = array();
		$status = true;

		$in = array(
			'isi' => $isi,
		);
		if (empty($isi)) {
			$status = false;
			$errorInputs[] = array('#isi', 'Silahkan Isi ');
		}

		if ($status) {

			if ($this->SemuaModel->EditData('setting', 'id_setting', $in, $id_setting)) {
				$status = true;

				$message = "Berhasil Mengubah Data #1";
			}
		} else {
			$status = false;
			$message = "Gagal Mengubah Data #1";
		}



		echo json_encode(array(
			'status' => $status,
			'message' => $message,
			'errorInputs' => $errorInputs
		));
	}
	public function isLoggedInAdmin()
	{
		// var_dump($_SESSION);die;
		// Cek apakah terdapat session "admin_session"
		if ($this->session->userdata('admin_session'))
			return true; // sudah login
		else
			return false; // belum login
	}
	public function logout()
	{
		$id_admin = $_SESSION['id_admin'];
		$CI = &get_instance();
		$CI->load->library('session');
		$CI->session->sess_destroy();
		$pesan = "Berhasil Keluar";
		$eror = false;
		// $aksi = 'Log Out';
		// $id_kategori = 82;
		// $this->AdminModel->log($id_admin, $id_kategori, $aksi);
		redirect('LoginAdmin');
		// return $this->login;
		echo json_encode(array(
			'pesan' => $pesan,
			'error' => $eror,
		));
	}
	public function master_transaksi()
	{
		$obj['header'] = $this->SemuaModel->getSeting();

		$this->cekLogin();

		$id_admin = $this->session->userdata('id_admin');
		$role = $this->AdminModel->getRole($id_admin, 'master_transaksi')->r;
		// var_dump($role);die;
		if ($role == 1) {
			$obj['ci'] = $this;
			$obj['listKategori'] = $this->SemuaModel->getAllRole();
			$obj['content'] = 'cabang/master_transaksi';
			$this->load->view('admin/templates/index', $obj);
		} elseif ($role == 0) {
			$obj['ci'] = $this;
			$obj['content'] =  "admin/blank";
			$this->load->view('admin/templates/index', $obj);
		}
	}
	public function master_role()
	{
		$obj['header'] = $this->SemuaModel->getSeting();


		$this->cekLogin();
		$id_admin = $this->session->userdata('id_admin');
		$role = $this->AdminModel->getRole($id_admin, 'data_admin')->r;
		if ($role == 1) {
			$obj['ci'] = $this;
			$obj['content'] = 'admin/master_role';
			$this->load->view('admin/templates/index', $obj);
		} elseif ($role == 0) {
			$obj['ci'] = $this;
			$obj['content'] =  "admin/blank";
			$this->load->view('admin/templates/index', $obj);
		}


		# code...
	}
	public function tambah_role()
	{
		$nama = $this->input->post('nama', TRUE);
		$master_admin = ($this->input->post('master_admin', TRUE) == "true") ? 1 : 0;
		$master_kasir = ($this->input->post('master_kasir', TRUE) == "true") ? 1 : 0;
		$master_menu = ($this->input->post('master_menu', TRUE) == "true") ? 1 : 0;
		$master_transaksi = ($this->input->post('master_transaksi', TRUE) == "true") ? 1 : 0;
		$histori = ($this->input->post('histori', TRUE) == "true") ? 1 : 0;
		$setting = ($this->input->post('setting', TRUE) == "true") ? 1 : 0;

		$message = 'Gagal menambahkan Role Baru!<br>Silahkan lengkapi data yang diperlukan.';
		$errorInputs = array();
		$status = true;
		$in = array(
			'role_name' => $nama,
		);
		// var_dump($in);die();
		$noRoleSelected = true;
		if (
			$master_admin == 1 ||  $master_kasir == 1 || $master_menu == 1 || $master_transaksi == 1 || $histori == 1 || $setting
		) $noRoleSelected = false;
		else if ($noRoleSelected) {
			$status = false;
			$errorInputs[] = ('Silahkan pilih Role minimal 1 Role!');
		}
		// var_dump($noRoleSelected);die;
		if ($status) {
			// $id_admin = $this->AdminModel->get_last_id()->last_id;
			$admin_role = array(
				'nama_role' => $nama,
				'data_admin' => $master_admin,
				'data_kasir' => $master_kasir,
				'master_menu' => $master_menu,
				'master_transaksi' => $master_transaksi,
				'histori' => $histori,
				'seeting' => $setting,

			);
			if ($this->AdminModel->tambah_new_admin_role($admin_role)) {
				$status = true;
				$message = 'Berhasil Menambah Role ';
				$id_admin = $this->session->userdata('id_admin');
				$aksi = 'Tambah Role ' . $nama;
				$id_kategori = 10;
				$this->AdminModel->log($id_admin, $id_kategori, $aksi);
			} else {
				$message = 'Gagal ';
			}
		} else {
			$message = 'Username Sudah Ada! ';
		}
		echo json_encode(array(
			'status' => $status,
			'message' => $message,
			'errorInputs' => $errorInputs
		));
	}
	public function edit_role()
	{
		$id_role = $this->input->post('id_role', TRUE);
		$nama = $this->input->post('nama', TRUE);

		$data_admin = ($this->input->post('data_admin', TRUE) == "true") ? 1 : 0;
		$data_kasir = ($this->input->post('data_kasir', TRUE) == "true") ? 1 : 0;
		$master_menu = ($this->input->post('master_menu', TRUE) == "true") ? 1 : 0;
		$master_transaksi = ($this->input->post('master_transaksi', TRUE) == "true") ? 1 : 0;

		$Histori = ($this->input->post('Histori', TRUE) == "true") ? 1 : 0;
		$Setting = ($this->input->post('Setting', TRUE) == "true") ? 1 : 0;

		$message = 'Gagal menambahkan Produk Baru!<br>Silahkan lengkapi data yang diperlukan.';
		$errorInputs = array();
		$status = true;
		$noRoleSelected = true;
		if (
			$data_admin == 1 || $data_kasir == 1 || $master_menu == 1
			|| $master_transaksi  == 1 || $Setting == 1 || $Histori == 1
		) $noRoleSelected = false;
		else if ($noRoleSelected) {
			$status = false;
			$errorInputs[] = ('Silahkan pilih id_role');
		}

		if (empty($nama)) {
			$status = false;
			$errorInputs[] = array('#nama', 'Silahkan Masukan Nama');
		}

		if ($status) {
			$admin_role = array(
				'nama_role' => $nama,
				'data_admin' => $data_admin,
				'data_kasir' => $data_kasir,
				'master_menu' => $master_menu,
				'master_transaksi' => $master_transaksi,
				'histori' => $Histori,
				'seeting' => $Setting,
			);

			if ($this->AdminModel->edit_new_admin_role($admin_role, $id_role)) {
				$message = 'Berhasil Mengubah Data Role ';
				$status = true;

				$id_admin = $this->session->userdata('id_admin');
				$aksi = 'Edit Role ' . $nama;
				$id_kategori = 11;
				$this->AdminModel->log($id_admin, $id_kategori, $aksi);
			} else {
				$message = 'Gagal ';
				$status = false;
			}
		} else {
			$message = 'Username Sudah Ada! ';
		}
		echo json_encode(array(
			'status' => $status,
			'message' => $message,
			'errorInputs' => $errorInputs
		));
	}
	public function hapusRole()
	{
		if (!$this->isLoggedInAdmin()) {
			echo '403 Forbidden!';
			exit();
		}
		$id_admin = $this->input->post('id_role', true);
		$data = $this->AdminModel->getRoleById($id_admin);
		$status = false;
		$message = 'Gagal menghapus Role!';
		if (count($data) == 0) {
			$message .= '<br>Tidak terdapat Role yang dimaksud.';
		} else {
			$hasil = $this->AdminModel->hapusRole($id_admin);
			if ($hasil) {
				$status = true;
				$message = 'Berhasil menghapus Role: <b>' . $data[0]->nama_role . '</b>';
				$id_admin = $this->session->userdata('id_admin');
				$aksi = 'hapus Role ' . $data[0]->nama_role;
				$id_kategori = 12;
				$this->AdminModel->log($id_admin, $id_kategori, $aksi);
			} else {
				$message .= 'Terjadi kesalahan. #ADM0028';
			}
		}

		echo json_encode(array(
			'status' => $status,
			'message' => $message,
		));
	}
	public function master_histori()
	{
		$obj['header'] = $this->SemuaModel->getSeting();

		$this->cekLogin();
		$id_admin = $this->session->userdata('id_admin');
		$role = $this->AdminModel->getRole($id_admin, 'histori')->r;
		if ($role == 1) {
			$obj['ci'] = $this;

			$obj['listkategorilog'] = $this->AdminModel->getAllKategorilogadmin();
			$obj['content'] = 'cabang/histori_admin';
			$this->load->view('admin/templates/index', $obj);
		} elseif ($role == 0) {
			$obj['ci'] = $this;
			$obj['content'] =  "admin/blank";
			$this->load->view('admin/templates/index', $obj);
		}


		# code...
	}
	public function master_slider()
	{
		$id_admin = $this->session->userdata('id_admin');
		// $role = $this->AdminModel->getRole($id_admin, 'data_admin')->r;
		$role = 1;
		if ($role == 1) {
			$obj['listKategori'] = $this->SemuaModel->getAllRole();
			$obj['content'] = 'cabang/master_slider';
			$this->load->view('admin/templates/index', $obj);
		} elseif ($role == 0) {
			$obj['ci'] = $this;
			$obj['content'] =  "admin/blank";

			$this->load->view('admin/templates/index', $obj);
		}
	}
	public function getFotoSlider()
	{
		$id = $this->input->post('id');
		$data = $this->SemuaModel->getFotoSliderById($id);

		echo json_encode(array(
			'data' => $data,
		));
	}
	public function ubah_slider_proses()
	{
		// var_dump($_POST,$_FILES);die;
		$id_setting = $this->input->post('id_setting', TRUE);
		$konten = $this->input->post('konten', TRUE);
		$message = 'Gagal mengedit data !<br>Silahkan lengkapi data yang diperlukan.';
		$errorInputs = array();
		$status = true;
		// var_dump($_FILES['isi']['name']==null);die;
		if ($_FILES['isi']['name'] != null) {

			$_FILES['f']['name']     = $_FILES['isi']['name'];
			$_FILES['f']['type']     = $_FILES['isi']['type'];
			$_FILES['f']['tmp_name'] = $_FILES['isi']['tmp_name'];
			$_FILES['f']['error']     = $_FILES['isi']['error'];
			$_FILES['f']['size']     = $_FILES['isi']['size'];
			$config['upload_path']          = './assets/images/slider';
			$config['allowed_types']        = 'jpg|jpeg|png|gif';
			$config['max_size']             = 3 * 1024;
			$config['max_width']            = 10 * 1024;
			$config['max_height']           = 10 * 1024;
			$config['file_name'] = $id_setting . "-" . date("Y-m-d-H-i-s") . ".jpg";
			$this->load->library('image_lib');
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$this->image_lib->resize();

			if (!$this->upload->do_upload('f')) {
				$errorUpload = $this->upload->display_errors() . '<br>';
				$status = false;
				$errorInputs[] = array('#foto', $errorUpload);
			} else {
				$fileName = $this->upload->data()["file_name"];
				$foto = array(
					'foto' => $fileName,
					'nama_foto' => $konten,
				);
				if ($this->SemuaModel->EditData('slider', 'id_slider', $foto, $id_setting)) {
					$status = true;
					$message = "Berhasil Mengubah Data #1";
				} else {
					$status = false;
					$message = "Gagal Mengubah Data #1";
				}
			}
		} else {
			$foto = array(
				'nama_foto' => $konten,
			);
			if ($this->SemuaModel->EditData('slider', 'id_slider', $foto, $id_setting)) {
				$status = true;
				$message = "Berhasil Mengubah Data #1";
			} else {
				$status = false;
				$message = "Gagal Mengubah Data #1";
			}
		}




		echo json_encode(array(
			'status' => $status,
			'message' => $message,
			'errorInputs' => $errorInputs
		));
	}
	public function master_kategori()
	{
		$obj['header'] = $this->SemuaModel->getSeting();

		$this->cekLogin();
		$id_admin = $this->session->userdata('id_admin');
		$role = $this->AdminModel->getRole($id_admin, 'master_kategori')->r;
		if ($role == 1) {

			$obj['listKategori'] = $this->MenuModel->getKategori();
			$obj['ci'] = $this;
			$obj['content'] = 'cabang/master_kategori';
			$this->load->view('admin/templates/index', $obj);
		} elseif ($role == 0) {
			$obj['ci'] = $this;
			$obj['content'] =  "admin/blank";
			$this->load->view('admin/templates/index', $obj);
		}
	}
	public function master_laporan()
	{
		$obj['header'] = $this->SemuaModel->getSeting();
		$obj['id_cabang'] = $this->session->userdata('id_cabang');
		$this->load->helper('form');
		$id_admin = $this->session->userdata('id_admin');
		$role = 1;
		if ($role == 1) {
			$obj['listKategori'] = $this->SemuaModel->getAllRole();
			$obj['content'] = 'cabang/master_laporan';
			$this->load->view('admin/templates/index', $obj);
		} elseif ($role == 0) {
			$obj['ci'] = $this;
			$obj['content'] =  "admin/blank";
			$this->load->view('admin/templates/index', $obj);
		}
	}
	public function lihat_laporan_harian()
	{
		$tanggal = $this->input->get_post('tanggal');
		$bulan = $this->input->get_post('bulan');
		$obj['header'] = $this->SemuaModel->getSeting();

		$obj['id_cabang'] = $this->session->userdata('id_cabang');


		$tahun = $this->input->get_post('tahun');
		$obj['data'] = $this->TransaksiModel->getLaporanHarian($tanggal, $bulan, $tahun);
		$obj['tanggal'] = $tanggal;
		$obj['bulan'] = $bulan;
		$obj['tahun'] = $tahun;
		$id_admin = $this->session->userdata('id_admin');
		$role = $this->AdminModel->getRole($id_admin, 'data_admin')->r;
		if ($role == 1) {
			$obj['listKategori'] = $this->SemuaModel->getAllRole();
			$obj['content'] = 'admin/master_laporan_harian';
			$this->load->view('admin/templates/index', $obj);
		} elseif ($role == 0) {
			$obj['ci'] = $this;
			$obj['content'] =  "admin/blank";
			$this->load->view('admin/templates/index', $obj);
		}
	}
	public function lihat_laporan_bulanan()
	{
		$obj['header'] = $this->SemuaModel->getSeting();
		$bulan = $this->input->get_post('bulan');
		$tahun = $this->input->get_post('tahun');
		$obj['data'] = $this->TransaksiModel->getLaporanBulanan($bulan, $tahun);
		$obj['bulan'] = $bulan;
		$obj['tahun'] = $tahun;
		$id_admin = $this->session->userdata('id_admin');
		$role = $this->AdminModel->getRole($id_admin, 'data_admin')->r;
		if ($role == 1) {
			$obj['listKategori'] = $this->SemuaModel->getAllRole();
			$obj['content'] = 'admin/master_laporan_bulanan';
			$this->load->view('admin/templates/index', $obj);
		} elseif ($role == 0) {
			$obj['ci'] = $this;
			$obj['content'] =  "admin/blank";
			$this->load->view('admin/templates/index', $obj);
		}
	}
	public function lihat_laporan_tahunan()
	{
		$tahun = $this->input->get_post('tahun');
		$obj['data'] = $this->TransaksiModel->getLaporanTahunan($tahun);
		$obj['tahun'] = $tahun;
		$id_admin = $this->session->userdata('id_admin');
		$role = $this->AdminModel->getRole($id_admin, 'data_admin')->r;
		if ($role == 1) {
			$obj['listKategori'] = $this->SemuaModel->getAllRole();
			$obj['content'] = 'admin/master_laporan_tahunan';
			$this->load->view('admin/templates/index', $obj);
		} elseif ($role == 0) {
			$obj['ci'] = $this;
			$obj['content'] =  "admin/blank";
			$this->load->view('admin/templates/index', $obj);
		}
	}
	public function laporan_harian_pdf()
	{

		$this->load->library('pdf');
		$tanggal = $this->input->get_post('tanggal');
		$bulan = $this->input->get_post('bulan');
		$tahun = $this->input->get_post('tahun');
		$obj['data'] = $this->TransaksiModel->getLaporanHarian($tanggal, $bulan, $tahun);
		$obj['tanggal'] = $tanggal;
		$obj['bulan'] = $bulan;
		$obj['tahun'] = $tahun;
		$obj['ci'] = $this;
		$obj['bu'] = base_url();

		$this->pdf->setPaper('A4', 'potrait');
		$this->pdf->set_base_path(base_url() . "assets/admin/vendors/bootstrap/dist/css/bootstrap.min.css");

		$obj['title'] = "laporan_harian_$tanggal-$bulan-$tahun.pdf";
		$this->pdf->load_view('admin/laporan_harian', $obj);
		// $this->load->view('admin/laporan_pdf', $obj);

	}
	public function laporan_bulanan_pdf()
	{

		$this->load->library('pdf');
		$bulan = $this->input->get_post('bulan');
		$tahun = $this->input->get_post('tahun');
		$obj['data'] = $this->TransaksiModel->getLaporanBulanan($bulan, $tahun);
		$obj['bulan'] = $bulan;
		$obj['tahun'] = $tahun;
		$obj['ci'] = $this;
		$obj['bu'] = base_url();

		$this->pdf->setPaper('A4', 'potrait');
		$this->pdf->set_base_path(base_url() . "assets/admin/vendors/bootstrap/dist/css/bootstrap.min.css");

		$obj['title'] = "laporan_bulanan_$bulan-$tahun.pdf";
		$this->pdf->load_view('admin/laporan_bulanan', $obj);
		// $this->load->view('admin/laporan_pdf', $obj);

	}
	public function laporan_tahunan_pdf()
	{

		$this->load->library('pdf');
		$tahun = $this->input->get_post('tahun');
		$obj['data'] = $this->TransaksiModel->getLaporanTahunan($tahun);
		$obj['tahun'] = $tahun;
		$obj['ci'] = $this;
		$obj['bu'] = base_url();

		$this->pdf->setPaper('A4', 'potrait');
		$this->pdf->set_base_path(base_url() . "assets/admin/vendors/bootstrap/dist/css/bootstrap.min.css");

		$obj['title'] = "laporan_tahunan_-$tahun.pdf";
		$this->pdf->load_view('admin/laporan_tahunan', $obj);
		// $this->load->view('admin/laporan_pdf', $obj);

	}
	public function tambah_meja()
	{
		$no = $this->input->post('no_meja');
		$id_cabang = $_SESSION['id_cabang'];
		$rand  = $this->generateRandomMeja(3);
		$link = base_url() . "User/Meja/$rand";
		$qrcode_name = $no . ".png";
		$this->qrcode($no, $link);
		$cari = array(
			'nomor_meja' => $no,
			'id_cabang' => $id_cabang,
		);
		$cekMeja = $this->SemuaModel->getDataById('meja', $cari, '*');
		$cekRandMeja = $this->SemuaModel->getDataById('meja', 'random', $rand);
		if ($cekRandMeja != null) {
			$rand = $this->generateRandomMeja(3);
		}

		if ($cekMeja != null) {
			echo json_encode(array('status' => false, 'msg' => 'Maaf, Nomor Meja Sudah Ada!'));
			die;
		}
		$data = array(
			'nomor_meja' => $no,
			'link' => $link,
			'id_cabang' => $id_cabang,
			'qrcode' => $qrcode_name,
			'random' => $rand,
		);
		if ($this->SemuaModel->tambah('meja', $data)) {
			echo json_encode(array('status' => true, 'msg' => 'Berhasil Menambah Meja'));
		} else {
			echo json_encode(array('status' => false, 'msg' => 'Gagal Menambah!'));
		}
	}
	public function tambah_kategori_proses()
	{
		$nama_kategori = $this->input->post('nama_kategori');
		$id_cabang = $_SESSION['id_cabang'];

		$data = array(
			'id_cabang' => $id_cabang,
			'nama_kategori' => $nama_kategori,
		);
		if ($this->SemuaModel->tambah('kategori', $data)) {
			echo json_encode(array('status' => true, 'msg' => 'Berhasil Menambah Kategori'));
		} else {
			echo json_encode(array('status' => false, 'msg' => 'Gagal Menambah!'));
		}
	}

	public function master_meja()
	{
		$this->cekLogin();
		$obj['header'] = $this->SemuaModel->getSeting();

		$id_admin = $this->session->userdata('id_admin');
		$role = $this->AdminModel->getRole($id_admin, 'master_menu')->r;
		if ($role == 1) {

			$obj['listKategori'] = $this->MenuModel->getKategori();
			$obj['ci'] = $this;
			$obj['content'] = 'cabang/master_meja';
			$this->load->view('admin/templates/index', $obj);
		} elseif ($role == 0) {
			$obj['ci'] = $this;
			$obj['content'] =  "admin/blank";
			$this->load->view('admin/templates/index', $obj);
		}
	}
	public function hapusMeja()
	{
		$id_meja = $this->input->post('id_meja', TRUE);
		$data = $this->SemuaModel->getDataById('meja', 'id_meja', $id_meja);
		$status = false;

		$message = 'Gagal menghapus Data!';
		if (count($data) == 0) {
			$message .= '<br>Tidak terdapat Data yang dimaksud.';
		} else {
			$qrcode = $data[0]->qrcode;
			unlink("uploads/images/" . $qrcode);
			$this->SemuaModel->HapusData('meja', 'id_meja', $id_meja);

			$id_admin = $this->session->userdata('id_admin');
			$aksi = 'Hapus Meja ' . $data[0]->nomor_meja;
			$id_kategori = 9;
			$this->AdminModel->log($id_admin, $id_kategori, $aksi);
			$status = true;
			$message = 'Berhasil menghapus No Meja : <b>' . $data[0]->nomor_meja . '</b>';
		}
		echo json_encode(array(
			'status' => $status,
			'message' => $message,
		));
	}
	public function master_penjualan_menu()
	{
		$obj['header'] = $this->SemuaModel->getSeting();
		$tanggal = $this->input->get_post('tanggal');
		$bulan = $this->input->get_post('bulan');
		$tahun = $this->input->get_post('tahun');
		$obj['data'] = $this->TransaksiModel->getLaporanHarian($tanggal, $bulan, $tahun);
		$obj['tanggal'] = $tanggal;
		$obj['bulan'] = $bulan;
		$obj['tahun'] = $tahun;
		$id_admin = $this->session->userdata('id_admin');
		// $role = $this->AdminModel->getRole($id_admin, 'data_admin')->r;
		$role = 1;
		if ($role == 1) {
			$obj['listKategori'] = $this->SemuaModel->getAllRole();
			$obj['content'] = 'cabang/penjualan_harian';
			$this->load->view('admin/templates/index', $obj);
		} elseif ($role == 0) {
			$obj['ci'] = $this;
			$obj['content'] =  "admin/blank";
			$this->load->view('admin/templates/index', $obj);
		}
	}
	public function laporan_menu_harian_pdf()
	{
		$obj['header'] = $this->SemuaModel->getSeting();


		$this->load->library('pdf');
		$tanggal = $this->input->get_post('tanggal');
		$obj['data'] = $this->TransaksiModel->getMenuLaporanHarian($tanggal);
		$obj['tanggal'] = $tanggal;
		$obj['ci'] = $this;
		$obj['bu'] = base_url();

		$this->pdf->setPaper('A4', 'potrait');
		$this->pdf->set_base_path(base_url() . "assets/admin/vendors/bootstrap/dist/css/bootstrap.min.css");

		$obj['title'] = "laporan_harian_$tanggal.pdf";
		$this->pdf->load_view('admin/laporan_menu_harian', $obj);
		// $this->load->view('admin/laporan_pdf', $obj);

	}
	public function laporan_menu_harian_pdf_cabang()
	{
		$obj['header'] = $this->SemuaModel->getSeting();


		$this->load->library('pdf');
		$tanggal = $this->input->get_post('tanggal');
		$obj['data'] = $this->TransaksiModel->getMenuLaporanHarianCabang($tanggal);
		$obj['tanggal'] = $tanggal;
		$obj['ci'] = $this;
		$obj['bu'] = base_url();

		$this->pdf->setPaper('A4', 'potrait');
		$this->pdf->set_base_path(base_url() . "assets/admin/vendors/bootstrap/dist/css/bootstrap.min.css");

		$obj['title'] = "laporan_harian_$tanggal.pdf";
		$this->pdf->load_view('admin/laporan_menu_harian', $obj);
		// $this->load->view('admin/laporan_pdf', $obj);

	}
	public function home()
	{
		$obj['header'] = $this->SemuaModel->getSeting();

		$date = date("Y-m-d");
		$dateYesterday = date("Y-m-d", strtotime("-1 days"));
		$obj['hariIni'] = $this->TransaksiModel->getCountTransaksiHarian($date);
		$obj['kemarin'] = $this->TransaksiModel->getCountTransaksiHarian($dateYesterday);
		$obj['menuHariIni'] = $this->TransaksiModel->getCountMenuHarian($date);
		$obj['menuKemarin'] = ($this->TransaksiModel->getCountMenuHarian($dateYesterday) == null) ? 0 : $this->TransaksiModel->getCountMenuHarian($dateYesterday);


		$id_admin = $this->session->userdata('id_admin');
		$role = $this->AdminModel->getRole($id_admin, 'data_admin')->r;
		if ($role == 1) {
			$obj['content'] = 'admin/home';
			$this->load->view('admin/templates/index', $obj);
		} elseif ($role == 0) {
			$obj['ci'] = $this;
			$obj['content'] =  "admin/blank";
			$this->load->view('admin/templates/index', $obj);
		}
	}
	public function master_transaksi_offline()
	{
		$obj['header'] = $this->SemuaModel->getSeting();

		$this->cekLogin();

		$id_admin = $this->session->userdata('id_admin');
		$role = $this->AdminModel->getRole($id_admin, 'master_transaksi')->r;
		if ($role == 1) {
			$obj['ci'] = $this;
			$obj['listKategori'] = $this->SemuaModel->getAllRole();
			$obj['content'] = 'cabang/master_transaksi_offline';
			$this->load->view('admin/templates/index', $obj);
		} elseif ($role == 0) {
			$obj['ci'] = $this;
			$obj['content'] =  "admin/blank";
			$this->load->view('admin/templates/index', $obj);
		}
	}
	public function proses_tambah_cabang()
	{
		$cabang = $this->input->post('cabang', TRUE);
		$alamat_cabang = $this->input->post('alamat', TRUE);

		$pic = $this->input->post('pic', TRUE);
		$nomor_telpon_pic = $this->input->post('nomor_telpon_pic', TRUE);
		$status = true;
		if ($cabang == null) {
			$status = false;
			$message = "Harap Masukan Nama!";
		}
		if ($alamat_cabang == null) {
			$status = false;
			$message = "Harap Isi alamat_cabang!";
		}
		if ($status) {
			$status = true;
			$in = array(
				'cabang' => $cabang,
				'pic' => $pic,
				'nomor_telpon_pic' => $nomor_telpon_pic,
				'alamat_cabang' => $alamat_cabang,
				'created_at' => date("Y-m-d H:i:s"),
			);
			$this->SemuaModel->Tambah('cabang', $in);
			$message = "Berhasil Menambah  #1";
		} else {
			$status = false;
			$message = "Gagal Menambah Data";
		}



		echo json_encode(array(
			'status' => $status,
			'message' => $message,
		));
	}
	public function proses_ubah_cabang()
	{
		$id_cabang = $this->input->post('id_cabang', TRUE);
		$cabang = $this->input->post('cabang', TRUE);
		$alamat = $this->input->post('alamat', TRUE);
		$pic = $this->input->post('pic', TRUE);
		$nomor_telpon_pic = $this->input->post('nomor_telpon_pic', TRUE);
		$message = 'Gagal mengedit data !<br>Silahkan lengkapi data yang diperlukan.';
		$errorInputs = array();
		$status = true;
		$in = array(
			'cabang' => $cabang,
			'pic' => $pic,
			'nomor_telpon_pic' => $nomor_telpon_pic,
			'alamat_cabang' => $alamat,
		);
		if (empty($cabang)) {
			$status = false;
			$errorInputs[] = array('#cabang', 'Silahkan Isi Nama');
		}
		if (empty($alamat)) {
			$status = false;
			$errorInputs[] = array('#alamat', 'Silahkan pilih Kelas');
		}
		if ($status) {
			if ($this->SemuaModel->EditData('cabang', 'id_cabang', $in, $id_cabang)) {
				$status = true;
				$message = "Berhasil Mengubah Data #1";
			}
		} else {
			$status = false;
			$message = "Gagal Mengubah Data #1";
		}
		echo json_encode(array(
			'status' => $status,
			'message' => $message,
			'errorInputs' => $errorInputs
		));
	}
	public function hapusCabang()
	{
		$id_cabang = $this->input->post('id_cabang', TRUE);
		$data = $this->SemuaModel->getDataById('cabang', 'id_cabang', $id_cabang);
		$status = false;
		$message = 'Gagal menghapus Data!';
		if (count($data) == 0) {
			$message .= '<br>Tidak terdapat Data yang dimaksud.';
		} else {
			$this->SemuaModel->HapusData('cabang', 'id_cabang', $id_cabang);
			$status = true;
			$message = 'Berhasil menghapus Data: <b>' . $data[0]->cabang . '</b>';
		}
		echo json_encode(array(
			'status' => $status,
			'message' => $message,
		));
	}
	public function tambah_slider_proses()
	{
		$id_cabang = $_SESSION['id_cabang'];
		$konten = $this->input->post('konten', TRUE);


		$message = 'Gagal mengedit data !<br>Silahkan lengkapi data yang diperlukan.';
		$errorInputs = array();
		$status = true;

		$_FILES['f']['name']     = $_FILES['isi']['name'];
		$_FILES['f']['type']     = $_FILES['isi']['type'];
		$_FILES['f']['tmp_name'] = $_FILES['isi']['tmp_name'];
		$_FILES['f']['error']     = $_FILES['isi']['error'];
		$_FILES['f']['size']     = $_FILES['isi']['size'];
		$config['upload_path']          = './assets/images/slider';
		$config['allowed_types']        = 'jpg|jpeg|png|gif';
		$config['max_size']             = 3 * 1024;
		$config['max_width']            = 10 * 1024;
		$config['max_height']           = 10 * 1024;
		$config['file_name'] = $id_cabang . "-" . date("Y-m-d-H-i-s") . ".jpg";
		$this->load->library('image_lib');
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		$this->image_lib->resize();

		if (!$this->upload->do_upload('f')) {
			$errorUpload = $this->upload->display_errors() . '<br>';
			$status = false;
			$errorInputs[] = array('#foto', $errorUpload);
		} else {
			$fileName = $this->upload->data()["file_name"];
			$foto = array(
				'foto' => $fileName,
				'id_cabang' => $id_cabang,
				'nama_foto' => $konten,
			);
			if ($this->SemuaModel->Tambah('slider', $foto)) {
				$status = true;
				$message = "Berhasil Menambah Data #1";
			} else {
				$status = false;
				$message = "Gagal Menambah Data #1";
			}
		}
		echo json_encode(array(
			'status' => $status,
			'message' => $message,
			'errorInputs' => $errorInputs
		));
	}
	public function laporan_bulanan_pdfCabang()
	{

		$this->load->library('pdf');
		$bulan = $this->input->get_post('bulan');
		$tahun = $this->input->get_post('tahun');
		$obj['data'] = $this->TransaksiModel->getLaporanBulananCabang($bulan, $tahun);
		$obj['bulan'] = $bulan;
		$obj['tahun'] = $tahun;
		$obj['ci'] = $this;
		$obj['bu'] = base_url();

		$this->pdf->setPaper('A4', 'potrait');
		$this->pdf->set_base_path(base_url() . "assets/admin/vendors/bootstrap/dist/css/bootstrap.min.css");

		$obj['title'] = "laporan_bulanan_$bulan-$tahun.pdf";
		$this->pdf->load_view('admin/laporan_bulanan', $obj);
		// $this->load->view('admin/laporan_pdf', $obj);

	}
	public function laporan_tahunan_pdfcabang()
	{

		$this->load->library('pdf');
		$tahun = $this->input->get_post('tahun');
		$obj['data'] = $this->TransaksiModel->getLaporanTahunanCabang($tahun);
		$obj['tahun'] = $tahun;
		$obj['ci'] = $this;
		$obj['bu'] = base_url();

		$this->pdf->setPaper('A4', 'potrait');
		$this->pdf->set_base_path(base_url() . "assets/admin/vendors/bootstrap/dist/css/bootstrap.min.css");

		$obj['title'] = "laporan_tahunan_-$tahun.pdf";
		$this->pdf->load_view('admin/laporan_tahunan', $obj);
		// $this->load->view('admin/laporan_pdf', $obj);

	}
	public function ubah_kategori_proses()
	{
		$id_kategori = $this->input->post('id_kategori', TRUE);
		$nama_kategori = $this->input->post('nama_kategori', TRUE);
		$message = 'Gagal mengedit data !<br>Silahkan lengkapi data yang diperlukan.';
		$errorInputs = array();
		$status = true;
		$in = array(
			'nama_kategori' => $nama_kategori,
		);
		if (empty($nama_kategori)) {
			$status = false;
			$errorInputs[] = array('#nama_kategori', 'Silahkan Isi Nama');
		}
		if ($status) {
			if ($this->SemuaModel->EditData('kategori', 'id_kategori', $in, $id_kategori)) {
				$status = true;
				$message = "Berhasil Mengubah Data #1";
				$id_admin = $this->session->userdata('id_admin');
				$aksi = 'Edit kategori ' . $nama_kategori;
				$id_kategori = 5;
				$this->AdminModel->log($id_admin, $id_kategori, $aksi);
			}
		} else {
			$status = false;
			$message = "Gagal Mengubah Data #1";
		}
		echo json_encode(array(
			'status' => $status,
			'message' => $message,
			'errorInputs' => $errorInputs
		));
	}
	public function hapusKategori()
	{
		$id_kategori = $this->input->post('id_kategori', TRUE);
		$data = $this->SemuaModel->getDataById('kategori', 'id_kategori', $id_kategori);
		$status = false;

		$message = 'Gagal menghapus Data!';
		if (count($data) == 0) {
			$message .= '<br>Tidak terdapat Data yang dimaksud.';
		} else {
			$this->SemuaModel->HapusData('kategori', 'id_kategori', $id_kategori);

			$id_admin = $this->session->userdata('id_admin');
			$aksi = 'Hapus kategori ' . $data[0]->nama_kategori;
			$id_kategori = 9;
			$this->AdminModel->log($id_admin, $id_kategori, $aksi);
			$status = true;
			$message = 'Berhasil menghapus Data: <b>' . $data[0]->nama_kategori . '</b>';
		}
		echo json_encode(array(
			'status' => $status,
			'message' => $message,
		));
	}
	public function ubah_Logo()
	{
		$id_logo = $this->input->post('id_logo', TRUE);
		$konten = $this->input->post('konten', TRUE);
		$message = 'Gagal mengedit data !<br>Silahkan lengkapi data yang diperlukan.';
		$errorInputs = array();
		$status = true;
		if ($_FILES['logo']['name'] != null) {

			$_FILES['f']['name']     = $_FILES['logo']['name'];
			$_FILES['f']['type']     = $_FILES['logo']['type'];
			$_FILES['f']['tmp_name'] = $_FILES['logo']['tmp_name'];
			$_FILES['f']['error']     = $_FILES['logo']['error'];
			$_FILES['f']['size']     = $_FILES['logo']['size'];
			$config['upload_path']          = './assets/images/admin';
			$config['allowed_types']        = '*';
			$config['max_size']             = 3 * 1024;
			$config['max_width']            = 10 * 1024;
			$config['max_height']           = 10 * 1024;
			$config['file_name'] = $id_logo . "-" . date("Y-m-d-H-i-s") . ".jpg";
			$this->load->library('image_lib');
			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			if (!$this->upload->do_upload('f')) {
				$errorUpload = $this->upload->display_errors() . '<br>';
				$status = false;
				$errorInputs[] = array('#foto', $errorUpload);
			} else {
				$fileName = $this->upload->data()["file_name"];
				$foto = array(
					'logo' => $fileName,
				);
				if ($this->SemuaModel->EditData('logo', 'id_logo', $foto, $id_logo)) {
					$status = true;
					$message = "Berhasil Mengubah Data #1";
				} else {
					$status = false;
					$message = "Gagal Mengubah Data #1";
				}
			}
		} else {
			$foto = array(
				'nama_foto' => $konten,
			);
			if ($this->SemuaModel->EditData('slider', 'id_slider', $foto, $id_logo)) {
				$status = true;
				$message = "Berhasil Mengubah Data #1";
			} else {
				$status = false;
				$message = "Gagal Mengubah Data #1";
			}
		}




		echo json_encode(array(
			'status' => $status,
			'message' => $message,
			'errorInputs' => $errorInputs
		));
	}
	public function test()
	{
		$data = $this->MenuModel->getData();
		foreach ($data as $key => $v) {

			$random = array('A', 'B', 'C');
			$r = $random[array_rand($random)];
			$a = 0;
			$b = 0;
			$c = 0;
			$d = 0;
			$e = 0;

			switch ($r) {
				case 'A':
					$a = 1;

					break;
				case 'B':
					$b = 1;
					# code...
					break;
				case 'C':
					$c = 1;
					# code...
					break;
				default:
					$e = 1;
					# code...
					break;
			}
			$comp = $v->companyId;

			$input = array(
				'descriptionId' =>39 , 
				'groupId' =>4 , 
				'companyId' =>$comp , 
				'jawaban' =>$r , 
				'jawabanA' =>$a , 
				'jawabanB' =>$b , 
				'jawabanC' =>$c , 
				'jawabanD' =>$d , 
				'jawabanE' =>$e , 
			
			);
		$this->MenuModel->Add($input);
			

		}
	}
}
