-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 02, 2022 at 10:40 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hasanposresto`
--
CREATE DATABASE IF NOT EXISTS `hasanposresto` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `hasanposresto`;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `id_admin` int(15) NOT NULL,
  `nama_admin` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `id_role` int(5) NOT NULL,
  `created_at` datetime NOT NULL,
  `id_cabang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `nama_admin`, `username`, `password`, `id_role`, `created_at`, `id_cabang`) VALUES
(1, 'admin utama', 'hasanadmin', 'hasanadmin', 5, '0000-00-00 00:00:00', 1),
(9, 'demo', 'demo', 'demo', 6, '2021-05-09 09:12:58', 0),
(10, 'edit', 'edi', 'edi', 6, '2022-07-12 10:31:13', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cabang`
--

DROP TABLE IF EXISTS `cabang`;
CREATE TABLE `cabang` (
  `id_cabang` int(11) NOT NULL,
  `cabang` varchar(11) NOT NULL,
  `alamat_cabang` varchar(255) NOT NULL,
  `pic` varchar(255) NOT NULL,
  `nomor_telpon_pic` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `nomor_telp` varchar(255) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `instagram` varchar(255) DEFAULT NULL,
  `twitter` varchar(255) DEFAULT NULL,
  `youtube` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cabang`
--

INSERT INTO `cabang` (`id_cabang`, `cabang`, `alamat_cabang`, `pic`, `nomor_telpon_pic`, `created_at`, `nomor_telp`, `facebook`, `instagram`, `twitter`, `youtube`) VALUES
(1, 'cabang itil', 'alamaaaaaqssss', '', '', '2022-07-12 06:06:44', '021999999333333333333', 'face.com', 'ig.com', 'twrtete.com', 'ytb.com'),
(2, 'cabang uhuy', 'alamaaaa', 'hasan', '89778787', '2022-07-12 06:06:44', NULL, NULL, NULL, NULL, NULL),
(3, 'cabang tiga', 'alamaaaa', '', '', '2022-07-25 17:29:33', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `histori_admin`
--

DROP TABLE IF EXISTS `histori_admin`;
CREATE TABLE `histori_admin` (
  `id_histori` int(15) NOT NULL,
  `id_admin` int(15) NOT NULL,
  `id_kategori` int(15) NOT NULL,
  `aksi` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `histori_admin`
--

INSERT INTO `histori_admin` (`id_histori`, `id_admin`, `id_kategori`, `aksi`, `created_at`) VALUES
(1, 1, 3, 'Hapus Admin kosong', '2021-05-09 14:11:20'),
(2, 1, 3, 'Hapus Admin coba baru edittttt', '2021-05-09 14:11:22'),
(3, 1, 3, 'Hapus Admin coba baru', '2021-05-09 14:11:23'),
(4, 1, 3, 'Hapus Admin coba baru', '2021-05-09 14:11:24'),
(5, 1, 3, 'Hapus Admin coba baru', '2021-05-09 14:11:26'),
(6, 1, 3, 'Hapus Admin second', '2021-05-09 14:11:28'),
(7, 1, 2, 'Edit Admin admin utama', '2021-05-09 14:11:40'),
(8, 1, 12, 'hapus Role All', '2021-05-09 14:11:48'),
(9, 1, 12, 'hapus Role Biasa', '2021-05-09 14:11:51'),
(10, 1, 12, 'hapus Role hasan', '2021-05-09 14:11:53'),
(11, 1, 11, 'Edit Role demo', '2021-05-09 14:12:14'),
(12, 1, 1, 'Tambah Admin demo', '2021-05-09 14:12:58'),
(13, 1, 7, 'Tambah menu Martabak Manis', '2021-05-09 14:24:10'),
(14, 1, 7, 'Tambah menu Gado Gado', '2021-05-09 14:25:04'),
(15, 1, 7, 'Tambah menu Es Campur', '2021-05-09 14:25:25'),
(16, 1, 7, 'Tambah menu Martabak Manis', '2021-05-09 14:25:38'),
(17, 1, 7, 'Tambah menu Lontong Isi', '2021-05-09 14:25:53'),
(18, 1, 7, 'Tambah menu Singkong Keju', '2021-05-09 14:26:08'),
(19, 1, 7, 'Tambah menu Gurame Goreng', '2021-05-09 14:26:23'),
(20, 1, 7, 'Tambah menu Es Blewah', '2021-05-09 14:26:41'),
(21, 1, 7, 'Tambah menu Es Campur 2', '2021-05-09 14:26:57'),
(22, 1, 7, 'Tambah menu Es Susu', '2021-05-09 14:27:23'),
(23, 1, 7, 'Tambah menu Es Pisang Ijo', '2021-05-09 14:27:40'),
(24, 1, 7, 'Tambah menu Es Teh', '2021-05-09 14:27:49'),
(25, 1, 7, 'Tambah menu Ketoprak', '2021-05-09 14:28:02'),
(26, 1, 7, 'Tambah menu Martabak Manis', '2021-05-09 14:28:18'),
(27, 1, 7, 'Tambah menu Nasi Goreng', '2021-05-09 14:28:29'),
(28, 1, 7, 'Tambah menu Es Lemon', '2021-05-09 14:28:41'),
(29, 1, 7, 'Tambah menu Wedang jahe', '2021-05-09 14:28:56'),
(30, 1, 4, 'Tambah Kasir wwwwwww', '2022-07-12 15:07:36'),
(31, 1, 5, 'Edit Kasir kasir gauledits', '2022-07-12 15:09:32'),
(32, 1, 1, 'Tambah Admin demowdwd', '2022-07-12 15:31:13'),
(33, 1, 2, 'Edit Admin edit', '2022-07-12 15:31:41'),
(34, 10, 9, 'Hapus Meja 1', '2022-07-24 01:00:50'),
(35, 10, 9, 'Hapus Meja 2', '2022-07-24 01:00:51'),
(36, 10, 9, 'Hapus Meja 1', '2022-07-24 02:11:18'),
(37, 10, 7, 'Tambah menu ijkkljdkj', '2022-07-30 22:11:30'),
(38, 10, 5, 'Edit kategori ssssssssssssssssssss', '2022-07-31 00:41:28'),
(39, 10, 5, 'Edit kategori q', '2022-07-31 00:41:33'),
(40, 10, 9, 'Hapus kategori q', '2022-07-31 00:43:53'),
(41, 10, 5, 'Edit Kasir wwwwwww', '2022-08-03 21:54:31'),
(42, 10, 5, 'Edit Kasir wwwwwww', '2022-08-04 20:49:14'),
(43, 10, 5, 'Edit Kasir wwwwwww', '2022-08-08 20:47:05'),
(44, 10, 7, 'Tambah menu okoko', '2022-08-30 20:52:44'),
(45, 10, 7, 'Tambah menu okoks', '2022-08-30 20:53:01'),
(46, 10, 7, 'Tambah menu icjskjwksdjwkd', '2022-08-30 20:53:22');

-- --------------------------------------------------------

--
-- Table structure for table `kasir`
--

DROP TABLE IF EXISTS `kasir`;
CREATE TABLE `kasir` (
  `id_kasir` int(11) NOT NULL,
  `id_cabang` int(11) NOT NULL,
  `nama_kasir` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `last_login` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kasir`
--

INSERT INTO `kasir` (`id_kasir`, `id_cabang`, `nama_kasir`, `created_at`, `username`, `password`, `last_login`) VALUES
(1, 1, 'wwwwwww', '2021-04-24 00:00:00', 'ww', 'ww', '2022-09-01 03:09:07'),
(2, 1, 'hasan', '2021-05-02 12:46:16', 'hasan', 'hasan', '2022-07-30 07:07:26'),
(3, 1, 'wwwwwww', '2022-07-12 10:07:36', 'w', 'w', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

DROP TABLE IF EXISTS `kategori`;
CREATE TABLE `kategori` (
  `id_kategori` int(15) NOT NULL,
  `id_cabang` int(11) NOT NULL,
  `nama_kategori` varchar(155) NOT NULL,
  `foto` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `id_cabang`, `nama_kategori`, `foto`) VALUES
(1, 1, 'Makanan', ''),
(2, 1, 'Minuman', ''),
(3, 2, 'All', ''),
(4, 1, 'Desert', ''),
(5, 1, 'Snack', '');

-- --------------------------------------------------------

--
-- Table structure for table `kategori_histori`
--

DROP TABLE IF EXISTS `kategori_histori`;
CREATE TABLE `kategori_histori` (
  `id_kategori` int(11) NOT NULL,
  `nama_kategori` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori_histori`
--

INSERT INTO `kategori_histori` (`id_kategori`, `nama_kategori`) VALUES
(1, 'Tambah Admin'),
(2, 'Edit Admin'),
(3, 'Hapus Admin'),
(4, 'Tambah Kasir'),
(5, 'Edit Kasir'),
(6, 'Hapus Kasir'),
(7, 'Tambah Menu'),
(8, 'Edit Menu'),
(9, 'Hapus Menu'),
(10, 'Tambah Role'),
(11, 'Edit Role'),
(12, 'Hapus ROle');

-- --------------------------------------------------------

--
-- Table structure for table `keranjang`
--

DROP TABLE IF EXISTS `keranjang`;
CREATE TABLE `keranjang` (
  `id_keranjang` int(15) NOT NULL,
  `id_cabang` int(11) NOT NULL,
  `id_user` varchar(15) NOT NULL,
  `id_produk` varchar(15) NOT NULL,
  `qty` int(15) NOT NULL,
  `catatan` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `keranjang`
--

INSERT INTO `keranjang` (`id_keranjang`, `id_cabang`, `id_user`, `id_produk`, `qty`, `catatan`, `created_at`, `updated_at`) VALUES
(38, 1, '1', '19', 24, 'hasan ', '2022-09-01', NULL),
(39, 1, '1', '20', 1, '  ssq', '2022-09-01', NULL),
(40, 1, '1', '2', 2, '', '2022-09-01', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `keranjang_meja_user`
--

DROP TABLE IF EXISTS `keranjang_meja_user`;
CREATE TABLE `keranjang_meja_user` (
  `id_keranjang` int(15) NOT NULL,
  `id_cabang` int(1) NOT NULL,
  `id_meja` varchar(15) NOT NULL,
  `id_produk` varchar(15) NOT NULL,
  `qty` int(15) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date DEFAULT NULL,
  `catatan` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `keranjang_meja_user`
--

INSERT INTO `keranjang_meja_user` (`id_keranjang`, `id_cabang`, `id_meja`, `id_produk`, `qty`, `created_at`, `updated_at`, `catatan`) VALUES
(77, 1, '12', '2', 1, '2022-08-28', NULL, '  test');

-- --------------------------------------------------------

--
-- Table structure for table `log_menu`
--

DROP TABLE IF EXISTS `log_menu`;
CREATE TABLE `log_menu` (
  `id_log_menu` int(11) NOT NULL,
  `id_kategori` int(15) NOT NULL,
  `id_menu` int(15) NOT NULL,
  `id_cabang` int(11) NOT NULL,
  `qty` int(15) NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `log_menu`
--

INSERT INTO `log_menu` (`id_log_menu`, `id_kategori`, `id_menu`, `id_cabang`, `qty`, `created_by`, `nama`, `created_at`) VALUES
(8, 1, 17, 0, 1000, 'kasir', 'hasan', '2022-07-10 19:55:24'),
(9, 1, 2, 0, 100, 'kasir', 'hasan', '2022-07-10 19:55:24'),
(10, 1, 1, 0, 2, 'kasir', 'hasan', '2022-07-10 19:55:24'),
(11, 1, 8, 0, 1, 'kasir', 'hasan', '2022-07-10 19:55:24'),
(12, 1, 7, 0, 1, 'kasir', 'hasan', '2022-07-10 19:55:24'),
(13, 1, 3, 0, 1, 'kasir', 'hasan', '2022-07-10 19:55:24'),
(14, 1, 17, 0, 1000, 'kasir', 'hasan', '2022-07-10 19:56:25'),
(15, 1, 2, 0, 100, 'kasir', 'hasan', '2022-07-10 19:56:25'),
(16, 1, 1, 0, 2, 'kasir', 'hasan', '2022-07-10 19:56:25'),
(17, 1, 8, 0, 1, 'kasir', 'hasan', '2022-07-10 19:56:25'),
(18, 1, 7, 0, 1, 'kasir', 'hasan', '2022-07-10 19:56:25'),
(19, 1, 3, 0, 1, 'kasir', 'hasan', '2022-07-10 19:56:25'),
(20, 1, 17, 0, 1000, 'kasir', 'hasan', '2022-07-10 19:56:27'),
(21, 1, 2, 0, 100, 'kasir', 'hasan', '2022-07-10 19:56:27'),
(22, 1, 1, 0, 2, 'kasir', 'hasan', '2022-07-10 19:56:27'),
(23, 1, 8, 0, 1, 'kasir', 'hasan', '2022-07-10 19:56:27'),
(24, 1, 7, 0, 1, 'kasir', 'hasan', '2022-07-10 19:56:27'),
(25, 1, 3, 0, 1, 'kasir', 'hasan', '2022-07-10 19:56:27'),
(26, 1, 17, 0, 1000, 'kasir', 'hasan', '2022-07-10 19:57:23'),
(27, 1, 2, 0, 100, 'kasir', 'hasan', '2022-07-10 19:57:23'),
(28, 1, 1, 0, 2, 'kasir', 'hasan', '2022-07-10 19:57:23'),
(29, 1, 8, 0, 1, 'kasir', 'hasan', '2022-07-10 19:57:23'),
(30, 1, 7, 0, 1, 'kasir', 'hasan', '2022-07-10 19:57:23'),
(31, 1, 3, 0, 1, 'kasir', 'hasan', '2022-07-10 19:57:23'),
(32, 1, 17, 0, 1000, 'kasir', 'hasan', '2022-07-10 20:01:28'),
(33, 1, 2, 0, 100, 'kasir', 'hasan', '2022-07-10 20:01:28'),
(34, 1, 1, 0, 2, 'kasir', 'hasan', '2022-07-10 20:01:29'),
(35, 1, 8, 0, 1, 'kasir', 'hasan', '2022-07-10 20:01:29'),
(36, 1, 7, 0, 1, 'kasir', 'hasan', '2022-07-10 20:01:29'),
(37, 1, 3, 0, 1, 'kasir', 'hasan', '2022-07-10 20:01:29'),
(38, 1, 17, 0, 1000, 'kasir', 'hasan', '2022-07-10 20:02:11'),
(39, 1, 2, 0, 100, 'kasir', 'hasan', '2022-07-10 20:02:11'),
(40, 1, 1, 0, 2, 'kasir', 'hasan', '2022-07-10 20:02:11'),
(41, 1, 8, 0, 1, 'kasir', 'hasan', '2022-07-10 20:02:11'),
(42, 1, 7, 0, 1, 'kasir', 'hasan', '2022-07-10 20:02:11'),
(43, 1, 3, 0, 1, 'kasir', 'hasan', '2022-07-10 20:02:11'),
(44, 1, 17, 0, 1000, 'kasir', 'hasan', '2022-07-10 20:02:35'),
(45, 1, 2, 0, 100, 'kasir', 'hasan', '2022-07-10 20:02:35'),
(46, 1, 1, 0, 2, 'kasir', 'hasan', '2022-07-10 20:02:35'),
(47, 1, 8, 0, 1, 'kasir', 'hasan', '2022-07-10 20:02:35'),
(48, 1, 7, 0, 1, 'kasir', 'hasan', '2022-07-10 20:02:35'),
(49, 1, 3, 0, 1, 'kasir', 'hasan', '2022-07-10 20:02:35'),
(50, 1, 3, 0, 1, 'kasir', 'hasan', '2022-07-10 22:06:16'),
(51, 1, 2, 0, 4, 'kasir', 'cabang1', '2022-07-12 21:55:10'),
(52, 1, 1, 0, 10, 'kasir', 'cabang1', '2022-07-12 21:55:10'),
(53, 1, 2, 0, 4, 'kasir', 'cabang1', '2022-07-12 21:57:25'),
(54, 1, 1, 0, 10, 'kasir', 'cabang1', '2022-07-12 21:57:25'),
(55, 1, 2, 0, 4, 'kasir', 'cabang1', '2022-07-12 21:58:31'),
(56, 1, 1, 0, 10, 'kasir', 'cabang1', '2022-07-12 21:58:31'),
(57, 1, 2, 0, 4, 'kasir', 'cabang1', '2022-07-12 22:00:41'),
(58, 1, 1, 0, 10, 'kasir', 'cabang1', '2022-07-12 22:00:41'),
(59, 1, 2, 0, 4, 'kasir', 'cabang1', '2022-07-12 22:01:27'),
(60, 1, 1, 0, 10, 'kasir', 'cabang1', '2022-07-12 22:01:27'),
(61, 1, 1, 0, 10, 'kasir', 'wwwwwww', '2022-08-04 22:11:02'),
(62, 1, 2, 0, 1, 'kasir', 'wwwwwww', '2022-08-04 22:11:02'),
(63, 1, 1, 0, 2, 'kasir', 'wwwwwww', '2022-08-04 22:16:40'),
(64, 1, 2, 0, 1, 'kasir', 'wwwwwww', '2022-08-04 22:16:40'),
(65, 1, 1, 0, 2, 'kasir', 'wwwwwww', '2022-08-04 22:17:01'),
(66, 1, 2, 0, 1, 'kasir', 'wwwwwww', '2022-08-04 22:17:01'),
(67, 1, 2, 0, 1, 'kasir', 'wwwwwww', '2022-08-08 21:09:19'),
(68, 1, 2, 0, 5, 'kasir', 'wwwwwww', '2022-08-16 21:34:34'),
(69, 1, 18, 0, 2, 'kasir', 'wwwwwww', '2022-08-16 21:34:34'),
(70, 1, 2, 0, 3, 'kasir', 'wwwwwww', '2022-08-16 21:41:14'),
(71, 1, 1, 0, 2, 'kasir', 'wwwwwww', '2022-08-16 21:46:34'),
(72, 1, 2, 0, 1, 'kasir', 'wwwwwww', '2022-08-16 21:47:41');

-- --------------------------------------------------------

--
-- Table structure for table `meja`
--

DROP TABLE IF EXISTS `meja`;
CREATE TABLE `meja` (
  `id_meja` int(11) NOT NULL,
  `id_cabang` int(11) NOT NULL,
  `nomor_meja` int(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `random` varchar(255) NOT NULL,
  `qrcode` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `meja`
--

INSERT INTO `meja` (`id_meja`, `id_cabang`, `nomor_meja`, `link`, `random`, `qrcode`, `created_at`) VALUES
(4, 0, 3, 'http://localhost/project/orderGItlab/User/Meja/1', '', '1.png', '2022-07-05 11:50:47'),
(5, 0, 1, 'http://localhost/project/orderGItlab/User/Meja/1', '', '1.png', '2022-07-05 11:51:37'),
(6, 0, 1, 'http://localhost/project/orderGItlab/User/Meja/1', '', '1.png', '2022-07-05 11:51:45'),
(7, 0, 1, 'http://localhost/project/orderGItlab/User/Meja/1', '', '1.png', '2022-07-05 11:52:38'),
(8, 0, 1, 'http://localhost/project/orderGItlab/User/Meja/1', '', '1.png', '2022-07-05 11:53:33'),
(11, 1, 2, 'http://localhost/projects/kooy/User/Meja/MJ_224HV', 'MJ_224HV', '2.png', '2022-07-24 02:04:01'),
(12, 1, 10, 'http://localhost/projects/kooy/User/Meja/MJ_22DD4', 'MJ_22DD4', '10.png', '2022-07-24 02:11:23');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id_menu` int(255) NOT NULL,
  `id_cabang` int(11) NOT NULL,
  `nama_menu` varchar(255) NOT NULL,
  `id_kategori` varchar(255) NOT NULL,
  `harga` varchar(255) NOT NULL,
  `foto` varchar(255) NOT NULL,
  `stok` int(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `pajak` int(255) DEFAULT NULL COMMENT 'kalo null, jadi nga ada pajak',
  `promo` int(255) DEFAULT NULL COMMENT 'kalo null nga ada promo\r\nharga = harga-promo'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id_menu`, `id_cabang`, `nama_menu`, `id_kategori`, `harga`, `foto`, `stok`, `created_at`, `pajak`, `promo`) VALUES
(1, 1, 'ManisMartabak Manis', '4', '20000', 'Martabak_Manis-2021-05-09-09-24-10.jpg', 6, '2021-05-09 09:24:10', NULL, NULL),
(2, 1, 'Gado Gado', '1', '21000', 'Gado_Gado-2021-05-09-09-25-04.jpg', 7, '2021-05-09 09:25:04', NULL, NULL),
(3, 0, 'Es Campur', '2', '15000', 'Es_Campur-2021-05-09-09-25-25.jpg', 19, '2021-05-09 09:25:25', NULL, NULL),
(4, 0, 'Martabak Manis', '1', '19000', 'Martabak_Manis-2021-05-09-09-25-38.jpg', 19, '2021-05-09 09:25:38', NULL, NULL),
(5, 0, 'Lontong Isi', '1', '11000', 'Lontong_Isi-2021-05-09-09-25-53.jpg', 19, '2021-05-09 09:25:53', NULL, NULL),
(6, 0, 'Singkong Keju', '4', '13000', 'Singkong_Keju-2021-05-09-09-26-08.jpg', 19, '2021-05-09 09:26:08', NULL, NULL),
(7, 0, 'Gurame Goreng', '1', '18000', 'Gurame_Goreng-2021-05-09-09-26-23.jpg', 19, '2021-05-09 09:26:23', NULL, NULL),
(8, 0, 'Es Blewah', '2', '15000', 'Es_Blewah-2021-05-09-09-26-41.jpg', 19, '2021-05-09 09:26:41', NULL, NULL),
(9, 0, 'Es Campur 2', '2', '19000', 'Es_Campur_2-2021-05-09-09-26-57.jpg', 19, '2021-05-09 09:26:57', NULL, NULL),
(10, 0, 'Es Susu', '2', '9000', 'Es_Susu-2021-05-09-09-27-23.jpg', 19, '2021-05-09 09:27:23', NULL, NULL),
(11, 0, 'Es Pisang Ijo', '2', '10000', 'Es_Pisang_Ijo-2021-05-09-09-27-40.jpg', 19, '2021-05-09 09:27:40', NULL, NULL),
(12, 0, 'Es Teh', '2', '10000', 'Es_Teh-2021-05-09-09-27-49.jpg', 19, '2021-05-09 09:27:49', NULL, NULL),
(14, 0, 'Martabak Manis', '1', '22000', 'Martabak_Manis-2021-05-09-09-28-18.jpg', 19, '2021-05-09 09:28:18', NULL, NULL),
(15, 0, 'Nasi Goreng', '1', '22000', 'Nasi_Goreng-2021-05-09-09-28-29.jpg', 19, '2021-05-09 09:28:29', NULL, NULL),
(16, 0, 'Es Lemon', '2', '22000', 'Es_Lemon-2021-05-09-09-28-41.jpg', 19, '2021-05-09 09:28:41', NULL, NULL),
(17, 0, 'Wedang jahe', '2', '20000', 'Wedang_jahe-2021-05-09-09-28-56.jpg', 19, '2021-05-09 09:28:56', NULL, NULL),
(18, 1, 'ijkkljdkj', '2', '10000', 'ijkkljdkj-2022-07-30-17-11-30.jpg', 0, '2022-07-30 17:11:30', 11, 1000),
(19, 1, 'okoko', '1', '3000', 'okoko-2022-08-30-15-52-44.jpg', 3, '2022-08-30 15:52:44', 11, 0),
(20, 1, 'okoks', '2', '900000', 'okoks-2022-08-30-15-53-01.jpg', 3, '2022-08-30 15:53:01', 11, 0),
(21, 1, 'icjskjwksdjwkd', '2', '9000', 'icjskjwksdjwkd-2022-08-30-15-53-22.jpg', 100, '2022-08-30 15:53:22', 11, 0);

-- --------------------------------------------------------

--
-- Table structure for table `owner`
--

DROP TABLE IF EXISTS `owner`;
CREATE TABLE `owner` (
  `id_owner` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `owner`
--

INSERT INTO `owner` (`id_owner`, `nama`, `username`, `password`, `created_at`) VALUES
(1, 'hasan', 'owner', 'owner', '2022-07-14 20:52:52');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id_role` int(15) NOT NULL,
  `nama_role` varchar(255) NOT NULL,
  `data_admin` int(1) NOT NULL DEFAULT 0,
  `data_kasir` int(1) NOT NULL DEFAULT 0,
  `master_menu` int(1) NOT NULL DEFAULT 0,
  `master_transaksi` int(1) NOT NULL DEFAULT 0,
  `histori` int(1) NOT NULL DEFAULT 0,
  `seeting` int(1) NOT NULL DEFAULT 0,
  `master_kategori` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id_role`, `nama_role`, `data_admin`, `data_kasir`, `master_menu`, `master_transaksi`, `histori`, `seeting`, `master_kategori`) VALUES
(5, 'hasan', 1, 1, 1, 1, 1, 1, 1),
(6, 'demo', 0, 1, 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

DROP TABLE IF EXISTS `setting`;
CREATE TABLE `setting` (
  `id_setting` int(15) NOT NULL,
  `konten` varchar(255) NOT NULL,
  `isi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id_setting`, `konten`, `isi`) VALUES
(1, 'Nama Resto', 'Hasan Resto\'s'),
(2, 'Jalan', 'Jalan Cengkareng'),
(3, 'Nomor Telpon', '089602350857'),
(4, 'facebook', 'abdul.gostand'),
(5, 'instagram', 'heyiamhasan'),
(6, 'twitter', 'heyiamhasan'),
(7, 'youtube', 'heyiamhasan'),
(8, 'facebook', 'abdul.gostand'),
(9, 'instagram', 'heyiamhasan'),
(10, 'twitter', 'heyiamhasan'),
(11, 'youtube', 'abdul.hasan388');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

DROP TABLE IF EXISTS `slider`;
CREATE TABLE `slider` (
  `id_slider` int(15) NOT NULL,
  `nama_foto` varchar(255) NOT NULL,
  `foto` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id_slider`, `nama_foto`, `foto`) VALUES
(1, 'foto_1', 'foto1.jpg'),
(2, 'foto_2', '2-2021-05-02-17-04-02.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

DROP TABLE IF EXISTS `transaksi`;
CREATE TABLE `transaksi` (
  `id_transaksi` int(155) NOT NULL,
  `id_cabang` int(11) NOT NULL,
  `kode_transaksi` varchar(255) NOT NULL,
  `nama_user` varchar(255) DEFAULT NULL,
  `tipe_pesan` enum('dine_in','take_away','gofood','grabfood','shopee','traveloka','wa') NOT NULL,
  `nomor_meja` varchar(15) DEFAULT NULL,
  `harga_total` int(155) NOT NULL,
  `uang_custumer` int(155) DEFAULT NULL,
  `kembalian` int(155) DEFAULT NULL,
  `id_user` int(11) NOT NULL,
  `by_user` int(11) NOT NULL,
  `type_penjualan_offline` int(5) NOT NULL DEFAULT 1 COMMENT 'true = offline, false = online',
  `created_at` datetime NOT NULL,
  `catatan` varchar(255) DEFAULT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'created' COMMENT 'created, success , cancle '
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `id_cabang`, `kode_transaksi`, `nama_user`, `tipe_pesan`, `nomor_meja`, `harga_total`, `uang_custumer`, `kembalian`, `id_user`, `by_user`, `type_penjualan_offline`, `created_at`, `catatan`, `status`) VALUES
(1, 1, 'TR_2022ES0PL', 'hasan-goput', 'gofood', '', 125000, NULL, NULL, 1, 0, 0, '2022-08-16 16:34:50', 'wdwdqwd', 'created'),
(2, 1, 'TR_20220H83S', 'hasan-grabfood', 'grabfood', '', 63000, NULL, NULL, 1, 0, 0, '2022-08-16 16:44:54', 'wdwdwdwd', 'created'),
(3, 1, 'TR_2022RNJ49', 'hasan-traveloka', 'traveloka', '', 40000, NULL, NULL, 1, 0, 0, '2022-08-16 16:46:47', 'dw', 'created'),
(4, 1, 'TR_2022KZTHQ', 'hasan ofline', 'take_away', '', 21000, NULL, NULL, 1, 0, 1, '2022-08-16 16:48:00', 'wdwdwdwd', 'created');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_detail`
--

DROP TABLE IF EXISTS `transaksi_detail`;
CREATE TABLE `transaksi_detail` (
  `id_transaksi_detail` int(15) NOT NULL,
  `id_transaksi` int(15) NOT NULL,
  `id_menu` int(255) NOT NULL,
  `qty` int(255) NOT NULL,
  `total` int(255) NOT NULL,
  `catatan` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `id_cabang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi_detail`
--

INSERT INTO `transaksi_detail` (`id_transaksi_detail`, `id_transaksi`, `id_menu`, `qty`, `total`, `catatan`, `created_at`, `id_cabang`) VALUES
(1, 1, 2, 5, 105000, '', '2022-08-16 21:34:50', 1),
(2, 1, 18, 2, 20000, '  sedang', '2022-08-16 21:34:50', 1),
(3, 1, 2, 3, 63000, '', '2022-08-16 21:44:54', 1),
(4, 1, 1, 2, 40000, '  pedas sekali', '2022-08-16 21:46:47', 1),
(5, 4, 2, 1, 21000, '  seedang', '2022-08-16 21:48:00', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `cabang`
--
ALTER TABLE `cabang`
  ADD PRIMARY KEY (`id_cabang`);

--
-- Indexes for table `histori_admin`
--
ALTER TABLE `histori_admin`
  ADD PRIMARY KEY (`id_histori`);

--
-- Indexes for table `kasir`
--
ALTER TABLE `kasir`
  ADD PRIMARY KEY (`id_kasir`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `kategori_histori`
--
ALTER TABLE `kategori_histori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `keranjang`
--
ALTER TABLE `keranjang`
  ADD PRIMARY KEY (`id_keranjang`);

--
-- Indexes for table `keranjang_meja_user`
--
ALTER TABLE `keranjang_meja_user`
  ADD PRIMARY KEY (`id_keranjang`);

--
-- Indexes for table `log_menu`
--
ALTER TABLE `log_menu`
  ADD PRIMARY KEY (`id_log_menu`);

--
-- Indexes for table `meja`
--
ALTER TABLE `meja`
  ADD PRIMARY KEY (`id_meja`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indexes for table `owner`
--
ALTER TABLE `owner`
  ADD PRIMARY KEY (`id_owner`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id_role`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id_setting`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id_slider`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`);

--
-- Indexes for table `transaksi_detail`
--
ALTER TABLE `transaksi_detail`
  ADD PRIMARY KEY (`id_transaksi_detail`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `cabang`
--
ALTER TABLE `cabang`
  MODIFY `id_cabang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `histori_admin`
--
ALTER TABLE `histori_admin`
  MODIFY `id_histori` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `kasir`
--
ALTER TABLE `kasir`
  MODIFY `id_kasir` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id_kategori` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `kategori_histori`
--
ALTER TABLE `kategori_histori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `keranjang`
--
ALTER TABLE `keranjang`
  MODIFY `id_keranjang` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `keranjang_meja_user`
--
ALTER TABLE `keranjang_meja_user`
  MODIFY `id_keranjang` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT for table `log_menu`
--
ALTER TABLE `log_menu`
  MODIFY `id_log_menu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `meja`
--
ALTER TABLE `meja`
  MODIFY `id_meja` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id_menu` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `owner`
--
ALTER TABLE `owner`
  MODIFY `id_owner` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id_role` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id_setting` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id_slider` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id_transaksi` int(155) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `transaksi_detail`
--
ALTER TABLE `transaksi_detail`
  MODIFY `id_transaksi_detail` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
